import sirv from 'sirv';
import polka from 'polka';
import compression from 'compression';
import * as sapper from '@sapper/server';
import {createAuth } from './utils/auth.mjs'
const { createProxyMiddleware } = require('http-proxy-middleware');

//import {polka } from '../../../../backend/classes/jenka/modules/polka/index.mjs'
//import {polka } from '../../../../backend/classes/jenka/index.mjs'


let { PORT, ELASTIC, FILES, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

if (!PORT) PORT = 3000
if (!ELASTIC) ELASTIC = 'locahost'
if (!FILES) FILES = 'localhost'

process.env.PORT = PORT
process.env.ELASTIC = ELASTIC
process.env.FILES = FILES


export const log = () => {
	return (req, res, next) => {
    console.log(JSON.stringify({timestamp:new Date(),user:req.user.employeeNumber, method: req.method, url:req.url}))
    return next()
  }
}

polka()

	.use(createAuth())
	.use(log())
  .use('elastic',
	  createProxyMiddleware({
	    target: `http://${ELASTIC}:9200`,
			pathRewrite: {'^/elastic' : ''}
	  })
	)
  .use('upload',
    createProxyMiddleware({
      target: `http://${FILES}:7200`,
      pathRewrite: {'^/upload' : '',
			logLevel: 'debug'}
    })
  )
	.use(
		//compression({ threshold: 0 }),
		sirv('static'),
		sapper.middleware({session: (req, res) => ({
				user: req.user
			})})
	)

	.listen(PORT, err => {
		if (err) console.log('error', err);
	});
