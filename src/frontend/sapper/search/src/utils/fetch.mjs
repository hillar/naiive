import fetch from 'node-fetch'

export const SLOWTIME = 1999

// TODO movet to better place
// TODO import defaults from constants
export const ELASTICHOST = process.env.ELASTIC
export const ELASTICPORT = process.env.ELASTICPORT || 9200

export const FILESHOST = process.env.FILES
export const FILESPORT = process.env.FILESPORT || 7200

export const WATCHERHOST = process.env.WATCHER
export const WATCHERPORT = process.env.WATCHERPORT || 7700

/*
const natives = Object.keys(process.binding('natives'))
*/


export async function _fetch(url,data) {

  let result
  try {
    result = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
  } catch (error) {
    console.error(JSON.stringify({WARNING:{timestamp: new Date(), ...error}}))
    // construct a fake fetch result
    result = {}
    result.ok = false
    // 504 "code":"ECONNREFUSED" cannot get a response
    // 503 code: 'ENOTFOUND' not ready to handle the request
    error.code === "ECONNREFUSED" ? result.status = 504: result.status = 503
    result.statusText = error.code
    result.text = () => { return new Promise( resolve => { resolve(undefined) } ) }
  }
  //console.dir({_fetch:result})
  return result
}

export async function fetchElastic(url,query,orig){
  let result = {}
  if (query.size === undefined) query.size = 0
  const start = process.hrtime.bigint();
  let response = await _fetch(url,query)
  const took = Number(process.hrtime.bigint() - start) / 1e6;
  if (took > SLOWTIME) console.log(JSON.stringify({SLOWQUERY:{timestamp: new Date(), took, url, query}}))
  //if (query.size) console.log(query.size,Math.round(took/query.size),Math.round(took))
  if (!response.ok) {
      // 500 The server has encountered a situation it doesn't know how to handle.
      result.status = response.status > 499 ? response.status : 500
      result.statusText = response.statusText

      if (response.status < 500) {
        // get calling chain for log
        const originalFunc = Error.prepareStackTrace;
        const callers = []
        let caller;
        try {
            const err = new Error();
            Error.prepareStackTrace = function (err, stack) { return stack; };
            while (err.stack.length) {
                caller = err.stack.pop();
                // as it all is rollup'ed we only need func name and line number
                const tmp = {functionName:caller.getFunctionName(),lineNumber:caller.getLineNumber()}
                // filter out self and process ticks, and sapper router
                const filter = ['fetchElastic','processTicksAndRejections','handle_route']
                if (!filter.includes(tmp.functionName))
                callers.push(tmp.lineNumber+ ' : ' + tmp.functionName)
            }
        } catch (e) {
          console.error(e)
        }
        Error.prepareStackTrace = originalFunc;
        // try to parse body for possible error message
        try {
          const text = await response.text()
          if (text && text.length && text.startsWith('{') && text.endsWith('}')){
            try {
              result.error = JSON.parse(text)
              result.error.callers = callers
            } catch (error) {
              result.error = {text,callers}
            }
          }
          console.error(JSON.stringify({CRITICAL:{timestamp: new Date(), callers, url, ...result,query}}))
        } catch (error) {
            result.error = { message : error.toString()}
            console.error(error)
            console.error(JSON.stringify({EMERGENCY:{timestamp: new Date(), callers, url, ...result, query}}))
        }
      }
  } else {
      result.ok = true
      result.status = response.status
      result.answer = await response.json()
      result.answer.hits.total.size = query.size
      result.answer.hits.total.from = query.from
      result.answer.hits.total.query = orig
      if (result.answer.hits.total.value > query.size && result.answer.hits.hits.length < query.size) result.answer.hits.total.missing = query.size - result.answer.hits.hits.length
      if (result.answer?._shards?.failed ) {
          console.error(JSON.stringify({CRITICAL:{timestamp: new Date(),  url, query, failures:result.answer._shards.failures}}))
          delete result.answer._shards
          result.status = 206 // Partial Content
      }

  }
  //console.dir({_elastic:result})
  return result
}
