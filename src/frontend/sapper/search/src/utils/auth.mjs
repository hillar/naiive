import NodeCache from 'node-cache'
import { fetch_ldap } from '../../../../../backend/classes/auth/freeipa.mjs'

const uCache = new NodeCache( { stdTTL: 60 * 2 })
const aCache = new NodeCache( { stdTTL: 60 * 10 })

export const AUTH_OPTS = {
  proto:'ldaps',
  host:'ipa.demo1.freeipa.org',
  port: 636,
  base: 'dc=demo1,dc=freeipa,dc=org',
  affix: 'cn=users,cn=accounts',
  binduser: 'manager',
  bindpass: 'Secret123',
  uidfield: 'uid',
  serialfield: 'employeeNumber',
  apikeyfield: 'carLicense',
  requiredfields: ['displayName','email','mobile','ou','manager','employeeNumber'],
  passwordExpiration: 'krbPasswordExpiration',
  rejectUnauthorized: false,
  roleUser: 'naiive_user',
  roleAdmin: 'naiive_admin'
}

const memberof2groupsandroles = (ru,ldap) => {
  //copy out groups and roles
  ru.roles = []
  ru.groups = []
  for (const memberof of ru.memberOf) {
    if (memberof.endsWith('cn=accounts,'+ldap.base)) {
      const tmp = memberof.split(',')
      const what = tmp[1]
      switch (tmp[1].split('=')[1]) {
        case 'roles':
                      ru.roles.push(tmp[0].split('=')[1].toLowerCase())
                      break
        case 'groups':
                      ru.groups.push(tmp[0].split('=')[1].toLowerCase())
                      break
        default:
                console.error({ERROR:{shouldnothappen:tmp}})
      }
    }
  }
  delete ru.memberOf
  return ru
}

const checkapikey = async (key, ldap) => {
    const uu = aCache.get(key)
    if (uu) return {ok:true,user:{api:key,...uu}}
    let filter = `(&(${ldap.apikeyfield }=${key})(memberof=cn=ipausers,cn=groups,cn=accounts,${ldap.base}))`
    let attributes = [...ldap.requiredfields ,'memberOf','krbPasswordExpiration','krbLastSuccessfulAuth']
    const ub = `${ldap.uidfield}=${ldap.binduser},${ldap.affix},${ldap.base}`
    let u
    try {
      u = await fetch_ldap(`${ldap.proto}://${ldap.host}`,ub,ldap.bindpass,ldap.base,filter,attributes)

      if (u instanceof Error ) {
        return {ok:false, statusCode: 500, reason:u.toString()}
      } else {
        if (u.length !== 1) {
          return {ok:false, statusCode: 403, reason:'bad user count: '+u.length}
        }
        u = u[0]
        // see https://github.com/freeipa/freeipa/blob/master/install/ui/src/freeipa/datetime.js
        let e = u.krbPasswordExpiration
        if (!e && ldap.host === 'ipa.demo1.freeipa.org')  e='20221231011529Z'
        if (!e)  return {ok:false, statusCode: 403, reason:'missing krbPasswordExpiration'}
        const ed = e.slice(0,4)+'-'+e.slice(4,6)+'-'+e.slice(6,8)+'T'+e.slice(8,10)+':'+e.slice(10,12)+':'+e.slice(12,14)+'Z'
        const passwordExpiration = new Date(ed)
        if (!(passwordExpiration instanceof Date) || isNaN(passwordExpiration)) {
          return {ok:false, statusCode: 500, reason:'krbPasswordExpiration not date '}
        }
        if (passwordExpiration < Date.now()){
          return {ok:false, statusCode: 403, reason:'password expired'}
        }
        u = memberof2groupsandroles(u,ldap)
        if (!(u.roles.includes(ldap.roleUser.toLowerCase()) || u.roles.includes(ldap.roleAdmin.toLowerCase()))){
          return {ok:false, statusCode: 403, reason:'no role ' + ldap.roleUser.toLowerCase()}
        }
        aCache.set(key,u)
        return {ok:true,user:{api:key,...u}}
      }
    } catch (error) {
      console.error('should not...',error)
      return {ok:false, reason:error.toString()}
    }
}

const checkuser = async (username,password,ldap) => {
  const user = uCache.get(username+password)
  if (user) return {ok:true, user}
  /*
  krbLastFailedAuth
  krbLastSuccessfulAuth
  krbLastPwdChange
  */
  let filter = `(&(${ldap.uidfield}=${username})(memberof=cn=ipausers,cn=groups,cn=accounts,${ldap.base}))`
  let attributes = [...ldap.requiredfields ,'memberOf','krbPasswordExpiration','krbLastSuccessfulAuth']
  const ub = `${ldap.uidfield}=${username},${ldap.affix},${ldap.base}`
  let u
  try {
    u = await fetch_ldap(`${ldap.proto}://${ldap.host}`,ub,password,ldap.base,filter,attributes)
    if (u instanceof Error ) {
      const em = Object.getPrototypeOf(u).toString()
      if (em === 'InvalidCredentialsError: InvalidCredentialsError' || em === 'NoSuchObjectError: NoSuchObjectError') {
        // TODO count errors ... 403
        return {ok:false, statusCode: 401, reason:'InvalidCredentialsError'}
      }
      else return {ok:false, statusCode: 500, reason:u.toString()}
    } else {
      if (u.length !== 1) {
        return {ok:false, statusCode: 403, reason:'bad user count: '+u.length}
      }
      u = u[0]
      // see https://github.com/freeipa/freeipa/blob/master/install/ui/src/freeipa/datetime.js
      let e = u[ldap.passwordExpiration]
      if (!e && ldap.host === 'ipa.demo1.freeipa.org')  e='20221231011529Z'
      if (!e)  return {ok:false, statusCode: 403, reason:'missing '+ldap.passwordExpiration}
      const ed = e.slice(0,4)+'-'+e.slice(4,6)+'-'+e.slice(6,8)+'T'+e.slice(8,10)+':'+e.slice(10,12)+':'+e.slice(12,14)+'Z'
      const passwordExpiration = new Date(ed)
      if (!(passwordExpiration instanceof Date) || isNaN(passwordExpiration)) {
        return {ok:false, statusCode: 500, reason: ldap.passwordExpiration+' not date '}
      }
      if (passwordExpiration < Date.now()){
        return {ok:false, statusCode: 403, reason:'password expired'}
      }
      // OK, check role
      u = memberof2groupsandroles(u,ldap)

      if (!(u.roles.includes(ldap.roleUser.toLowerCase()) || u.roles.includes(ldap.roleAdmin.toLowerCase()))){
        return {ok:false, statusCode: 403, reason:'no role ' + ldap.roleUser.toLowerCase()}
      }
      if (u.roles.includes(ldap.roleAdmin.toLowerCase())) u.isAdmin = true
      if (u.ou) u.ou = u.ou.toLowerCase()

      uCache.set(username+password,u)
      return {ok:true, user:u}
    }
  } catch (error) {
    console.error('should not...',error)
    return {ok:false, reason:error.toString()}
  }
}

const CREDENTIALS_REGEXP = /^ *(?:[Bb][Aa][Ss][Ii][Cc]) +([A-Za-z0-9._~+/-]+=*) *$/
const USER_PASS_REGEXP = /^([^:]*):(.*)$/

function decodeBase64 (str) {
  return Buffer.from(str, 'base64').toString()
}

function getUsernameandPass (string) {
  if (typeof string !== 'string') {
    return undefined
  }
  const match = CREDENTIALS_REGEXP.exec(string)
  if (!match) {
    return undefined
  }
  const userPass = USER_PASS_REGEXP.exec(decodeBase64(match[1]))
  if (!userPass) {
    return undefined
  }
  return {name:userPass[1], pass:userPass[2]}
}

export const createAuth = (opts) => {
  const cleaned = {}
  if (opts === undefined) opts = {}
  for (const key of Object.keys(AUTH_OPTS)){

    const envname = "AUTH_"+key.toUpperCase()
    if (process.env[envname] !== undefined && process.env[envname].toString().length) cleaned[key] = process.env[envname]
    else if (opts[key] !== undefined && opts[key].toString().length) cleaned[key] = opts[key]
    else cleaned[key] = AUTH_OPTS[key]
  }
  return  async (req, res, next) => {
    // api first :: curl -H "X-API-Key: $APIKEY" ...
    const apikey = req.headers['x-api-key'];
    if (apikey) {
      const result = await checkapikey(apikey, cleaned)
      if (result.ok) {
        req.user = result.user
        return next()
      } else {
        res.statusCode = result.statusCode ?  result.statusCode : 403 //Forbidden
        return res.end(result.reason)
      }
    } else {
      let token = req.headers['authorization'];
      if (!token) {
        res.setHeader('WWW-Authenticate', 'Basic realm=naiive')
        res.statusCode = 401
        return res.end()
      } else {
        const {name,pass} = getUsernameandPass(token)
        if (name && pass) {

          const result = await checkuser(name,pass,cleaned)
          if (result.ok) {
              if (req.headers['x-ssl-client-s-dn-serial'] && req.headers['x-ssl-client-s-dn-cn']) {
                const id = req.headers['x-ssl-client-s-dn-serial'].replace('PNOEE-','')
                if (result.user.uid !== id) {
                  console.log('ident',id,result.user)
                  res.statusCode = result.statusCode ?  result.statusCode : 403 //Forbidden
                  return res.end()
                }
              }
              req.user = result.user
              return next()
          } else {
            console.error(name, result)
            // `<html><body style="background: coral"><h1>${result.reason}</h1></body></html>`
            res.statusCode = result.statusCode ?  result.statusCode : 403 //Forbidden
            return res.end(`<html><body style="background: coral"><h1>${result.reason}</h1></body></html>`)
          }
        } else {
          res.setHeader('WWW-Authenticate', 'Basic realm=naiive')
          res.statusCode = 401
          return res.end()
        }
      }
    }
    // This Should Never Have Happened
    res.statusCode = 418
    return res.end()
  }
}
