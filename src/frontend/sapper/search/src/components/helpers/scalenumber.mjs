export function scale(value, powerOf=2, maxLen=20) {

  const suffix = ['', 'K', 'M', 'G', 'T', 'P']
  let result = ''
  let index = 0
  let divisor

  if (powerOf == 10) {
    divisor = 1000
  } else if (powerOf == 2) {
    divisor = 1024
  } else {
    throw new Error('invalid powerOf argument')
  }
  while (value >= divisor) {
    value = value / divisor
    index++
  }
  maxLen -= suffix[index].length // allow for suffix character(s)
  result = Math.round(value,2).toString()
  /*
  for (let p = result.length; result.length > maxLen; p--) {
    if (p < 1) p = 1
    result = value.toPrecision(p)
  }
  */
  return result + suffix[index]
}
