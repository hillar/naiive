export function makeQuery(q) {
  return {
    bool: {
      must: {
        simple_query_string: {
          query: q,
          fields: ['__all__^3', 'content','content-*'],
          default_operator: 'and', 
        },
      },
    },
  }
}
