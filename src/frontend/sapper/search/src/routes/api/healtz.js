import fetch from 'node-fetch'

import {
  ELASTICHOST, ELASTICPORT,
  FILESHOST,FILESPORT,
  WATCHERHOST, WATCHERPORT
} from '../../utils/fetch.mjs'


const INDEXFILES = encodeURI('*￣files￣*')
const INDEXMETA = encodeURI('*￣meta￣*')
const INDEXENTITIES = encodeURI('*￣entities￣*')
const INDEXTIMELINE = encodeURI('*￣timeline￣*')
const INDEXCONTENT = encodeURI('*￣content￣*')
const ELASTICURLFILES =  'http://' + ELASTICHOST + ':' + ELASTICPORT + '/' + INDEXFILES + '/_search'

const indexes = [INDEXCONTENT, INDEXTIMELINE, INDEXENTITIES, INDEXMETA, INDEXFILES]

async function getElasticStatus () {
  let stats = []
  let counter = 0
  let good = 0
  let status = {}
  let tmp = {}
  for (const index of indexes){
    const url = 'http://' + ELASTICHOST + ':' + ELASTICPORT + '/_cluster/health/' + index + '?level=indices'
    try {
      const result = await fetch(url)
      if (result) {
        const answer = await result.json()
        for (const i of Object.keys(answer.indices)){
          counter ++
          if (!tmp[answer.indices[i].status]) tmp[answer.indices[i].status] = 0
          tmp[answer.indices[i].status] ++
        }
        //stats.push({indices:answer.indices,pattern:index})
      }
    } catch (error) {
      console.error(url,error)
      status = {error:error.code}
      break
    }
  }
  if (!status.error){
    //all green
    if (Object.keys(tmp).filter(i=> i!=='green').length === 0) {
      status = {status:'green'}
    } else if (Object.keys(tmp).filter(i=> i==='red').length > 0 ){
      status = {status:'red'}
    } else status = {status:'yellow'}
  }
  return {index:status}
}

async function getFilesStatus(){
  const url = 'http://' + FILESHOST + ':' + FILESPORT + '/'
  try {
    const result = await fetch(url)
    if (result.ok) {
      const answer = await result.json()
      return {files:{status:'green'}}
    }
  } catch (error) {
    console.error(url,error)
    return {files:{status:'red',error:error.toString()}}
  }
}

async function getWatcherStatus(){
  const url = 'http://' + WATCHERHOST + ':' + WATCHERPORT + '/'
  try {
    const result = await fetch(url)
    if (result.ok) {
      const answer = await result.json()
      return {watcher:{status:'green'}}
    }
  } catch (error) {
    console.error(url,error)
    return {watcher:{status:'red',error:error.toString()}}
  }

}

export const get = async function _api_healtz_(req, res, next) {

  //const elastic = await getElasticStatus()

  const all = await Promise.all([getElasticStatus(),getFilesStatus(),getWatcherStatus()])
  console.dir(all)

  res.end(JSON.stringify({status:all},null,4))

}
