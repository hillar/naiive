/*

"size": 0,
"aggs": {
  "numoftags": {
    "cardinality": {
      "field": "tag.keyword"
    }
  },

POST dev-c3￣files￣p5-dev-watch‾7200/_search
{
  "size": 0,
  "aggs": {
    "tags":{"terms": {
      "field": "tag.keyword",
      "min_doc_count": 2,
      "size": 2547  // <- agg.cardinality
    }}
  }
}

*/

import fetch from 'node-fetch'
import {buildQuery} from './search.js'

const ELASTICHOST = process.env.ELASTIC
const ELASTICPORT = process.env.ELASTICPORT || 9200

let ELASTICURLFILES = 'http://' + ELASTICHOST + ':' + ELASTICPORT + '/' + encodeURI('*￣files￣*') + '/_search'
const ELASTICURLCONTENT = 'http://' + ELASTICHOST + ':' + ELASTICPORT + '/' + encodeURI('*￣content￣*') + '/_search'

export async function get(req, res, next) {
  let ELASTICURL
  let { filter, q } = req.query
  // get count
  let cdata = {
    "size": 0,
    "aggs": {
      "numoftags": {
        "cardinality": {
          "field": "tag.keyword"
        }
      }
    }
  }
  if (q) {
    ELASTICURL = ELASTICURLCONTENT
    cdata.query = buildQuery(q).query
    cdata.aggs.numoftags.cardinality.field = '__fields.tag.keyword'

  } else if (filter) {
    ELASTICURL = ELASTICURLFILES
    cdata.query =  {
        "wildcard": {
          "tag": {
            "value": "*"+filter+"*"
          }
        }
      }
  }

  try {
    const cresult = await fetch(ELASTICURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(cdata),
    })
    if (!cresult.ok){
      console.error(cresult)
      res.statusCode = cresult.status
      res.end()
    } else {
      const cresponse = await cresult.json()
      if (cresponse?.aggregations?.numoftags?.value) {
        const size = cresponse.aggregations.numoftags.value
        const docs = cresponse.hits.total.value

        // get tags
        let tdata
        if (q)  {
          tdata = cdata
          tdata.aggs =  {
            "tags":{"terms": {
              "field": "__fields.tag.keyword",
              size  // <- agg.cardinality
            }}
          }

        } else if (filter) {
           tdata = {
            "size": 0,
            "aggs": {
              "tags":{"terms": {
                "field": "tag.keyword",
                "min_doc_count": 2,
                size  // <- agg.cardinality
              }}
            }
          }
          if (filter) {
            tdata.query =  {
                "wildcard": {
                  "tag": {
                    "value": "*"+filter+"*"
                  }
                }
              }
          }
        }
        const tresult = await fetch(ELASTICURL, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(tdata),
        })
        if (!tresult.ok){
          console.error(tresult)
          res.statusCode = tresult.status
          res.end()
        } else {
          const tresponse = await tresult.json()

          if (tresponse?.aggregations?.tags?.buckets){
            const tags = {}
            tags.count = tresponse.aggregations.tags.buckets.length
            tags.tags = []
            // TODO 'better' score, for now it is max tagged doc count / current tag doc count
            const max = tresponse.aggregations.tags.buckets[0].doc_count
            for (const tag of tresponse.aggregations.tags.buckets) {
              tags.tags.push({name:tag.key,score:((tag.doc_count/max)*100)})
            }
            res.end(JSON.stringify({tags}))
          } else {
            res.statusCode = 404
            res.end()
          }

        }
      } else {
        res.statusCode = 404
        res.end()
      }
    }


  } catch (error) {
    console.error(error)
    res.statusCode = 502
    res.end()
  }
}
