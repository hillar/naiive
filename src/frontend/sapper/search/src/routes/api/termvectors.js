/*

GET ￣content￣/_termvectors/1e62f6663e1c9da74ec2ffe6ccdb93ce-9-13?fields=content&payloads=false&positions=false&offsets=false&term_statistics

{
  "_index" : "￣content￣",
  "_type" : "_doc",
  "_id" : "1e62f6663e1c9da74ec2ffe6ccdb93ce-9-13",
  "_version" : 1,
  "found" : true,
  "took" : 51,
  "term_vectors" : {
    "content" : {
      "field_statistics" : {
        "sum_doc_freq" : 35911901,
        "doc_count" : 31213,
        "sum_ttf" : 109995782
      },
      "terms" : {
        "01465627" : {
          "doc_freq" : 2,
          "ttf" : 4,
          "term_freq" : 3
        },
        "05.04.2012" : {
          "doc_freq" : 123,
          "ttf" : 532,
          "term_freq" : 1
        },

// -------------

        GET ￣content￣/_termvectors/1e62f6663e1c9da74ec2ffe6ccdb93ce-9-13
        {
          "fields": ["__languages.*._index_phrase"],
          "offsets" : false,
          "payloads" : false,
          "positions" : false,
          "term_statistics" : true,
          "field_statistics" : true,
          "filter": {
            "max_num_terms": 128,
            "min_word_length": 3,
            "max_word_length": 134,
            "min_doc_freq": 1,
            "max_doc_freq": 1
          }
        }

        {
  "_index" : "￣content￣",
  "_type" : "_doc",
  "_id" : "1e62f6663e1c9da74ec2ffe6ccdb93ce-9-13",
  "_version" : 1,
  "found" : true,
  "took" : 9,
  "term_vectors" : {
    "__languages.content-ESTONIAN._index_phrase" : {
      "field_statistics" : {
        "sum_doc_freq" : 5785947,
        "doc_count" : 15387,
        "sum_ttf" : 14758565
      },
      "terms" : {
        "01465627 20.04.2012" : {
          "doc_freq" : 1,
          "ttf" : 3,
          "term_freq" : 3,
          "score" : 29.84459
        },


*/
