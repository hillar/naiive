import { createHash } from 'crypto'
import fetch from 'node-fetch'
import { default as QuickLRU } from 'quick-lru'

const cache = new QuickLRU({ maxSize: 1024 })

const ELASTICHOST = process.env.ELASTIC
const ELASTICPORT = process.env.ELASTICPORT || 9200

const ELASTICURL = 'http://' + ELASTICHOST + ':' + ELASTICPORT + '/' + encodeURI('*content*') + '/_search'

const DEFAULTALGORITHM = 'md5'

function digestString(str, algorithm = DEFAULTALGORITHM, encoding = 'hex') {
  return createHash(algorithm).update(str).digest(encoding)
}


export function buildQuery(q,from=0,size=0,fields) {
  let filters = []
  if (q.includes(':')) {
    // build filter
    const tagregex = /(?<key>\w+)\:"(?<value>[^"]*)"/g
    const tmp = decodeURIComponent(q)
    let realq = tmp.replace(tagregex, '').trim()
    if (!realq.length) realq = '*'
    q = realq
    const _filters = [...tmp.matchAll(tagregex)]
    for (const filter of _filters) {
      const tmp = {}
      if (!filter.groups.key.startsWith('_')) tmp['__fields.' + filter.groups.key + '.keyword'] = filter.groups.value
      else tmp[filter.groups.key] = filter.groups.value
      filters.push({ term: tmp })
    }
  }
  // temp workaround of ela bug
  // failed to create query
  // type": "null_pointer_exception"
  // TODO issue elasticsearch
  let langsfields
  if (q.includes('*')) langsfields = 'content-*'
  else langsfields = '__languages.content-*'

  const data = {
    query: {
      bool: {
        must: {
          simple_query_string: {
            query: q,

            fields: ['__all__^4', 'content^2', langsfields],
            default_operator: 'and',
          },
        },
      },
    },

    highlight: {
      fields:
        //{'*': { require_field_match: false }}

        [{'__all__': { require_field_match: false }},
        {'content': { require_field_match: false }},
        {'__languages.content-*': { require_field_match: false }},
        {'__meta.*': { require_field_match: false }},
        {'__fields.*': { require_field_match: false }}
        ]

      ,
    },

    sort: ['_score'],
    from: from,
    size: size,
  }
  if (filters.length > 0) {
    data.query.bool.filter = filters
  }
  data._source = {}
  if (fields && fields.trim().length){
    if (fields.trim() === '*')   data._source.excludes = ['__meta.all', 'content', '__languages.content-*']
    else {
      data._source.includes = []
      for (const name of fields.split(',')) data._source.includes.push(name.trim())
    }
  } else data._source.excludes = ['*']
  if (q === '*'){
    delete data.highlight
    delete data.query.bool.must
    if (!data._source.excludes){
      if (!data._source.includes) data._source.includes = []
      data._source.includes.push('content')
    } else data._source.excludes = data._source.excludes.filter(i => i !== 'content')
    data.sort = ['_id']
  }

  return data
}

// TODO try again
export async function get(req, res, next) {
  let { q, start=0, size=0, fields, refresh } = req.query
  // TODO fields
  if (q && q.trim().replace('*','').length) {
    const hash = digestString(JSON.stringify(req.query))
    let item
    if (refresh === undefined) item = cache.get(hash)
    if (!item) {
      const data = buildQuery(q,start,size,fields)
      //console.log(q)
      //console.log(JSON.stringify(data,null,4))
      let indexed
      let tryoncemore = false
      try {
        indexed = await fetch(ELASTICURL, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data),
        })
      } catch (e) {
        indexed = {}
        indexed.ok = false
        indexed.status = 502
        indexed.message = e.toString()
        console.error(e)
        //console.error(req)
        //console.error(res)
      }
      if (!indexed.ok) {
        // elastic error ?
        try {
          const elaerror = await indexed.json()
          res.statusCode = indexed.status > 499 ? indexed.status : 500
          res.end(JSON.stringify({error:elaerror, query:data},null,4))
        } catch (e) {
          res.statusCode = indexed.status
          res.end()
        }
      } else {
        const response = await indexed.json()
        if (response._shards.failures) console.error(JSON.stringify({failures:response._shards.failures,data,q},null,4))
        if (response.hits && response.hits.total) {
          if (response.hits.hits.length || Number(size) === 0) {
            item = response.hits
            item.total.from = Number(start)
            item.total.size = Math.min(Number(size),item.total.value)
            //item.total.hits = response.hits.hits.length
            if (response.hits.hits.length < size) item.total.missing = size - response.hits.hits.length
            item.total.query = decodeURIComponent(q)
            // if no query, only filter, then content -> highlight
            //if (item.hits[0] && !item.hits[0].highlight && item.hits[0]._source.content) {
              for (const hit of item.hits){
                if (!hit.highlight) {
                  if (hit._source.content && hit._source.content.length) hit.highlight = {content: hit._source.content.slice(0,2048).replace(new RegExp('( \n)', 'gmu'),'\n').replace(new RegExp('(\n){2,}', 'gim'),'<p>').replace(new RegExp('(\n){1,}', 'gim'),'<br>').slice(0,1024)}
                  else hit.highlight = {content : '<i> no content ;( </i>'}
                  delete hit._source.content
                }
              }
            res.setHeader('Content-Type', 'application/json')
            //console.log(JSON.stringify(item,null,4))
            res.end(JSON.stringify(item))
            item.cachetime = new Date()
            if (!response._shards.failures) cache.set(hash, item)
          } else {
            if (response._shards.failures){
              res.statusCode = 500
              res.end(JSON.stringify({error:{ failures: response._shards.failures, query:data}},null,4))
            } else {
              if (size === 0 ) {
                res.end()
              } else {
              res.statusCode = 404
                res.end()
              }
            }
          }
        } else {
          res.statusCode = 502
          console.error(req)
          console.error(response)
        }
      }
      if (tryoncemore) {

      }
    } else {
      // from cache
      const age = (new Date()) - (new Date(item.cachetime))
      item.age = age
      res.setHeader('Content-Type', 'application/json')
      res.end(JSON.stringify(item))
    }
  } else {
    // no q
    res.statusCode = 415
  }
  res.end()
}
