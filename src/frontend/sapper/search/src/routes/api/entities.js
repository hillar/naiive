/*

TODO harmonize field names case

GET *￣line￣* /_search
{
  "query": {"bool": {
    "filter": [
      {"term": {
        "iban.keyword": "EE891010220034796011"
      }}
    ]
  }
  }
}

GET *￣timeline￣* /_search
{
  "query": {"bool": {
    "filter": [
      {"term": {
        "IBAN.keyword": "EE891010220034796011"
      }}
    ]
  }
  }
}

GET *￣entities￣* /_search
{
  "query": {"bool": {
    "filter": [
      {"term": {
        "iban.keyword": "EE891010220034796011"
      }}
    ]
  }
  }
}

*/

import { ELASTICHOST, ELASTICPORT, fetchElastic } from '../../utils/fetch.mjs'
import { createHash } from 'crypto'
import { default as QuickLRU } from 'quick-lru'

// TODO get them from constants
const INDEXENTITIES = encodeURI('*￣entities￣*')
const INDEXTIMELINE = encodeURI('*￣timeline￣*')
const INDEXLINE = encodeURI('*￣line￣*')

const cache = new QuickLRU({ maxSize: 1024 * 16 })

const DEFAULTALGORITHM = 'md5'
function digestString(str, algorithm = DEFAULTALGORITHM, encoding = 'hex') {
  return createHash(algorithm).update(str).digest(encoding)
}

let ELASTICURLENTITIES = 'http://' + ELASTICHOST + ':' + ELASTICPORT + '/' + INDEXENTITIES + '/_search'

export const get = async function _api_entities_(req, res, next) {
  let { from = 0, size = 0, fields, refresh = false, pretty = false } = req.query

  pretty = pretty === false ? false : true
  refresh = refresh === false ? false : true
  const hash = digestString(JSON.stringify(req.query))
  let item
  if (!refresh) item = cache.get(hash)
  if (!item) {
    const notfield = ['start', 'from', 'size', 'fields', 'refresh', 'pretty']
    const filterfields = Object.keys(req.query).filter((i) => !notfield.includes(i))
    //console.dir(filterfields)
    if (!filterfields.length > 0) {
      res.statusCode = 415
      if (pretty) res.end('missing entitie filter')
      else res.end()
    } else {
      const filters = []
      const q = {}
      for (const key of filterfields) {
        if (key.length) {
          const tmp = {}
          q[key] = req.query[key]
          if (key === 'ipv4' || key === 'ipv6') {
            tmp[key] = req.query[key]
            filters.push({ match_phrase: tmp })
          } else {
            tmp[key + '.keyword'] = req.query[key]
            filters.push({ term: tmp })
          }
        }
      }
      const query = {
        from: from,
        size: size,
        //sort: ['_id'],
        query: {
          bool: {
            filter: filters,
          },
        },
        _source: {
          excludes: ['*'],
        },
      }
      let result = await fetchElastic(ELASTICURLENTITIES, query, q)
      res.statusCode = result.status
      if (!result.answer) {
        if (pretty) res.end(result.status + ' ' + result.statusText + '\n\n' + result.error.callers.join('\n') + '\n\n' + JSON.stringify({ q, from, size, fields, refresh, pretty, query, result }, null, 4))
        else res.end()
      } else {
        if (result?.answer?.hits?.hits) {
          result.answer.hits.cachetime = new Date()
          cache.set(hash, result.answer.hits)
          if (pretty) res.end(JSON.stringify(result.answer.hits, null, 4))
          else res.end(JSON.stringify(result.answer.hits))
        } else {
          res.statusCode = 404
          res.end()
        }
      }
    }
  } else {
    // from cache
    const age = new Date() - new Date(item.cachetime)
    item.age = age
    res.setHeader('Content-Type', 'application/json')
    if (pretty) res.end(JSON.stringify(item, null, 4))
    else res.end(JSON.stringify(item))
  }
}
