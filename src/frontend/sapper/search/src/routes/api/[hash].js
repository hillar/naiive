import fetch from 'node-fetch'
import { default as QuickLRU } from 'quick-lru'

const cache = new QuickLRU({ maxSize: 10000 })

const ELASTICHOST = process.env.ELASTIC
const ELASTICPORT = process.env.ELASTICPORT || 9200
const FILESHOST = process.env.FILES
const FILESPORT = process.env.FILESPORT || 7200

const ELASTICURL = 'http://'+ELASTICHOST+':'+ELASTICPORT+'/' + encodeURI('*files*') + '/_search'
const FILESURL = 'http://'+FILESHOST+':'+FILESPORT+'/'

export async function get(req, res, next) {

  const { hash } = req.params
  let item = cache.get(hash)
  if (!item) {
    const data = {
      query: {
        bool: {
          must: {
            term: { __contenthash__: hash },
          },
        },
      },
    }

    const indexed = await fetch(ELASTICURL,{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
    if (!indexed.ok) {
      error = { status: indexed.status, statusText: indexed.statusText }
      res.statusCode = 500
      res.end()
    } else {
      const response = await indexed.json()
      if (response.hits.total.value) {
        item = {}
        item.files = response.hits.hits
        cache.set(hash, item)
        res.setHeader('Content-Type', 'application/json')
        res.end(JSON.stringify(item))
      } else {
        const exists = await fetch( FILESURL+ hash, { method: 'HEAD'})
        // https://tools.ietf.org/html/rfc7231#section-7.1.3
        // indicate how long the user agent ought to wait
        // before making a follow-up request
        if (exists.status === 200) {
          res.statusCode = 201
          res.setHeader('Retry-After', 3)
        } else {
          res.statusCode = 404
        }
        res.end()
      }
    }
  } else {
    res.setHeader('Content-Type', 'application/json')
    res.end(JSON.stringify(item))
  }
}
