import { ELASTICHOST, ELASTICPORT, fetchElastic } from '../../utils/fetch.mjs'


const INDEXFILES = encodeURI('*￣files￣*')
const ELASTICURLFILES =  'http://' + ELASTICHOST + ':' + ELASTICPORT + '/' + INDEXFILES + '/_search'

export const get = async function _api_files_(req, res, next) {
  let { id, size=0, pretty=false } = req.query
  pretty = pretty === false ? false : true
  const query = {
    "size": 0,
    //"_source": "__namehash__",
    //"sort": ["__namehash__.keyword",{"__timestamp__":{"order" : "desc", "mode":"max"}}],
    "query": {
      "bool": {
        "must": [
          {"term": {
            "__contenthash__": id
          }}
        ]
      }
    },
    "aggs": {
      "names": {
        "terms": {
          "field": "__namehash__.keyword",
          "size": 1000
        }
      }
    },
  }
  let result = await fetchElastic(ELASTICURLFILES, query)
  console.dir(result)
  res.statusCode = result.status
  if (!result.answer) {
    if (pretty) res.end(result.status + ' ' + result.statusText + '\n\n' + result.error.callers.join('\n') + '\n\n' + JSON.stringify({ q, start, size, fields, refresh, pretty, query, result }, null, 4))
    else res.end()
  } else {
    if (result.answer?.hits?.total?.value && result.answer?.aggregations?.names?.buckets?.length){
      const names = result.answer.aggregations.names.buckets.map(i=>i.key)
      if (pretty) res.end(JSON.stringify({names}, null, 4))
      else res.end(JSON.stringify({names}))
    } else {
      res.statusCode = 404
      res.end()
    }
  }
}
