import { ELASTICHOST, ELASTICPORT, fetchElastic } from '../../utils/fetch.mjs'


const INDEXCONTENT = encodeURI('*￣content￣*')
const ELASTICURLCONTENT =  'http://' + ELASTICHOST + ':' + ELASTICPORT + '/' + INDEXCONTENT + '/_search'

export const get = async function _api_mlt_(req, res, next) {
  let { id, hash, field='content', size=1024, pretty=false } = req.query
  pretty = pretty === false ? false : true
  const query = {
      "size":1024,
      "query": {
        "more_like_this": {
          "fields": [field],
          "like": {
            "_id": id
          },
          "max_query_terms": 128,
          "min_term_freq": 1,
          "min_word_length": 4,
          "max_word_length": 32
        }
      },
      "_source": ["__meta.resourcename","__fields.*"]
  }

  query._source.push(field)
  let result = await fetchElastic(ELASTICURLCONTENT, query)
  console.dir({result})
  res.statusCode = result.status
  if (!result.answer) {
    if (pretty) res.end(result.status + ' ' + result.statusText + '\n\n' + result.error.callers.join('\n') + '\n\n' + JSON.stringify({ q, start, size, fields, refresh, pretty, query, result }, null, 4))
    else res.end()
  } else {
    if (result.answer?.hits?.total?.value){

      if (result?.answer?.hits?.hits) {
        if (field === 'content') for (const hit of result.answer.hits.hits) {
            hit.content =  hit._source.content.slice(0,2048).replace(new RegExp('( \n)', 'gmu'),'\n').replace(new RegExp('(\n){2,}', 'gim'),'<p>').replace(new RegExp('(\n){1,}', 'gim'),'<br>').slice(0,1024)
        }
      }
      if (pretty) res.end(JSON.stringify(result.answer.hits, null, 4))
      else res.end(JSON.stringify(result.answer.hits))
    } else {
      res.statusCode = 404
      res.end()
    }
  }
}
