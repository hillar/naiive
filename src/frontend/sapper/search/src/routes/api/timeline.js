/*
GET deleteme￣timeline￣watch‾7200/_search?size=0
{
   "query": {
    "term": {
      "__id__.keyword": {
        "value": "002ee067a85302071396ebfb64b088e5-0-1"
      }
    }
  }
}

GET ￣timeline￣/_search?size=0
{
  "size": 0,
  "aggs" : {
    "total" : {
        "filter" :
          { "match" : { "__id__" : "002f4375bd93826337bede547350b775-0-1"   }
        }

    }
  }
}

{
  "took" : 243,
  "timed_out" : false,
  "_shards" : {
    "total" : 2,
    "successful" : 2,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : {
      "value" : 10000,
      "relation" : "gte"
    },
    "max_score" : null,
    "hits" : [ ]
  },
  "aggregations" : {
    "total" : {
      "doc_count" : 1702527
    }
  }
}





*/


/*

TODO harmonize field names case

GET *￣line￣* /_search
{
  "query": {"bool": {
    "filter": [
      {"term": {
        "iban.keyword": "EE891010220034796011"
      }}
    ]
  }
  }
}

GET *￣timeline￣* /_search
{
  "query": {"bool": {
    "filter": [
      {"term": {
        "IBAN.keyword": "EE891010220034796011"
      }}
    ]
  }
  }
}

GET *￣entities￣* /_search
{
  "query": {"bool": {
    "filter": [
      {"term": {
        "iban.keyword": "EE891010220034796011"
      }}
    ]
  }
  }
}

*/

import { ELASTICHOST, ELASTICPORT, fetchElastic } from '../../utils/fetch.mjs'
import { createHash } from 'crypto'
import { default as QuickLRU } from 'quick-lru'

// TODO get them from constants
const INDEXTIMELINE = encodeURI('*￣timeline￣*')

const cache = new QuickLRU({ maxSize: 1024 * 16 })

const DEFAULTALGORITHM = 'md5'
function digestString(str, algorithm = DEFAULTALGORITHM, encoding = 'hex') {
  return createHash(algorithm).update(str).digest(encoding)
}

let ELASTICURLTIMELINE = 'http://' + ELASTICHOST + ':' + ELASTICPORT + '/' + INDEXTIMELINE + '/_search'

export const get = async function _api_entities_(req, res, next) {
  let { doc, refresh = false, pretty = false } = req.query

  pretty = pretty === false ? false : true
  refresh = refresh === false ? false : true
  const hash = digestString(JSON.stringify(req.query))
  let item
  if (!refresh) item = cache.get(hash)
  if (!item) {
    if (!doc) {
      res.statusCode = 415
      if (pretty) res.end('missing doc id')
      else res.end()
    } else {
      const query = {
        "size": 0,
        "query": {
          "bool": {
            "must": [
              {
                "term": {
                  "__id__.keyword": {
                    "value": doc
                  }
                }
              }

            ]
          }
        }
      }
      let result = await fetchElastic(ELASTICURLTIMELINE, query)
      console.dir(result)
      res.statusCode = result.status
      if (!result.answer) {
        if (pretty) res.end(result.status + ' ' + result.statusText + '\n\n' + result.error.callers.join('\n') + '\n\n' + JSON.stringify({ doc }, null, 4))
        else res.end()
      } else {
        if (result?.answer?.hits?.hits) {
          result.answer.hits.cachetime = new Date()
          cache.set(hash, result.answer.hits)
          if (pretty) res.end(JSON.stringify(result.answer.hits, null, 4))
          else res.end(JSON.stringify(result.answer.hits))
        } else {
          res.statusCode = 404
          res.end()
        }
      } 
    }
  } else {
    // from cache
    const age = new Date() - new Date(item.cachetime)
    item.age = age
    res.setHeader('Content-Type', 'application/json')
    if (pretty) res.end(JSON.stringify(item, null, 4))
    else res.end(JSON.stringify(item))
  }
}
