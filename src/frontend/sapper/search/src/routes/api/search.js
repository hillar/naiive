
import { ELASTICHOST, ELASTICPORT, fetchElastic } from '../../utils/fetch.mjs'
import { createHash } from 'crypto'
import { default as QuickLRU } from 'quick-lru'

// TODO get them from constants
const INDEXFILES = encodeURI('*￣files￣*')
const INDEXCONTENT = encodeURI('*￣content￣*')

let ELASTICURLCONTENT = 'http://' + ELASTICHOST + ':' + ELASTICPORT + '/' + INDEXCONTENT + '/_search'

const cache = new QuickLRU({ maxSize: 1024*16 })

const DEFAULTALGORITHM = 'md5'
function digestString(str, algorithm = DEFAULTALGORITHM, encoding = 'hex') {
  return createHash(algorithm).update(str).digest(encoding)
}

export function buildQuery(q, from = 0, size = 0, fields, nohighlight) {
  let filters = []
  if (q.includes(':')) {

    // build filter
    const tagregex = /(?<key>[\w.]+)\:"(?<value>[^"]*)"/g
    const tmp = decodeURIComponent(q)
    let realq = tmp.replace(tagregex, '').trim()
    if (!realq.length) realq = '*'
    q = realq
    const _filters = [...tmp.matchAll(tagregex)]
    for (const filter of _filters) {
      const tmp = {}
      if (!filter.groups.key.startsWith('_')) tmp['__fields.' + filter.groups.key + '.keyword'] = filter.groups.value
      else tmp[filter.groups.key] = filter.groups.value
      filters.push({ term: tmp })
    }
  }
  // temp workaround of ela bug
  // failed to create query
  // type": "null_pointer_exception"
  // TODO issue elasticsearch
  let langsfields
  if (q.includes('*')||q.includes('"')) langsfields = 'content-*'
  else langsfields = '__languages.content-*'

  const data = {
    query: {
      bool: {
        must: {
          simple_query_string: {
            query: q,

            fields: ['__all__^4', 'content^2', langsfields],
            default_operator: 'and',
          },
        },
      },
    },

    highlight: {
      fields:
        //{'*': { require_field_match: false }}

        [{ __all__: { require_field_match: false } }, { content: { require_field_match: false } }, { '__languages.content-*': { require_field_match: false } }, { '__meta.all': { require_field_match: false } }, { '__fields.*': { require_field_match: false } }],
    },

    sort: ['_score'],
    from: from,
    size: size,
  }
  if (filters.length > 0) {

    data.query.bool.filter = filters
  }
  data._source = {}
  if (fields && fields.trim().length) {
    if (fields.trim() === '*') data._source.excludes = ['__meta.all', 'content', '__languages.content-*']
    else {
      data._source.includes = []
      for (const name of fields.split(',')) data._source.includes.push(name.trim())
    }
  } else data._source.excludes = ['*']
  if (q === '*') {
    delete data.highlight
    delete data.query.bool.must
    if (!data._source.excludes) {
      if (!data._source.includes) data._source.includes = []
      data._source.includes.push('content')
    } else data._source.excludes = data._source.excludes.filter((i) => i !== 'content')
    data.sort = ['_id']
    // TODO #! Deprecation: Loading the fielddata on the _id field is deprecated and will be removed in future versions. If you require sorting or aggregating on this field you should also include the id in the body of your documents, and map this field as a keyword field that has [doc_values] enabled

  }
  if (nohighlight) delete data.highlight
  return data
}

export const get = async function _api_search_(req, res, next) {
  let { q, start = 0, from, size = 0, fields, refresh=false, pretty=false, nohighlight=false } = req.query
  pretty = pretty === false ? false : true
  refresh = refresh === false ? false : true
  nohighlight = nohighlight === false ? false : true
  if (from !== undefined) start = from
  if (q && q.trim &&q.trim().replace('*', '').length) {
    const hash = digestString(JSON.stringify(req.query))
    let item
    if (!refresh) item = cache.get(hash)
    if (!item) {
      const query = buildQuery(q, start, size, fields,nohighlight)
      //console.log(JSON.stringify(query,null,4))
      let result = await fetchElastic(ELASTICURLCONTENT, query, q)
      res.statusCode = result.status
      if (!result.answer) {
        if (pretty) res.end(result.status + ' ' + result.statusText + '\n\n' + result.error.callers.join('\n') + '\n\n' + JSON.stringify({ q, start, size, fields, refresh, pretty, query, result }, null, 4))
        else res.end()
      } else {
        // if no highlight set it to content head first 1KB
        if (result?.answer?.hits?.hits) {
          for (const hit of result.answer.hits.hits){
            if (!hit.highlight) {
              if (hit._source.content && hit._source.content.length) hit.highlight = {content: hit._source.content.slice(0,2048).replace(new RegExp('( \n)', 'gmu'),'\n').replace(new RegExp('(\n){2,}', 'gim'),'<p>').replace(new RegExp('(\n){1,}', 'gim'),'<br>').slice(0,1024)}
              else hit.highlight = {content : '<i> no content ;( </i>'}
              delete hit._source.content
            }
          }
          result.answer.hits.cachetime = new Date()
          cache.set(hash, result.answer.hits)
          if (pretty) res.end(JSON.stringify(result.answer.hits, null, 4))
          else res.end(JSON.stringify(result.answer.hits))
        } else {
          console.dir(result.answer)
          res.statusCode = res.statusCode !== 200 ? res.statusCode : 404
          res.end()
        }
      }
    } else {
      // from cache
      const age = new Date() - new Date(item.cachetime)
      item.age = age
      res.setHeader('Content-Type', 'application/json')
      if (pretty) res.end(JSON.stringify(item,null,4))
      else res.end(JSON.stringify(item))
    }
  } else {
    // no q
    res.statusCode = 415
  }
  res.end()
}
