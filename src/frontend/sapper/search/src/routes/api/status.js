import { ELASTICHOST, ELASTICPORT, fetchElastic } from '../../utils/fetch.mjs'


const INDEXFILES = encodeURI('*￣files￣*')
const INDEXMETA = encodeURI('*￣meta￣*')
const INDEXENTITIES = encodeURI('*￣entities￣*')
const INDEXTIMELINE = encodeURI('*￣timeline￣*')
const INDEXCONTENT = encodeURI('*￣content￣*')
const ELASTICURLFILES =  'http://' + ELASTICHOST + ':' + ELASTICPORT + '/' + INDEXFILES + '/_search'

const indexes = [INDEXCONTENT, INDEXTIMELINE, INDEXENTITIES, INDEXMETA, INDEXFILES]

export const get = async function _api_status_(req, res, next) {
  const { hash } = req.query
  const query = {
    "size": 0,
    "query": {
      "bool": {
        "must": [
          {"term": {
            "__contenthash__": hash
          }}
        ]
      }
    }
  }
  let exists = false
  res.statusCode = 404
  for (const index of indexes){
    const url = 'http://' + ELASTICHOST + ':' + ELASTICPORT + '/' + index + '/_search'
    const result = await fetchElastic(url, query)
    console.log(url,result.status)
    if (result.answer?.hits?.total?.value) {
      exists = index
      res.statusCode = result.status
      break
    } else if (result.status !== 200){
      res.statusCode = 500
      break
    }
  }
  if (exists) res.end(JSON.stringify({exists:decodeURI(exists), done:(exists === INDEXCONTENT)}))
  else res.end()

}
