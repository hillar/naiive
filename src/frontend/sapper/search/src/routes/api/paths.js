import {resolve, join } from 'path'
import { readFileSync, writeFileSync, mkdirSync } from 'fs'


const DATADIR = './data'
try {
  mkdirSync(DATADIR,{recursive:true})
} catch (error) {
  console.error(error)
  process.exit(-1)
}



export const get = async function _api_paths_(req, res, next) {
  const {ou} = req.query
  const pathsfilename = join(DATADIR,'paths.json')
  let paths = []
  try {
      const all = JSON.parse(readFileSync(pathsfilename))
      paths = all[ou.toLowerCase()]
  } catch (error) {
    console.error(pathsfilename)
    console.error(error)
  }

  res.end(JSON.stringify(paths ? paths : {}))
}

export const post = async function _api_paths_(req, res, next) {
  const {ou} = req.query
  const user = req.user
  console.dir({ou,user})
  const pathsfilename = join(DATADIR,'paths.json')
  const data = []
  let all = {}
  try {
      all = JSON.parse(readFileSync(pathsfilename))
  } catch (error) {
    //noop
  }
  req.on('data',(chunk) => data.push(chunk) )
  req.on('end',()=>{
    try {
      const paths = JSON.parse(Buffer.concat(data).toString())
      all[ou] = paths
      console.dir(all)
      try {
        writeFileSync(pathsfilename,JSON.stringify(all))
        JSON.stringify({ou,user,all})
      } catch (error) {
        console.error(error)
        res.statusCode = 500
      }
    } catch (error) {
      console.error(error)
      res.statusCode = 415
    }
    res.end()
  })


}
