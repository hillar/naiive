import { join, resolve as resolvename } from 'path'
import { parse as parseurl } from 'url'
import { exec } from 'child_process'
import { exists } from '../../../../../../backend/utils/fs.mjs'
import { createReadStream } from 'fs'





const NWJSVERSION =  '0.49.0'//'0.40.1'
const NWJSDL = 'https://dl.nwjs.io/v'
const NWJSSDK = '-sdk'
const KNOWNCLIENTS = ['osx','win','linux']
const CLIENTEXT = {'osx':'zip','win':'zip','linux':'tar.gz'}
const SDK = '-sdk' // ! keep in sync with prep-desktop-clients.sh !
const NAIIVE = 'naiive'+SDK

const prep = (host,targetdir,nwjsversion) => {
  return new Promise( (resolve) => {
    const prepdesktopclients = `./prep-desktop-clients.sh "${host}" "${targetdir}" "${nwjsversion}"`
    exec(prepdesktopclients, {stdio:['inherit','pipe','pipe']}, (err, output, code) => {
      if (!(err === null)) {
          console.error({code,err,output})
      } else {
          for (const line of output.split('\n'))
          console.log(line)
      }
      resolve()
    })

  })
}

export const get = async function _get_download_ (req, res, next) {
  const { os } = req.params
  const { referer } = req.headers
  console.dir({b:(!os || !KNOWNCLIENTS.includes(os) || !referer?.startsWith('http')),os,referer,u:req.user})
  if (!os || !KNOWNCLIENTS.includes(os) || !referer?.startsWith('http')) {
    res.statusCode = 404
    res.end()
  } else {
    const {protocol, hostname} = parseurl(referer)
    const path = `${process.cwd()}/static/${hostname}`
    const filename = `${path}/${NAIIVE}-v${NWJSVERSION}-${os}-x64.${CLIENTEXT[os]}`
    if (!exists(filename)) await prep(protocol+'//'+hostname,path,NWJSVERSION)
    if (!exists(filename)) {
      res.statusCode = 500
      res.end()
    } else {
      res.setHeader('Content-Disposition', 'attachment; filename=' + hostname+'.'+NWJSVERSION+'.'+CLIENTEXT[os])
      res.setHeader('Content-Type', 'application/zip')
      // TODO catch pipe errors
      createReadStream(filename).pipe(res)
    }
    //console.dir({origin,filename})
    //res.end(filename)
  }

}
