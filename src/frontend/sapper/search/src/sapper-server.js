import sirv from 'sirv';
import polka from 'polka';
import compression from 'compression';
import * as sapper from '@sapper/server';
import {authenticate} from './utils/auth.mjs'
const { createProxyMiddleware } = require('http-proxy-middleware');


let { PORT, ELASTIC, FILES, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

if (!PORT) PORT = 3000
if (!ELASTIC) ELASTIC = 'locahost'
if (!FILES) FILES = 'localhost'

process.env.PORT = PORT
process.env.ELASTIC = ELASTIC
process.env.FILES = FILES


export const log = () => {
	return async (req, res, next) => {
    console.log(new Date(),req.user)
    return next()
  }
}

polka() // You can also use Express
  .use(authenticate())
  .use(log())
  .use('elastic',
	  createProxyMiddleware({
	    target: 'http://'+ELASTIC+':9200',
	    changeOrigin: true,
			pathRewrite: {'^/elastic' : ''}
	  })
	)
  .use('upload',
    createProxyMiddleware({
      target: 'http://'+FILES+':7200',
      changeOrigin: true,
      pathRewrite: {'^/upload' : ''}
    })
  )
	.use(
		//compression({ threshold: 0 }),
		sirv('static'),
		sapper.middleware()
	)
	.listen(PORT, err => {
		if (err) console.log('error', err);
	});
