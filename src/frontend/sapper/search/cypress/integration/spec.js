describe('Sapper template app', () => {
	beforeEach(() => {
		cy.visit('/')
	});

	it('has the correct <h1>', () => {
		cy.contains('h1', 'Great success!')
	});

	it('navigates to /search', () => {
		cy.get('nav a').contains('search').click();
		cy.url().should('include', '/search');
		cy.get('form').submit()  
	});


});
