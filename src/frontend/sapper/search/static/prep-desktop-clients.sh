#!/bin/sh

# params
# 1 server
# 2 target dirname
# 3 nwjs version

# keep with sync with backend & frontend !
NWJSDL="https://dl.nwjs.io/v"
NWJSSDK="-sdk"


log() { echo "$(date) $(basename $0): $*"; }
die() { log "$*" >&2; exit 1; }


running=1
while [ $running -eq 1 ]
do
  for pid in $(pidof  $(basename $0)); do
      if [ $pid != $$ ]; then
         export running=1
  	     log "$0 is already running, trying again in 10s .. "
  	     sleep 10
      else
	       export running=0
      fi
  done
done

for pid in $(pidof  $(basename $0)); do
    if [ $pid != $$ ]; then
	     log "$0 is already running, trying again later "
	     sleep 3
    fi
done

SERVER=${1:-$(hostname)}
TARGETDIR=${2:-$(pwd)}
NWJSVER=${3:-0.48.2}
CACHEDIR="${TARGETDIR}/../cache/"

log "start ${SERVER} ${TARGETDIR} ${NWJSVER} "
[ -d "${TARGETDIR}" ] ||  (mkdir -p "${TARGETDIR}" || die "can not make target dir ${TARGETDIR} ")
cd ${TARGETDIR} || die "no target dir ${TARGETDIR} "
[ -f "package.json.last" ] && rm package.json.last
[ -f "package.json" ] && cp package.json package.json.last
cat > package.json <<EOF
{
  "name": "NAIIVE ${NWJSVER}",
  "main": "${SERVER}",
  "nodejs": true,
  "node-remote": "${SERVER}",
  "chromium-args": "--enable-node-worker",
  "user-agent": "${SERVER} ${NWJSVER} $(hostname) naiive desktop",
  "window":{
    "width": 1280,
    "height":720,
    "title":"Naïve"
  }
}
EOF
[ -f "package.json" ] || die "failed to create package.json"

#nwjs-sdk-v0.40.1-linux-x64.tar.gz
#nwjs-sdk-v0.40.1-osx-x64.zip
#nwjs-sdk-v0.40.1-win-x64.zip

#nwjs-v0.40.1-linux-x64.tar.gz
#nwjs-v0.40.1-osx-x64.zip
#nwjs-v0.40.1-win-x64.zip



NWJSOSX="nwjs${NWJSSDK}-v${NWJSVER}-osx-x64.zip"
NWJSOSXDL="${NWJSDL}${NWJSVER}/${NWJSOSX}"
NAIIVEOSX="naiive${NWJSSDK}-v${NWJSVER}-osx-x64.zip"

NWJSWIN="nwjs${NWJSSDK}-v${NWJSVER}-win-x64.zip"
NWJSWINDL="${NWJSDL}${NWJSVER}/${NWJSWIN}"
NAIIVEWIN="naiive${NWJSSDK}-v${NWJSVER}-win-x64.zip"

NWJSLINUX="nwjs${NWJSSDK}-v${NWJSVER}-linux-x64.tar.gz"
NWJSLINUXDL="${NWJSDL}${NWJSVER}/${NWJSLINUX}"
NAIIVELINUX="naiive${NWJSSDK}-v${NWJSVER}-linux-x64.tar.gz"

if [ -f "package.json.last" ]; then
  if [ $(diff package.json package.json.last | wc -l) -eq 0 ]; then
    if [ -f ${NAIIVEOSX} ] && [ -f ${NAIIVEWIN} ] && [ -f ${NAIIVELINUX} ]; then
      log 'all same, nothing to do'
      exit 0
    fi
  fi
fi

[ -f "${NWJSOSX}" ] || wget -q "${NWJSOSXDL}" || die "failed to download ${NWJSOSXDL}"
[ -f "${NWJSOSX}" ] && cp ${NWJSOSX} ${NAIIVEOSX} || die "failed to copy ${NWJSOSX} ${NAIIVEOSX}"
[ -f "${NAIIVEOSX}" ] && mkdir -p "nwjs${NWJSSDK}-v${NWJSVER}-osx-x64/nwjs.app/Contents/Resources/app.nw" && cp package.json nwjs${NWJSSDK}-v${NWJSVER}-osx-x64/nwjs.app/Contents/Resources/app.nw || die "failed to copy package.json to nwjs${NWJSSDK}-v${NWJSVER}-osx-x64/nwjs.app/Contents/Resources/app.nw"
zip ${NAIIVEOSX} nwjs${NWJSSDK}-v${NWJSVER}-osx-x64/nwjs.app/Contents/Resources/app.nw/package.json || die "failed to add package.json to zip ${NAIIVEOSX}"
rm -rf nwjs${NWJSSDK}-v${NWJSVER}-osx-x64

[ -f "${NWJSWIN}" ] || wget -q "${NWJSWINDL}" || die "failed to download ${NWJSWINDL}"
[ -f "${NWJSWIN}" ] && cp ${NWJSWIN} ${NAIIVEWIN} || die "failed to copy ${NWJSWIN} ${NAIIVEWIN}"
[ -f "${NAIIVEWIN}" ] && mkdir nwjs${NWJSSDK}-v${NWJSVER}-win-x64 && cp package.json ./nwjs${NWJSSDK}-v${NWJSVER}-win-x64 || die "failed to copy package.sjont to "
zip ${NAIIVEWIN} nwjs${NWJSSDK}-v${NWJSVER}-win-x64/package.json || die "failed to add package.json to zip ${NAIIVEWIN}"
rm -rf nwjs${NWJSSDK}-v${NWJSVER}-win-x64

[ -f "${NWJSLINUX}" ] || wget -q "${NWJSLINUXDL}" || die "failed to download ${NWJSLINUXDL}"
[ -f "${NWJSLINUX}" ] && cp ${NWJSLINUX} ${NAIIVELINUX} || die "failed to copy ${NWJSLINUX} ${NAIIVELINUX}"
# The tar command don’t have a option to add files to an existing compressed tar.gz
tar -xzf ${NWJSLINUX} || die "failed to unpack ${NWJSLINUX}"
cp package.json nwjs${NWJSSDK}-v${NWJSVER}-linux-x64 || die "failed to copy package.json to nwjs${NWJSSDK}-v${NWJSVER}-linux-x64"
tar -xzf ${NWJSLINUX} nwjs${NWJSSDK}-v${NWJSVER}-linux-x64 || die "pailed to pack ${NWJSLINUX}"
rm -rf nwjs${NWJSSDK}-v${NWJSVER}-linux-x64

log "done"
