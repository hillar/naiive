;(async () => {
  const { ReadStream,  statSync, readFileSync, writeFileSync, mkdirSync, copyFileSync, unlinkSync } = require('fs')
  const { sep, join, basename, dirname, resolve } = require('path')
  const { userInfo, hostname, networkInterfaces, tmpdir } = require('os')
  const { createHash } = require('crypto')
  const { readdir, stat } = require('fs').promises

  let currentdir = ''

  async function* getFiles(dir) {
    let dirents
    try {
      dirents = await readdir(dir, { withFileTypes: true })
    } catch (error) {
      console.error(error)
      notify({error:error.toString(),readdir:{path:dir}})
      return
    }
    if (dirents)
      for (const dirent of dirents) {
        const res = resolve(dir, dirent.name)
        if (dirent.isDirectory()) {
          currentdir = res
          yield* getFiles(res)
        } else if (dirent.isFile()) {
          const tmp = {}
          tmp.name = dirent.name
          tmp.fullname = res
          let _stats
          try {
            _stats = await stat(res)
            tmp.stats = {}
            for (const statname of Object.keys(_stats)) {
              tmp.stats[statname] = _stats[statname]
            }
            yield { ...tmp }
          } catch (error) {
            notify({error:error.toString(),stat:{filename:res}})
            console.error(error)
          }
        }
      }
  }

  //const { STATUS_CODES } = require("http");
  //const { userInfo, hostname, networkInterfaces } = require("os");

  const broadcastChannel = new BroadcastChannel('GLOBSENDSTATUS')

  const pid = process.pid
  const ppid = process.ppid

  const localuser = userInfo()
  const username = localuser.username
  const host = hostname()


  const DEFAULTHASHALGORITHM = 'md5'

  function stringHash(str, algorithm = DEFAULTHASHALGORITHM) {
    return createHash(algorithm).update(str).digest('hex')
  }
  // TODO make it param
  const MAXFILESIZE = 20 // GB
  const TOBIG = stringHash('TOBIG')

  function fileHash(filename, size, algorithm = DEFAULTHASHALGORITHM) {
    return new Promise((resolve) => {
      if (size > 1024*1024*1024*MAXFILESIZE) {
        notify({ 'to big': { size, max:1024*1024*1024*MAXFILESIZE,filename } })
        resolve(TOBIG)
      } else {
        const start = new Date()
        //notify({ hashing: { size, filename } })
        let shasum = createHash(algorithm)
        try {
          let s = ReadStream(filename)
          s.on('data', function (data) {
            shasum.update(data)
          })
          s.on('end', function () {
            const hash = shasum.digest('hex')
            const took = new Date() - start
            //notify({ hashing: { took, size, hash, filename } })
            resolve(hash)
          })
          s.on('error', function (e) {
            console.error(e)
            notify({ error: e.toString(), filename })
            resolve()
          })
        } catch (e) {
          console.error(e)
          notify({ error: e.toString(), filename })
          resolve()
        }
    }
    })
  }

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

  async function get(url) {
    let answer
    let result
    try {
      result = await fetch(url)
    } catch (error) {
      notify({ error: error.toString(), get: url })
    }
    if (result.ok) answer = await result.json()
    else {
      notify({ error: { http: { code: result.status, text: result.statusText }, get: url } })
    }
    if (answer) {
      if (Array.isArray(answer)) return [...answer]
      else return { ...answer }
    } else {
      await sleep(3000)
      return get(url)
    }
  }

  async function upload(contentHash, filename, stats) {
    let result
    try {
      result = await fetch('/upload/' + contentHash, { method: 'HEAD' })
    } catch (error) {
      notify({error:error.toString(),head:{contentHash,filename}})
      return
    }
    if (result && result.status && result.status === 404) {
      const shortNameHash = stringHash(basename(filename))
      const nameHash = stringHash(filename)
      const form = new FormData()
      form.append('user', username)
      form.append('hostname', host)
      form.append('contentHash', contentHash)
      form.append('shortNameHash', shortNameHash)
      form.append('nameHash', nameHash)
      form.append('fullname', filename)
      form.append('basename', basename(filename))
      for (const tag of dirname(filename).split(sep)) {
        if (tag.trim()) form.append('tag', tag)
      }
      for (const statname of Object.keys(stats)) {
        form.append(statname, stats[statname])
      }
      let tmpfilename
      try {
        let file
        if (filename.length > 258) {
          // wait for Chrome to fix longnames
          tmpfilename = join(tmpdir(), contentHash)
          notify({ longname: { filename, tmpfilename } })
          try {
            copyFileSync(filename, tmpfilename)
          } catch (error) {
            console.error(error)
            notify({ error: error.toString(), copy: { from: filename, to: tmpfilename } })
          }
          file = new File(tmpfilename, tmpfilename)
        } else file = new File(filename, filename)
        if (file) form.append('file', file)
        else {
          console.error('no formdata file', filename)
        }
      } catch (e) {
        console.error(e)
        notify({ error: e.toString(), msg: 'formdata' })
      }
      const started = new Date()
      //  HttpProxyMiddleware is auto decoding encoded uri and throws
      // UnhandledPromiseRejectionWarning: RangeError [ERR_HTTP_INVALID_STATUS_CODE]: Invalid status code: ERR_UNESCAPED_CHARACTERS
      // so can not put filename ;(
      const uri = `/upload/`
      let result
      try {
        result = await fetch(uri, { method: 'PUT', body: form })
      } catch (error) {
        console.error(error)
        notify({ error: error.toString(), upload: { uri, contentHash, filename } })
        result = {}
      }
      const took = new Date() - started
      if (result.ok) {
        console.log(took, contentHash, filename)
        notify({ upload: { took, size: stats.size, contentHash, filename } })
        return true
      } else {
        notify({ error: { http: { code: result.status, text: result.statusText }, size: stats.size, contentHash, filename, uri } })
      }
      if (tmpfilename) {
        try {
          unlinkSync(tmpfilename)
        } catch (error) {
          // noop
        }
      }
    } else if (result && result.status && result.status === 200) return true
  }

  function notify(m) {
    broadcastChannel.postMessage({ ...m })
  }

  setInterval(() => {
    //notify('ping')
    const memoryUsage = process.memoryUsage()
    notify({ memoryUsage: { ...memoryUsage, pid, ppid, currentdir } })
  }, 1000 * 2)


  const seendir = join(localuser.homedir, '.naiive')
  try {
    statSync(seendir)
  } catch (error) {
    mkdirSync(seendir)
  }
  let seenfilename = join(seendir, 'seen.json')
  let seen
  try {
    seen = JSON.parse(readFileSync(seenfilename))
  } catch (error) {
    seen = {}
    try {
      writeFileSync(seenfilename, JSON.stringify(seen))
    } catch (error) {
      seenfilename = join(sep + 'tmp', 'seen.json')
      try {
        seen = JSON.parse(readFileSync(seenfilename))
      } catch (error) {
        writeFileSync(seenfilename, JSON.stringify(seen))
      }
    }
  }
  let hashesfilename = join(seendir, 'hashes.json')
  let hashes
  try {
    hashes = JSON.parse(readFileSync(hashesfilename))
  } catch (error) {
    hashes = {}
    writeFileSync(hashesfilename, JSON.stringify(hashes))
  }
  const user = await get('/api/user')
  const ignore = [process.cwd(), dirname(seenfilename)]
  const GLOBINTERVAL = 1000 * 100
  let nexttimeout

  async function glob() {
    if (user && user.ou) {
      const paths = await get('api/paths?ou=' + user.ou)
      if (paths) {
        notify({ glob: { msg: 'start' } })
        const start = new Date()
        let total = 0
        for (const path of paths) {
          const start = new Date()
          notify({ path, start })
          let count = 0
          let counter = 0
          try {
            for await (const { name, fullname, stats } of getFiles(path)) {
              //console.dir({name,fullname, stats})
              counter ++
              if (counter % 100 === 0) {
                try {
                  const _seen = JSON.stringify(seen)
                  writeFileSync(seenfilename, _seen)
                  const _hashes = JSON.stringify(hashes)
                  writeFileSync(hashesfilename, _hashes)
                } catch (error) {
                  console.error(error)
                  notify({ error: error.toString(), writeFileSync: 'localstate', path })
                }
              }
              if (stats.size > 1) {
                const { size, ctimeMs } = stats
                if (!seen[fullname]) {
                  //newCount++;
                  seen[fullname] = ctimeMs
                  const contentHash = await fileHash(fullname, size)
                  if (!hashes[contentHash] && contentHash !== TOBIG) {
                    const ok = await upload(contentHash, fullname, { ...stats })
                    if (ok) hashes[contentHash] = 1
                    else seen[fullname] = false
                  }
                } else {
                  if (!(ctimeMs === seen[fullname])) {
                    //changedCount++;
                    seen[fullname] = ctimeMs
                    const contentHash = await fileHash(fullname, size)
                    if (!hashes[contentHash] && contentHash !== TOBIG) {
                      const ok = await upload(contentHash, fullname, { ...stats })
                      if (ok) hashes[contentHash] = 1
                      else seen[fullname] = false
                    }
                  }
                }
              }
            }
          } catch (error) {
            console.error(error)
            notify({ error: error.toString(), path })
          }
          const took = new Date() - start
          notify({ path, took, count })
          try {
            const _seen = JSON.stringify(seen)
            writeFileSync(seenfilename, _seen)
            const _hashes = JSON.stringify(hashes)
            writeFileSync(hashesfilename, _hashes)
          } catch (error) {
            console.error(error)
            notify({ error: error.toString(), writeFileSync: 'localstate', path })
          }
        }
        const took = new Date() - start
        notify({ glob: { total, took } })
      }
    }
    nexttimeout = setTimeout(() => {
      glob()
    }, GLOBINTERVAL)
  }
  // TODO wait for user log in
  nexttimeout = setTimeout(() => {
    glob()
  }, 7000)
})()
