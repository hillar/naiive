import svelte from 'rollup-plugin-svelte';
import autoPreprocess from 'svelte-preprocess'
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import copy from 'rollup-plugin-copy'
import { terser } from 'rollup-plugin-terser';

const production = !process.env.ROLLUP_WATCH;

export default {
	input: 'src/main.js',
	output: {
		sourcemap: true,
		format: 'iife',
		name: 'app',
		file: 'dist/bundle.js'
	},
	plugins: [
		svelte({
			dev: !production,

			preprocess: autoPreprocess({
        postcss: {
          plugins: [require('autoprefixer')()],
        },
      }),
			
			css: css => {
				css.write('dist/bundle.css');
			}
		}),
		copy({
      targets: [
        { src: 'src/public/**', dest: 'dist/' }
      ]
    }),
		resolve({ browser: true }),
		commonjs(),
		production && terser()
	],
	watch: {
		clearScreen: false
	}
};
