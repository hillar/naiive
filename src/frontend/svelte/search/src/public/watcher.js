//if (global.nw && global.nw.require) var require = global.nw.require

const { readdirSync, statSync, readFileSync, writeFileSync } = require('fs')
const { join, dirname } = require('path')

const CACHEFILE = 'seen.json'
let seen = {}
let errors = {}
let paths
let ignore
let changedCount = 0
let newCount = 0
let pause = true
let counter = 0
/*
function isPause(ms=10) {
    return new Promise(resolve => setTimeout(resolve(pause), ms));
}
*/
function isPause() {
    return new Promise(resolve => setImmediate(resolve(pause)));
}



async function readDirectory(path){
    return new Promise( async (resolve) => {
        const paused = await isPause()
        if (!paused) {
            try {
                const files = readdirSync(path)
                let subdirs = []
                while (files.length > 0) {
                    const file = files.shift()
                    const paused = await isPause()
                    if (paused) {
                         //console.log('file break on pause',path,file)
                         break
                    }
                    await new Promise( resolve => {
                    setTimeout( async () => {

                        if ( counter % 999 === 0) {
                            //console.log('progress', counter / Object.keys(seen).length )
                            postMessage({globbing:{seen:Object.keys(seen).length,errors:Object.keys(errors).length,newCount,changedCount,counter}})
                        }
                        const filename = join(path,file)
                        try {
                            const stats = statSync(filename)
                            if (stats.isDirectory() && !ignore.includes(filename)){
                                subdirs.push(filename)
                            } else if (stats.isFile() && stats.size > 1){
                                counter ++
                                if (!(seen[filename])) {
                                    newCount ++
                                    seen[filename] = stats.ctimeMs
                                    postMessage({filename})
                                } else {
                                    if (!(stats.ctimeMs === seen[filename])){
                                        changedCount ++
                                        seen[filename] = stats.ctimeMs
                                        postMessage({filename})
                                    }
                                }
                            }
                        } catch (e) {
                            if (!(errors[path])) {
                                errors[path] = e
                                postMessage({error:e.message})
                            }
                        }
                        resolve()
                    },0)
                    })
                }
                while (subdirs.length > 0) {
                    const subdir = subdirs.shift()
                    const paused = await isPause()
                    if (paused) {
                        //console.log('subdir break on pause')
                        break
                    }
                    //setTimeout( async () => {
                        await readDirectory(subdir)
                    //},0)
                }
            } catch (e) {
                if (!(errors[path])) {
                    errors[path] = e
                    postMessage({error:e.message})
                }
            }

        }
        resolve(pause)
    })
}

let globberTimeout = null
const POLLINTERVAL = 1000 * 30
async function globber() {
    if (globberTimeout) {
        //console.log('return on existing next timeout')
        return
    }
    if (!paths) {
        postMessage({error:'no paths'})
        return
    }
    if (!ignore) {
        postMessage({error:'no ignore'})
        return
    }
    changedCount = 0
    newCount = 0
    counter = 0
    //console.log('start globbing')
    postMessage({globbing:{seen:Object.keys(seen).length,errors:Object.keys(errors).length,newCount,changedCount,counter}})
    for (const p of paths) {
        const paused = await isPause()
        if (paused) {
             //console.log('paths break on pause',p)
             break
        }
        postMessage({globbing:{seen:Object.keys(seen).length,errors:Object.keys(errors).length,newCount,changedCount,counter}})
        await readDirectory(p.path)
    }
    const paused = await isPause()
    if (!paused) {
        //console.log('set timeout')
        postMessage({running:{seen:Object.keys(seen).length,errors:Object.keys(errors).length,newCount,changedCount}})
        globberTimeout = setTimeout(function() {
            globberTimeout = null
            if (!pause) globber()
        }, POLLINTERVAL)
    } else {
        //console.log('pausing')
        postMessage({paused:{seen:Object.keys(seen).length,errors:Object.keys(errors).length,newCount,changedCount}})
    }
}

onmessage = function(m) {
    if (m.data.init && m.data.init.paths && m.data.init.ignore) {
        paths = m.data.init.paths
        ignore = m.data.init.ignore
        try {
            seen = JSON.parse(readFileSync(CACHEFILE))
        } catch (e) {
            seen = {}
            if (e.code === "ENOENT") {
                //noop
            } else {
                console.error(e)
            }
        }
        if (Object.keys(seen).length === 0) {
            pause = false
            globber()
        } else {
            postMessage({paused:{seen:Object.keys(seen).length,errors:Object.keys(errors).length,newCount,changedCount}})
        }

    } else if (m.data.stop){
        pause =  true
        try {
            writeFileSync(CACHEFILE,JSON.stringify({}))
        } catch (e) {
            console.error(e)
        }
        seen = {}
        errors = {}

    } else if (m.data.pause) {
            pause = true;
            //console.log('got message pausing',globberTimeout)
        if (globberTimeout) {
            //console.log('canceling next run')
            clearTimeout(globberTimeout)
            postMessage({paused:{seen:Object.keys(seen).length,errors:Object.keys(errors).length,newCount,changedCount}})
            globberTimeout = null
        }
        try {
            writeFileSync(CACHEFILE,JSON.stringify(seen))
        } catch (e) {
            console.error(e)
        }

    } else if (m.data.resume){
        pause = false
        globber()
    } else {
        console.error(m.data)
    }
}
