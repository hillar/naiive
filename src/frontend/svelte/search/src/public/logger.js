//if (global.nw && global.nw.require) var require = global.nw.require

  const { join } = require('path')
  const { appendFileSync } = require('fs')

const PREFIX = 'log'
let logDir = process.cwd()
let prefix = PREFIX

// do the first write
const msg = {}
msg.loggerTimestamp = new Date()
msg.msg = 'LOGGER STARTING ...'
const today = msg.loggerTimestamp.toISOString().split('T')[0]
const filename = join(logDir,`${prefix}-${today}.jsonl`)
const str = JSON.stringify(msg)
appendFileSync(filename,str+'\n')


onmessage = function(m) {
    //console.log('logger',m.data)
    if (m.data.logDirectory) {
        logDir = m.data.logDirectory
        if (m.data.prefix) prefix = m.data.prefix
        postMessage(`loggin to ${logDir}`)
        appendFileSync(filename,JSON.stringify({loggerTimestamp:new Date(),'init':m.data})+'\n')

    } else if (logDir) {
        const msg = {}
        msg.loggerTimestamp = new Date()
        msg.msg = m.data
        const today = msg.loggerTimestamp.toISOString().split('T')[0]
        const filename = join(logDir,`${prefix}-${today}.jsonl`)
        const str = JSON.stringify(msg)
        try {
            appendFileSync(filename,str+'\n')
            //console.log('logger',filename,str)
        } catch (e) {
            postMessage({error:e.message})
        }

    } else {
        //noop
    }

}
