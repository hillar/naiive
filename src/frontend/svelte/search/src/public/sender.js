//if (global.nw && global.nw.require) var require = global.nw.require

const { createHash } = require('crypto')
const { ReadStream, writeFileSync, readFileSync, statSync } = require('fs')
const { STATUS_CODES } = require('http')
const { userInfo, hostname,  networkInterfaces} = require('os')
const { sep, basename, dirname, resolve } = require('path')

const API = {"HASH":"", "LINK":"", "UPLOAD":""}

function debug(...args) {/*console.log('DEBUG',__filename, ...args)*/}

let pause = true
function isPause(ms=10) {
    return new Promise(resolve => setTimeout(resolve(pause), ms));
}

const user = userInfo().username
const host = hostname()

const DEFAULTHASHALGORITHM = 'md5'

function stringHash(str,algorithm = DEFAULTHASHALGORITHM){ return createHash(algorithm).update(str).digest("hex") }

function fileHash(filename, algorithm = DEFAULTHASHALGORITHM) {
    return new Promise( (resolve) => {
      let shasum = createHash(algorithm)
      try {
        let s = ReadStream(filename)
        s.on('data', function (data) {
          shasum.update(data)
        })
        s.on('end', function () {
          const hash = shasum.digest('hex')
          resolve(hash)
        })
        s.on('error', function (e) {
            //console.error(e)
            postMessage({error:e.message})
            resolve()

        })
      } catch (e) {
        //console.error(e)
        postMessage({error:e.message})
        resolve()
      }
    })
}


const TRACKERCACHEFILE = 'tracker.json'
const QUEUECACHEFILE = 'queue.json'
let queue = []
let tracker = {}
let errors = {}
let paths
let servers
let rate = -1

function getServer(filename,ps){
    for (const p of ps){
        if (filename.startsWith(p.path+sep)) return p.server
    }
    return
}

function _fetch( ...args ) {
    return new Promise ( async (resolve) => {
        try {
            const result = await fetch( ...args )
            //if (result.status === 507) postMessage({error:{message:'507 Insufficient Storage',from:'server'}})
            if (result.status > 399 && result.status !== 404) postMessage({error:{http:{code:result.status,description:STATUS_CODES[result.status]},url:args[0]}})
            resolve(result)
        } catch (e) {
            console.error(e)
            console.log(...args)
            postMessage({error:{message:e.message,from:'fetch'}})
            resolve({status:0,ok:false,message:e.message})
        }
    })
}

const POLLINTERVAL = 1000 * 10
let checkerTimeout = null
async function checker() {
    if (checkerTimeout) { /*console.log('return on existing next timeout');*/ return}
    let counter = 0
    let progress = -1
    let start = queue.length
    if (Object.keys(errors).length>0) await _fetch(`${paths[0].server}/${API.HASH}/d41d8cd98f00b204e9800998ecf8427e`)
    errors = {}
    //console.log('start loop')
    //postMessage({sending:{counter,start,queue:queue.length,rate}})
    postMessage({sending:{queue:queue.length,errors:Object.keys(errors).length,counter,tracked:Object.keys(tracker).length,start,rate}})
    while (queue.length > 0) {
        const paused = await isPause()
        if (paused) {
            //console.log('break on pause')
            break
        }
        if (Object.keys(errors).length > 0) {
            //console.log('break on errors')
            //console.error(errors)
            break
        }
        const current = queue.shift()
        const contentHash = await fileHash(current)
        if (contentHash) {
            const server = getServer( current, paths )
            const nameHash = stringHash(current)
            const shortNameHash = stringHash(basename(current))
            const form = new FormData()
            form.append('user', user)
            form.append('hostname', host)
            form.append('contentHash', contentHash)
            form.append('shortNameHash',shortNameHash)
            form.append('nameHash', nameHash)
            form.append('fullname', current)
            form.append('basename',basename(current))
            //form.append('tags',dirname(current).split(sep))
            for (const tag of dirname(current).split(sep)) {
              if (tag.trim()) form.append('tag',tag)
            }
            let stats
            let size
            try {
                stats = statSync(current)
                for (const statname of Object.keys(stats)){
                  form.append(statname,stats[statname])
                }
                //form.append('stats',JSON.stringify(stats))
                size = stats.size
            } catch (e) {
                postMessage({error:{message:e.message,from:'json.stringy(stats)'}})
            }
            if (tracker[contentHash]) {
                if (!(tracker[contentHash].includes(nameHash))){
                    const body = JSON.stringify({filename:current,contentHash, shortNameHash,nameHash,user,hostname:host})
                    const dupe = await _fetch(`${server}/${API.LINK}/${contentHash}/${shortNameHash}.json`, {
                            method:  "PUT",
                            body: form
                    })
                    if ( dupe.status === 200) {
                        tracker[contentHash].push(nameHash)
                    } else {
                        // link failed
                        // put i back to try again later
                        errors[`${server}/${API.LINK}/${nameHash}`] = dupe.status
                        setTimeout(function() {
                            queue.push(current)
                        }, POLLINTERVAL*2)
                    }
                }
            } else {
                const content  = await _fetch(`${server}/${API.HASH}/${contentHash}`,{method:'HEAD'})
                if (content.status === 200) {
                        const body = JSON.stringify({filename:current,contentHash, shortNameHash,nameHash,user,hostname:host})
                        const i = await _fetch(`${server}/${API.LINK}/${contentHash}/${shortNameHash}.json`, {
                            method:  "PUT",
                            body: form
                        })

                        if ( i.status === 200) {
                            tracker[contentHash] = []
                            tracker[contentHash].push(nameHash)
                        } else {
                            // link failed
                            // put i back to try again later
                            errors[`${server}/${API.LINK}/${nameHash}`] = i.status
                            //console.error(i)
                            setTimeout(function() {
                                queue.push(current)
                            }, POLLINTERVAL*2)
                        }
                } else if (content.status === 404){
                    // not indexed, upload file
                    console.log('uplading',contentHash,size,current)
                    const file = new File(current,current)
                    form.append('file', file)
                    const started = new Date()
                    const upld = await _fetch(`${server}/${API.UPLOAD}`, {
                        method: "PUT",
                        body: form
                    })
                    const took = new Date() - started
                    console.log('upladed',contentHash,took,current)
                    if (queue.length > start) start = queue.length
                    if (took > 999) rate = (((size*8)/(took/1000))/1024)/1024 // Mb/s
                    if (took > 99)  postMessage({sending:{queue:queue.length,errors:Object.keys(errors).length,counter,tracked:Object.keys(tracker).length,start,rate}})
                    if (upld.status === 200) {
                        tracker[contentHash] = []
                        tracker[contentHash].push(nameHash)
                        // TODO ? save or not to save
                        try {
                            writeFileSync(TRACKERCACHEFILE,JSON.stringify(tracker))
                            writeFileSync(QUEUECACHEFILE,JSON.stringify(queue))
                        } catch (e) {
                            console.error(e)
                        }

                    } else {
                        //upload failed
                        // put it back to try again later
                        // console.error(upld)
                        errors[`${server}/${API.UPLOAD}`] = upld.status
                        setTimeout(function() {
                            queue.push(current)
                        }, POLLINTERVAL*2)
                    }
                } else {
                    // failed to get md5 for content
                    // put i back to try again later
                    //console.error(content)
                    errors[`${server}/${API.HASH}/${contentHash}`] = content
                    setTimeout(function() {
                        queue.push(current)
                    }, POLLINTERVAL*2)
                }

            }
        } else {
            // filed file hashing
            // push it back !?
        }
        counter ++
        if (queue.length > start) start = queue.length
        if ((counter % 33 ) === 0) {
          postMessage({sending:{queue:queue.length,errors:Object.keys(errors).length,counter,tracked:Object.keys(tracker).length,start,rate}})

        }
        if ((counter % 100 ) === 0) {
        // TODO ? save or not to save
        try {
            writeFileSync(TRACKERCACHEFILE,JSON.stringify(tracker))
            writeFileSync(QUEUECACHEFILE,JSON.stringify(queue))
        } catch (e) {
            console.error(e)
        }
        }
    }

    const paused = await isPause()
    if (!paused)  {
        //console.log('set timeout')
        postMessage({running:{queue:queue.length,errors:Object.keys(errors).length,counter,tracked:Object.keys(tracker).length}})
        checkerTimeout = setTimeout(async function() {
            checkerTimeout = null
            if (!pause)  checker()
        }, POLLINTERVAL)
        // TODO ? save or not to save ?
        try {
            writeFileSync(QUEUECACHEFILE,JSON.stringify(queue))
            writeFileSync(TRACKERCACHEFILE,JSON.stringify(tracker))
        } catch (e) {
            console.error(e)
        }
    } else {
        //console.log('pausing')
        postMessage({ paused: {queue:queue.length, tracked:Object.keys(tracker).length} })
    }

}

onmessage = async function(m) {
    if (m.data.init && m.data.init.paths && m.data.init.servers) {

            try {
                tracker = JSON.parse(readFileSync(TRACKERCACHEFILE))
                queue = JSON.parse(readFileSync(QUEUECACHEFILE))
            } catch (e) {
                if (e.code === "ENOENT") {
                    // noop
                } else {
                    console.error(e)
                }
            }

        paths = m.data.init.paths
        servers = m.data.init.servers
        //  map paths to servers
        let defaultServer = ''
        for (const s of servers) {
            if (s.default) defaultServer = s.server
        }
        // set default first
        for (const p of paths) {
            p.server = defaultServer
        }
        if (servers.length > 1) {
            //TODO set nondefault servers
        }

        if (queue.length > 0) {
            pause = false
            // do dummy fetch to get pin prompt window
            const ss = await _fetch(`${paths[0].server}/${API.HASH}/d41d8cd98f00b204e9800998ecf8427e`)
            const ssok = (ss.status === 404)
            //console.log(`${paths[0].server}/${API.HASH}/d41d8cd98f00b204e9800998ecf8427e`,ss.status)
            checker()
        } else {
            postMessage({ paused: {queue:queue.length, tracked:Object.keys(tracker).length} })
        }

    } else if (m.data.stop){
        pause = true
        queue = []
        tracker = {}
        errors = {}
        try {
            writeFileSync(QUEUECACHEFILE,JSON.stringify(queue))
            writeFileSync(TRACKERCACHEFILE,JSON.stringify(tracker))
        } catch (e) {
            console.error(e)
        }

    } else if (m.data.pause) {
        pause = true
        if (checkerTimeout) {
            //console.log('canceling next run')
            clearTimeout(checkerTimeout)
            checkerTimeout = null
        }
        try {
            writeFileSync(QUEUECACHEFILE,JSON.stringify(queue))
            if (queue.length > 0) {
                writeFileSync(TRACKERCACHEFILE,JSON.stringify(tracker))
            } else {
                writeFileSync(TRACKERCACHEFILE,JSON.stringify({}))
            }
        } catch (e) {
            console.error(e)
        }

    } else if (m.data.resume){
        pause = false
        // do dummy fetch to get pin prompt window
        const ss = await _fetch(`${paths[0].server}/${API.HASH}/d41d8cd98f00b204e9800998ecf8427e`)
        const ssok = (ss.status === 404)
        //console.log(`${paths[0].server}/${API.HASH}/d41d8cd98f00b204e9800998ecf8427e`,ss.status)
        checker()

    } else if (m.data.filename) {
        if (!(queue.includes(m.data.filename))) {
            queue.push(m.data.filename)
            // TODO check tracker length to detect first run
            if ((queue.length % 10000) === 0) {
                //console.log({'received':queue.length})
                // TODO check if checker is running or scheduled to run, not pause
                if (pause) {
                    postMessage({ paused: {queue:queue.length, tracked:Object.keys(tracker).length} })
                    pause = false
                    const ss = await _fetch(`${paths[0].server}/${API.HASH}/d41d8cd98f00b204e9800998ecf8427e`)
                    const ssok = (ss.status === 404)
                    //console.log(`${paths[0].server}/${API.HASH}/d41d8cd98f00b204e9800998ecf8427e`,ss.status)
                    checker()
                }
            }

        }
    } else {

        console.error(m.data)
    }
}
