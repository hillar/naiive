import { derived } from 'svelte/store'
import { hardware , localuser, fingerprint  } from './sysinfo.js'

export const remoteuser = derived([hardware,localuser,fingerprint], ([$hardware,$localuser,$fingerprint], set) => {
  if (!$fingerprint) {
      set(undefined)
  } else {
    fetch(window.location.origin+'/user/',{ headers: {
      'Content-Type': 'application/json',
      'ain-client-data': JSON.stringify({hw:$hardware,user:$localuser,fp:$fingerprint})
    }})
    .then(function(response) { return response.json() })
    .then(function(json) {
      set(json)
    });
  }
}, undefined);
