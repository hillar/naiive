import { writable } from 'svelte/store';

let _usersettings

const __usersettings = localStorage.getItem('usersettings')

if (__usersettings)
  try {
    _usersettings = JSON.parse(__usersettings)
  } catch (e) {
    _usersettings = {}
    _usersettings.paths = []
  }
else {
  _usersettings = {}
  _usersettings.paths = []
}

export const usersettings = writable(_usersettings)
