import { readable, derived } from 'svelte/store'
import { usersettings  as usersettingsfromlocalstorage} from './usersettings.js'

let _nodejs
let os
let _platform
let _hostname
let exec

if (window.nw) {
  os = window.nw.require('os')
  _platform = os.platform()
  _hostname = os.hostname()
  exec = nw.require('child_process').exec
  _nodejs = true
} else {
  _platform = (window.navigator && window.navigator.platform || '').toLowerCase();
}
let _localuser
let _remoteuser
let _hardware
let _netuse
let _fingerprint

export const nodejs = () => _nodejs

export const localuser = readable(undefined, function  start(set) {
  if (window.nw) {
  if (_localuser) set(_localuser)
  else {
    _localuser = os.userInfo()
    _localuser.hostname = _hostname
    set(_localuser)
  }}
	return function stop() {};
})

export const hardware = readable(undefined, function  start(set) {
  if (window.nw) {
  if (_hardware) set(_hardware)
  else {
    if (_platform.startsWith('darwin')) {
      exec("system_profiler SPHardwareDataType", (error,output,code) => {
        if (!error && output) {
          _hardware = {}
          const lines = output.split(os.EOL)
          for (const i in lines) {
            const bittes = lines[i].trim().split(':')
            if (bittes[1] && bittes[1].trim()) {
              //_hardware[bittes[0]] = bittes[1].trim()
              if ( bittes[0] === 'Hardware UUID')_hardware['uuid'] = bittes[1].trim()
              if ( bittes[0] === 'Serial Number (system)')_hardware['serial'] = bittes[1].trim()
            }
          }
          set(_hardware)
        } else {
          console.error(error)
          set({error})
        }
      })
    } else if (_platform.startsWith('win')) {
      exec("wmic CSPRODUCT get /format:list", (error,output,code) => {
        if (!error && output) {
          _hardware = {}
          const lines = output.split(os.EOL)
          for (const i in lines) {
            const bittes = lines[i].trim().split('=')
            if (bittes[1] && bittes[1].trim()) {
              //_hardware[bittes[0]] = bittes[1].trim()
              if ( bittes[0] === 'UUID') _hardware['uuid'] = bittes[1].trim()
              if ( bittes[0] === 'IdentifyingNumber') _hardware['serial'] = bittes[1].trim()
            }
          }
          set(_hardware)
        } else {
          console.error(error)
          set({error})
        }
      })
    } else {
      console.error('_platform', _platform, 'not supported (yet)')
      _hardware = {error:'not supported'}
      set(_hardware)
    }
  }
  }
	return function stop() {};
})

export const platform = readable(undefined, function  start(set) {
  set(_platform)
})

export const netuse = readable(undefined, function  start(set) {
    if (window.nw) {
    if (_platform.startsWith('win')) {
      exec("net use", (error,output,code) => {
        if (!error && output) {
          _netuse = []
          const lines = output.split(os.EOL)
          for (const line of lines) {
            const bittes = line.split(':')
            if (bittes[1] && bittes[1].trim()) _netuse.push({local:bittes[0].trim(),remote:bittes[1].trim()})
          }
          set(_netuse)
        } else {
          console.error(error)
          set({error})
        }
      })
    } else {
      console.error('_platform', _platform, 'not supported (yet)')
      _netuse = {error: _platform + ' not supported'}
      set(_netuse)
    }
    }
	return function stop() {};
})

export const fingerprint = readable(undefined, function  start(set) {
  if (_fingerprint) set(_fingerprint)
  else {
    setTimeout(function () {
    Fingerprint2.get((components) => {
      const values = components.map(function (component) {return component.value })
      const murmur = Fingerprint2.x64hash128(values.join(''), 31)
      _fingerprint = murmur
      set(_fingerprint)
    })
  }, 1000)
  }
  return function stop() {};
})

export const usersettings = readable(undefined, function  start(set) {
  const unsubscribeusersettings = usersettingsfromlocalstorage.subscribe(value => {
    set(value)
  });
})
