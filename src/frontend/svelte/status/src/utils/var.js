

//import { sep, resolve } from 'path'
let sep = '/'
let resolve
if (!!window.nw) {
  sep = window.nw.require('path').sep
  resolve = window.nw.require('path').resolve
}

export function cleanPaths(dirs,netuse) {
	    const r = []
	    const n = {}
	    // trim & uniq
	    for (const d of dirs) {
	      if (d.path && !(d.path.trim()==='')){
	        d.path = resolve(d.path)
	        // replace net use
	        if (netuse && !netuse.error && netuse.length){
	          for (const share of netuse){
	            if (d.path.startsWith(share.local+':')){
	              d.path = share.remote + d.path.slice(2)
	            } else if (d.path.startsWith(sep+sep)){
	              // only hostname in path, not fqdn as in netuse ;(
	              const nbittes = share.remote.split(sep)
	              const pbittes = d.path.split(sep)
	              // replace hostname with netuse value
	              if (nbittes[2].startsWith(pbittes[2]) && nbittes[3] == pbittes[3]) {
	                pbittes[2] = nbittes[2]
	                d.path = pbittes.join(sep)
	              }
	            }
	          }
	        }
	        if (!n[d.path]){
	          n[d.path] = d
	        }
	      }
	    }
	    // remove subdirs
	    let last = ''
	    for (const k of Object.keys(n).sort()){
	      if (!(last != '' && k.startsWith(last+sep))) {
	        r.push(n[k])
	        last = k
	      }
	    }

	    return r
	  }

    export async function* readbylinefromurl(fileURL) {
      const utf8Decoder = new TextDecoder("utf-8");
      let response = await fetch(fileURL);
      let reader = response.body.getReader();
      let {value: chunk, done: readerDone} = await reader.read();
      chunk = chunk ? utf8Decoder.decode(chunk) : "";
      let re = /\n|\r|\r\n/gm;
      let startIndex = 0;
      let result;
      for (;;) {
        let result = re.exec(chunk);
        if (!result) {
          if (readerDone) {
            break;
          }
          let remainder = chunk.substr(startIndex);
          ({value: chunk, done: readerDone} = await reader.read());
          chunk = remainder + (chunk ? utf8Decoder.decode(chunk) : "");
          startIndex = re.lastIndex = 0;
          continue;
        }
        yield chunk.substring(startIndex, result.index);
        startIndex = re.lastIndex;
      }
      if (startIndex < chunk.length) {
        // last line didn't end in a newline char
        yield chunk.substr(startIndex);
      }
    }
