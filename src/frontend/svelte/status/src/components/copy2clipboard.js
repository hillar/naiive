import {create as createToast} from '../../../../../../packages/vendor/svelma/src/components/Toast/index.js'

export   async function copy2Clipboard(data,key) {
    const { clipboard } = navigator;
    if (clipboard){
      await clipboard.writeText(data);
    } else {
      const textArea = document.createElement("textarea");
      textArea.value = data;
      document.body.appendChild(textArea);
      textArea.select();
      document.execCommand('copy');
      document.body.removeChild(textArea);
    }
    createToast({ message: key +' copied to clipboard', type:'is-success', position:'is-top' })
  }
