import { createClusterHTTPServer as createServer } from "../../classes/base/clusterserver.mjs";
import { LD as LDSERVERPORT } from "../../constants/ports.mjs"
import { languageize } from "./detectlanguage.mjs";
import { constants } from "buffer";


const MAXLENGTH = Math.round(constants.MAX_LENGTH - 1024 * 1024); // let some 'room' for return json

const LDServer = createServer({
  opts: {
    port: p => { return p ? Number(p) : LDSERVERPORT},
    maxsize: n => (n ? Math.min(n, MAXLENGTH) : MAXLENGTH)
  },
  methods: {
    put: {
      fn: async (req, res, logger) => {
        let contentlength = 0;
        if (req.headers["content-length"]) {
          const size = Number(req.headers["content-length"]);
          if (size === NaN) {
            res.statusCode = 400;
            res.end();
            logger.info({ rejected: "400 content-length not a number" });
          } else if (size > MAXLENGTH) {
            // 413 Payload Too Large
            res.statusCode = 413;
            res.end();
            logger.info({ rejected: "413 Payload Too Large" });
          }
        }
        req.on("error", error => {
          logger.err({ error });
          res.statusCode = 500;
          res.end();
        });
        let chunks = [];
        let size = 0;
        req.on("data", chunk => {
          chunks.push(chunk);
          size += chunk.length;
          if (size > MAXLENGTH) {
            // 413 Payload Too Large
            res.statusCode = 413;
            res.end();
            logger.info({ rejected: "413 Payload Too Large" });
          }
        });
        req.on("end", async () => {
          /*

          (node:60997) UnhandledPromiseRejectionWarning: RangeError [ERR_INVALID_OPT_VALUE]: The value "4053656468" is invalid for option "size"
              at Function.allocUnsafe (buffer.js:346:3)
              at Function.concat (buffer.js:531:25)
              at IncomingMessage.<anonymous> (file:///Users/hillar/aju1/gitlab.com/naiive/src/backend/servers/ld/index.mjs:21:32)

          */
          const start = process.hrtime.bigint();
          const text = Buffer.concat(chunks).toString();
          chunks = undefined;
          const languages = await languageize(text);
          const took = Number(process.hrtime.bigint() - start) / 1e6;

          if (Object.keys(languages).length > 1) {
            /*
              (node:62689) UnhandledPromiseRejectionWarning: RangeError: Invalid string length
    at Array.join (<anonymous>)
    at IncomingMessage.<anonymous> (file:///Users/hillar/aju1/gitlab.com/naiive/src/backend/servers/ld/index.mjs:36:55)

              */
            const undetected = languages.undetected.join(" ").length;
            delete languages.undetected;
            res.end(
              JSON.stringify({
                took,
                lper: undetected / text.length,
                count: Object.keys(languages).length,
                languages
              })
            );
          } else {
            if (languages) {
              res.statusCode = 204;
              res.write(JSON.stringify({ took }));
            } else res.statusCode = 502;
            res.end();
          }
        });
      }
    }
  }
});

LDServer.listen();
