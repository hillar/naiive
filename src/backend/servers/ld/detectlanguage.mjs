import { default as cld } from  'cld'

const END_OF_SEQUENCE = Symbol()

function getNextItem(iterator) {
    let item = iterator.next();
    return item.done ? END_OF_SEQUENCE : item.value;
}

function* paragraphize(text) {
    text = '  ' + text
    let iterator = text[Symbol.iterator]()
    let ch = getNextItem(iterator);
    let last = ch
    let prelast  = ''
    do {
        let paragraph = ''
        do {

          if (typeof last === 'string') paragraph += last !== '\n' ? last : ' ' ;
          prelast = last
          last = ch
          ch = getNextItem(iterator)
          if (ch === '\r') ch = getNextItem(iterator)
        } while (!(last === '\n' && (prelast === '.' || prelast === '!' || prelast === '?'|| prelast === ';'|| prelast === '\n')) && ch !== END_OF_SEQUENCE)
        if (ch === END_OF_SEQUENCE) paragraph += last
        paragraph = paragraph.trim()
        if (paragraph.length) yield paragraph

    } while (ch !== END_OF_SEQUENCE);
}

function* sentenceize(paragraph) {
    paragraph = '   ' + paragraph
    let iterator = paragraph[Symbol.iterator]()
    let ch = getNextItem(iterator);
    let last = ch
    let prelast  = ''
    do {
        let sentence = ''
        do {
          if (typeof last === 'string') sentence += last !== '\n' ? last : ' ' ;
          prelast = last
          last = ch
          ch = getNextItem(iterator)
          if (ch === '\r') ch = getNextItem(iterator)
        } while (!(last === ' ' && (prelast === '.' || prelast === '!' || prelast === '?')) && ch !== END_OF_SEQUENCE)
        if (ch === END_OF_SEQUENCE) sentence += last
        sentence = sentence.trim()
        if (sentence.length) yield sentence

    } while (ch !== END_OF_SEQUENCE);
}





export async function detect(txt){
  return new Promise( resolve => {
    cld.detect(txt, function(err, result) {
      if (err) {
        if (err.message !== 'Failed to identify language') {
          console.error('--- This Should Not Be Happening ---',err)
          console.error(err)
        }
        resolve()
      } else {
        if (result.reliable !== undefined ) {
          if (result.chunks && result.chunks.length) {
            // TODO !? add percent: and score: to each chunk
            resolve(result.chunks)
          } else if (result.languages && result.languages.length) {
            if (result.languages.length === 1) {
              // so we need to construct chunk
              // { name: 'ENGLISH', code: 'en', offset: 0, bytes: 252 }
              result.languages[0].offset = 0
              result.languages[0].bytes =  Buffer.from(txt).length
              resolve(result.languages)
            }
            else {
              // This is pretty common ;(
              // we don not whitch it is nor where each start
              // so we just dont know
              resolve()
            }
          } else {
            console.error('--- This Should Not Be Happening ---')
            console.error('no chunks nor languages')
            console.error(result)
            console.error(txt.substr(0,80))
            console.error('--- end ---')
            resolve()
          }
        } else resolve() // no reliable result
      }
    })
  })
}

export async function languageize(txt) {
  const result = {undetected:[]}
  for (let para of paragraphize(txt)) {
        const para_languages = await detect(para)
        if ( para_languages && para_languages.length) {
          // if it is single language, we are done here
          if (para_languages.length === 1) {
            const language = para_languages[0].name
            if (!result[language]) result[language] = []
            result[language].push(para)
          } else {
            //  For mixed-language input, CLD2 returns the top three languages found
            //  So we break this paragraph into sentences and detect each one
            let foundsome = false
            const undetected = []
            for (let sentence of sentenceize(para)) {
              const sentence_languages = await detect(sentence)
              if ( sentence_languages && sentence_languages.length) {
                // if single language, we are done here
                if (sentence_languages.length === 1) {
                  foundsome = true
                  const language = sentence_languages[0].name
                  if (!result[language]) result[language] = []
                  result[language].push(sentence)
                // multi language sentence
                // tear out piece for each language
                // and at the end push rest to undetected
                } else if (sentence_languages.length > 1){
                  const b = Buffer.from(sentence)
                  for (const found of sentence_languages){
                    if ( found.name && found.bytes){
                      if (!result[found.name]) result[found.name] = []
                      const t = b.slice(found.offset,found.offset+found.bytes).toString()
                      sentence = sentence.replace(t,'')
                      result[found.name].push(t)
                    }
                  }
                  sentence = sentence.trim()
                  if (sentence.length) undetected.push(sentence)
                } else undetected.push(sentence)
              } else undetected.push(sentence)
            }
            // no languages in sentences, but found some in paragraph
            // so we do same, as for multi language sentence before
            // tear out piece for each language
            // and what's left goes to undetected
            if (para_languages.length > 1 && !foundsome) {
              const b = Buffer.from(para)
              for (const found of para_languages){
                if ( found.name && found.bytes){
                  if (!result[found.name]) result[found.name] = []
                  const t = b.slice(found.offset,found.offset+found.bytes).toString()
                  para = para.replace(t,'')
                  result[found.name].push(t)
                }
              }
              para = para.trim()
              if (para.length) result.undetected.push(para)
            // found some, join undetected to one paragraph
            } else if (foundsome) {
              if (undetected.length) result.undetected.push(undetected.join(' '))
            }
          } // end of multi language para
        } else result.undetected.push(para) // no languages
  }
  return result
}
