import {
  HASHFIELD,
  ALLINDEX,
  CONTENTINDEX,
  FILESINDEX,
  METAINDEX,
  ENTITYINDEX,
  MAXPREVIEWSIZE,
  CONTENTFIELD,
  indexName
} from "../../constants/elastic.mjs";
import { isHash, DEFAULTALGORITHM as HASHALGORITHM } from '../../utils/hasher.mjs'

import { PORT as FILESSERVERPORT } from "../../constants/files.mjs";
import { SEARCH as SEARCHSERVERPORT } from "../../constants/ports.mjs";
//import { createSimpleHTTPServer as createServer } from '../../classes/base/simpleserver.mjs'
import { createClusterHTTPServer as createServer } from "../../classes/base/clusterserver.mjs";
import {
  elasticSimpleQuery,
  elasticQuery,
  getDoc,
  elasticMLT,
  elasticTermVector
} from "../../utils/elastic.mjs";
import { parseHostnamePort } from "../../utils/var.mjs";

const myserver = createServer({
  about: async () => {
    // ela = await clusterhealt(myserver.elastics)
    return { elasticsearchservers: myserver.elastics };
  },
  opts: {
    port: p => { return p ? Number(p) : SEARCHSERVERPORT},
    elastics: list => {
      return list && list.split
        ? list.split(",").map(hp => parseHostnamePort(hp, 9200))
        : [{ hostname: "localhost", port: 9200 }];
    },
    prefix: p => {
      return p ? p.toLowerCase() : "*";
    },
    suffix: p => {
      return p ? p.toLowerCase() : "*";
    },
    content: p => {
      return p ? p.toLowerCase() : ALLINDEX;
      //return p ? p.toLowerCase() : CONTENTINDEX;
    },
    field: p => {
      return p ? p : HASHFIELD;
    }, // <- case sensitive !
    contentfield: p => {
      return p ? p : CONTENTFIELD;
    }, // <- case sensitive !
    files: p => {
      return p ? p.toLowerCase() : FILESINDEX;
    },
    entities: p => {
      return p ? p.toLowerCase() : ENTITYINDEX;
    },
    meta: p => {
      return p ? p.toLowerCase() : METAINDEX;
    }
  },
  methods: {
    put: {
        fn: async (req, res, logger) => {
          // TODO
          // a) just proxy to files, poll for indexing done, return MLT & entities
          // b) extract entities ..
          // c) ...
          logger.info({req_url:req.url})
          res.writeHead(501)
          res.end()
        }
    },
    get: {
      fn: async (req, res, logger) => {
        // search -----------------------
        if (req.url.startsWith("/?q=")) {
          const params = req.url.substring(2).split("&");
          const p = {};
          p.start = 0;
          p.size = 3;
          for (const param of params) {
            let kv = param.split("=");
            if (kv[1] && kv[1].trim().length) p[kv[0]] = kv[1].trim();
          }
          let filters = []
          if (p.q.includes(':')) {

            const tagregex = /(?<key>\w+)\:"(?<value>[^"]*)"/g
            const tmp = decodeURIComponent(p.q)
            //console.dir({tmp})
            const realq = tmp.replace(tagregex, '').trim()
            p.q = realq
            //console.dir({realq})
            const _filters = [ ...tmp.matchAll(tagregex)]
            //console.dir({filters})
            /*
            https://www.elastic.co/guide/en/elasticsearch/reference/7.2/query-filter-context.html
            "filter": [
              { "term":  { "status": "published" }},
            */
            for (const filter of _filters) {
              //console.log(filter.groups.key,filter.groups.value)
              const tmp = {}
              tmp['fields.'+filter.groups.key+'.keyword'] = filter.groups.value
              filters.push({"term":tmp})
            }
            //console.dir({filters})
          }
          if (p.q && p.q.length && p.q.length > 0) {
            const contenIndex = indexName(
              myserver.prefix,
              myserver.content,
              myserver.suffix
            );
            const query = decodeURIComponent(p.q);
            const sr = await elasticSimpleQuery(
              query,
              filters,
              contenIndex,
              p.start,
              p.size,
              myserver.elastics,
              logger
            )
            if (sr && sr.hits) {
              for (const srhit of sr.hits) {
                console.log(srhit)
                const filesIndex = indexName(
                  myserver.prefix,
                  myserver.files,
                  myserver.suffix
                );
                // paralellll
                await Promise.all([
                new Promise( async ( resolve) => {
                  console.log(srhit._id,
                  myserver.field,
                  filesIndex)
                  const fr = await elasticQuery(
                                                srhit._id,
                                                myserver.field,
                                                filesIndex,
                                                myserver.elastics,
                                                logger
                                              );
                  if (fr.hits) {
                    console.dir(fr.hits)
                    srhit.files = [];
                    for (const frhit of fr.hits) {
                      // TODO do fix field names
                      const r = frhit._source;
                      srhit.files.push(r);
                    }
                  }
                  resolve()
                }),
                new Promise( async ( resolve) => {
                // TODO params
                  srhit.term_uniq =  await elasticTermVector(srhit._id,
                                                          myserver.contentfield,
                                                          {"max_num_terms": 1024,
                                                          "min_word_length": 4,
                                                          "max_word_length": 34,
                                                          "max_doc_freq": 1,
                                                          "max_term_freq": 1},
                                                          encodeURI(srhit._index),
                                                          myserver.elastics,
                                                          logger)
                 resolve()
                }),
                new Promise( async ( resolve) => {
                  srhit.term_vectors =  await elasticTermVector(srhit._id,
                                                          myserver.contentfield,
                                                          {"max_num_terms": 128,
                                                          "min_word_length": 4,
                                                          "max_word_length": 34,
                                                          "min_doc_freq":2,
                                                          "max_doc_freq": 96},
                                                          encodeURI(srhit._index),
                                                          myserver.elastics,
                                                          logger)
                  resolve()
                })])

              }
              // sometimes elastic reports total > 0 but hits is empty ;(
              if (sr.total.value &&  !sr.hits.length) sr.hits.push({error:'ERROR, contact your admin (found but no hits)'})
              res.write(JSON.stringify(sr));
            } else res.writeHead(503);

          } else {
            res.writeHead(404);
            //res.end();
          }
          /*
          } else if (req.url.startsWith('/?get=')){
            const id = req.url.split('=')[1]
            const sr = await getDoc(id,index,ELASTICSERVER)
            res.write(JSON.stringify(sr))
            res.end()
          */
          // preview ----------------------------------
        } else if (req.url.startsWith("/?preview=")) {
          const params = req.url.substring(2).split("&");
          const p = {};
          p.length = MAXPREVIEWSIZE;
          p.preview = ''
          for (const param of params) {
            let kv = param.split("=");
            if (kv[1] && kv[1].trim().length) p[kv[0]] = kv[1].trim();
          }
          const id = p.preview.trim()
          const length = p.length

          if (!isHash(id,HASHALGORITHM)) {
            res.writeHead(415)
            logger.notice({nothash:{id,HASHALGORITHM}})

          } else {
            const contenIndex = indexName(
              myserver.prefix,
              myserver.content,
              myserver.suffix
            );
            const i = await elasticQuery(
              id,
              "_id",
              contenIndex,
              myserver.elastics,
              logger
            );
            if (i && i.hits && i.hits[0] && i.hits[0]._index) {
              const sr = await getDoc(
                id,
                encodeURIComponent(i.hits[0]._index),
                myserver.elastics,
                logger
              );
              if (sr && sr.content && sr.content.length) {
                if (Array.isArray(sr.content)) {
                  let trimmed = ''
                  for (const chunk of sr.content){
                    trimmed = chunk.replace(new RegExp('(\n){3,}', 'gim') , '\n').substring(0, length) + '\n'
                    if (trimmed.length > length) break
                  }
                res.write( JSON.stringify({ content: trimmed }));
              } else {
                res.write(
                  JSON.stringify({
                    content: sr.content.replace(new RegExp('(\n){3,}', 'gim') , '\n').substring(0, length)
                  })
                );

              }
              } else res.writeHead(404);
            } else res.writeHead(404);
          }
          // files ---------------------------------
        } else if (req.url.startsWith("/?files=")) {
          const id = req.url.split("=")[1];
          const filesIndex = indexName(
            myserver.prefix,
            myserver.files,
            myserver.suffix
          );
          const fr = await elasticQuery(
            id,
            myserver.field,
            filesIndex,
            myserver.elastics,
            logger
          );
          if (fr && fr.hits && fr.hits.length > 0) {
            const fs = [];
            for (const h of fr.hits) fs.push(h._source);
            res.write(JSON.stringify(fs));
          } else res.writeHead(404);
          // meta ---------------------------------
        } else if (req.url.startsWith("/?meta=")) {
          const id = req.url.split("=")[1];
          const metaIndex = indexName(
            myserver.prefix,
            myserver.meta,
            myserver.suffix
          );
          const i = await elasticQuery(
            id,
            "_id",
            metaIndex,
            myserver.elastics,
            logger
          );
          if (i && i.hits && i.hits[0] && i.hits[0]._index) {
            const sr = await getDoc(
              id,
              encodeURIComponent(i.hits[0]._index),
              myserver.elastics,
              logger
            );
            if (sr) {
              res.write(JSON.stringify(sr));
            } else res.writeHead(404);
          } else res.writeHead(404);
          //  entities ------------------------------------------
        } else if (req.url.startsWith("/?entities=")) {
          const id = req.url.split("=")[1];
          const entityIndex = indexName(
            myserver.prefix,
            myserver.entities,
            myserver.suffix
          );
          const fr = await elasticQuery(
            id,
            "_id",
            entityIndex,
            myserver.elastics,
            logger
          );
          if (fr && fr.hits && fr.hits.length > 0) {
            res.write(JSON.stringify(fr.hits[0]._source));
          } else res.writeHead(404);
          // download ---------------------------------
        } else if (req.url.startsWith("/?download=")) {
          const hash = req.url.split("=")[1];
          const filesIndex = indexName(
            myserver.prefix,
            myserver.files,
            myserver.suffix
          );
          const i = await elasticQuery(
            hash,
            myserver.field,
            filesIndex,
            myserver.elastics,
            logger
          );
          if (i && i.hits && i.hits[0] && i.hits[0]._index) {
            let server;
            if (i.hits[0]._source && i.hits[0]._source.server) {
              server = i.hits[0]._source.server;
            } else {
              server = i.hits[0]._index.split("-").pop();
            }
            server = parseHostnamePort(server, FILESSERVERPORT);
            res.write(JSON.stringify({ server, hash }));
          } else res.writeHead(404);

        } else if (req.url.startsWith("/?mlt=")) {
          const params = req.url.substring(2).split("&");
          const p = {};
          p.start = 0;
          p.size = 3;
          for (const param of params) {
            let kv = param.split("=");
            if (kv[1] && kv[1].trim().length) p[kv[0]] = kv[1].trim();
          }
          const contenIndex = indexName(
            myserver.prefix,
            myserver.content,
            myserver.suffix
          );
          const id = p.mlt
          const from = p.start
          const size = p.size
          if (!id) res.writeHead(404)
          else {
            const sr = await elasticMLT(contenIndex,id,from,size,myserver.elastics,logger)
            if (sr && sr.hits) {
              //console.log(sr.hits)
              for (const srhit of sr.hits) {
                const filesIndex = indexName(
                  myserver.prefix,
                  myserver.files,
                  myserver.suffix
                );
                const fr = await elasticQuery(
                  srhit._id,
                  myserver.field,
                  filesIndex,
                  myserver.elastics,
                  logger
                );
                if (fr.hits) {
                  srhit.files = [];
                  for (const frhit of fr.hits) {
                    // TODO do fix field names
                    const r = frhit._source;
                    srhit.files.push(r);
                  }
                }
              }
              res.write(JSON.stringify(sr));
            }
             else res.writeHead(503);
          }

        // not implemented -------------
        } else {
          logger.notice({ error: 501 });
          res.writeHead(501);
        }
        res.end();
      }, // end fn
      ping: async logger => {
        const contenIndex = indexName(
          myserver.prefix,
          myserver.content,
          myserver.suffix
        );
        const sr = await elasticSimpleQuery(
          "",
          [],
          contenIndex,
          0,
          1,
          myserver.elastics,
          logger
        );
        logger.info({ elastic: sr });
        return sr;
      }
    } // end get
  }
});

myserver.listen();
