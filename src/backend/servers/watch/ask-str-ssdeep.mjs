import { request } from 'http'
import { kStringMaxLength as STRINGMAXLENGTH} from 'buffer'
import { Event, LogEvent, ErrorEvent } from '../../classes/base/event-levels.mjs'
import { Ask, StringStream, printProgress } from '../../classes/pipe/dynpipe.mjs'
import { LANGUAGES, SSDEEPFIELDPREFIX } from '../../constants/elastic.mjs'
import { hilbert } from '../../utils/hilbert.mjs'
import { putstr, putfile } from '../../utils/put.mjs'



const PREFIX = SSDEEPFIELDPREFIX

export class STR2SSDeep extends Ask{
  constructor(options){
    if (!options.idField) options.idField = '__contenthash__'
    super(options)
    if (options.fieldname) this.fieldname = options.fieldname
    else throw new Error('missing source fieldname for ssdeep')
  }
  get hashFieldame(){
    return PREFIX+this.fieldname
  }


  _ping(hostname,port,data,self){
    return new Promise( async(resolve,reject)=>{
      const result = await putstr('ping',hostname,port)
      if (result instanceof Event) {
        reject(data)
      } else if (result.answer) {
        resolve(true)
      }
    })
  }

  _ask(hostname,port,data,self) {
    return new Promise( async ( resolve,reject) => {
      //console.log(this.hashFieldame, this.fieldname)
      if (data[this.fieldname]) {
          const result = await putstr(data[this.fieldname],hostname,port)
          if (result.answer) {
            data[this.hashFieldame] = result.answer.ssdeep
            resolve(data)
          } else if (result instanceof ErrorEvent) {
            resolve(result)
          } else if (result instanceof LogEvent) {
            reject(result)
          } else {
            console.dir({badresult:result})
            console.dir(result)
            resolve({})
          }
      } else {
        //throw new Error('no '+this.fieldname)
        resolve(data)
      }
    })
  }
};

export class File2SSDeep extends Ask{
  constructor(options){
    if (!options.fieldname) throw new Error('missing source filename field for ssdeep')
    options.idField = options.fieldname
    super(options)
    this.fieldname = options.fieldname

  }
  get hashFieldame(){
    return PREFIX+'binary'
  }


  _ping(hostname,port,data,self){
    return new Promise( async(resolve,reject)=>{
      const result = await putstr('ping',hostname,port)
      if (result instanceof Event) {
        reject(data)
      } else if (result.answer) {
        resolve(true)
      }
    })
  }

  _ask(hostname,port,data,self) {
    return new Promise( async ( resolve,reject) => {
      //console.log(this.hashFieldame, this.fieldname)
      if (data[this.fieldname]) {
          const result = await putfile(data[this.fieldname],hostname,port)
          if (result.answer) {
            data[this.hashFieldame] = result.answer.ssdeep
            resolve(data)
          } else if (result instanceof ErrorEvent) {
            resolve(result)
          } else if (result instanceof LogEvent) {
            reject(result)
          } else {
            console.dir({badresult:result})
            console.dir(result)
            resolve({})
          }
      } else {
        //throw new Error('no '+this.fieldname)
        resolve(data)
      }
    })
  }
};
