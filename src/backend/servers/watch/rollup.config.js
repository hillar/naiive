import commonjs from '@rollup/plugin-commonjs'
// https://github.com/acornjs/acorn-private-class-elements
// https://github.com/acornjs/acorn-class-fields
import classFields from 'acorn-class-fields'
// https://github.com/acornjs/acorn-static-class-features
import staticFields from 'acorn-static-class-features'

export default [{

    acornInjectPlugins: [
        staticFields, classFields
    ],
    plugins: [
    commonjs({
      extensions: [ '.js' ],
      sourceMap: false,

    })
  ],
  output: {
    file: './dist/index.mjs',
    format: 'esm'
  },
  input: 'index.mjs'

}];
