import { request } from "http"
import { get } from '../../utils/put.mjs '

// deleteme￣content￣dummys-mbp‾7200/_search?_source_excludes=*&size=1024'

function _getDocs(index, server, port, limit = 1024) {
  return new Promise((resolve) => {
    const options = {
      hostname: server,
      port: port,
      path: encodeURI(`/${index}/_search?_source_excludes=*&size=${limit}`),
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        Accept: "application/json",
      }
    };
    const req = request(options, (res) => {
      let data = [];
      res.on("data", (chunk) => {
        try {
          data.push(chunk);
        } catch (e) {
          console.error(e);
          console.error(query);
          resolve({});
        }
      });
      res.on("end", () => {
        let r;

        try {
          r = JSON.parse(Buffer.concat(data).toString());
          const ids = []
          for (const hit of r.hits.hits) {
            ids.push(hit._id)
          }
          resolve(ids);
        } catch (e) {
          resolve({});
        }
      });
    });
    req.on("error", (error) => {
      resolve();
    });
    req.end(`
      {"query": {
    "bool": {
      "filter": [
        {
          "exists": {
            "field": "ssdeep___plines"
          }
        }
      ]
    }
  }}
    `);
  });
}

function _getDoc(id, fields, index, server, port) {
  return new Promise((resolve) => {
    const options = {
      hostname: server,
      port: port,
      path: encodeURI(`/${index}/_doc/${id}/_source?_source_includes=${fields}`),
      method: "GET",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        Accept: "application/json",
      }
    };
    const req = request(options, (res) => {
      let data = [];
      res.on("data", (chunk) => {
        try {
          data.push(chunk);
        } catch (e) {
          console.error(e);
          console.error(query);
          resolve({});
        }
      });
      res.on("end", () => {
        let r;
        try {
          r = JSON.parse(Buffer.concat(data).toString());
          resolve(r);
        } catch (e) {
          resolve({});
        }
      });
    });
    req.on("error", (error) => {
      resolve();
    });
    req.end();
  });
}

export function hash_split(ss) {
  if (!ss) {
    let [chunksize, chunk, double_chunk] = [];
    return { chunksize, chunk, double_chunk };
  }
  let [chunksize, chunk, double_chunk] = ss.split(":");
  chunksize = Number(chunksize);
  return { chunksize, chunk, double_chunk };
}

//function _elasticSimpleQuery(query,index,from,size,hostname,port,logger){
function elasticQuery(q, index, elasticserver, port) {
  //console.log({query,filter,index,from,size,hostname,port})
  return new Promise((resolve) => {
    const qq = q;

    const options = {
      hostname: elasticserver,
      port: port,
      path: encodeURI(`/${index}/_search`),
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        Accept: "application/json",
      },
    };
    const req = request(options, (res) => {
      let data = [];
      res.on("data", (chunk) => {
        try {
          data.push(chunk);
        } catch (e) {
          console.error(e);
          console.error(query);
          resolve({});
        }
      });
      res.on("end", () => {
        let r;
        try {
          r = JSON.parse(Buffer.concat(data).toString());
          //console.dir(r)
          if (r.hits.total.value && !r.hits.hits.length) logger.warning({ r });
          if (r.hits) resolve(r.hits);
          else {
            console.error(r.error);
            resolve({});
          }
        } catch (e) {
          resolve({});
        }
      });
    });
    req.on("error", (error) => {
      resolve(false);
    });
    //console.log(q)
    req.write(q);
    req.end();
  });
}

export function findSimilar(field, id, index, elasticserver, port, ssdeepserver,ssdeepport) {
  return new Promise(async (resolve) => {
    const result = [];
    const similar = []
    //const field = '__plines'

    const ssdeep = await _getDoc(id, `ssdeep_${field}`, index, elasticserver, port);
    //console.dir(ssdeep)
    //console.dir({k:field,v:ssdeep[`ssdeep_${field}`]})
    if (ssdeep) {
      let { chunksize, chunk, double_chunk } = hash_split(ssdeep[`ssdeep_${field}`]);
      //console.dir({chunksize, chunk, double_chunk})
      // chunksize___snake ssdeep___snake.keyword
      // chunksize_content ssdeep_content
      const q = `{
        "query": {
          "bool": {
            "must": [
              {
                "terms": {
                  "chunksize_${field}": [${chunksize}, ${chunksize * 2}, ${Math.round(chunksize / 2)}]
                }
              },
              {
                "bool": {
                  "should": [
                    {
                      "match": {
                        "chunk_${field}": {
                          "query": "${chunk}"
                        }
                      }
                    },
                    {
                      "match": {
                        "double_chunk_${field}": {
                          "query": "${double_chunk}"
                        }
                      }
                    }
                  ],
                  "minimum_should_match": 1
                }
              }
            ]
          }
        }
      }`;

      const results = await elasticQuery(q, index, elasticserver, port);
      //console.dir(results)
      if (results.hits) {
        for (const hit of results.hits) {
          //console.dir(hit)
          result.push({ id: hit._id, score: hit._score, ssdeep:hit._source[`ssdeep_${field}`] });
        }
        result.shift()
        for (const maybe of result){
          //console.dir(`/${ssdeep.ssdeep_snake}&${maybe.ssdeep}`)
          //console.dir({i1:r.id,i2:maybe.basename,h1:r.ssdeep,h2:maybe.ssdeep})
          const score = maybe.score
          const simi = await get(ssdeepserver,ssdeepport,`/${ssdeep['ssdeep_'+field]}&${maybe.ssdeep}`)
          //console.dir({s:simi.answer.similarity,score})
          //console.log(simi.answer.similarity,score)
          if (simi.answer.similarity) similar.push({o:id,s:simi.answer.similarity,...maybe})
        }
      }
    } else {
      //debug("no ssdeep", id, index);
    }
    if (similar.length) resolve({id,ssdeep:ssdeep.ssdeep,similar});
    else resolve(null)
  });
}

export function printProgress(...progress) {
  if (process.stdout.isTTY) {
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(progress.join(" "));
  } else console.debug(progress.join(" "));
}

(async function () {
  const index = "kustutamind-ssdeep￣content￣pauk5-watch‾7200";
  const elasticserver = "localhost";
  const eport = 9200;
  const ssdeepserver = 'localhost'
  const sport = 9997
  let ids = await _getDocs(index,elasticserver,eport,10000)
  console.dir(ids.length)
  //ids = ['006f1e06b70615150f431fdd3c345847']
  //const ids = ['018697ad65588671c2bdd7b3ec2bdef3','04bf0952f710c774bd9c759b11998416']
  for (const id of ids) {
  try {

    //const id = "04bf0952f710c774bd9c759b11998416";

    const r = await findSimilar('__snake',id, index, elasticserver, eport, ssdeepserver,sport);
    if (r) {
      //console.dir(r)

      for (const maybe of r.similar){

        //if (maybe.s < 100)
        console.log(maybe.s,maybe.o,maybe.id)
      }

    }
    const rr = await findSimilar('__plines',id, index, elasticserver, eport, ssdeepserver,sport);
    console.log('')
    if (rr) {
      //console.dir(r)

      for (const maybe of rr.similar){

        //if (maybe.s < 100)
        console.log(maybe.s,maybe.o,maybe.id)
      }

    }
    console.log('----')
  } catch (e) {
    console.dir({ e });
  }
}
})();
