import { hostname, networkInterfaces } from 'os'
import { request, globalAgent, STATUS_CODES } from 'http'
import { createHash } from 'crypto'
import { putstr,poststr } from '../../utils/put.mjs'
import { Ask, wait } from '../../classes/pipe/dynpipe.mjs'
import { Event, LogEvent, ErrorEvent, Notice, Warn } from '../../classes/base/event-levels.mjs'
import { IDFIELD, HASHFIELD, LANGUAGES as SUPPORTEDLANGS, SSDEEPFIELDPREFIX } from '../../constants/elastic.mjs'


const LANGUAGES = [...SUPPORTEDLANGS]
export const NUMBEROFREPLICAS = 2

//globalAgent.keepAlive = true

// TODO move to separate module
const DEFAULTALGORITHM = 'md5'
function digestString(str, algorithm = DEFAULTALGORITHM, encoding = 'hex') {
  if (!str) str = Math.random()+Date.now() // TODO better way to survive random disk reading errors
  return createHash(algorithm).update(str).digest(encoding)
}


/*

curl -X POST "localhost:9200/_bulk?pretty" -H 'Content-Type: application/json' -d'
{ "index" : { "_index" : "test", "_id" : "1" } }
{ "field1" : "value1" }
{ "delete" : { "_index" : "test", "_id" : "2" } }
{ "create" : { "_index" : "test", "_id" : "3" } }
{ "field1" : "value3" }
{ "update" : {"_id" : "1", "_index" : "test"} }
{ "doc" : {"field2" : "value2"} }
'


POST /<index>/_bulk

The index and create actions expect a source on the next line, and have the same semantics as the op_type parameter in the standard index API: create fails if a document with the same name already exists in the index, index adds or replaces a document as necessary.

Response body

took
(integer) How long, in milliseconds, it took to process the bulk request.
errors
(boolean) If true, one or more of the operations in the bulk request did not complete successfully.
items
(array of objects) Contains the result of each operation in the bulk request, in the order they were submitted.


!!!

When using the HTTP API, make sure that the client does not send HTTP chunks, as this will slow things down.


-------

Sending a 'Content-Length' header will disable the default chunked encoding.


If I remember correctly Node.js behaviour is:

Use chunked transfer encoding by default if res.write() is used.
Do not use chunked transfer encoding by default if only res.end() is used.

*/

const CHUNKSIZE = 1024 * 1024 * 99 // elastics accepts 100Mb

function _bulk2Elastic(id,arr,index,hostname, port){
  const path = `/${index}/_bulk`
  return new Promise ( async (resolve, reject) => {
    let chunk = ''
    let counter = 0
    for (const item of arr) {
      const tmp = {}
      tmp[IDFIELD] = id
      const current = '{"index":{}}\n'+JSON.stringify({...tmp,...item})+'\n'
      const length = chunk.length + current.length
      if (length > CHUNKSIZE) {
        const result = await putstr(chunk,hostname,port,path)
        if (result.answer) {
          if (result.answer.errors && result.answer.errors.length > counter/2){
            reject(new Notice('more than half are errors',{count:counter,errors:result.answer.errors}))
            return
          } else {
             resolve(true)
          }
        } else if (result instanceof ErrorEvent) {
          resolve(result) // do not try again, if error
        } else if (result instanceof LogEvent) {
          reject(result) // try again else {
        } else {
          console.dir({badresult:result})
          resolve({})
        }
          chunk = current
          counter = 1

      } else {
        counter ++
        chunk += current
      }
    }
    // put leftovers
    if (chunk.length) {
      const result = await putstr(chunk,hostname,port,path)
      if (result.answer) {
        if (result.answer.errors && result.answer.errors.length > counter/2){
          console.dir(result.answer)
          reject(new Notice('more than half are errors',{count:counter,errors:result.answer.errors}))
          return
        } else {
           resolve(true)
        }
      } else if (result instanceof ErrorEvent) {
        resolve(result) // do not try again, if error
      } else if (result instanceof LogEvent) {
        reject(result) // try again else {
      } else {
        console.dir({badresult:result})
        resolve({})
      }
    } else {
      resolve(true)
    }


  })
}

function _put2Elastic(doc,path,hostname, port){

  return new Promise ( (resolve, reject) => {
    const options = {
      hostname: hostname,
      port: port,
      path: `${path}`,
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
    }
    try {
      const req = request(options, (res) => {
        let data = []
        res.on('data', (chunk) => {
            data.push(chunk)
        })
        res.on('end',() => {
          let r
          try {
            r = JSON.parse(Buffer.concat(data).toString())

            if (res.statusCode > 299) {
              const error = new Error(hostname+':'+port)
              error.elasticsearch = r
              error.elasticsearch.path = path
              error.elasticsearch.doc = doc
              reject(error)
            } else resolve(r)
          } catch (error) {

            error.badresponse = Buffer.concat(data).toString()
            error.docsize = doc.length
            error.dochead = doc.substring(0,1024)
            error.doctail = doc.substring(-1024)
            if (!error.badresponse.length) {
              resolve(new Warn(error))
            } else {
              reject(new Notice(error))
            }

          }
        })
      })
      req.on('error', (error) => {
        reject(error)
      })
      req.write(doc,'utf-8')
      req.end()
    } catch (error) {
      reject(error)
    }
  })
}

function putDoc2Elastic(doc,id,index,hostname,port){
    const path = `/${index}/_doc/${id}`
    return _put2Elastic(doc,path,hostname,port)
}

function _existElastic(path,hostname,port){
  return new Promise( (resolve, reject) => {
    const options = {
      hostname: hostname,
      port: port,
      path: path,
      method: 'HEAD'
    }
    const req = request(options, (res) => {
      resolve( (res.statusCode === 200) )
    })
    req.on('error', (error) => {
      reject(new Notice(error))
    })
    req.end()
  })
}

function docExist(id,index,hostname,port){
    const path =  `/${index}/_doc/${id}`
    return _existElastic(path,hostname,port)
}

function indexExist(index,hostname,port){
    const path =  `/${index}`
    return _existElastic(path,hostname,port)
}


export function add2Elastic(obj,id,index,hostname,port){
  return new Promise( async (resolve, reject) => {
    const doc = JSON.stringify(obj)
    const _id = id || digestString(doc)
    try {
      const exists = await docExist(_id,index,hostname,port)
      if (exists) resolve(_id)
      else {
        try {
          const r = await putDoc2Elastic(doc,_id,index,hostname,port)
          resolve(_id)
        } catch (error) {
          console.error(obj.basename)
          console.error(error)
          reject(error)
        }
      }
    } catch (error) {
      reject(error)
    }
  })
}




export class save2Elastic extends Ask{
  constructor(options){
    options.tti = options.tti || 20
    if (!options.idField) options.idField = IDFIELD //TODO param
    super(options)
    this._askState.ready = false
    this.cluster = options.cluster
    if (!this.cluster) throw new Error('no cluster')
    this.index = options.index
    if (!this.index) throw new Error('no index')

    this.settings = options.settings || {settings:{"index.mapping.ignore_malformed": true,"number_of_replicas":NUMBEROFREPLICAS, "index.refresh_interval": "10s"}}

  }

  _ping(hostname,port,data,self){
    return new Promise( async(resolve,reject)=>{
      const options = {
        hostname: hostname,
        port: port,
        path: '/_cat/health',
        method: 'GET',
        headers: {
          'Accept': 'application/json'
        }
      }
      const req = request(options, (res) => {
        const body = []
        res.on('data', (chunk) => {body.push(chunk)})
        res.on('end', async () => {
          let answer
          try {
            answer = JSON.parse(Buffer.concat(body).toString())
          } catch (error) {
            reject(error)
          }
          if (answer && answer[0] && answer[0].status && answer[0].status !== 'red' ) {
            if (answer[0].cluster !== this.cluster) this.destroy(new Error('wrong cluster, expected '+this.cluster + ' got '+answer[0].cluster))
            else {
              try {
                const ie = await indexExist(this.index,hostname,port)
                if (!ie){
                  try {
                    const a = await _put2Elastic(JSON.stringify(this.settings),this.index,hostname,port)

                    resolve(true)
                  } catch (error) {
                    console.dir(error)
                    if (error.elasticsearch.error.type === 'resource_already_exists_exception') resolve(true)
                    else reject(new Warn(error))
                  }
                } else resolve(true)
              } catch (error) {
                reject(new Notice(error))
              }
            }
          } else {
            const error = new Error(hostname+':'+port)
            error.elasticsearch = answer
            reject(new Notice(error))
          }
        })
      })
      req.on('error', (error) => {
        reject(new Notice(error))
      })
      req.end()
    })
  }
  _ask(hostname,port,data,self){
    if (!this._elaask) {
      console.error(this.name + ": _elaask not implemented");
      process.exit(-1);
    }
    return new Promise( async (resolve,reject) => {
      try {
        const answer = await this._elaask(hostname,port,data,self)
        resolve(answer)
      } catch (error) {
        //console.error('should not happen')
        //console.error(error)
        reject(error)
      }
    })
  }
}




export class saveEntities extends save2Elastic{
  constructor(options){
    options.settings = options.settings || {settings: {
            "index.mapping.ignore_malformed": true,
            "number_of_replicas":NUMBEROFREPLICAS,
            "index.refresh_interval":"10s"
          },

          "mappings": {
              "dynamic": true,
              "dynamic_templates": [
                {
                  "ip": {
                    "match": "ipv*",
                    "match_mapping_type": "string",
                    "mapping": {
                      "type": "ip"
                    }
                  }
                }
              ]
            }

        }
    super(options)
    this.entitiesfield = options.field || '__entities'
    //this.idField = options.id || '__contenthash__'
  }
  _elaask(hostname,port,data,self){
    return new Promise( async ( resolve,reject) => {
      if (!data[this.entitiesfield]) resolve(data)
      else {
        try {

           await add2Elastic(data[this.entitiesfield],data[this.idField],this.index,hostname,port)
           //delete data[this.entitiesfield]
          resolve(data)
        } catch (error) {

          reject(error)
        }
      }
    })
  }
}


export class saveLine extends save2Elastic{
  constructor(options){
    options.settings = options.settings || {settings: {
            "index.mapping.ignore_malformed": true,
            "number_of_replicas":NUMBEROFREPLICAS,
            "index.refresh_interval":"10s"
          },

          "mappings": {
              "dynamic": true,
              "dynamic_templates": [
                {
                  "ip": {
                    "match": "ipv*",
                    "match_mapping_type": "string",
                    "mapping": {
                      "type": "ip"
                    }
                  }
                },
                {
                  "ip": {
                    "match": "IPV*",
                    "match_mapping_type": "string",
                    "mapping": {
                      "type": "ip"
                    }
                  }
                }
              ]
            }

        }

    super(options)
    this.linefield = options.field || '__line'
    //this.idField = options.id || '__contenthash__'
  }
  _elaask(hostname,port,data,self){
    return new Promise( async ( resolve,reject) => {

      if (!data[this.linefield]) resolve(data)
      else {
          try {
            const result = await _bulk2Elastic(data[this.idField],data[this.linefield],this.index,hostname,port)
            delete data[this.linefield]
            resolve(data)
          } catch (error) {
            reject(error) // try again
          }
      }
    })
  }
}

export class saveTimeline extends save2Elastic{
  constructor(options){
    options.settings = options.settings || {settings: {
            "index.mapping.ignore_malformed": true,
            "number_of_replicas":NUMBEROFREPLICAS,
            "index.refresh_interval":"10s"
          },

          "mappings": {
              "dynamic": true,
              "dynamic_templates": [
                {
                  "ip": {
                    "match": "ipv*",
                    "match_mapping_type": "string",
                    "mapping": {
                      "type": "ip"
                    }
                  }
                },
                {
                  "ip": {
                    "match": "IPV*",
                    "match_mapping_type": "string",
                    "mapping": {
                      "type": "ip"
                    }
                  }
                }
              ]
            }

        }

    super(options)
    this.timelinefield = options.field || '__timeline'
    //this.idField = options.id || '__contenthash__'
  }
  _elaask(hostname,port,data,self){
    return new Promise( async ( resolve,reject) => {
      //console.dir(data)
      if (!data[this.timelinefield]) resolve(data)
      else {
          try {
            const result = await _bulk2Elastic(data[this.idField],data[this.timelinefield],this.index,hostname,port)
            delete data[this.timelinefield]
            resolve(data)
          } catch (error) {
            reject(error) // try again
          }


        /*
        try {
            const tmp = {}
            tmp[this.idField] = data[this.idField]

            for (const i of data[this.timelinefield]) {
              await add2Elastic({...tmp, ...i},null,this.index,hostname,port)
          }
          delete data[this.timelinefield]
          resolve(data)
        } catch (error) {
          console.error(error)
          reject(error)
        }
        */

      }
    })
  }
}


// just strip away tika field names
function o2t(o){
  let t = ''
  for (const k of Object.keys(o)) {
    if (o[k].length > 2 && !o[k].toString().startsWith('org.apache.tika.') && !t.includes(o[k].toString()))
    t+=o[k].toString()+'\n'
  }
  return t
}

export class saveMeta extends save2Elastic{
  constructor(options){
    options.settings = options.settings || {settings: {
            "index.mapping.ignore_malformed": true,
            "number_of_replicas":NUMBEROFREPLICAS,
            "index.refresh_interval":"10s"
          },
          "mappings": {
            "properties": {
                "location": {
                  "type": "geo_point"
                }
              }
            }
        }

    super(options)
    this.datetimefields = ['modified','created','last-modified','last-printed','creation-date','last-save-date','date','save-date','metadatadate','date-stamp-mode','date-time','date-time-digitized','date-time-original','file-modified-date','datetimeoriginal','print-date','profile-date-time']
    this.metafields = [
      'md5',
      'basename',
      'long','lat','gps-altitude','gps-altitude-ref','gps-date-stamp','gps-latitude','gps-latitude-ref','gps-longitude','gps-longitude-ref','gps-time-stamp','gps-version-id',
      'content-type','resourcename',
      'author','last-author', 'subject','description',
      'message-from','from-name','message-to','to-email','to','from','to-name','x-originating-ip','message-cc'
    ]
  }
  _elaask(hostname,port,data,self){
    return new Promise( async ( resolve,reject) => {

      const _data = {}
      const carryon = Object.keys(data).filter(i => i.startsWith('__'))
      for (const key of carryon){
        _data[key] = data[key]
        if (key !== this.idField) delete data[key]
      }
      _data.content = data.content
      delete data.content

    const id = {}
    id[this.idField] = data[this.idField]
    const meta = {...id,json:JSON.stringify(data),all:o2t(data)}


    for (const field of this.metafields){
      if (data[field]) meta[field] = data[field]
    }
    for (const field of this.datetimefields){
      if (data[field]) {
        if (!Array.isArray(data[field])) data[field] = [data[field]]
        const okDates = []
        for (const test of data[field]) {
          const tested = new Date(test)
          if (!isNaN(tested)) okDates.push(tested)
          else {
            // TODO try harder
            // D:20200318111329+02'00'
            //console.log('bad date', field,test, id)
          }
        }
        if (okDates.length) meta[field] = okDates
      }
    }
    // fix iPhone bad Date
    if (meta['gps-date-stamp']) meta['gps-date-stamp'] = meta['gps-date-stamp'].replace(/\:/g,'/')
    // lat, long
    if (meta.long && meta.lat) {
      meta.location = {lat:Number(meta.lat),lon:Number(meta.long)}
      if (meta['gps-date-stamp'] && meta['gps-time-stamp']){
          meta.location_timestamp = new Date(meta['gps-date-stamp'] +' '+ meta['gps-time-stamp'])
      }
    }

    try {
      meta.id = await add2Elastic(meta,data[this.idField],this.index,hostname,port)
      _data.__meta = meta
      resolve(_data)
    } catch (error) {
      console.error(error)
      reject(error)
    }
    })
  }
}





// TODO TODO chicken egg savelang checkExists

export class checkExists extends save2Elastic{
  constructor(options){

    options.settings = {
        "settings": {
                "index.mapping.ignore_malformed": true,
                "number_of_replicas":NUMBEROFREPLICAS,
                "index.refresh_interval": "10s",
                //"index.highlight.max_analyzed_offset": 10000000,
                "analysis": {
                  "filter": {
                    "length_3_to_32_char": {
                      "type": "length",
                      "min": 3,
                      "max": 32
                    }
                  },
                  "analyzer": {
                    "standard_lowercase_asciifolding": {
              			     "tokenizer": "standard",
                         //  TODO add more langs to stopwords filter
                  			"filter": ["lowercase", "asciifolding","stop", "length_3_to_32_char"]
              			},
                    "ssdeep_analyzer": {
                      "tokenizer": "ssdeep_tokenizer"
                    },
                    "fulltext_analyzer": {
                      "type": "custom",
                      "tokenizer": "standard",
                      "filter": [
                        "lowercase",
                        "type_as_payload"
                      ]
                    }
                  },
                  "tokenizer": {
                    "ssdeep_tokenizer": {
                      "type": "ngram",
                      "min_gram": 7,
                      "max_gram": 7
                    }
                  }
                }
              },
              "mappings": {
                  "dynamic": true,
                  "dynamic_templates": [ ],
                  "properties": {

                    "content": {
                      "type": "text",
                      "term_vector": "with_positions_offsets_payloads",
                      "store" : true,
                      "analyzer" : "fulltext_analyzer",
                      "index_options": "positions",
                      "index_phrases": true,
                      "fields": {
                        "keyword": {
                          "store":false,
                          "type": "text",
      			              "analyzer": "standard_lowercase_asciifolding",
      			              "fielddata": true,
                        }
                      }
                    },

                    "__all__": {
                      "type": "text",
                      //"term_vector": "with_positions_offsets_payloads",
                      "term_vector": "yes",
                      "store" : false,
                      "analyzer" : "fulltext_analyzer",
                      "fields": {
                        "keyword": {
                          "store":false,
                          "type": "text",
      			              "analyzer": "standard_lowercase_asciifolding",
      			              "fielddata": true,
                        }
                      }
                    },
                    "__meta.all":{
                      "copy_to": "__all__",
                      "type": "text",
                      "index": false
                    },
                    "__meta.json":{
                      "type":"object",
                      "enabled":false
                    }
                  }
                }
            }

            //options.settings.mappings.dynamic_templates = []
            for (const language of LANGUAGES) {
              const mapping = {}
              mapping[language] = {
                            "path_match" : "*-"+language,
                            "match_mapping_type": "string",
                            "mapping": {
                              "type": "text",
                              "store": true,
                              "analyzer" : language.toLowerCase(),
                              "term_vector": "with_positions_offsets_payloads",
                              "index_options": "positions",
                              "index_phrases": true
                            }
                          }
              options.settings.mappings.dynamic_templates.push(mapping)
            }

            options.settings.mappings.dynamic_templates.push({'unknown':{
                          "path_match" : "*-unknown",
                          "match_mapping_type": "string",
                          "mapping": {
                            "type": "text",
                            "term_vector": "with_positions_offsets_payloads",
                            "store" : true,
                            "analyzer" : "fulltext_analyzer",
                            "index_options": "positions",
                            "index_phrases": true,
                          }
                        }})

            options.settings.mappings.dynamic_templates.push({"chunk_": {
              "match": "chunk_*",
              "mapping": {
                "type": "text",
                "store" : false,
                "analyzer": "ssdeep_analyzer",
              }
            }})
            options.settings.mappings.dynamic_templates.push({"double_chunk_": {
              "match": "double_chunk_*",
              "mapping": {
                "store" : false,
                "type": "text",
                "analyzer": "ssdeep_analyzer"
              }
            }})
            options.settings.mappings.dynamic_templates.push({"__all__": {
              "match": ["__fields.*","__entities.*"],
              "mapping": {
                "type": "text",
                "copy_to": "__all__",
                "index": false
              }
            }})


    super(options)
    this.langsfield = options.field || '__languages'
    //this.idField = options.id || '__contenthash__'


  }
  // TODO   HASHFIELD param !!

  _elaask(hostname,port,data,self){
    return new Promise( async ( resolve,reject) => {
      const { basename, pathname} = data
      let exists = false
        //try {
          //const exists = await docExist(basename,this.index,hostname,port)
          const query =  `{
                        "query": {
                            "bool": {
                                "must": {
                                    "term": {"${HASHFIELD}": "${basename}"}
                            }
                        }
                      },
                      "size": 0
                    }`
          const response = await poststr(query,hostname,port,'/'+this.index+'/_search')

          exists = response?.answer?.hits?.total?.value

          if (!exists) {
            if (response instanceof Event) reject(response)
            else resolve(data)
          } else resolve({})
        //} catch (error) {
          //reject(error)
        //}

    })
  }
}


export class saveLangs extends checkExists{
  constructor(options){
    super(options)
    /*
    options.settings =  {
      "settings": {
                "index.mapping.ignore_malformed": true,
                "number_of_replicas":NUMBEROFREPLICAS,
                "index.refresh_interval": "10s",
                //"index.highlight.max_analyzed_offset": 10000000,
                "analysis": {
                  "filter": {
                    "length_3_to_32_char": {
                      "type": "length",
                      "min": 3,
                      "max": 32
                    }
                  },
                  "analyzer": {
                    "standard_lowercase_asciifolding": {
              			     "tokenizer": "standard",
                         //  TODO add more langs to stopwords filter
                  			"filter": ["lowercase", "asciifolding","stop", "length_3_to_32_char"]
              			},
                    "ssdeep_analyzer": {
                      "tokenizer": "ssdeep_tokenizer"
                    },
                    "fulltext_analyzer": {
                      "type": "custom",
                      "tokenizer": "standard",
                      "filter": [
                        "lowercase",
                        "type_as_payload"
                      ]
                    }
                  },
                  "tokenizer": {
                    "ssdeep_tokenizer": {
                      "type": "ngram",
                      "min_gram": 7,
                      "max_gram": 7
                    }
                  }
                }
              },
              "mappings": {
                  "dynamic": true,
                  "dynamic_templates": [ ],
                  "properties": {

                    "content": {
                      "type": "text",
                      "term_vector": "with_positions_offsets_payloads",
                      "store" : false,
                      "analyzer" : "fulltext_analyzer",
                      "index_options": "positions",
                      "index_phrases": true,
                    },

                    "__all__": {
                      "type": "text",
                      //"term_vector": "with_positions_offsets_payloads",
                      "term_vector": "yes",
                      "store" : false,
                      "analyzer" : "fulltext_analyzer",
                      "fields": {
                        "keyword": {
                          "store":false,
                          "type": "text",
      			              "analyzer": "standard_lowercase_asciifolding",
      			              "fielddata": true,
                        }
                      }
                    },
                    "__meta.all":{
                      "copy_to": "__all__",
                      "type": "text",
                      "analyzer" : "fulltext_analyzer",
                      "term_vector": "with_positions_offsets_payloads",
                      "store" : false,
                      "index_options": "positions",
                      "index_phrases": true
                    }
                  }
                }
            }


            options.settings.mappings.dynamic_templates = []
            for (const language of LANGUAGES) {
              const mapping = {}
              mapping['content-'+language] = {
                            //"match_pattern": "regex",
                            //"match": '__languages.content-'+language,
                            "path_match" : "*.content-"+language,
                            "match_mapping_type": "string",
                            "mapping": {
                              "type": "text",
                              "analyzer" : language.toLowerCase(),
                              "term_vector": "with_positions_offsets_payloads",
                              "store" : false,
                              "index_options": "positions",
                              "index_phrases": true
                            }
                          }
              options.settings.mappings.dynamic_templates.push(mapping)
            }
            const mapping = {}
            mapping['content-unknown'] = {
                          //"match_pattern": "regex",
                          //"match": '__languages.content-'+language,
                          "path_match" : "*.content-unknown",
                          "match_mapping_type": "string",
                          "mapping": {
                            "type": "text",
                            "term_vector": "with_positions_offsets_payloads",
                            "store" : false,
                            "analyzer" : "fulltext_analyzer",
                            "index_options": "positions",
                            "index_phrases": true,
                          }
                        }
            mapping['content-all'] = {
              "match_mapping_type": "string",
                "mapping": {
                  "type": "text",
                  "copy_to": "__all__"
                }
              }

            options.settings.mappings.dynamic_templates.push(mapping)


    super(options)
    this.langsfield = options.field || '__languages'
    //this.idField = options.id || '__contenthash__'
    */
  }
  _elaask(hostname,port,data,self){
    return new Promise( async ( resolve,reject) => {
      if (!data[this.idField]) resolve(data)
      else {

        try {

          //let id = null
            let ssdeep = {}
            //if (data.__embedded_no === '0') id = data[this.idField]
            if (data.ssdeep) {
              let [chunksize, chunk, double_chunk] = data.ssdeep.split(':')
              ssdeep = {chunksize, chunk, double_chunk, ssdeep:data.ssdeep}
            }
            const ssdeeps = {}
            for (const key of Object.keys(data).filter( i => i.startsWith(SSDEEPFIELDPREFIX))){
              const name = key.replace(SSDEEPFIELDPREFIX,'')

              let [chunksize, chunk, double_chunk] = data[key].split(':')
              ssdeeps['ssdeep_'+name] = data[key]
              ssdeeps['chunksize_'+name] = chunksize
              ssdeeps['chunk_'+name] = chunk
              ssdeeps['double_chunk_'+name] = double_chunk
            }
            //const doc = {basename, ...ssdeep, ...ssdeeps,...data[this.langsfield], 'content-raw':data.content}
            if (data.content) { //data.content = '' // TODO
              const sizes = { content: data.content.length}
              if (data.__languages) for (const key of Object.keys(data.__languages)) {
                sizes[key] = data.__languages[key].join('\n').length
              }
              if (sizes.content > 1000000) {
                // ? TODO split here
              }

              const doc = {...data, ...ssdeeps, sizes}

              const kala = await add2Elastic(doc, data[this.idField], this.index, hostname, port)
              delete data[this.langsfield]
            } else console.dir({nocontent:data})

            resolve(data)
        } catch (error) {
          console.error(error)
          reject(error)
        }
      }
    })
  }
}


export class checkFields extends save2Elastic{
  constructor(options){
    super(options)

  }
  _elaask(hostname,port,data,self){
    return new Promise( async ( resolve,reject) => {
      const { basename, pathname, __fields } = data
      for (const f of __fields){
        // TODO delete old versions
        const id = f.__fieldscontenthash__
        try {
          const exists = await docExist(id,this.index,hostname,port)
          if (!exists) {
            try {
              await putDoc2Elastic(JSON.stringify(f),id,this.index,hostname,port)
            } catch (error) {
              error.id = data[this.idField]
              error.name = 'checkFields.save'
              resolve(new Warn(error))
            }
          }
        } catch (error) {
          error.id = data[this.idField]
          error.name = 'checkFields.exists'
          reject(new Notice(error))
        }
      }
      resolve(data)
    })
  }
}
