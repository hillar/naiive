
import { kStringMaxLength as STRINGMAXLENGTH} from 'buffer'
import { IDFIELD, LANGUAGES } from '../../constants/elastic.mjs'
import { Event, LogEvent, ErrorEvent } from '../../classes/base/event-levels.mjs'
import { Ask, printProgress } from '../../classes/pipe/dynpipe.mjs'
import { putstr,putfile } from '../../utils/put.mjs'

const APIPATH = '/'
const REQUESTTIMEOUT = 120 * 1000

export function chunkString(str, length=9000) {
  // /(.|[\r\n]){1,n}/g
  return str.match(new RegExp('(.|[\r\n]){1,'+length+'}', 'g'));
}

export class EntityReqognizer extends Ask{

  constructor(options){
    if (!options.idField) options.idField = IDFIELD
    super(options)
    this.minlength = options.minlength || 11
    this.fieldname = options.fieldname
    if (!this.fieldname || !this.fieldname.trim()) throw new Error("missing fieldname")
  }

  _ping(hostname,port){
    return new Promise( async(resolve,reject)=>{
      const result = await putstr('',hostname,port)
      if (result instanceof Event) {
        reject(result)
      } else if (result.answer) {
        resolve(true)
      }
    })
  }

  _ask(hostname,port,data,self) {
    return new Promise( async ( resolve,reject) => {

      if (data[this.fieldname] && data[this.fieldname].length > this.minlength){
        const result = await putstr(data[this.fieldname],hostname,port,APIPATH,REQUESTTIMEOUT)
        if (result.answer) {
          if (result.answer.entities) {
            data.__entities = result.answer.entities
            if (Array.isArray(result.answer.timeline)) data.__timeline = result.answer.timeline
            if (Array.isArray(result.answer.objectlines)) data.__line = result.answer.objectlines
          }
          resolve(data)
        } else if (result instanceof ErrorEvent) {
          resolve(result) // do not try again, if error
        } else if (result instanceof LogEvent) {
          reject(result) // try again
        } else {
          console.dir({badresult:result})
          resolve({})
        }
      } else resolve(data)
    })
  }
};
