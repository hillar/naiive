import { request } from "http"
import { get } from '../../utils/put.mjs '

// deleteme￣content￣dummys-mbp‾7200/_search?_source_excludes=*&size=1024'

function _getDocs(index, server, port, limit = 1024) {
  return new Promise((resolve) => {
    const options = {
      hostname: server,
      port: port,
      path: encodeURI(`/${index}/_search?_source_excludes=*&size=${limit}`),
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        Accept: "application/json",
      }
    };
    const req = request(options, (res) => {
      let data = [];
      res.on("data", (chunk) => {
        try {
          data.push(chunk);
        } catch (e) {
          console.error(e);
          console.error(query);
          resolve({});
        }
      });
      res.on("end", () => {
        let r;

        try {
          r = JSON.parse(Buffer.concat(data).toString());
          const ids = []
          for (const hit of r.hits.hits) {
            ids.push(hit._id)
          }
          resolve(ids);
        } catch (e) {
          resolve({});
        }
      });
    });
    req.on("error", (error) => {
      resolve();
    });
    req.end(`
      {"query": {
    "bool": {
      "filter": [
        {
          "exists": {
            "field": "ssdeep"
          }
        }
      ]
    }
  }}
    `);
  });
}

function _getDoc(id, fields, index, server, port) {
  return new Promise((resolve) => {
    const options = {
      hostname: server,
      port: port,
      path: encodeURI(`/${index}/_doc/${id}/_source?_source_includes=${fields}`),
      method: "GET",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        Accept: "application/json",
      }
    };
    const req = request(options, (res) => {
      let data = [];
      res.on("data", (chunk) => {
        try {
          data.push(chunk);
        } catch (e) {
          console.error(e);
          console.error(query);
          resolve({});
        }
      });
      res.on("end", () => {
        let r;
        try {
          r = JSON.parse(Buffer.concat(data).toString());
          resolve(r);
        } catch (e) {
          resolve({});
        }
      });
    });
    req.on("error", (error) => {
      resolve();
    });
    req.end();
  });
}

export function hash_split(ss) {
  if (!ss) {
    let [chunksize, chunk, double_chunk] = [];
    return { chunksize, chunk, double_chunk };
  }
  let [chunksize, chunk, double_chunk] = ss.split(":");
  chunksize = Number(chunksize);
  return { chunksize, chunk, double_chunk };
}

//function _elasticSimpleQuery(query,index,from,size,hostname,port,logger){
function elasticQuery(q, index, elasticserver, port) {
  //console.log({query,filter,index,from,size,hostname,port})
  return new Promise((resolve) => {
    const qq = q;

    const options = {
      hostname: elasticserver,
      port: port,
      path: encodeURI(`/${index}/_search`),
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        Accept: "application/json",
      },
    };
    const req = request(options, (res) => {
      let data = [];
      res.on("data", (chunk) => {
        try {
          data.push(chunk);
        } catch (e) {
          console.error(e);
          console.error(query);
          resolve({});
        }
      });
      res.on("end", () => {
        let r;
        try {
          r = JSON.parse(Buffer.concat(data).toString());
          if (r.hits.total.value && !r.hits.hits.length) logger.warning({ r });
          if (r.hits) resolve(r.hits);
          else {
            console.error(r.error);
            resolve({});
          }
        } catch (e) {
          resolve({});
        }
      });
    });
    req.on("error", (error) => {
      resolve(false);
    });
    req.write(JSON.stringify(qq));
    req.end();
  });
}

export function findSimilar(id, index, elasticserver, port, ssdeepserver,ssdeepport) {
  return new Promise(async (resolve) => {
    const result = [];
    const similar = []
    const ssdeep = await _getDoc(id, "ssdeep", index, elasticserver, port);
    if (ssdeep) {
      let { chunksize, chunk, double_chunk } = hash_split(ssdeep.ssdeep);
      const q = {
        query: {
          bool: {
            must: [
              {
                terms: {
                  chunksize: [chunksize, chunksize * 2, Math.round(chunksize / 2)],
                },
              },
              {
                bool: {
                  should: [
                    {
                      match: {
                        chunk: {
                          query: chunk,
                        },
                      },
                    },
                    {
                      match: {
                        double_chunk: {
                          query: double_chunk,
                        },
                      },
                    },
                  ],
                  minimum_should_match: 1,
                },
              },
            ],
          },
        },
      };

      const results = await elasticQuery(q, index, elasticserver, port);
      if (results.hits) {
        for (const hit of results.hits) {
          result.push({ id: hit._id, score: hit._score, ssdeep:hit._source.ssdeep });
        }
        result.shift()
        for (const maybe of result){
          //console.dir({i1:r.id,i2:maybe.basename,h1:r.ssdeep,h2:maybe.ssdeep})
          const score = maybe.score
          const simi = await get(ssdeepserver,ssdeepport,`/${ssdeep.ssdeep}&${maybe.ssdeep}`)
          //console.dir({s:simi.answer.similarity,score})
          //console.log(simi.answer.similarity,score)
          if (simi.answer.similarity) similar.push({o:id,s:simi.answer.similarity,...maybe})
        }
      }
    } else {
      //debug("no ssdeep", id, index);
    }
    if (similar.length) resolve({id,ssdeep:ssdeep.ssdeep,similar});
    else resolve(null)
  });
}

export function printProgress(...progress) {
  if (process.stdout.isTTY) {
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(progress.join(" "));
  } else console.debug(progress.join(" "));
}

(async function () {
  const index = "deleteme￣content￣dummys-macbook-pro‾7200";
  const elasticserver = "localhost";
  const eport = 9200;
  const ssdeepserver = 'localhost'
  const sport = 9997
  let ids = await _getDocs(index,elasticserver,eport,10000)
  console.dir(ids.length)
  //ids = ['006f1e06b70615150f431fdd3c345847']
  //const ids = ['018697ad65588671c2bdd7b3ec2bdef3','04bf0952f710c774bd9c759b11998416']
  for (const id of ids) {
  try {

    //const id = "04bf0952f710c774bd9c759b11998416";

    const r = await findSimilar(id, index, elasticserver, eport, ssdeepserver,sport);
    if (r) {
      //console.dir(r)

      for (const maybe of r.similar){

        console.log(maybe.s,maybe.o,maybe.id)
      }

    }
  } catch (e) {
    console.dir({ e });
  }
}
})();

/*

there is no linear relation between elastic score and ssdeep similarity ;(

cat /tmp/scores.txt| grep '0 ' | cut -f2 -d" " | cut -f1 -d. | sort | uniq -c | sort -nr
1876 5
1866 6
 786 7
 555 4
 226 8
 119 3
  68 9
  52 13
  52 10
  48 11
  47 12
  34 14
  12 16
  12 15
   9 2
   5 19
   4 40
   4 36
   4 27
   2 44
   2 42
   2 37
   2 32
   2 30
   2 29
   2 20
   1 33
   1 25
   1 21

   % cat /tmp/scores.txt| grep -v '0 ' | cut -f2 -d" " | cut -f1 -d. | sort | uniq -c | sort -nr
 797 6
 767 7
 349 8
 237 5
 227 10
 186 14
 162 12
 161 13
 154 9
  99 11
  80 16
  58 15
  26 4
  22 18
  21 17
  12 19
  11 22
   4 21
   3 20
   2 27
   1 6582
   1 30
   1 3
   1 28
   1 23

   cat /tmp/scores.txt | grep -v '0 ' | cut -f1 -d. | awk '{print substr($1, 0, 1)" "$2}'| sort | uniq -c | sort -nr | awk '{print $2" "$3" "$1}'| sort -nr
9 9 27
9 8 25
9 7 37
9 6 117
9 5 3
9 30 1
9 28 1
9 27 2
9 23 1
9 22 11
9 21 1
9 20 2
9 19 7
9 17 10
9 16 26
9 15 4
9 14 2
9 13 25
9 12 16
9 11 6
9 10 142
8 9 25
8 8 32
8 7 47
8 6 51
8 5 8
8 20 1
8 19 2
8 18 3
8 16 14
8 15 2
8 14 3
8 13 7
8 12 4
8 11 7
8 10 5
7 9 25
7 8 48
7 7 100
7 6 110
7 5 25
7 4 2
7 21 3
7 18 6
7 16 3
7 15 1
7 14 5
7 13 5
7 12 9
7 11 8
7 10 8
6 9 25
6 8 101
6 7 171
6 6 252
6 5 66
6 4 7
6 19 2
6 18 11
6 17 10
6 16 9
6 15 4
6 14 6
6 13 9
6 12 7
6 11 23
6 10 25
6  1
5 9 24
5 8 113
5 7 273
5 6 148
5 5 59
5 4 2
5 18 2
5 15 5
5 14 7
5 13 5
5 12 6
5 11 5
5 10 8
4 9 8
4 8 24
4 7 67
4 6 53
4 5 41
4 4 9
4 19 1
4 17 1
4 16 8
4 15 21
4 14 55
4 13 35
4 12 50
4 11 19
4 10 18
3 9 4
3 8 3
3 7 37
3 6 46
3 5 19
3 3 1
3 16 16
3 15 19
3 14 61
3 13 44
3 12 41
3 11 19
3 10 15
2 9 16
2 8 3
2 7 35
2 6 20
2 5 16
2 4 6
2 16 4
2 15 2
2 14 47
2 13 31
2 12 29
2 11 12
2 10 6

naiive/src/backend/servers/watch master % cat /tmp/scores.txt | grep -v '0 ' | cut -f1 -d. | awk '{print substr($1, 0, 1)" "$2}'| sort | uniq -c | sort -nr | awk '{print $3" "$2" "$1}'| sort -nr
30 9 1
28 9 1
27 9 2
23 9 1
22 9 11
21 9 1
21 7 3
20 9 2
20 8 1
19 9 7
19 8 2
19 6 2
19 4 1
18 8 3
18 7 6
18 6 11
18 5 2
17 9 10
17 6 10
17 4 1
16 9 26
16 8 14
16 7 3
16 6 9
16 4 8
16 3 16
16 2 4
15 9 4
15 8 2
15 7 1
15 6 4
15 5 5
15 4 21
15 3 19
15 2 2
14 9 2
14 8 3
14 7 5
14 6 6
14 5 7
14 4 55
14 3 61
14 2 47
13 9 25
13 8 7
13 7 5
13 6 9
13 5 5
13 4 35
13 3 44
13 2 31
12 9 16
12 8 4
12 7 9
12 6 7
12 5 6
12 4 50
12 3 41
12 2 29
11 9 6
11 8 7
11 7 8
11 6 23
11 5 5
11 4 19
11 3 19
11 2 12
10 9 142
10 8 5
10 7 8
10 6 25
10 5 8
10 4 18
10 3 15
10 2 6
9 9 27
9 8 25
9 7 25
9 6 25
9 5 24
9 4 8
9 3 4
9 2 16
8 9 25
8 8 32
8 7 48
8 6 101
8 5 113
8 4 24
8 3 3
8 2 3
7 9 37
7 8 47
7 7 100
7 6 171
7 5 273
7 4 67
7 3 37
7 2 35
6 9 117
6 8 51
6 7 110
6 6 252
6 5 148
6 4 53
6 3 46
6 2 20
 6 1
5 9 3
5 8 8
5 7 25
5 6 66
5 5 59
5 4 41
5 3 19
5 2 16
4 7 2
4 6 7
4 5 2
4 4 9
4 2 6
3 3 1

*/
