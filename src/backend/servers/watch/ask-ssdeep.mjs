import { createReadStream } from 'fs'
import { request } from 'http'
import { kStringMaxLength as STRINGMAXLENGTH} from 'buffer'
import { printProgress } from '../../classes/pipe/dynpipe.mjs'


const REQUESTTIMEOUT = 1000 * 30 // tambov !?


// TODO move to separate module
class Warning extends Error {
  constructor(message,o={}) {
    super(message);
    this.msg = message
    //this.name = this.constructor.name;
    for (const key of Object.keys(o)) this[key] = o[key]
    Error.captureStackTrace(this, this.constructor);
  }
}

export function pingssdeep(hostname,port,data,self,timeout = REQUESTTIMEOUT){
  return new Promise( async(resolve,reject)=>{
    const options = {
      hostname: hostname,
      port: port,
      timeout,
      path: '/',
      method: 'PUT',
      headers: {
        'Accept': 'application/json'
      }
    }
    const req = request(options, (res) => {
      res.on('data', () => {})
      res.on('end', () => {resolve(true)} )
    })
    req.on('error', (error) => {
      reject(error)
    })
    req.end()
  })
}

export function askssdeep(hostname,port,data,self,timeout = REQUESTTIMEOUT){
  return new Promise( async(resolve,reject)=>{
    const { basename, pathname, fields} = data
    let rs
    try {
      rs = createReadStream(pathname)
    } catch (error) {
      error.pathname = pathname
      resolve({__warning:error})
    }
    if (!rs) return
    //let start
    const options = {
      hostname: hostname,
      port: port,
      timeout,
      path: '/',
      method: 'PUT',
      headers: {
        'Accept': 'application/json'
      }
    }

    const req = request(options, (res) => {
      let body = []
      let counter = 0
      let total = 0
      let endevent = 'normal'
      res.on('timeout', () => {
        endevent = 'timeout'
        const error = new Warning('Response timeout',{hostname,port,pathname})
        resolve({__warning:error})
      })
      res.on('aborted', () => {
        endevent = 'aborted'
        const error = new Warning('Response aborted',{hostname,port,pathname})
        resolve({__warning:error})
      })
      res.on('error', (error) => {
        endevent = 'error'
        error.pathname = pathname
        error.basename = basename
        reject(error)
      })
      res.on('data', (chunk) => {
         body.push(chunk)
      })
      res.on('end', () => {
        if (endevent!== 'normal') return
        const took = Number(process.hrtime.bigint() - start)/1e+6;
        let answer
        const str = Buffer.concat(body).toString()
        body = []
        try {
          answer = JSON.parse(str)
          data.ssdeep = answer.ssdeep
          resolve(data)
        } catch (error) {
          if (str.trim().length) {
            const e = new Warning('Bad response',{hostname,port,pathname,basename,s:str.slice(0,1024),e:str.slice(-1024)})
            reject(e)
          } else {
            const e = new Warning('Empty response',{hostname,port,pathname,basename})
            resolve({__warning:e})
          }
        }
      })
    })

    req.on('error', (error) => {
      rs.unpipe(req)
      error.pathname = pathname
      error.basename = basename
      reject(error)
    })
    req.on('timeout', () => {
      req.abort()
      const error = new Warning('Request timeout',{hostname,port,pathname})
      resolve({__warning:error})
    })

    let stotal = 0
    rs.on('data', chunk =>{
      stotal += chunk.length
      //if (stotal > 1024 * 1024 *1024)
      //printProgress(hostname,port,basename, '->',stotal)
    })

    rs.on('error', (error) => {
        rs.unpipe(req)
        req.abort()
        error.basename = basename
        error.pathname = pathname
        resolve({__warning:error})
      })

    const start = process.hrtime.bigint();
    rs.pipe(req)
  })
}


export function askssdeepstr(hostname,port,data,self,timeout = REQUESTTIMEOUT){
  return putstr(JSON.stringify(data),hostname,port)
}

export function putstr(str, hostname, port, path = '/') {
  return new Promise(async (resolve, reject) => {
    const options = {
      hostname,
      port,
      path,
      method: "PUT",
      headers: {
        Accept: "application/json",
      }
    };

    const req = request(options, (res) => {
      let size = 0
      let body = [];
      let endevent = "normal";
      res.on("timeout", () => {
        endevent = "timeout";
        const error = new Warning("Response timeout", { hostname, port });
        reject(new Warning("Response timeout", { hostname, port }));
      });
      res.on("aborted", () => {
        endevent = "aborted";
        reject(new Warning("Response aborted", { hostname, port }));
      });
      res.on("error", (error) => {
        endevent = "error";
        reject(error);
      });
      res.on("data", (chunk) => {
        body.push(chunk);
        size += chunk.length
        if (size > STRINGMAXLENGTH) {
          res.emit('error',new Warning("Response to big", { hostname, port }))
        }
      });
      res.on("end", () => {
        if (endevent !== "normal") return;
        const took = Number(process.hrtime.bigint() - start) / 1e6;
        let answer;
        const str = Buffer.concat(body).toString();
        body = [];
        try {
          answer = JSON.parse(str);
          resolve({answer,took});
        } catch (error) {
          if (str.trim().length) {
            reject(new Warning("Bad response", { hostname, port }))
          } else {
            reject(new Warning("Empty response", { hostname, port }))
          }
        }
      })
    })

    req.on("error", (error) => {
      reject(error);
    })

    req.on("timeout", () => {
      req.abort();
      reject(new Warning("Request timeout", { hostname, port }));
    })

    const start = process.hrtime.bigint();
    req.end(str);
  })
}
