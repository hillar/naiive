import { request } from 'http'
import { kStringMaxLength as STRINGMAXLENGTH} from 'buffer'
import { Ask, StringStream, printProgress } from '../../classes/pipe/dynpipe.mjs'
import { LANGUAGES } from '../../constants/elastic.mjs'
import { hilbert } from '../../utils/hilbert.mjs'
import { putstr } from '../../utils/put.mjs'


export function chunkString(str, length = (1000000-1)) {
  /*
  The length of [content-ENGLISH] field of [8024d875347b1db54476514352f3c1e4] doc
  of [deleteme￣content￣macbook-pro‾7200] index has exceeded [1000000] -
  maximum allowed to be analyzed for highlighting.
  */
  // /(.|[\r\n]){1,n}/g
  return str.match(new RegExp('(.|[\r\n]){1,'+length+'}', 'g'));
}

export class LanguageDetect extends Ask{
  constructor(options){
    if (!options.idField) options.idField = '__contenthash__'
    super(options)
    this.minlength = options.minlength || 32
    this.fieldname = options.fieldname || 'content'
  }
  _ping(hostname,port,data,self){
    return new Promise( async(resolve,reject)=>{
      const options = {
        hostname: hostname,
        port: port,
        path: '/',
        method: 'PUT',
        headers: {
          'Accept': 'application/json'
        }
      }
      const req = request(options, (res) => {
        res.on('data', () => {})
        res.on('end', () => {resolve(true)} )
      })
      req.on('error', (error) => {
        reject(error)
      })
      req.end()
    })
  }
  _ask(hostname,port,data,self) {
    return new Promise( async ( resolve,reject) => {
      if (data[this.fieldname] && data[this.fieldname].length > this.minlength){
        data.__plines = data[this.fieldname].substring(8,1024*4).replace(/[^\d\s\W]/gi,'a').replace(/\d/gi,'0')
        data.__snake = hilbert(data.__plines)
        const options = {
          hostname: hostname,
          port: port,
          path: '/',
          method: 'PUT',
          headers: {
            'Accept': 'application/json'
          }
        }
        const req = request(options, (res) => {
          const body = []
          let counter = 0
          let total = 0
          res.on('data', (chunk) => {
            counter ++
            total += chunk.length
            //if (total > 1024 *1024 *1024)
            //printProgress(hostname,port,basename, '<-',total)
            if (total + 65536 > STRINGMAXLENGTH) {
              const error = new Warning('Response is to big',{pathname, basename,this:this.name})
              resolve({__warning:error})
              res.destroy(error)

            } else body.push(chunk)
            //printProgress(this.name,total)

          })
          res.on('end', () => {

            let answer
            try {
              answer = JSON.parse(Buffer.concat(body).toString())
            } catch (error) {
              answer = {__warning:new Error('Response is not valid json.')}
            }
            if (answer && answer.languages) {
              data.__languages = {}
              for (const language of Object.keys(answer.languages)){
                  //data[language] = answer.languages[language]
                  let sl
                  if (LANGUAGES.includes(language)) sl = 'content-'+language
                  else sl = 'content-unknown'
                  data.__languages[sl] = []
                  for (const item of answer.languages[language]) {
                    for ( const chunk  of chunkString(item))
                    if (chunk.trim().length) data.__languages[sl].push(chunk)
                  }
              }
            }
            resolve(data)
          } )
        })
        req.on('error', (error) => {
          reject(error)
        })
        new StringStream(data[this.fieldname]).pipe(req)
        //req.end(data[this.fieldname])
      } else resolve(data)
    })
  }
};
