

import { createReadStream } from 'fs'
import { request } from 'http'
import { kStringMaxLength as STRINGMAXLENGTH} from 'buffer'

import { Event, LogEvent, ErrorEvent } from '../../classes/base/event-levels.mjs'
import { Ask, StringStream, printProgress } from '../../classes/pipe/dynpipe.mjs'
import { putstr,putfile } from '../../utils/put.mjs'


const REQUESTTIMEOUT = 1000 * 130 // tambov !?
const APIPATH = '/rmeta/text'

export class Tika extends Ask{
  constructor(hashFieldame,options){
    options.idField = hashFieldame
    super(options)
    this.hashFieldame = options.idField
    //if (hashFieldame) this.hashFieldame = hashFieldame
    //else throw new Error('missing hashFieldame')
  }

  _ping(hostname,port){
    return new Promise( async(resolve,reject)=>{
      const result = await putstr('ping',hostname,port,APIPATH,REQUESTTIMEOUT)
      if (result instanceof Event) {
        reject(result)
      } else if (result.answer) {
        resolve(true)
      }
    })
  }

  _ask(hostname,port,data) {
    return new Promise( async ( resolve,reject) => {
      if (data.pathname) {
          const result = await putfile(data.pathname,hostname,port,APIPATH,REQUESTTIMEOUT)
          if (result.answer && Array.isArray(result.answer ) && result.answer.length ) {
            const carryon = Object.keys(data).filter(i => i.startsWith('__'))
            for (const i in result.answer) {
              result.answer[i].__embedded_no = i
              for (const key of carryon){
                result.answer[i][key] = data[key]
              }
              //item[this.hashFieldame] = data[this.hashFieldame]
              //item.fields = data.fields
            }

            resolve(result.answer)
          } else if (result instanceof ErrorEvent) {
            resolve(result) // do not try again, if error
          } else if (result instanceof LogEvent) {
            reject(result) // try again
          } else {
            console.dir({badresult:result})
            resolve({})
          }
      } else {
        //throw new Error('no '+this.fieldname)
        resolve(data)
      }
    })
  }
};



/*
// TODO move to separate module
class Warning extends Error {
  constructor(message,o={}) {
    super(message);
    this.msg = message
    //this.name = this.constructor.name;
    for (const key of Object.keys(o)) this[key] = o[key]
    Error.captureStackTrace(this, this.constructor);
  }
}

export function pingtika(hostname,port,data,self,timeout = REQUESTTIMEOUT){
  return new Promise( async(resolve,reject)=>{
    const options = {
      hostname: hostname,
      port: port,
      timeout,
      path: '/rmeta/text',
      method: 'PUT',
      headers: {
        'Accept': 'application/json'
      }
    }
    const req = request(options, (res) => {
      res.on('data', () => {})
      res.on('end', () => {resolve(true)} )
    })
    req.on('error', (error) => {
      reject(error)
    })
    req.end()
  })
}

export function asktika(hostname,port,data,self,timeout = REQUESTTIMEOUT){
  return new Promise( async(resolve,reject)=>{
    const { basename, pathname, fields, ssdeep} = data
    let rs
    try {
      rs = createReadStream(pathname)
    } catch (error) {
      error.pathname = pathname
      resolve({__warning:error})
    }
    if (!rs) return
    //let start
    const options = {
      hostname: hostname,
      port: port,
      timeout,
      path: '/rmeta/text',
      method: 'PUT',
      headers: {
        'Accept': 'application/json'
      }
    }

    const req = request(options, (res) => {
      let data = []
      let body = []
      let counter = 0
      let total = 0
      let endevent = 'normal'
      res.on('timeout', () => {
        endevent = 'timeout'
        const error = new Warning('Response timeout',{hostname,port,pathname})
        resolve({__warning:error})
      })
      res.on('aborted', () => {
        endevent = 'aborted'
        const error = new Warning('Response aborted',{hostname,port,pathname})
        resolve({__warning:error})
      })
      res.on('error', (error) => {
        endevent = 'error'
        error.pathname = pathname
        error.basename = basename
        reject(error)
      })
      res.on('data', (chunk) => {
        counter ++
        total += chunk.length
        //if (total > 1024 *1024 *1024)
        //printProgress(hostname,port,basename, '<-',total)
        if (total + 65536 > STRINGMAXLENGTH) {
          endevent = 'tobig'
          const error = new Warning('Response is to big',{pathname, basename,this:this.name})
          resolve({__warning:error})
          res.destroy(error)

        } else body.push(chunk)
        //printProgress(this.name,total,chunk.length)
        //printProgress(counter,total,basename)
      })
      res.on('end', () => {
        if (endevent!== 'normal') return
        const took = Number(process.hrtime.bigint() - start)/1e+6;
        let answer
        const str = Buffer.concat(body).toString()
        body = []
        try {
          answer = JSON.parse(str)
          for (const i in answer) {
            answer[i].fields = fields
            answer[i].basename = basename
            answer[i].embedded_no = i
          }
          answer[0].ssdeep = ssdeep
          resolve(answer)
        } catch (error) {
          if (str.trim().length) {
            const e = new Warning('Bad response',{hostname,port,pathname,basename,s:str.slice(0,1024),e:str.slice(-1024)})
            reject(e)
          } else {
            const e = new Warning('Empty response',{hostname,port,pathname,basename})
            resolve({__warning:e})
          }
        }
      })
    })

    req.on('error', (error) => {
      rs.unpipe(req)
      error.pathname = pathname
      error.basename = basename
      reject(error)
    })
    req.on('timeout', () => {
      req.abort()
      const error = new Warning('Request timeout',{hostname,port,pathname})
      resolve({__warning:error})
    })

    let stotal = 0
    rs.on('data', chunk =>{
      stotal += chunk.length
      //if (stotal > 1024 * 1024 *1024)
      //printProgress(hostname,port,basename, '->',stotal)
    })

    rs.on('error', (error) => {
        rs.unpipe(req)
        req.abort()
        error.basename = basename
        error.pathname = pathname
        resolve({__warning:error})
      })

    const start = process.hrtime.bigint();
    rs.pipe(req)
  })
}
*/
