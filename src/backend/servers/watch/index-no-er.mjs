import { createSimpleCli } from '../../classes/base/simplecli.mjs'
import { HASHFIELD, CONTENTINDEX, FILESINDEX, METAINDEX, ENTITYINDEX, MAXPREVIEWSIZE, indexName } from '../../constants/elastic.mjs'
import { PORT as FILESSERVERPORT, SEPARATOR as PORTSEPARATOR } from '../../constants/files.mjs'
import { elasticSimpleQuery, elasticQuery, getDoc } from '../../utils/elastic.mjs'
import { parseHostnamePort } from '../../utils/var.mjs'
import { hostname } from 'os'
import { resolve } from 'path'
import { readdirSync, accessSync, constants, watch } from 'fs'
import { processDir } from '../../utils/postprocess.mjs'

function printProgress(progress){
    if (!process.stdout.isTTY) {
      console.log(progress)
    } else {
      process.stdout.clearLine()
      process.stdout.cursorTo(0)
      process.stdout.write(''+progress)
    }
}

const glob = createSimpleCli({
  opts: {   poll : p => { return (p  && Number(p)) ? Number(p) : 10 },
            filespath : p => {
                const d = p ? resolve(p) : resolve('')
                try {
                  accessSync(d, constants.R_OK)
                } catch (error) {
                  glob.log.emerg({notreadable:d})
                  process.exit(-1)
                }
                return d
             },
             tikas: list => {return (list && list.split)? list.split(',').map( hp => parseHostnamePort(hp,9998)) : [{hostname:'localhost',port:9998}]},
             elastics: list => {return (list && list.split)? list.split(',').map( hp => parseHostnamePort(hp,9200)) : [{hostname:'localhost',port:9200}]},
             prefix: p => {return p ? p.toLowerCase() : '' },
             // https://www.elastic.co/guide/en/elasticsearch/reference/current/breaking-changes-7.0.html#_literal_literal_is_no_longer_allowed_in_index_name
             // : is no longer allowed in index name
             suffix: p => {return p ? p.toLowerCase() : hostname().split('.').shift()+PORTSEPARATOR+FILESSERVERPORT },
             content: p => {return p ? p.toLowerCase() : CONTENTINDEX },
             field: p => {return p ? p : HASHFIELD }, // <- case sensitive !
             files: p => {return p ? p.toLowerCase() : FILESINDEX },
             entities: p => {return p ? p.toLowerCase() : ENTITYINDEX },
             meta: p => {return p ? p.toLowerCase() : METAINDEX }
  },
  fn: async (log) => {
    log.info({startinwith:glob.defaults})
    await new Promise( resolve => {
      printProgress('starting ..')

      const queue = []

      const watcher = watch(glob.filespath ,(eventType, filename) => {
        //console.dir({eventType, filename})
        log.info('saw',eventType, filename)
        if ((/[a-fA-F0-9]{32}/).test(filename)) {
          if (!queue.includes(filename)){
            queue.push(filename)
            log.info({eventType, filename,ql:queue.length})
          }
        }
      })

      process.on('SIGHUP',  () => {
            log.info('got SIGHUP')
            // TODO reload conf

      })

      process.on('SIGINT', async () => {
        if (process.gotsinint) {
          log.info('got another SIGINT, force exit')
          watcher.close()
          resolve()
        }
        if (!process.gotsinint) process.gotsinint = true
        watcher.close()
        log.info('got SIGINT, pushing last queue')
        let counter = 0
        while (queue.length > 0) {
          counter ++
          const current = queue.shift()
          await processDir(current,glob.filespath,glob.tikas,glob.elastics,glob.prefix,glob.suffix,glob.content,glob.field,glob.files,glob.entities,glob.meta,log)
          printProgress(counter + ' ' + queue.length + ' ' + current)
        }
        log.info('got SIGINT, done pushing')
        resolve()
        //process.exit(0)

      })

      process.on('SIGTERM', () => {
          log.info('got SIGTERM')
          watcher.close()
          resolve()
      })

      const POLLINTERVAL = glob.poll * 1000
      let checkerTimeout
      async function checker() {
          if (checkerTimeout) return
          printProgress('pushing .. ' + queue.length)
          let counter = 0
          let start = queue.length
          //errors = {}
          while (queue.length > 0) {
            counter ++
            printProgress(counter + ' ' + queue.length)
            const current = queue.shift()
            let ff =  await processDir(current,glob.filespath,glob.tikas,glob.elastics,glob.prefix,glob.suffix,glob.content,glob.field,glob.files,glob.entities,glob.meta,log)
            printProgress(counter + ' ' + queue.length + ' ' + current)
            if (!ff) {
              // put it back for next time
              setTimeout(function() {
                  queue.push(current)
              }, POLLINTERVAL*2)
            }
          }
          printProgress('watching ..')
          checkerTimeout = setTimeout(function() {
              checkerTimeout = null
              try {
                  checker()
              } catch (error) {
                log.emerg({checker:error})
              }
          }, POLLINTERVAL)
        }
        try {
            checker()
        } catch (error) {
          log.emerg({checker:error})
        }
    })
  }
})

glob.run()
