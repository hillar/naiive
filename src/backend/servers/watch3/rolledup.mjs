import { pipeline } from 'stream/promises';
import 'util';
import { accessSync, constants, readdirSync, readFileSync, createReadStream } from 'fs';
import { resolve, join } from 'path';
import { Readable, Transform, Writable } from 'stream';
import 'net';

/**
 *
 * https://github.com/3rd-Eden/argh
 *
 * Argv is a extremely light weight options parser for Node.js it's been built
 * for just one single purpose, parsing arguments. It does nothing more than
 * that.
 *
 * @param {Array} argv The arguments that need to be parsed, defaults to process.argv
 * @returns {Object} Parsed arguments.
 * @api public
 */
 function parse(argv) {
    argv = argv || process.argv.slice(2);

    /**
     * This is where the actual parsing happens, we can use array#reduce to
     * iterate over the arguments and change it to an object. We can easily bail
     * out of the loop by destroying `argv` array.
     *
     * @param {Object} argh The object that stores the parsed arguments
     * @param {String} option The command line flag
     * @param {Number} index The position of the flag in argv's
     * @param {Array} argv The argument variables
     * @returns {Object} argh, the parsed commands
     * @api private
     */
    return argv.reduce(function parser(argh, option, index, argv) {
      const next = argv[index + 1];
      let value;
      let data;

      //
      // Special case, -- indicates that it should stop parsing the options and
      // store everything in a "special" key.
      //
      if (option === '--') {
        //
        // By splicing the argv array, we also cancel the reduce as there are no
        // more options to parse.
        //
        argh.argv = argh.argv || [];
        argh.argv = argh.argv.concat(argv.splice(index + 1));
        return argh;
      }

      if (data = /^--?(?:no|disable)-(.*)/.exec(option)) {
        //
        // --no-<key> indicates that this is a boolean value.
        //
        insert(argh, data[1], false, option);
      } else if (data = /^-(?!-)(.*)/.exec(option)) {
        insert(argh, data[1], true, option);
      } else if (data = /^--([^=]+)=["']?([\s!#$%&\x28-\x7e]+)["']?$/.exec(option)) {
        //
        // --foo="bar" and --foo=bar are alternate styles to --foo bar.
        //
        insert(argh, data[1], data[2], option);
      } else if (data = /^--(.*)/.exec(option)) {
        //
        // Check if this was a bool argument
        //
        if (!next || next.charAt(0) === '-' || (value = /^true|false$/.test(next))) {
          insert(argh, data[1], value ? argv.splice(index + 1, 1)[0] : true, option);
        } else {
          value = argv.splice(index + 1, 1)[0];
          insert(argh, data[1], value, option);
        }
      } else {
        //
        // This argument is not prefixed.
        //
        if (!argh.argv) argh.argv = [];
        argh.argv.push(option);
      }

      return argh;
    }, Object.create(null));
  }

  /**
   * Inserts the value in the argh object
   *
   * @param {Object} argh The object where we store the values
   * @param {String} key The received command line flag
   * @param {String} value The command line flag's value
   * @param {String} option The actual option
   * @api private
   */
  function insert(argh, key, value, option) {
    //
    // Automatic value conversion. This makes sure we store the correct "type"
    //
    if ('string' === typeof value && !isNaN(+value)) value = +value;
    if (value === 'true' || value === 'false') value = value === 'true';

    const single = option.charAt(1) !== '-';
    const properties = key.split('.');
    let position = argh;

    if (single && key.length > 1) return key.split('').forEach(function short(char) {
      insert(argh, char, value, option);
    });

    //
    // We don't have any deeply nested properties, so we should just bail out
    // early enough so we don't have to do any processing
    //
    if (!properties.length) return argh[key] = value;

    while (properties.length) {
      const property = properties.shift();
      const current = position[property];

      if (properties.length) {
        if ('object' !== typeof current && !Array.isArray(current)) {
          position[property] = Object.create(null);
        }
      } else {
        if (property in position) {
          if (Array.isArray(current)) current.push(value);
          else position[property] = [current, value];
        } else {
          position[property] = value;
        }
      }

      position = position[property];
    }
  }

const settables = (c) => {
    if (!c) return []
    let _self = [];
    const proto = Object.getPrototypeOf(c);
    for (const name of Object.getOwnPropertyNames(proto)) {
        if (!name.startsWith("__")) { // '__proto__' && ..
        const desc = Object.getOwnPropertyDescriptor(proto, name);
        if (desc && typeof desc.set === "function") _self.push(name);
        }
    }
    const parent = Object.getPrototypeOf(c);
    if (parent && parent.constructor.name !== "Object") return [...new Set([..._self, ...settables(parent)])]
    else return _self
};

class Pipeline {
    notsettables = [
        '_transformState',
        'writable',
        'destroyed',
        'readable',
        'readableFlowing',
      ]
    #processors = {}
    constructor (source, transforms, destination, abortcontroller) {
        this.source = source;
        this.transforms = Array.isArray(transforms) ? transforms : [transforms];
        this.destination = destination;
        this.abortcontroller = abortcontroller;
        this.#processors[source.constructor.name.toLowerCase()] = source;
        for (const transform of this.transforms) {
            this.#processors[transform.constructor.name.toLowerCase()] = transform;
        }
        this.#processors[destination.constructor.name.toLowerCase()] = destination;
    }
    get settables() {
        return this.settings.map(x=>Object.keys(x)[0])
    }
    get settings() {
        const current = [];
        for (const settable of settables(this).filter(x=>!this.notsettables.includes(x)) ) {
            const tmp = {};
            tmp[settable.toLowerCase()] = this[settable];
            current.push(tmp);
        }
        for (const settable of settables(this.source).filter(x=>!this.notsettables.includes(x)) ) {
            const tmp = {};
            tmp[this.source.constructor.name.toLowerCase()+'-'+settable] = this.source[settable];
            current.push(tmp);
        }
        for (const transform of this.transforms) {
            for (const settable of settables(transform).filter(x=>!this.notsettables.includes(x)) ) {
                const tmp = {};
                tmp[transform.constructor.name.toLowerCase()+'-'+settable] = transform[settable];
                current.push(tmp);
            }
        }
        for (const settable of settables(this.destination).filter(x=>!this.notsettables.includes(x)) ) {
            const tmp = {};
            tmp[transform.constructor.name.toLowerCase()+'-'+settable] = this[settable];
            current.push(tmp);
        }
        return current
    }
    _set(_key,value) {
        let [sc,key] = _key.split('-');
        sc = sc.toLowerCase();
        if (!key) {
          try {
            if (this[sc]) this[sc] = value;
            for (const processor of Object.keys(this.#processors)) {
              if (this.#processors[processor][sc] === undefined )
                this.#processors[processor][sc] = value;
            }
          } catch (error) {
            console.error(this.constructor.name +' can not set ' +sc+ ' to '+value+ '\n' + error.toString());
          }
            //else throw new Error('not a pipeline setting: '+ sc )
        } else {
            try {
              if (this.#processors[sc]) this.#processors[sc][key] = value;
              //else throw new Error('not a pipeline processor: '+ sc )
            } catch (error) {
              console.error(sc + ' can not set ' +key+ ' to '+value+ '\n' + error.toString());
            }
        }
    }
    /*
    key, value
    {key, value}
    [{key, value},..]
    key ===  fieldname || processorname-fieldname
    */
    set(key,value) {
        if (key && value ){
            this._set(key,value);
        } else if (!value) {
            if (Array.isArray(key)) {
                for (const i of key) {
                    const key = Object.keys(i)[0];
                    const value = i[key];
                    this._set(key,value);
                }
            } else {
                //const k = Object.keys(key)[0]
                for (const k  of Object.keys(key)) if (k) {
                    this._set(k,key[k]);
                }
            }
        }
    }
    async run () {
        const options = {};
        if (this.abortcontroller) options.signal =  this.abortcontroller.signal;
        try {
            await pipeline(this.source, ...this.transforms, this.destination, options);
        } catch (error) {
            console.error('--- Pipeline error  ---');
            console.error(error.toString());
            console.error('-----');
        }
    }
}

class basenames extends Readable {
  #path
  #names
  constructor(path, opts) {
    super(opts);
    if (path) this.path = path;
  }
  get path() {
    return this.#path
  }
  set path(v) {
    this.#path = resolve(v);
    try {
      accessSync(this.#path, constants.R_OK );
    } catch (error) {
      throw new Error(this.constructor.name+' can not set path. '+error.toString())
    }
  }
  _read() {
    if (!this.#path) this.destroy(new Error(this.constructor.name+' path not set'));
    else {
      if (!this.#names) {
        try {
          this.#names = readdirSync(this.#path);
        } catch (error) {
          this.destroy(new Error(this.constructor.name+' '+error.toString()));
        }
      }
      if (this.#names && this.#names.length) {
        const basename = this.#names.pop();
        this.push({basename});
      } else {
        this.push(null);
      }
    }
  }
}

function createSource(options={}) {
  const path  = options.path;
  return new basenames(path,{objectMode: true,highWaterMark:1})
}

const HASHFUNCTION = 'md5';
const FILESPATH = 'files';

function isHash(hash, alg = HASHFUNCTION) {
  switch (alg) {
    case "md5":
      return /^[a-fA-F0-9]{32}$/.test(hash);
    case "sha256":
      return /^[a-fA-F0-9]{64}$/.test(hash);
    default:
      console.error('not supported: '+alg);
      process.exit(-1);
      return false;
  }
}

const notsettables = [
        '_transformState',
        'writable',
        'destroyed',
        'readable',
        'readableFlowing',
      ];

class Processor extends Transform {
  #ready
  #basename
  constructor() {
    const options = {};
    options.objectMode = true;
    options.highWaterMark = 1;
    super(options);
  }
  get basename() {return this.#basename}
  set basename(v) { this.#basename = v; }
  async _transform(data,e,next) {
    console.log(this.constructor.name, data);
    if (data instanceof Error) {
          this.push(data);
          next();
      }  else {
          if (!this.#ready) {
            for (const setting of settables(this).filter(x=>!notsettables.includes(x))) {
              if (this[setting] === undefined) {
                this.destroy(new Error(this.constructor.name + ' not set ' + setting));
              }
              this.#ready = true;
            }
          }
          try {
          await this._process(data,next);
        } catch (error) {
          if (error instanceof TypeError || error instanceof ReferenceError) {
            console.error(error);
            process.exit(-1);
          } else {
            this.push(new Error(this.constructor.name+' process error \n' + error.toString()));
          }

        }
      }
  }
  _process(data,next) {
    this.destroy(this.constructor.name+ ' not implemented');
  }
}

class FilterBasename extends Processor {
  _process(data,next) {
      if (isHash(data[this.basename])) this.push(data);
      //else console.error('not hash:',data.basename)
      next();
  }
}

function trimArrayOfStrings(a) {
  return [...new Set(a.map((s) => (s.trim().length > 0 ? s.trim() : undefined)))].filter((i) => !(i === undefined))
}

function cleanup(obj) {
  if (obj) {
    if (typeof obj === "object") {
      if (Array.isArray(obj)) {
        if (typeof obj[0] === "object") {
          const r = {};
          for (const i in obj) {
            const tmp = cleanup(obj[i]);
            if (tmp) {
              for (const key of Object.keys(tmp)) {
                if (tmp[key]) {
                  if (!r[key]) r[key] = tmp[key];
                  else {
                    if (Array.isArray(r[key])) {
                      r[key] = trimArrayOfStrings([r[key], tmp[key]].flat());
                    } else {
                      r[key] = trimArrayOfStrings([r[key], tmp[key]].flat());
                    }
                    if (Array.isArray(r[key]) && r[key].length === 1) r[key] = r[key][0];
                  }
                }
              }
            }
          }
          if (Object.keys(r).length) return r
          else return
        } else return obj.flat()
      } else {
        const r = {};
        for (const key of Object.keys(obj)) {
          const tmp = cleanup(obj[key]);
          if (tmp) r[key] = tmp;
        }
        if (Object.keys(r).length) return r
        else return
      }
    } else return obj
  } else return
}

class ReadJournal extends Processor {
  #path
  constructor(path) {
    super();
    if (path) this.path = path;
  }
  get path() {
    return this.#path
  }
  set path(v) {
    this.#path = resolve(v);
    try {
      accessSync(this.#path, constants.R_OK);
    } catch (error) {
      throw new Error(this.constructor.name + " can not set path. " + error.toString())
    }
  }
  _process(data, next) {
    const pathname = join(this.path, data[this.basename], FILESPATH);
    //console.log({pathname})
    data.__fields = [];
    try {
      for (const f of readdirSync(pathname)) {
        if (f.endsWith(".json")) {
          const filename = join(pathname, f);
          try {
            //const id = await digestFile(filename)
            let doc = cleanup(JSON.parse(readFileSync(filename)));
            //doc = cleanup(doc)
            //doc.__fieldscontenthash__ = id
            data.__fields.push(doc);
          } catch (error) {
            this.push(new Error(this.constructor.name + " " + data[this.basename] + "" + filename + "\n" + error.toString()));
          }
        }
      }
      data.__fields = cleanup(data.__fields);
      this.push(data);
    } catch (error) {
      this.push(new Error(this.constructor.name + " " + data[this.basename] + "\n" + error.toString()));
    }
    next();
  }
}

class TextORTika extends Processor {
  async _process(data,next) {
    /*

__mimetype__: 'text/plain; charset=us-ascii',
__mimehuman__: 'ASCII text',

__mimetype__: 'inode/x-empty; charset=binary',
__mimehuman__: 'UTF-8 Unicode text',

__mimetype__: [
  'inode/x-empty; charset=binary',
  'application/zip; charset=binary'
],
__mimehuman__: 'Zip data (MIME type "application/vnd.etsi.asic-e+zip"?)',

    */

    if (!Array.isArray(data.__fields.__mimetype__)) data.__fields.__mimetype__ = [data.__fields.__mimetype__];
    else data.__fields.__mimetype__ = data.__fields.__mimetype__.filter(x=>x!=='inode/x-empty; charset=binary');
    let [mimetype, charset] = data.__fields.__mimetype__[0].split(';');
    if (!Array.isArray(data.__fields.__mimehuman__)) data.__fields.__mimehuman__ = [data.__fields.__mimehuman__];
    const istext = data.__fields.__mimehuman__[0].endsWith(' text') ? true : (mimetype.startsWith('text/') ? true : false);
    if (istext) {
      let chunks = 0;
      let size = 0;
      let last = '';
      const filename = join(this.path,data[this.basename],data[this.basename]);
      const highWaterMark = 1000000;
      const encoding = 'utf-8';
      try {
        for await (const chunk of createReadStream(filename,{ highWaterMark, encoding })) {
            chunks ++;
            //TODO something more effective
            const tmp = (last + chunk).split(/\r\n|\n|\r/);
            last = tmp.pop();
            const text = tmp.join('\n');
            size += text.length;
            data.content = text;
            data.chunkno = chunks;
            this.push(data);
        }
        //const took = Number(process.hrtime.bigint() - start) / 1e6;
        //const rate = (size / (took/1000)) / 1024 / 1024 //MB/s
        //this.push({filename,size,chunks,took,rate})
      } catch (error) {
          error.processor = this.constructor.name;
          this.push(error);
      }
    } else {
      //await tika()
      next();
    }

  }
}

const transforms = [
  new FilterBasename(),
  new ReadJournal(),
  new TextORTika(),
];

function createDestination() {

  return new Writable({
    objectMode: true,
    async write(data, encoding, next) {
        console.log('testWritable',data);
        if (data instanceof Error) {
            console.error('**** error from pipe ******');
            console.error(data);
            console.error('**********');
            next();
        } else {
            if (data.size) {
                console.log(data);
            }
        //console.dir(Object.keys(data))
        //const size = data.kola.kala.length
        //const name = data.__fields.filename
        //const took = data.took
        //const rate = data.rate
        //console.dir({name,size})
        //await sleep(100)
        next();
        }

    }
})


}

const arghs = parse();
const dp = new Pipeline(createSource(), transforms, createDestination());
dp.set(arghs);


if (arghs.help || arghs.h || (arghs.argv && arghs.argv.includes('help'))) {
  console.dir(dp.settings);
  process.exit(0);
} else {
  console.log(JSON.stringify(dp.settings));
  await dp.run();
}
