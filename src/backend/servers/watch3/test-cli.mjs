import { parse as parseArghs } from './src/argh.mjs'
import { Pipeline } from './src/pipeline.mjs'
import { createSource } from './processors/source/readfiles.mjs'
import { transforms } from './processors/transforms/index.mjs'
import { createDestination } from './processors/destination/dummy.mjs'
import { sleep} from './src/utils.mjs'

const arghs = parseArghs()
const dp = new Pipeline(createSource(), transforms, createDestination())
dp.set(arghs)


if (arghs.help || arghs.h || (arghs.argv && arghs.argv.includes('help'))) {
  console.dir(dp.settings)
  process.exit(0)
} else {
  console.log(JSON.stringify(dp.settings))
  await dp.run()
}
