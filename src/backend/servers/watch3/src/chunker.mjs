import { createReadStream } from 'fs'
import {Transform} from 'stream'


function getFieldValue(o,fieldname) {
    const bittes = fieldname.split('.')
    let value = o
    for (const sub of bittes) {
        value = value[sub]
    }
    return value
}
function setFieldValue(o,fieldname,v){
    const bittes = fieldname.split('.')
    let value = o
    const last = bittes.pop()
    for (const sub of bittes) {
        if (!value[sub]) value[sub] = {}
        value = value[sub]
    }
    value[last] = v
}

export class TextFileChunker extends Transform {
    #size
    #textField
    #encodingField
    #filenameField
    // https://www.elastic.co/guide/en/elasticsearch/reference/7.13/index-modules.html#index-max-analyzed-offset
    // Defaults to 1000000
    constructor(size = 1000000, filenameField, encodingField, textField, options) {
      options = options || {}
      options.objectMode = true
      options.highWaterMark = 1
      super(options);
      this.size = size
      this.encodingField = encodingField
      this.filenameField = filenameField
      this.textField = textField
    }

    get size() { return this.#size }
    set size(v) { this.#size = v }
    get textField() { return this.#textField }
    set textField(v) { if (v && v.length) this.#textField = v
        else throw new Error(this.constructor.name+' missing textField') }
    get encodingField() { return this.#encodingField }
    set encodingField(v) { if (v && v.length) this.#encodingField = v
        else throw new Error(this.constructor.name+' missing encodingField')}
    get filenameField() { return this.#filenameField }
    set filenameField(v) { if (v && v.length) this.#filenameField = v
        else throw new Error(this.constructor.name+' missing filenameField')}

    async _transform(data,e,next) {
        console.dir(data)
        if (data instanceof Error) {
            this.push(data)
            next()
        } else {
          const highWaterMark = 1 * this.size
          // TODO subobj
          const encoding = getFieldValue(data,this.encodingField)
          const filename = getFieldValue(data,this.filenameField)
          let last = ''
          const start = process.hrtime.bigint();
          let chunks = 0
          let size = 0
          try {
            for await (const chunk of createReadStream(filename,{ highWaterMark, encoding })) {
                chunks ++
                //TODO something more effective
                const tmp = (last + chunk).split(/\r\n|\n|\r/)
                last = tmp.pop()
                const text = tmp.join('\n')
                size += text.length
                setFieldValue(data,this.textField, text)
                this.push(data)
            }
            const took = Number(process.hrtime.bigint() - start) / 1e6;
            const rate = (size / (took/1000)) / 1024 / 1024 //MB/s
            this.push({filename,size,chunks,took,rate})
          } catch (error) {
              error.processor = this.constructor.name
              this.push(error)
          }
          next()
        }
    }
  }
