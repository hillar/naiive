import { inspect } from "util"

export const pending = (p) => inspect(p) === "Promise { <pending> }"
export const sleep = (time) => new Promise(resolve => setTimeout(resolve, time))

export const settables = (c) => {
    if (!c) return []
    let _self = []
    const proto = Object.getPrototypeOf(c)
    for (const name of Object.getOwnPropertyNames(proto)) {
        if (!name.startsWith("__")) { // '__proto__' && ..
        const desc = Object.getOwnPropertyDescriptor(proto, name)
        if (desc && typeof desc.set === "function") _self.push(name)
        }
    }
    const parent = Object.getPrototypeOf(c)
    if (parent && parent.constructor.name !== "Object") return [...new Set([..._self, ...settables(parent)])]
    else return _self
}
