import { pipeline } from 'stream/promises'
import { settables } from './utils.mjs'

/**
 *  runpipeline(source, transform(s), destination [, abortcontroller])
 *
*/

export const runpipeline = async (source, transforms, destination, abortcontroller) => {
    const options = {}
    if (abortcontroller) options.signal =  abortcontroller.signal
    if (!Array.isArray(transforms)) transforms = [transforms]
    try {
        await pipeline(source, ...transforms, destination, options)
    } catch (error) {
        console.error(error.toString())
    }
  }

export class Pipeline {
    notsettables = [
        '_transformState',
        'writable',
        'destroyed',
        'readable',
        'readableFlowing',
      ]
    #processors = {}
    constructor (source, transforms, destination, abortcontroller) {
        this.source = source
        this.transforms = Array.isArray(transforms) ? transforms : [transforms]
        this.destination = destination
        this.abortcontroller = abortcontroller
        this.#processors[source.constructor.name.toLowerCase()] = source
        for (const transform of this.transforms) {
            this.#processors[transform.constructor.name.toLowerCase()] = transform
        }
        this.#processors[destination.constructor.name.toLowerCase()] = destination
    }
    get settables() {
        return this.settings.map(x=>Object.keys(x)[0])
    }
    get settings() {
        const current = []
        for (const settable of settables(this).filter(x=>!this.notsettables.includes(x)) ) {
            const tmp = {}
            tmp[settable.toLowerCase()] = this[settable]
            current.push(tmp)
        }
        for (const settable of settables(this.source).filter(x=>!this.notsettables.includes(x)) ) {
            const tmp = {}
            tmp[this.source.constructor.name.toLowerCase()+'-'+settable] = this.source[settable]
            current.push(tmp)
        }
        for (const transform of this.transforms) {
            for (const settable of settables(transform).filter(x=>!this.notsettables.includes(x)) ) {
                const tmp = {}
                tmp[transform.constructor.name.toLowerCase()+'-'+settable] = transform[settable]
                current.push(tmp)
            }
        }
        for (const settable of settables(this.destination).filter(x=>!this.notsettables.includes(x)) ) {
            const tmp = {}
            tmp[transform.constructor.name.toLowerCase()+'-'+settable] = this[settable]
            current.push(tmp)
        }
        return current
    }
    _set(_key,value) {
        let [sc,key] = _key.split('-')
        sc = sc.toLowerCase()
        if (!key) {
          try {
            if (this[sc]) this[sc] = value
            for (const processor of Object.keys(this.#processors)) {
              if (this.#processors[processor][sc] === undefined )
                this.#processors[processor][sc] = value
            }
          } catch (error) {
            console.error(this.constructor.name +' can not set ' +sc+ ' to '+value+ '\n' + error.toString())
          }
            //else throw new Error('not a pipeline setting: '+ sc )
        } else {
            try {
              if (this.#processors[sc]) this.#processors[sc][key] = value
              //else throw new Error('not a pipeline processor: '+ sc )
            } catch (error) {
              console.error(sc + ' can not set ' +key+ ' to '+value+ '\n' + error.toString())
            }
        }
    }
    /*
    key, value
    {key, value}
    [{key, value},..]
    key ===  fieldname || processorname-fieldname
    */
    set(key,value) {
        if (key && value ){
            this._set(key,value)
        } else if (!value) {
            if (Array.isArray(key)) {
                for (const i of key) {
                    const key = Object.keys(i)[0]
                    const value = i[key]
                    this._set(key,value)
                }
            } else {
                //const k = Object.keys(key)[0]
                for (const k  of Object.keys(key)) if (k) {
                    this._set(k,key[k])
                }
            }
        }
    }
    async run () {
        const options = {}
        if (this.abortcontroller) options.signal =  this.abortcontroller.signal
        try {
            await pipeline(this.source, ...this.transforms, this.destination, options)
        } catch (error) {
            console.error('--- Pipeline error  ---')
            console.error(error.toString())
            console.error('-----')
        }
    }
}
