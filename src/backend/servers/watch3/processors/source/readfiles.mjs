import { readdirSync, accessSync, constants } from 'fs'
import { join, resolve } from 'path'
import  { Readable } from 'stream'

class basenames extends Readable {
  #path
  #names
  constructor(path, opts) {
    super(opts)
    if (path) this.path = path
  }
  get path() {
    return this.#path
  }
  set path(v) {
    this.#path = resolve(v)
    try {
      accessSync(this.#path, constants.R_OK )
    } catch (error) {
      throw new Error(this.constructor.name+' can not set path. '+error.toString())
    }
  }
  _read() {
    if (!this.#path) this.destroy(new Error(this.constructor.name+' path not set'))
    else {
      if (!this.#names) {
        try {
          this.#names = readdirSync(this.#path)
        } catch (error) {
          this.destroy(new Error(this.constructor.name+' '+error.toString()))
        }
      }
      if (this.#names && this.#names.length) {
        const basename = this.#names.pop()
        this.push({basename})
      } else {
        this.push(null);
      }
    }
  }
}

export function createSource(options={}) {
  const path  = options.path
  return new basenames(path,{objectMode: true,highWaterMark:1})
}
