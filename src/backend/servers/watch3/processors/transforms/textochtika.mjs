import {Processor} from './processor.mjs'
import {createReadStream} from 'fs'
import { join } from 'path'
import { sleep } from '../../src/utils.mjs'


export class TextORTika extends Processor {
  async _process(data,next) {
    if (!data.__fields) return
    /*

__mimetype__: 'text/plain; charset=us-ascii',
__mimehuman__: 'ASCII text',

__mimetype__: 'inode/x-empty; charset=binary',
__mimehuman__: 'UTF-8 Unicode text',

__mimetype__: [
  'inode/x-empty; charset=binary',
  'application/zip; charset=binary'
],
__mimehuman__: 'Zip data (MIME type "application/vnd.etsi.asic-e+zip"?)',

    */

    if (!Array.isArray(data.__fields.__mimetype__)) data.__fields.__mimetype__ = [data.__fields.__mimetype__]
    else data.__fields.__mimetype__ = data.__fields.__mimetype__.filter(x=>x!=='inode/x-empty; charset=binary')
    let [mimetype, charset] = data.__fields.__mimetype__[0].split(';')
    if (!Array.isArray(data.__fields.__mimehuman__)) data.__fields.__mimehuman__ = [data.__fields.__mimehuman__]
    const istext = data.__fields.__mimehuman__[0].endsWith(' text') ? true : (mimetype.startsWith('text/') ? true : false)
    if (istext) {
      let chunks = 0
      let size = 0
      let last = ''
      const filename = join(this.path,data[this.basename],data[this.basename])
      const highWaterMark = 1000000
      const encoding = 'utf-8'
      try {
        for await (const chunk of createReadStream(filename,{ highWaterMark, encoding })) {
            chunks ++
            // TODO something more effective
            // TODO send last after end
            const tmp = (last + chunk).split(/\r\n|\n|\r/)
            last = tmp.pop()
            const text = tmp.join('\n')
            size += text.length
            data.content = text
            data.chunkno = chunks
            this.push(data)
        }
        //const took = Number(process.hrtime.bigint() - start) / 1e6;
        //const rate = (size / (took/1000)) / 1024 / 1024 //MB/s
        //this.push({filename,size,chunks,took,rate})
        next()
      } catch (error) {
          error.processor = this.constructor.name
          this.push(error)
      }
    } else {
      await this.tika(data)
      next()
    }
  }
  async tika(data){
    //await sleep(1000)
    this.push(data)
  }

}
