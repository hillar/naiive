import { FilterBasename } from './filterbasename.mjs'
import { ReadJournal } from './readjournal.mjs'
import { TextORTika } from './textochtika.mjs'
import {Processor} from './processor.mjs'
import { checkExists } from '../../../watch/ask-ela.mjs'

const ceo = {tries:1,
  pool:['sm-4nodes-a.c3-lab:9200','sm-4nodes-b.c3-lab:9200','sm-4nodes-c.c3-lab:9200','sm-4nodes-d.c3-lab:9200'],
  cluster:'c3-4node',
  index:'test__deleteme_test',
  sendmetrix:()=>{}
}
export const transforms = [
  new FilterBasename(),
  new ReadJournal(),
  new checkExists(ceo),
  new TextORTika(),
  //new Processor(),
]
