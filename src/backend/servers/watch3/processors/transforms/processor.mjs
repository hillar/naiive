import { Transform } from 'stream'
import  { settables } from '../../src/utils.mjs'

const notsettables = [
        '_transformState',
        'writable',
        'destroyed',
        'readable',
        'readableFlowing',
      ]

export class Processor extends Transform {
  #ready
  #basename
  constructor() {
    const options = {}
    options.objectMode = true
    options.highWaterMark = 1
    super(options);
  }

  get basename() {return this.#basename}
  set basename(v) { this.#basename = v }

  push(data) {
    try {
      super.push(data)
    } catch (error) {
      console.error(error)
      console.error(this.constructor.name + ' push error\n'+ error.toString())
      this.destroy(new Error(this.constructor.name + ' push error\n'+ error.toString()))
    }

  }
  async _transform(data,e,next) {
//    console.log(this.constructor.name, data)
    if (data instanceof Error) {
          this.push(data)
          next()
      }  else {
          if (!this.#ready) {
            for (const setting of settables(this).filter(x=>!notsettables.includes(x))) {
              if (this[setting] === undefined) {
                this.destroy(new Error(this.constructor.name + ' not set ' + setting))
              }
            }
            this.#ready = true
          }
          try {
            await this._process(data,next)
          } catch (error) {
            if (error instanceof TypeError || error instanceof ReferenceError) {
              console.error(error)
              process.exit(-1)
            } else {
              this.push(new Error(this.constructor.name+' process error \n' + error.toString()))
            }
          }
      }
  }
  _process(data,next) {
    console.error(new Error(this.constructor.name+ ' not implemented'))
    process.exit(-1)
  }
}

export class AskFrom extends Processor {
  #pool
  #service
  constructor(){
    super()
  }
  async _process(data,next){
    try {
      await _ask(data)
      next()
    } catch(error) {
      this.push(new Error(this.constructor.name+ 'service\n'+error.toString()))
    }

  }
  _ask(data){
    this.destroy(new Error(this.constructor.name+ 'not implemented'))
  }
}
