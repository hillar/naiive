import {isHash} from '../../../../utils/var.mjs'
import {Processor} from './processor.mjs'

export class FilterBasename extends Processor {
  _process(data,next) {
      if (isHash(data[this.basename])) this.push(data)
      //else console.error('not hash:',data.basename)
      next()
  }
}
