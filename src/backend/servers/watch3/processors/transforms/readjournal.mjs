import { FILESPATH } from "../../../../constants/files.mjs"
import { Processor } from "./processor.mjs"
import { join, resolve } from "path"
import { readdirSync, readFileSync, accessSync, constants } from "fs"

function trimArrayOfStrings(a) {
  return [...new Set(a.map((s) => (s.trim().length > 0 ? s.trim() : undefined)))].filter((i) => !(i === undefined))
}

function cleanup(obj) {
  if (obj) {
    if (typeof obj === "object") {
      if (Array.isArray(obj)) {
        if (typeof obj[0] === "object") {
          const r = {}
          for (const i in obj) {
            const tmp = cleanup(obj[i])
            if (tmp) {
              for (const key of Object.keys(tmp)) {
                if (tmp[key]) {
                  if (!r[key]) r[key] = tmp[key]
                  else {
                    if (Array.isArray(r[key])) {
                      r[key] = trimArrayOfStrings([r[key], tmp[key]].flat())
                    } else {
                      r[key] = trimArrayOfStrings([r[key], tmp[key]].flat())
                    }
                    if (Array.isArray(r[key]) && r[key].length === 1) r[key] = r[key][0]
                  }
                }
              }
            }
          }
          if (Object.keys(r).length) return r
          else return
        } else return obj.flat()
      } else {
        const r = {}
        for (const key of Object.keys(obj)) {
          const tmp = cleanup(obj[key])
          if (tmp) r[key] = tmp
        }
        if (Object.keys(r).length) return r
        else return
      }
    } else return obj
  } else return
}

export class ReadJournal extends Processor {
  #path
  constructor(path) {
    super()
    if (path) this.path = path
  }
  get path() {
    return this.#path
  }
  set path(v) {
    this.#path = resolve(v)
    try {
      accessSync(this.#path, constants.R_OK)
    } catch (error) {
      throw new Error(this.constructor.name + " can not set path. " + error.toString())
    }
  }
  _process(data, next) {
    const pathname = join(this.path, data[this.basename], FILESPATH)
    //console.log({pathname})
    data.__fields = []
    try {
      for (const f of readdirSync(pathname)) {
        if (f.endsWith(".json")) {
          const filename = join(pathname, f)
          try {
            //const id = await digestFile(filename)
            let doc = cleanup(JSON.parse(readFileSync(filename)))
            //doc = cleanup(doc)
            //doc.__fieldscontenthash__ = id
            data.__fields.push(doc)
          } catch (error) {
            this.push(new Error(this.constructor.name + " " + data[this.basename] + "" + filename + "\n" + error.toString()))
          }
        }
      }
      data.__fields = cleanup(data.__fields)
      this.push(data)
    } catch (error) {
      this.push(new Error(this.constructor.name + " " + data[this.basename] + "\n" + error.toString()))
    }
    next()
  }
}
