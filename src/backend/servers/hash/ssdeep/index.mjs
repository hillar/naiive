import { createServer } from 'http'
//import { mkdtempSync, createWriteStream, unlinkSync, rmdirSync } from 'fs'
//import { join, sep } from 'path'
//import { tmpdir } from 'os'
import { kStringMaxLength as STRINGMAXLENGTH} from 'buffer'
import { default as ss } from 'ssdeep'


const DEFAULTPORT = 9997 // TODO sync with constants/ports.mjs

function printHelp(){
  console.log('Options:')
  console.log()
  console.log('  --port=[number]              server port (default: '+DEFAULTPORT+')')
  console.log()
}

function printError(){
  console.error()
  console.error('bad port number ;(')
  console.error()
  printHelp()
}

let port = DEFAULTPORT

if (process.env.PORT){
  port = Number(process.env.PORT)
  if (!port || isNaN(port) || port > 65536) {
    printError()
    process.exit(1)
  } else console.log('env PORT',port)
}

process.argv.forEach((val, index) => {
  if (val.indexOf('-h') != -1) {
    printHelp()
    process.exit(0)
  }
  if (val.indexOf('--port=') != -1) {
    port = Number(val.split('=')[1])
    if (!port || isNaN(port) || port > 65536) {
      printError()
      process.exit(1)
    } else console.log('cmd arg PORT',port)
  }
});

//const tmpDir = tmpdir();

const server = createServer((req, res) => {

      const begin = process.hrtime.bigint();
      console.log(JSON.stringify({start:{timestamp:new Date(),ip:req.socket.address(),method:req.method, ...req.headers}}))
      let size = 0
      let took


      res.on('error', error => {
        //noop, just print it to stderr
        console.error(error)
      })

      res.on('finish',()=>{
        console.log(JSON.stringify({end:{took,total:Number(process.hrtime.bigint() - begin)/1e+6,size,timestamp:new Date(),ip:req.socket.address(),statusCode:res.statusCode}}))
      })

      if (req.headers['content-length'] > STRINGMAXLENGTH) {
        res.statusCode = 413
        res.end()
      }

      // curl -XPUT -T somefile localhost:9997
      if (req.method === 'PUT') {

          req.on('error', (error) => {
            console.error(error)
            res.statusCode = 599
            res.end()

          })

          const body = []

          req.on('data', chunk => {
            body.push(chunk)
            size += chunk.length
          })

          req.on('end', async () => {
            const start = process.hrtime.bigint();
            let hash
            if (size < STRINGMAXLENGTH) {
              hash = ss.hash(Buffer.concat(body).toString())
              took = Number(process.hrtime.bigint() - start)/1e+6;
              if (hash) {
                //  default json
                if (req.headers.accept && req.headers.accept.indexOf('text') === -1) res.write(JSON.stringify({ssdeep:hash,took}))
                else res.write(hash)
              } else res.statusCode = 500
            } else res.statusCode = 413
            res.end()
            console.log(took,hash)
          })

    // curl localhost:9997/hash1&hash2
    } else if (req.method === 'GET') {
      const bittes = req.url.split('&')
      if (bittes[1]){
        const start = process.hrtime.bigint();
        const similarity = ss.compare(bittes[0].substr(1),bittes[1])
        took = Number(process.hrtime.bigint() - start)/1e+6;
        // default json
        if (req.headers.accept && req.headers.accept.indexOf('text') === -1) res.write(JSON.stringify({similarity,took}))
        else res.write(`${similarity}`)
        res.end()
        console.log(took,similarity)
      } else {
        res.statusCode = 415
        res.end()
      }

    // not implemented
    }  else {
      res.statusCode = 501
      res.end()
    }


})

server.on('listening', () => {
  console.log('listening',server.address())
});

server.on('clientError', (err, socket) => {
  socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
});

server.listen(port);
