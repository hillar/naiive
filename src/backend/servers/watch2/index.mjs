import { createSimpleHTTPServer } from '../../classes/base/simpleserver.mjs'
import { WATCH as WATCHPORT } from '../../constants/ports.mjs'

import { watch } from 'fs'
import { Readable } from 'stream'

import { opts } from './opts.mjs'
import { runPipeline } from './pipeline.mjs'



opts.port = p => { return p ? Number(p) : WATCHPORT}

const state = {}

const spot = createSimpleHTTPServer({
  opts,
  about: () => {
    const settings = {}
    for (const s of spot.settings) settings[s] = spot[s]
    return {settings, defaults:spot.defaults,state}
  },
  methods: {
    get : () => {}
  }

})

class DirCanges extends Readable {
    constructor(logger,dirname,options) {
      super(options)
      logger.info({WATCHING:spot.filespath})
      watch(dirname,  (eventType, basename) => {
        if (basename) {
          // harcode delay to wait disk sync :(
          setTimeout(()=>{
            this.push({basename:encodeURIComponent(basename)})
          },1500)
        }
      });
    }
    _read () {
      //noop
    }
}

spot.listen( async () => {
  const readable = new DirCanges(spot.logger, spot.filespath, {objectMode: true,highWaterMark:1});
  const result = await runPipeline(state,spot.logger, readable, spot.defaults, 0)

})
