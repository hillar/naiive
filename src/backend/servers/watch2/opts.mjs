/*

options for glob'er and spot'er


*/

import {
  ELASTIC as DEFAULT_PORT_ELASTIC,
  TIKA as DEFAULT_PORT_TIKA,
  SSDEEP as DEFAULT_PORT_SSDEEP,
  INFLUXDB as DEFAULT_PORT_INFLUXDB,
  ER as DEFAULT_PORT_ER,
  LD as DEFAULT_PORT_LD
} from "../../constants/ports.mjs";
import {
  HASHFIELD,
  CONTENTINDEX,
  FILESINDEX,
  METAINDEX,
  ENTITYINDEX,
  TIMELINEINDEX,
  LINEINDEX,
  MAXPREVIEWSIZE,
  indexName
} from "../../constants/elastic.mjs";
import {
  PORT as DEFAULT_PORT_FILES,
  SEPARATOR as PORTSEPARATOR
} from "../../constants/files.mjs";

import { parseHostnamePort } from "../../utils/var.mjs";
import { hostname } from "os";
import { resolve } from "path";
import { accessSync, constants } from "fs";


export const opts = {
  filespath: p => {
    const d = p ? resolve(p) : resolve("");
    try {
      accessSync(d, constants.R_OK);
    } catch (error) {
      throw new Error('not readable: ' + d)
    }
    return d;
  },
  influxdb: list => {
    return list && list.split ? list.split(",").map(hp => parseHostnamePort(hp, DEFAULT_PORT_INFLUXDB)) : [{ hostname: "localhost", port: DEFAULT_PORT_INFLUXDB }];
  },
  tikas: list => {
    return list && list.split ? list.split(",").map(hp => parseHostnamePort(hp, DEFAULT_PORT_TIKA)) : [{ hostname: "localhost", port: DEFAULT_PORT_TIKA }];
  },
  ssdeeps: list => {
    return list && list.split ? list.split(",").map(hp => parseHostnamePort(hp, DEFAULT_PORT_SSDEEP)) : [{ hostname: "localhost", port: DEFAULT_PORT_SSDEEP }];
  },
  elastics: list => {
    return list && list.split ? list.split(",").map(hp => parseHostnamePort(hp, DEFAULT_PORT_ELASTIC)) : [{ hostname: "localhost", port: DEFAULT_PORT_ELASTIC }];
  },
  clustername: p => {
    return p ? p.toLowerCase() : "singlehost-cluster";
  },
  ers: list => {
    return list && list.split ? list.split(",").map(hp => parseHostnamePort(hp, DEFAULT_PORT_ER)) : [{ hostname: "localhost", port: DEFAULT_PORT_ER }];
  },
  lds: list => {
    return list && list.split ? list.split(",").map(hp => parseHostnamePort(hp, DEFAULT_PORT_LD)) : [{ hostname: "localhost", port: DEFAULT_PORT_LD }];
  },
  prefix: p => {
    return p ? p.toLowerCase() : "DELETEME";
  },
  // https://www.elastic.co/guide/en/elasticsearch/reference/current/breaking-changes-7.0.html#_literal_literal_is_no_longer_allowed_in_index_name
  // : is no longer allowed in index name
  suffix: p => {
    return p
      ? p.toLowerCase()
      : hostname()
          .split(".")
          .shift() +
          PORTSEPARATOR +
          DEFAULT_PORT_FILES;
  },
  content: p => {
    return p ? p.toLowerCase() : CONTENTINDEX;
  },
  field: p => {
    return p ? p : HASHFIELD;
  }, // <- case sensitive !
  files: p => {
    return p ? p.toLowerCase() : FILESINDEX;
  },
  entities: p => {
    return p ? p.toLowerCase() : ENTITYINDEX;
  },
  timeline: p => {
    return p ? p.toLowerCase() : TIMELINEINDEX;
  },
  line: p => {
    return p ? p.toLowerCase() : LINEINDEX;
  },
  meta: p => {
    return p ? p.toLowerCase() : METAINDEX;
  }
};
