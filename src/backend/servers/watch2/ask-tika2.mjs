import { createReadStream, statSync } from 'fs'
import { request } from 'http'
import { kStringMaxLength as STRINGMAXLENGTH } from 'buffer'

import { Event, LogEvent, ErrorEvent } from '../../classes/base/event-levels.mjs'
import { Ask, StringStream, printProgress } from '../../classes/pipe/dynpipe.mjs'
import { putstr, putfile } from '../../utils/put.mjs'

import { kStringMaxLength } from 'buffer'
import { Transform } from 'stream'

const REQUESTTIMEOUT = 1000 * 130 // tambov !?
const APIPATH = '/rmeta/text'

export class Tika extends Ask {
  constructor(hashFieldame, options) {
    options.idField = hashFieldame
    super(options)
    this.hashFieldame = options.idField
    //if (hashFieldame) this.hashFieldame = hashFieldame
    //else throw new Error('missing hashFieldame')
  }

  _ping(hostname, port) {
    return new Promise(async (resolve, reject) => {
      const result = await putstr('ping', hostname, port, APIPATH, REQUESTTIMEOUT)
      if (result instanceof Event) {
        reject(result)
      } else if (result.answer) {
        resolve(true)
      }
    })
  }

  _ask(hostname, port, data, parent) {
    return new Promise(async (resolve, reject) => {
      if (data.pathname) {
        if (!Array.isArray(data.__fields.__mimetype__)) data.__fields.__mimetype__ = [data.__fields.__mimetype__]
        else data.__fields.__mimetype__ = data.__fields.__mimetype__.filter((x) => x !== 'inode/x-empty; charset=binary')
        let [mimetype, charset] = data.__fields.__mimetype__[0].split(';')
        if (!Array.isArray(data.__fields.__mimehuman__)) data.__fields.__mimehuman__ = [data.__fields.__mimehuman__]
        const istext = data.__fields.__mimehuman__[0].endsWith(' text') ? true : mimetype.startsWith('text/') ? true : false

        if (istext) {
          let chunks = 0
          let size = 0
          let last = ''
          const highWaterMark = Math.round(kStringMaxLength/1024) -1024
          const encoding = 'utf-8'

          try {
            const stat = statSync(data.pathname)
            const totalchunks = Math.round(stat.size / highWaterMark + 0.5)
            const rtf = createReadStream(data.pathname, { highWaterMark, encoding })
            const wrtbl = this._readableState.pipes[0]
            let chunks = 0
            let last = ''
            class F2D extends Transform {
              constructor() {
                super({ objectMode: true, highWaterMark: 1 })
              }
              _transform(chunk, _, next) {
                chunks++
                const tmp = (last + chunk).split(/\r\n|\n|\r/)
                last = tmp.pop()
                this.push({ ...data, content: tmp.join('\n'), __chunk_no: chunks, __chunks_total: totalchunks })
                next()
              }
            }
            const f2d = new F2D()
            function close(e) {
              if (e) {
                this.push(new Error(this.constructor.name + ' ' + error.toString()))
              }
              f2d.unpipe(wrtbl)
              rtf.unpipe(f2d)
              resolve(true)
            }
            rtf.on('error', close)
            rtf.on('close', close)
            rtf.on('end', close)
            rtf.pipe(f2d)
            f2d.pipe(wrtbl)
            /*
            for await (const chunk of createReadStream(data.pathname,{ highWaterMark, encoding })) {
                chunks ++
                // TODO something more effective
                // TODO send last after end
                const tmp = (last + chunk).split(/\r\n|\n|\r/)
                last = tmp.pop()
                //const text = tmp.join('\n')
                //size += text.length
                //data.content = text
                //data.__chunk_no = chunks//counter++ + '-'+ totalchunks + '-' + stat.size
                console.dir(this)
                this.push({...data, content: tmp.join('\n'), __chunk_no: chunks, __chunks_total: totalchunks})
                console.dir(this._readableState.pipes[0])

            }
            */
          } catch (error) {
            error.processor = this.constructor.name
            console.error(error)
            this.push(error)
            resolve(true)
          }
        } else {
          resolve(true)
          /*
          const result = await putfile(data.pathname,hostname,port,APIPATH,REQUESTTIMEOUT)
          if (result.answer && Array.isArray(result.answer ) && result.answer.length ) {
            const carryon = Object.keys(data).filter(i => i.startsWith('__'))
            for (const i in result.answer) {
              result.answer[i].__embedded_no = i
              for (const key of carryon){
                result.answer[i][key] = data[key]
              }
              //item[this.hashFieldame] = data[this.hashFieldame]
              //item.fields = data.fields
            }

            resolve(result.answer)
          } else if (result instanceof ErrorEvent) {
            resolve(result) // do not try again, if error
          } else if (result instanceof LogEvent) {
            reject(result) // try again
          } else {
            console.dir({badresult:result})
            resolve({})
          }
          */
        }
      } else {
        //throw new Error('no '+this.fieldname)
        resolve(data)
      }
    })
  }
}
