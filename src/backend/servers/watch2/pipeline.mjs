/*

glob'er and spot'er use same pipelein with different starting point

what must emit 'basename'

*/

import { FILESPATH } from '../../constants/files.mjs'
import { IDFIELD, indexName } from '../../constants/elastic.mjs'
import { Event } from '../../classes/base/event-levels.mjs'
import { is, isHash, isReadable, creatorName, trimArrayOfStrings } from '../../utils/var.mjs'
import { digestFile } from '../../utils/hasher.mjs'

import { Ask, printProgress } from '../../classes/pipe/dynpipe.mjs'
import { checkFields, checkExists, saveMeta, saveLangs, saveEntities, saveTimeline, saveLine } from '../watch/ask-ela.mjs'
import { Tika } from './ask-tika2.mjs'
//import { askssdeep, askssdeepstr,pingssdeep } from './ask-ssdeep.mjs'
import { File2SSDeep, STR2SSDeep } from '../watch/ask-str-ssdeep.mjs'
import { EntityReqognizer } from '../watch/ask-er.mjs'
import { LanguageDetect } from '../watch/ask-ld.mjs'


import { pipeline as Pipeline, Readable, Writable, Transform } from 'stream'
import { promisify } from 'util'
import { join } from 'path'
import { readdirSync, readFileSync } from 'fs'





class filterBasename extends Transform {
  constructor(options) {
    options = options || {}
    options.objectMode = true
    options.highWaterMark = 1
    super(options);
  }
  _transform(data,e,next) {
      if (isHash(data.basename)) this.push(data)
      //else console.error('not hash:',data.basename)
      next()
  }
}


class Warning extends Error {
  constructor(message,o={}) {
    super(message);
    this.msg = message
    //this.name = this.constructor.name;
    for (const key of Object.keys(o)) this[key] = o[key]
    Error.captureStackTrace(this, this.constructor);
  }
}

function cleanup(obj){
  if (obj) {
    if (typeof obj === 'object') {
      if (Array.isArray(obj)) {
        if (typeof obj[0] === 'object') {
          const r = {}
          for (const i in obj) {
            const tmp = cleanup(obj[i])
            if (tmp) {
              for (const key of Object.keys(tmp)) {
                if (tmp[key]) {

                  if (!r[key]) r[key] = tmp[key]
                  else {
                    if (Array.isArray(r[key])) {
                      r[key] = trimArrayOfStrings([r[key],tmp[key]].flat())
                    } else {
                      r[key] = trimArrayOfStrings([r[key],tmp[key]].flat())
                    }
                    if (Array.isArray(r[key]) && r[key].length === 1 ) r[key] = r[key][0]
                  }
                }
              }
            }
          }
          if (Object.keys(r).length) return r
          else return
        } else return obj.flat()
      } else {
        const r = {}
        for (const key of Object.keys(obj)) {
          const tmp = cleanup(obj[key])
          if (tmp) r[key] = tmp
        }
        if (Object.keys(r).length) return r
        else return
      }
    } else return obj
  } else return
}

class checkFieldsFile extends Transform {
  constructor(logger,dirname,hashFieldame,options) {
    options = options || {}
    options.objectMode = true
    options.highWaterMark = 1
    super(options);
    this.dirname = dirname
    if (hashFieldame) this.hashFieldame = hashFieldame
    else throw new Error('missing hashFieldame')
  }
  async _transform(data,e,next) {
      const pathname = join(this.dirname,data.basename,FILESPATH)
      if ( isReadable(pathname) ) {
        data.__fields = []
        for (const f of readdirSync(pathname)) {
          if (f.endsWith('.json')) {
            const filename = join(pathname,f)
             try {
               const id = await digestFile(filename)
               let doc = cleanup(JSON.parse(readFileSync(filename)))
               doc = cleanup(doc)
               doc.__fieldscontenthash__ = id
               data.__fields.push(doc)
             } catch (error) {
               error.filename = filename
               error.basename = data.basename
               console.error(error)
               this.push(error)
             }
          }
        }

        data.pathname = join(this.dirname,data.basename,data.basename)
        data[this.hashFieldame] = data.basename
        if (data.__fields.length < 1) this.push(new Warning('missing fields file(s)',{pathname, basename:data.basename}))
        else this.push(data)
      } else {
        this.push(new Warning('not readable',{pathname, basename:data.basename}))
      }
      //else console.error('not hash:',data.basename)
      next()
  }
}


class deArrayfy extends Transform {
  constructor(masterhashFieldName,hashFieldame,options) {
    options = options || {}
    options.objectMode = true
    options.highWaterMark = 1
    super(options);
    if (masterhashFieldName) this.masterhashFieldame = masterhashFieldName
    else throw new Error('missing master (main) hash Fieldame')
    if (hashFieldame) this.hashFieldame = hashFieldame
    else throw new Error('missing hash Fieldame')
  }
  _transform(data,e,next) {
      //if (data instanceof Error || data instanceof Event) this.push(data)
      //else {
        if (Array.isArray(data)){
          const length = data.length
          for (const i in data) {
            if (!data[i][this.masterhashFieldame]) {
              console.dir(data[i])
              process.exit(-1)
            }
            if (!data[i][this.hashFieldame]){
              data[i][this.hashFieldame] = data[i][this.masterhashFieldame] + '-' + i + '-' + length
            }
            this.push(data[i])
          }
        } else {
          //if (!data[this.hashFieldame]){
            data[this.hashFieldame] = data[this.masterhashFieldame] + '-' + data.__chunk_no + '-' + data.__chunks_total
          //} else {
            //console.dir(data[this.hashFieldame])
          //}
          this.push(data)
        }
      //}
      next()
  }
}

class unFlatten extends Transform {
  // 'X-TIKA:digest:MD5'-> 'md5'
  constructor(hashFieldame,options) {
    options = options || {}
    options.objectMode = true
    options.highWaterMark = 1
    super(options);
    if (hashFieldame) this.hashFieldame = hashFieldame
    else throw new Error('missing hashFieldame')
    this.separator = ':'
    this.map = {}
  }
  _transform(data,e,next) {
      if (data instanceof Error || data instanceof Event) this.push(data)
      else {
        const unflattened = {}
        for (const key of Object.keys(data)){
          if (key.startsWith('__')) {
            unflattened[key] = data[key]
          } else if (data[key] && !key.includes('EXCEPTION')) {
            const bittes = key.split(this.separator)
            const i = bittes.length -1
            const newname = bittes[i].replace(/\W/gi,'-').toLowerCase()

            if (!this.map[key]) this.map[key] = 0
            this.map[key] += 1
            if (!unflattened[newname]) unflattened[newname] = data[key]
            else {
              if (!Array.isArray(unflattened[newname])) unflattened[newname] = [unflattened[newname]]
              if (!unflattened[newname].includes(data[key])) unflattened[newname].push(data[key])
            }
          }
        }
        this.push(unflattened)
      }
      next()
  }
  _flush(last){
    //this.push({__map__:this.map})
    last()
  }
}







export function runPipeline(state, logger, first, params, tries = 3) {

  logger.info({PIPELINE:params})

  class delSomething extends Transform {
    constructor(fieldname,options) {
      options = options || {}
      options.objectMode = true
      options.highWaterMark = 1
      super(options);
      this.field2delete = fieldname

    }
    _transform(data,e,next) {
        delete data[this.field2delete]
        this.push(data)
        next()
    }
  }

  class allpySomething extends Transform {
    constructor(fieldname,fn,options) {
      options = options || {}
      options.objectMode = true
      options.highWaterMark = 1
      super(options);
      this.field2delete = fieldname
      this.fn = fn

    }
    _transform(data,e,next) {
        data[this.field2delete] = this.fn(data[this.field2delete])
        this.push(data)
        next()
    }
  }


  class Debug extends Transform {
    constructor(name,options) {
      options = options || {}
      options.objectMode = true
      options.highWaterMark = 1
      super(options);
      this.name = name
      this.counter = 0
      this.errors = 0
      this.events = 0
      this.messages = 0
      state[this.name] = {}
      state[this.name].counters = {}
    }

    _transform(data,e,next) {
        //console.log(this.name,JSON.stringify(data))
        this.counter ++
        if (data instanceof Error) this.errors++
        else if (data instanceof Event) this.events++
        else if (Object.keys(data).filter(k=>!k.startsWith('__')).length===0) this.messages ++
        //printProgress(this.name,this.counter,data.basename)
        state[this.name].counters = { total:this.counter,
          data:(this.counter-this.errors-this.messages),
          messages:this.messages,
          events:this.events,
          errors:this.errors}

        this.push(data)

        next()
    }
  }
  return new Promise( async (resolve) => {

  const sendmetrix = ()=>{}
  const checkfields = new checkFields({tries,pool:params.elastics, cluster:params.clustername, index:indexName(params.prefix,params.files,params.suffix),sendmetrix})
  const checkexists = new checkExists({tries,pool:params.elastics, cluster:params.clustername, index:indexName(params.prefix,params.content,params.suffix),sendmetrix})
  const tika = new Tika(params.field,{tries,pool:params.tikas,sendmetrix})
  const basessdeep = new File2SSDeep({tries,pool:params.ssdeeps,fieldname:'pathname',sendmetrix})
  const content2ssdeep = new STR2SSDeep({tries,pool:params.ssdeeps,fieldname:params.content,sendmetrix})
  const snake2ssdeep = new STR2SSDeep({tries,pool:params.ssdeeps,fieldname:'__snake',sendmetrix})
  const plines2ssdeep = new STR2SSDeep({tries,pool:params.ssdeeps,fieldname:'__plines',sendmetrix})
  const erbf = new EntityReqognizer({tries,fieldname:params.content,pool:params.ers,sendmetrix})
  const st = new saveTimeline({tries,pool:params.elastics, cluster:params.clustername, index:indexName(params.prefix,params.timeline,params.suffix),sendmetrix})
  const sline = new saveLine({tries,pool:params.elastics, cluster:params.clustername, index:indexName(params.prefix,params.line,params.suffix),sendmetrix})

  const se = new saveEntities({tries,pool:params.elastics, cluster:params.clustername, index:indexName(params.prefix,params.entities,params.suffix),sendmetrix})
  const sm = new saveMeta({tries,pool:params.elastics, cluster:params.clustername, index:indexName(params.prefix,params.meta,params.suffix),sendmetrix})
  const ld = new LanguageDetect({tries,pool:params.lds,sendmetrix})
  //const ldsdeep = new Ask({name:'ldssdeep',tries,ask:askssdeepstr, ping:pingssdeep, pool:params.ssdeeps,sendmetrix})
  const sl = new saveLangs({tries,pool:params.elastics, cluster:params.clustername, index:indexName(params.prefix,params.content,params.suffix),sendmetrix})


    try {
      let counter = 0
      const pipeline = promisify(Pipeline);
      const pp = await pipeline(
        first,
        new Debug('first'),
        new filterBasename(),
        new Debug('filterBasename'),
        new checkFieldsFile(logger, params.filespath, params.field ),
        new Debug('checkfieldsfile'),
        checkfields,
        new allpySomething('__fields',cleanup),
        new Debug('checkfields'),
        checkexists,
        new Debug('checkexists'),
        //basessdeep,
        //new Debug('ssdeep'),
        tika,
        new Debug('tika'),
        new deArrayfy(params.field,IDFIELD),
        new Debug('deArrayfy'),
        new unFlatten(params.field),
        new Debug('unFlatten'),
        sm,
        new Debug('sm'),
        erbf,
        new Debug('erbf'),
        st,
        new Debug('st'),
        //sline,
        //new Debug('sline'),
        se,
        new Debug('se'),
        content2ssdeep,
        ld,
        new Debug('ld'),
        snake2ssdeep,
        new delSomething('__snake'),
        plines2ssdeep,
        new delSomething('__plines'),
        //ldsdeep,
        new Debug('ldssdeep'),
        sl,
        new Debug('sl'),

        new Writable({objectMode: true, write: (d,e,next) => {
            //console.dir(d)
            if (d instanceof Error) {
              logger.notice(d)
            } else {
              if (Object.keys(d).filter(k=>!k.startsWith('__')).length){
                counter ++
                //printProgress('done',counter,d.basename)
              } else {
                //console.dir(JSON.stringify(d,null,4))
              }
            }

            next()
          }
        })
      );
      console.log('result last -----')
      //console.dir(state)
      console.log(JSON.stringify(state,null,4))
      resolve(true)
    } catch (error){
      console.log('error last -------')
      console.error(error)
      resolve(error)
    }
  })

}
