import { createServer } from "../../classes/base/server.mjs";
import { createCacheRoute } from "../../classes/routes/cache.mjs";
import { createStaticRoute } from "../../classes/routes/static.mjs";
import { createProxyRoute } from "../../classes/routes/proxy.mjs";

// vue || svelte || .. dist directory
const __STATIC__ = "./dist"
const __VENDOR__ = "./dist"

const myserver = createServer({
  roles: "*",
  groups: "*",
  routes: {
    vendor: createCacheRoute({ root: __VENDOR__ }),
    dist: createStaticRoute({ root: __STATIC__ }),
    files: createProxyRoute({
      host: "127.0.0.1",
      port: 7200,
      methods: ["get","head","put"]
    }),
    search: createProxyRoute({
      host: "127.0.0.1",
      port: 6200,
      methods: ["GET"]
    }),
    elastic: createProxyRoute({
      host: "127.0.0.1",
      port: 9200,
      methods: ["GET"]
    }),
    status: {
      get: async (req, res, user, log) => {
        const [search, files, elastic ] = await Promise.all([
          myserver.router.search.ping(),
          myserver.router.files.ping(),
          myserver.router.elastic.ping()
        ]);
        const status = {
          status: search && files ? true : false,
          backends: { search, files, elastic }
        };
        log.info({ returned: status });
        res.write(JSON.stringify({ status }));
      }
    },
    user: {
      get: async (req, res, user, log) => {
        log.info({ headers:req.headers,returned: user });
        res.write(JSON.stringify(user));
      }
    }
  }
});

myserver.router.default = "dist";
myserver.listen();
