SERVER=localhost
PORT=6789
find ./ -type file| head -30 | while read f
do
  hash=$(md5sum "${f}" | awk '{print $1}')
  filename=$(basename "${f}")
  filenamehash=$(echo -en "${filename}" | md5sum | awk '{print $1}')
  echo " ${hash} ${filename} ${f}"

  code=$(curl --silent --write-out %{response_code} -XPUT \
  -F hash=${hash} \
  -F name="${filename}" \
  "http://${SERVER}:${PORT}/${hash}/${filenamehash}")

  echo -e "\n\n $? \n\n ${code}"
done
