import { createMate, MATEAPI, STATSAPI } from '../../classes/base/mate.mjs'
import { PORT as FILESSERVERPORT, PATH as FILESSERVERPATH, HASHFUNCTION as HASHALG, FILESPATH } from '../../constants/files.mjs'
import { HASHFIELD, FILENAMEFIELD, MIMETYPEFIELD, MIMEHUMANFIELD } from '../../constants/elastic.mjs'
import { Busboy } from '../../../../packages/vendor/busboy/index.mjs'

import { isHash } from '../../utils/var.mjs'

import { createHash } from 'crypto'
import { accessSync, constants, createWriteStream, renameSync, mkdirSync, unlinkSync, writeFileSync, readFileSync, createReadStream, readdirSync, utimesSync } from 'fs'
import { join, dirname } from 'path'
import { exec } from 'child_process'
import { request } from 'http'

//const HASHALG = 'md5'
const MAXCAPACITY = 85

function o2s(o) {
  return JSON.stringify(o,function(k, v) { if (v === undefined) { return null; } return v; },'\t')
}


function exists(filename) {
  try {
    accessSync(filename, constants.F_OK);
    return true;
  } catch (e) {
    return false;
  }
}

function writable(filename) {
  try {
    accessSync(filename, constants.W_OK);
    return true;
  } catch (e) {
    return false;
  }
}

function doihaveit(paths, ...parts){
  for (const path of paths) if (exists(join(path, ...parts))) return path
  return false
}

function update(filename, o) {
  let old = [];
  let needsupdate = {}
  if (exists(filename)) {
    try {
      old = JSON.parse(readFileSync(filename));
      if (!Array.isArray(old)) old = [old];
    } catch (error) {
      throw new Error("update failed " + error.message);
    }
  }
  // check is three new or updated field
    for (const key of Object.keys(o)){
      if (key !== "__timestamp__") {
        needsupdate[key] = true
        for (const oo of old){
          if (oo[key] && o[key] === oo[key]) {
            needsupdate[key] = false
            break
          }
        }
      }
    }
    for (const key of Object.keys(needsupdate)) {
      if (needsupdate[key] === true) {
        needsupdate = true
        break
      }
    }
    if (!(needsupdate === true)) {
      // nothing to update
      return
    }

    o["__timestamp__"] = new Date();
    old.push(o);
    try {
      writeFileSync(filename, JSON.stringify(old));
      // as watch  recursive option is not supported
      // we touch directory
      const time = new Date();
      utimesSync(dirname(dirname(filename)),time,time)
    } catch (error) {
      throw new Error("update failed " + error.message);
    }
}

function diskUsage(path) {
  return new Promise ( (resolve) => {
    if (!exists(path)) {
      resolve(new Error('not exists ' + path))
      return
    }
      const du = `du -ks ${path}`
      exec(du, {stdio:['inherit','pipe','pipe']}, function(err, output, code) {
        if (!(err === null)) {
          resolve (new Error('df ' + code))
        } else if (output) {
          /*
          25548	/tmp/kala
          */
          let [used,path ] = output.trim().split('\t')
          if (!used) {
            resolve(new Error('du no used'))
          } else {
            try {
              used = Number(used.trim())
              resolve({used})
            } catch (e) {
              resolve( new Error('du used not a number '))
            }
          }
        } else {
          resolve( new Error('du unknown'))
        }
      })
  })
}

function diskFree(path) {
  return new Promise ( (resolve) => {
    if (!exists(path)) {
      resolve(new Error('not exists ' + path))
      return
    }
    /*
    -k      Use 1024-byte (1-Kbyte) blocks, rather than the default.  Note that this overrides the BLOCKSIZE specification from the environment.
    -P      Use (the default) 512-byte blocks.  This is only useful as a way to override an BLOCKSIZE specification from the environment.
    */
    const df = `df -kP ${path}`
    exec(df, {stdio:['inherit','pipe','pipe']}, function(err, output, code) {
      if (!(err === null)) {
        resolve (new Error('df ' + code))
      } else if (output) {
        /*
        'Filesystem 1024-blocks      Used Available Capacity  Mounted on',
        '/dev/disk1   243915264 188083924  55575340    78%    /',
        */
        let [filesystem,blocks,used,available,capacity,mounted ] = output.split('\n')[1].trim().replace(/\s{2,}/g, ' ').split(' ')
        if (!capacity) {
          resolve(new Error('df no capacity'))
        } else {
          try {
            capacity = Number(capacity.trim().slice(0, -1))
            resolve({filesystem,blocks:Number(blocks),used:Number(used),available:Number(available),capacity,mounted})
          } catch (e) {
            resolve( new Error('df capacity not a number '))
          }
        }
      } else {
        resolve( new Error('df unknown'))
      }
    })
  })
}

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

// ? move to ...
// used to 'drain' broken .pipe( 's
function drainStream (stream) {
  stream.on('readable', stream.read.bind(stream))
}

function getmimetype(filename) {
  return new Promise ( (resolve) => {
    /*
    -i, --mime
        Causes the file command to output mime type strings rather than the more traditional human readable ones.  Thus it may say 'text/plain; charset=us-ascii' rather
        than ``ASCII text''.
    */
    const file = `file -i ${filename}`
    exec(file, {stdio:['inherit','pipe','pipe']}, function(err, output, code) {
      if (!(err === null)) {
        console.error(err)
        throw err
        resolve ()
      } else if (output) {
        const m = output.split(':')[1].trim()
        const r = m ? m : 'application/octet-stream;'
        resolve(r)
      }
    })
  })
}

function getmimetypehuman(filename) {
  return new Promise ( (resolve) => {
    /*
   traditional human readable
    */
    const file = `file ${filename}`
    exec(file, {stdio:['inherit','pipe','pipe']}, function(err, output, code) {
      if (!(err === null)) {
        console.error(err)
        throw err
        resolve ()
      } else if (output) {
        const m = output.split(':')[1].trim()
        const r = m ? m : 'unknown'
        resolve(r)
      }
    })
  })
}

function obj2multi(fields) {
  const LINE_BREAK = '\r\n'
  let boundary = '------------------------'
  for (var i = 0; i < 16; i++) boundary += Math.floor(Math.random() * 10).toString(16)
  const last = '--' + boundary + '--' + LINE_BREAK
  let str = ''
  //const fields = {filename, fullname:req.url}
  for ( const key of Object.keys(fields)){
    str += '--' + boundary + LINE_BREAK + 'Content-Disposition: form-data; name="'+ key+'"'+ LINE_BREAK
    str += LINE_BREAK+ fields[key] + LINE_BREAK
  }
  str += last
  return { boundary, body:str }
}

function pipeform(options,body,res,log) {
  return new Promise ( resolve => {
    const putform = request(options, function (r) {
      r.on('error', (error) => {
        r.unpipe(res)
        log.warning({pathcfailed:{options,error}})
        res.statusCode = 503
        res.end()
      })
      r.on('abort', (error) => {
        r.unpipe(res)
        log.warning({pathcfailed:{options,error}})
        res.statusCode = 503
        res.end()
      })
      r.on('end',() => {
        log.info({PIPED:{options}})
        resolve()
      })
      // !? will pipe also pass headers
      //res.writeHead(r.statusCode,r.headers)
      r.pipe(res)
    })
    putform.on('error',(error) => {
      log.warning({pathcfailed:{options,error}})
      res.statusCode = 503
      res.end()
    })
    putform.end(body)
  })
}

// ---------------------------------------------------

// du is 'expensive'
// globals for stats

let usage
let lastusagecheck
let usagecache = 2000
let maxrate = 0

const ms = createMate({
  opts : {
           port: p => { return p ? Number(p) : FILESSERVERPORT},
           hash: p => {return p ? p.trim() : HASHFIELD },
           filename: p => {return p ? p.trim() : FILENAMEFIELD },
           paths: p => { return p ? p.split(',') : [FILESSERVERPATH] },
           capacity: p => { return p ? Number(p) : MAXCAPACITY}
  },
  health: () => {
    for (const path of ms.paths) {
      if (writable(path)) return 'rw'
    }
    for (const path of ms.paths) {
      if (exists(path)) return 'ro'
    }
    return 'na'
  },
  stats: async () => {
    const now = Date.now()
    if (!usage || (lastusagecheck + usagecache) < now) {
      usage = []
      lastusagecheck = now
      for (const path of ms.paths) {
        if (writable(path)) {
          const du = await diskUsage(path)
          const df = await diskFree(path)
          usage.push({path,health:'rw',df,du,max:ms.capacity})
        } else if (exists(path)){
          const du = await diskUsage(path)
          usage.push({path,health:'ro',du})
        } else usage.push({path,health:'na'})
      }
    }
    return {usage,maxrate}
  },
  methods:{

    head: async (req,res,log) => {
      const bittes = req.url.replace('//','/').split('/')
      const  contenthash = bittes[1]
      if (! contenthash) {
        res.statusCode = 400
      } else {
        if (!isHash( contenthash)) {
          log.notice({code:415,nothash: contenthash})
          res.statusCode = 415
        } else {
          const namehash = bittes[2] ? bittes[2].split('.')[0] : ''
          if (namehash && !isHash(namehash)) {
            res.statusCode = 415
          } else {
            let path
            if (namehash) path = doihaveit(ms.paths, contenthash,FILESPATH,namehash+'.json')
            else path = doihaveit(ms.paths, contenthash)
            if (path) { // found ;)
              log.info({found: contenthash})
              res.statusCode = 200
            } else {
              if (ms.isMateReq(req.headers)) {
                res.statusCode = 404
              } else { // ask other mates
                const mrs = await ms.matesRequest('HEAD',ms.mates,null, contenthash+'/'+namehash)
                let mate
                for (const mr of mrs) {
                  if (mr.statusCode === 200){
                    if (mate) log.warning({DUPLICATE:{ contenthash,mates:[mate,mr.mate]}})
                    mate = mr.mate
                  }
                  if (mr.error) log.warning({mr})
                }
                if (mate) {
                  res.statusCode = 303
                  res.setHeader("Location",`http://${mate.hostname}:${mate.port}/${ contenthash}/${namehash}`)
                } else {
                  res.statusCode = 404
                } // not found
              } // not here
            } // found
          } // not hash namehash
        } // not hash contenthash
      } // no contenthash
      res.end()
    },

    get: async (req,res,log) => {

      const bittes = req.url.replace('//','/').split('/')
      const contenthash = bittes[1]
      log.info({url:req.url, contenthash})
      if (!contenthash) {
        res.statusCode = 400
        res.end()
      } else {
        if (!(isHash(contenthash))){
          res.statusCode = 415
          res.end()
        } else {
          const path = doihaveit(ms.paths,contenthash)
          if (!path) {
            if (ms.isMateReq(req.headers)) {
              res.statusCode = 404
              res.end()
            } else {
                // ask other mates
                req.pause()
                const mrs = await ms.matesRequest('HEAD',ms.mates,null,contenthash)
                let mate
                for (const mr of mrs) {
                  if (mr.statusCode && mr.statusCode !== 200 && mr.statusCode !== 404) log.emerg({mr})
                  if (mr.error) log.notice({mr})
                  if (mr.statusCode === 200) mate = mr.mate
                }
                if (!mate) {
                  res.statusCode = 404
                  res.end()
                } else {
                  // pipe to mate
                  const headers = req.headers
                  headers['x-mate-hostnames'] = JSON.stringify(ms.mynames)
                  headers['x-mate-port'] = ms.port
                  headers['x-mate-message'] = JSON.stringify({remoteAddress : req.socket.remoteAddress})
                  const options = {
                    hostname: mate.hostname,
                    port: mate.port,
                    path: req.url,
                    method: req.method,
                    headers: headers
                  }
                  const proxy = request(options, function (r) {
                    res.writeHead(r.statusCode,r.headers)
                    // TODO if statusCode != 200
                    r.on('error', (error) => {
                      r.unpipe(res)
                      log.warning({proxyerror:{mate, error}})
                      res.statusCode = 503
                      res.end()
                    })
                    r.on('abort', (error) => {
                      r.unpipe(res)
                      log.warning({proxyerror:{mate, error}})
                      res.statusCode = 503
                      res.end()
                    })
                    r.on('end',() => {
                      log.info({proxydone:mate,options})
                    })
                    r.on('data', chunk => res.write(chunk))
                    //r.pipe(res)
                  })
                  proxy.on('error', (error) => {
                    log.warning({proxyerror:{mate, error}})
                    req.unpipe(proxy)
                    res.statusCode = 503
                    res.end()
                  })
                  req.pipe(proxy)
                  req.resume()
                }
            }
          } else {
            const namehash = bittes[2] ? bittes[2].split('.')[0] : ''
            if (namehash) {
              if (!isHash(namehash)){
                res.statusCode = 415
                res.end()
              } else {
                const path = doihaveit(ms.paths,contenthash,FILESPATH,namehash+'.json')
                if (!path) {
                    res.statusCode = 404
                    res.end()
                } else {
                  const fieldsname = join(path,contenthash,FILESPATH,namehash+'.json')

                  if (bittes[3] && bittes[3].toUpperCase() === 'FIELDS') {
                     // respond with fields
                     // TODO catch pipe errors
                     createReadStream(fieldsname).pipe(res)
                     req.resume()
                     log.info({fieldsname})
                  } else {
                    // respond with file
                    let fields
                    try {
                      fields = JSON.parse(readFileSync(fieldsname))
                    } catch (error) {
                      log.err({fieldsname,error});
                      res.statusCode = 500
                    }
                    if (!(fields && Array.isArray(fields) && fields.length)) {
                      log.err({fieldsname,error:'no fields in fields file'},fields)
                      res.statusCode = 500
                      res.end()
                    } else {
                      if (!(fields[0]['__filename__'] || fields[0]['basename'])) {
                        log.err({fieldsname,error:'no filename in fields'},fields)
                        res.statusCode = 500
                        res.end()
                      } else {
                          // send a file --------------------------------------
                          const filename = fields[0]['__filename__'] ? fields[0]['__filename__'] : fields[0]['basename']
                          const contentfile = join(path,contenthash,contenthash)
                          let mimetype
                          if ((fields[0]['__mimetype__'])) {
                            mimetype = fields[0]['__mimetype__']
                          } else {
                            mimetype = await getmimetype(contentfile)
                          }
                          res.setHeader('Content-Disposition', 'attachment; filename=' + filename)
                          res.setHeader('Content-Type', mimetype)
                          // TODO catch pipe errors
                          createReadStream(contentfile).pipe(res)
                          if (req.headers['x-mate-message']) {
                            const remote = JSON.parse(req.headers['x-mate-message'])
                            log.notice({remote,contenthash,contentfile,namehash,filename,mimetype})
                          } else log.notice({contenthash,contentfile,namehash,filename,mimetype})
                      }
                    }
                  }
                }
              }
            } else {
              // constenhash only, respond with namehash'es list
              const fieldsdir = join(path,contenthash,FILESPATH)
              let fieldsfiles
              try {
                fieldsfiles = readdirSync(fieldsdir)
                for (const file of fieldsfiles) {
                  const hash = file.split('.').shift()
                  if (isHash(hash)) res.write(file.split('.').shift()+'\n')
                  else log.warning({GARBAGE:{fieldsdir,file}})
                }
                log.info({contenthash,fieldsfiles})
              } catch (error) {
                log.err({fieldsdir,error})
              }
              res.end()
            }
          }
        }
      }
    },

    put: async (req, res, log) => {
      // upload
      // curl -T filename http://hostname:port/
      // curl -X PUT -F data=@filename -F somefield=value ... http://hostname:port/
      if (!(req.headers['content-length'])) {
        log.notice({ip:req.socket.remoteAddress, code:415})
        res.statusCode = 415
        res.end()
        return
      } else {
        if (!((req.headers['content-type'] && req.headers['content-type'].startsWith('multipart/form-data')) || (req.url.trim().length > 1))){
          log.notice({ip:req.socket.remoteAddress, code:415})
          res.statusCode = 415
          res.end()
        } else {
          let writeto
          for (const path of ms.paths) {
            if (writable(path)) {
              const df = await diskFree(path)
              if (ms.isMateReq(req.headers)) {
                // let mates patch
                if (df.available > req.headers['content-length'] ) {
                  writeto = path
                  break
                }
              } else {
                // clients can upload up to capacity
                if (df.capacity < ms.capacity ) {
                  writeto = path
                  break
                }
              }
            }
          }
          if (!(writeto)) {
              if (ms.isMateReq(req.headers)) {
                res.statusCode = 507
                res.end()
              } else {
                // ask other mates
                req.pause()
                // TODO special api endpoint !?
                const mrs = await ms.matesRequest('REPORT',ms.mates,null,MATEAPI+'/'+STATSAPI)
                let mate
                // find mate with min capacity used
                for (const mr of mrs) {
                  if (mr.error) log.warning(mr)
                  if (mr.statusCode === 200 && mr.data && mr.data.stats && mr.data.stats.usage ) {
                    let capacity = 100
                    for (const p of mr.data.stats.usage) {
                      // df default is kB
                      if (p.health === 'rw' && (p.df.available * 1024) > req.headers['content-length']) {
                        capacity = Math.min(capacity,p.df.capacity)
                      }
                    }
                    if (capacity < 99 ){
                      if (!mate) {
                        mate = mr.mate
                        mate.capacity = capacity
                      } else {
                        if (mate.capacity > capacity) {
                          mate = mr.mate
                          mate.capacity = capacity
                        }
                      }
                    }
                  }
                }
                if (!mate) {
                    log.emerg({nospace:mrs,})
                    req.resume()
                    res.statusCode = 507
                    res.end()
                } else {
                  log.info({proxy507:mate})
                  const headers = req.headers
                  //headers['x-mate-hostnames'] = JSON.stringify(ms.mynames)
                  //headers['x-mate-port'] = ms.port
                  headers['x-mate-message'] = JSON.stringify({remoteAddress : req.socket.remoteAddress})
                  const options = {
                    hostname: mate.hostname,
                    port: mate.port,
                    path: req.url,
                    method: req.method,
                    headers: headers
                  }
                  const proxy = request(options, function (r) {
                    res.writeHead(r.statusCode,r.headers)
                    // TODO if statusCode != 200
                    r.on('error', (error) => {
                      r.unpipe(res)
                      log.warning({proxyerror:{mate, error}})
                      res.statusCode = 503
                      res.end()
                    })
                    r.on('abort', (error) => {
                      r.unpipe(res)
                      log.warning({proxyerror:{mate, error}})
                      res.statusCode = 503
                      res.end()
                    })
                    r.on('end',() => {
                      log.info({proxydone507:mate})
                      res.end()
                    })
                    r.on('data', chunk => res.write(chunk))
                    //r.pipe(res)
                  })
                  proxy.on('error', (error) => {
                    log.warning({proxyerror:{mate, error}})
                    req.unpipe(proxy)
                    res.statusCode = 503
                    res.end()
                  })
                  req.pipe(proxy)
                  req.resume()
                }
              }

          } else {
            //save it here
            log.info({writeto,u:req.url})
            if (req.headers['content-type'] && req.headers['content-type'].startsWith('multipart/form-data')) {
// -------------------------
              // curl -XPUT -F data=@filename host:port
              req.pause()
              const fields = {}
              const files = []
              const busboy = new Busboy({ headers: req.headers })
              busboy.on('error', (error) => {
                log.notice({busboy:{error,fields,files}})
                res.emit('error',error)
                req.unpipe(busboy)
                drainStream(req)
                busboy.removeAllListeners()
              })

              busboy.on('field', (fieldname, buffer, fieldnameTruncated, valTruncated, encoding, mimetype) => {
                const val = buffer.toString()
                if (fields[fieldname]) {
                  if (!Array.isArray(fields[fieldname])){
                    fields[fieldname] = [fields[fieldname]]
                    fields[fieldname].push(val)
                  }  else fields[fieldname].push(val)
                } else fields[fieldname] = val

              })

              busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
                const hasher = createHash(HASHALG)
                const created = JSON.stringify(new Date).replace(/"/g,'')
                const start = process.hrtime.bigint()
                const uid = guid()
                const spoolname = join(writeto,uid)
                const spoolfile = createWriteStream(spoolname)
                spoolfile.on('error', (error) => {
                  file.emit('error',error)
                })
                file.on('error', (error) => {
                  log.err({fieldname, filename, encoding, mimetype,error})
                  busboy.emit('error',error)
                })
                let chunks = 0
                let size = 0
                file.on('data', (data) => {
                  hasher.update(data)
                  chunks ++
                  size += data.length
                })
                file.on('end', () => {
                  const took = Number(process.hrtime.bigint() - start) //nanoseconds
                  //const hash = hasher.digest('hex')
                  //           bits / nano -> micro -> milli - sec
                  const rate = (size * 8) / (took / 1000 / 1000 / 1000)
                  const Mbps = rate / 1024 / 1024
                  const MBps = Mbps/8
                  maxrate = Math.max(maxrate,MBps)
                  const tmp = {spoolname, uid, chunks, size, took, created,Mbps,MBps}
                  tmp[ms.hash] = hasher.digest('hex')
                  tmp[ms.filename] = filename
                  tmp['__namehash__'] = createHash(HASHALG).update(filename).digest("hex")
                  files.push(tmp)
                })
                file.pipe(spoolfile)
              })
              // - * - * - * - * -
              busboy.on('finish', async () => {
                if (files.length < 1) { // no files, just fields
                  const bittes = req.url.replace('//','/').split('/')

                  const  contenthash = bittes[1]
                  if (! contenthash) {
                    res.statusCode = 400
                    res.end()
                    return
                  }
                  if (!isHash( contenthash)) {
                    log.warning({code:415,nothash: contenthash})
                    res.statusCode = 415
                    res.end()
                    return
                  } else {
                    let path
                    const namehash = bittes[2] ? bittes[2].split('.')[0] : ''
                    if (namehash && !isHash(namehash)) {
                      res.statusCode = 415
                      res.end()
                      return
                    }
                    path = doihaveit(ms.paths, contenthash)
                    if (path) { // we have it, update fields
                      const fieldsfilename =  join(path, contenthash,FILESPATH,namehash+'.json')
                      fields[ms.hash] =  contenthash
                      fields['__namehash__'] = namehash
                      try {
                        update(fieldsfilename,fields)
                        log.info({BBFO:{ contenthash,namehash,fields}})
                        res.write('http://'+ms.hostname+':'+ms.port+'/'+ contenthash+'/'+namehash+'\n')
                      } catch (error) {
                        log.err({update:{error, contenthash,namehash,fields}})
                        res.statusCode = 500
                      }
                      res.end()
                    } else {
                      if (ms.isMateReq(req.headers)) {
                        res.statusCode = 404
                      } else { // ask other mates
                        const mrs = await ms.matesRequest('HEAD',ms.mates,null, contenthash)
                        let mate
                        for (const mr of mrs) {
                          if (mr.statusCode === 200) mate = mr.mate
                          if (mr.statusCode !== 200 && mr.statusCode !== 404) log.warning({mr})
                        }
                        if (!mate) {
                          log.notice({pathc404:{ contenthash,namehash}})
                          res.statusCode = 404
                          res.end()
                        } else {
                            const { boundary, body } = obj2multi(fields)
                            const options = {
                              hostname: mate.hostname,
                              port: mate.port,
                              path: '/'+ contenthash+'/'+namehash,
                              method: 'PUT',
                              headers: {'content-type': 'multipart/form-data; boundary='+boundary,
                                        'content-length': body.length,
                                        'x-mate-hostnames': JSON.stringify(ms.mynames),
                                        'x-mate-port': ms.port,
                                        'x-mate-yourname': mate.hostname,
                                        'x-mate-message':JSON.stringify({remoteAddress:req.socket.remoteAddress})}
                            }
                            const putform = request(options, function (r) {
                              res.writeHead(r.statusCode,r.headers)
                              r.on('error', (error) => {
                                r.unpipe(res)
                                log.warning({pathcfailed:{mate,error,contenthash,namehash}})
                                res.statusCode = 503
                                res.end()
                              })
                              r.on('abort', (error) => {
                                r.unpipe(res)
                                log.warning({pathcfailed:{mate,error,contenthash,namehash}})
                                res.statusCode = 503
                                res.end()
                              })
                              r.on('end',() => {
                                log.info({patched:{mate, contenthash,namehash,fullname:req.url}})
                              })
                              r.pipe(res)
                            })
                            putform.on('error',(error) => {
                              log.warning({pathcfailed:{mate,error,contenthash,namehash}})
                              res.statusCode = 503
                              res.end()
                            })
                            putform.end(body)
                        }
                      }
                    }
                  }
                } else { // same field  set for every file
                  for (const file of files) {
                    const cloned = Object.assign({}, fields)
                    for (const key of Object.keys(cloned)) {
                      if (cloned[key] === file[ms.hash]) delete cloned[key]
                    }
                    cloned[ms.hash] = file[ms.hash] // overwrite always
                    cloned[ms.filename] = file[ms.filename]
                    cloned['__namehash__'] = file['__namehash__']
                    //log.info({file, cloned})
                    let mate
                    const currentpath = doihaveit(ms.paths,file[ms.hash])
                    if (!currentpath) { // check mates first
                      if (ms.isMateReq(req.headers)) {
                        res.statusCode = 404
                        res.end()
                      } else { // ask other mates
                        cloned[MIMETYPEFIELD] =  await getmimetype(file.spoolname)
                        cloned[MIMEHUMANFIELD] = await getmimetypehuman(file.spoolname)
                        const mrs = await ms.matesRequest('HEAD',ms.mates,null,file[ms.hash])
                        for (const mr of mrs) {
                          if (mr.statusCode === 200) mate = mr.mate
                          if (mr.statusCode !== 200 && mr.statusCode !== 404) log.warning({mr})
                        }
                        if (mate) {
                          const contenthash = file[ms.hash]
                          const namehash = file['__namehash__']
                          log.info({BBFILEONOTHER:{mate,contenthash,namehash,fields}})
                          const  { boundary, body } = obj2multi(cloned)
                          const options = {
                            hostname: mate.hostname,
                            port: mate.port,
                            path: '/'+contenthash+'/'+namehash,
                            method: 'PUT',
                            headers: {'content-type': 'multipart/form-data; boundary='+boundary,
                                      'content-length': body.length,
                                      'x-mate-hostnames': JSON.stringify(ms.mynames),
                                      'x-mate-port': ms.port,
                                      'x-mate-yourname': mate.hostname,
                                      'x-mate-message':JSON.stringify({remoteAddress:req.socket.remoteAddress})}
                          }
                          await new Promise ( resolve => {
                            const putform = request(options, function (r) {
                              r.on('error', (error) => {
                                r.unpipe(res)
                                log.warning({pathcfailed:{options,error}})
                                res.statusCode = 503
                                res.end()
                                resolve()
                              })
                              r.on('abort', (error) => {
                                r.unpipe(res)
                                log.warning({pathcfailed:{options,error}})
                                res.statusCode = 503
                                res.end()
                                resolve()
                              })
                              const chunks = []
                              r.on('data', chunk => res.write(chunk) )
                              r.on('end',() => {
                                log.info({PIPED:{options}})
                                resolve()
                              })

                            })
                            putform.on('error',(error) => {
                              log.warning({pathcfailed:{options,error}})
                              res.statusCode = 503
                              res.end()
                              resolve()
                            })
                            putform.end(body)
                          })
                          try {
                            unlinkSync(file.spoolname)
                          } catch (error) {
                            log.warning({unlinkSync:{file:file.spoolname,error,contenthash}})
                          }
                        }
                      }
                    }

                    if (currentpath || !mate) { // save it here
                      if (currentpath) {
                        // old, just update
                        const fieldsfilename =  join(currentpath,file[ms.hash],FILESPATH,file['__namehash__']+'.json')
                        try {
                          update(fieldsfilename,cloned)
                          log.info({exists2:{ contenthash:file[ms.hash],namehash:file['__namehash__'],cloned}})
                          res.write('http://'+ms.hostname+':'+ms.port+'/'+file[ms.hash]+'/'+file['__namehash__']+'\n')
                        } catch (error) {
                          log.err({filename:fieldsfilename,error})
                        }
                        try {
                          unlinkSync(file.spoolname)
                        } catch (error) {
                          log.warning({unlinkSync:{file:file.spoolname,error,contenthash:file[ms.hash]}})
                        }
                      } else {
                        log.info({NEW:{contenthash:file[ms.hash],namehash:file['__namehash__']}})
                        const from = file.spoolname
                        const writeto = dirname(from)
                        const to = join(writeto,file[ms.hash],file[ms.hash])
                        try {
                          if (!exists(join(writeto,file[ms.hash]))) mkdirSync(join(writeto,file[ms.hash]))
                          file[MIMETYPEFIELD] = await getmimetype(from)
                          if (!exists(join(writeto,file[ms.hash],FILESPATH))) mkdirSync(join(writeto,file[ms.hash],FILESPATH))
                          renameSync(from,to)
                          file[MIMEHUMANFIELD] = await getmimetypehuman(to)
                        } catch (error) {
                          log.emerg({renameSync:{from,to,error}})
                          res.statusCode = 500
                          res.end()
                          return
                        }
                        const fieldsfilename =  join(writeto,file[ms.hash],FILESPATH,file['__namehash__']+'.json')
                        try {
                          update(fieldsfilename,cloned)
                          log.info({new:{contenthash:file[ms.hash],namehash:file['__namehash__'],cloned}})
                          res.write('http://'+ms.hostname+':'+ms.port+'/'+file[ms.hash]+'/'+file['__namehash__']+'\n')
                          //res.end()
                        } catch (error) {
                          log.err({filename:fieldsfilename,error})
                          res.statusCode = 500
                          res.end()
                          return
                        }

                      }
                    }

                  } // done with all files
                  res.end('koik')
                }
              })

              req.pipe(busboy);
              req.resume()

// ************************

            } else if (req.url.trim().length > 1 ) {
              // curl -T filename host:port/[fullpath]
              // TODO handle case when filename and fullpath end do not match
              const filename = req.url.trim().split('/').pop()
              let fullpath = req.url.slice(0,req.url.lastIndexOf('/'))
              const namehash = createHash(HASHALG).update(filename).digest("hex")
              log.notice({namehash,filename,ip:req.socket.remoteAddress, upload:req.url})
              const hasher = createHash(HASHALG)
              const created = JSON.stringify(new Date).replace(/"/g,'')
              const start = process.hrtime.bigint()
              const uid = guid()
              const spoolname = join(writeto,uid)
              const spoolfile = createWriteStream(spoolname)
              spoolfile.on('error', (error) => {
                req.emit('error',error)
              })
              req.on('error', (error) => {
                log.err({filename, error})
                res.statusCode = 500
                res.end()
              })
              let chunks = 0
              let size = 0
              req.on('data', (data) => {
                hasher.update(data)
                chunks ++
                size += data.length
              })
              req.on('end', async () => {
                const took = Number(process.hrtime.bigint() - start) //nanoseconds
                const rate = (size * 8) / (took / 1000 / 1000 / 1000)
                const Mbps = rate / 1024 / 1024
                const MBps = Mbps/8
                maxrate = Math.max(maxrate,MBps)
                const tmp = {spoolname, uid, chunks, size, took, created, Mbps, MBps}
                const contenthash = hasher.digest('hex')
                const currentpath = doihaveit(ms.paths,contenthash)
                const cloned = {}
                if (currentpath) { // we have it, so delete spool and update fields file
                  try {
                    cloned[MIMETYPEFIELD] =  await getmimetype(spoolname)
                    cloned[MIMEHUMANFIELD] = await getmimetypehuman(spoolname) // waste a little bit of  time
                    unlinkSync(spoolname)
                  } catch (error) {
                    log.warning({unlinkSync:{file:spoolname,error,contenthash}})
                  }

                  cloned[ms.hash] = contenthash
                  cloned[ms.filename] = filename
                  cloned['__namehash__'] = namehash
                  if (fullpath) cloned['__path__'] = fullpath
                  const fieldsfilename =  join(writeto,contenthash,FILESPATH,namehash+'.json')
                  try {
                    update(fieldsfilename,cloned)
                    log.info({exists:{contenthash,namehash,cloned}})
                    res.write('http://'+ms.hostname+':'+ms.port+'/'+contenthash+'/'+namehash+'\n')
                    res.end()
                  } catch (error) {
                    log.err({filename:fieldsfilename,error})
                    res.statusCode = 500
                    res.end()
                    return
                  }
                } else {
                  // we do not have it, maybe someone else has it
                  const mrs = await ms.matesRequest('HEAD',ms.mates,null,contenthash)
                  let mate
                  for (const mr of mrs){
                    if (mr.error) log.warning(mr)
                    if (mr.statusCode === 200) {
                      if (mate) log.warning({DUPLICATE:{contenthash,mates:[mate,mr.mate]}})
                      mate = mr.mate
                    }
                  }
                  if (mate) {  // someone else has it, just update fields
                    const fields = {}
                    fields[ms.hash] = contenthash
                    fields[ms.filename] = filename
                    fields['__namehash__'] = namehash
                    fields[MIMETYPEFIELD] = await getmimetype(spoolname)
                    fields[MIMEHUMANFIELD] = await getmimetypehuman(spoolname)
                    if (fullpath) fields['__path__'] = fullpath
                    const  { boundary, body } = obj2multi(fields)
                    const options = {
                      hostname: mate.hostname,
                      port: mate.port,
                      path: '/'+contenthash+'/'+namehash,
                      method: 'PUT',
                      headers: {'content-type': 'multipart/form-data; boundary='+boundary,
                                'content-length': body.length,
                                'x-mate-hostnames': JSON.stringify(ms.mynames),
                                'x-mate-port': ms.port,
                                'x-mate-yourname': mate.hostname,
                                'x-mate-message':JSON.stringify({remoteAddress:req.socket.remoteAddress})}
                    }
                    await pipeform(options, body, res, log)
                    try {
                      unlinkSync(spoolname)
                    } catch (error) {
                      log.warning({unlinkSync:{file:spoolname,error,contenthash}})
                    }
                  } else { // no one has it, or not avail, so we store it now as new
                    const from = spoolname
                    const to = join(writeto,contenthash,contenthash)
                    const cloned = {}
                    try {
                      if (!exists(join(writeto,contenthash))) mkdirSync(join(writeto,contenthash))
                      cloned[MIMETYPEFIELD] = await getmimetype(from)
                      if (!exists(join(writeto,contenthash,FILESPATH))) mkdirSync(join(writeto,contenthash,FILESPATH))
                      renameSync(from,to)
                      cloned[MIMEHUMANFIELD] = await getmimetypehuman(to)
                    } catch (error) {
                      log.emerg({renameSync:{from,to,error}})
                      res.statusCode = 500
                      res.end()
                      return
                    }
                    // make fields
                    cloned[ms.hash] = contenthash
                    cloned[ms.filename] = filename
                    cloned['__namehash__'] = namehash
                    if (fullpath) cloned['__path__'] = fullpath
                    const fieldsfilename =  join(writeto,contenthash,FILESPATH,namehash+'.json')
                    try {
                      update(fieldsfilename,cloned)
                      log.info({new:{contenthash,namehash,cloned}})
                      res.write('http://'+ms.hostname+':'+ms.port+'/'+contenthash+'/'+namehash+'\n')
                      res.end()
                    } catch (error) {
                      log.err({filename:fieldsfilename,error})
                      res.statusCode = 500
                      res.end()
                      return
                    }
                  }

                }
              })
              req.pipe(spoolfile)
              req.resume()

            } else {
              log.emerg('SHOULDNOTEVERHAPPEN')
            }
          }
        }
      }
    } // end put
  }
})

ms.listen(()=>{})
