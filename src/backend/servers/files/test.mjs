import { createServer } from 'http';
import { inspect } from 'util';
import { Busboy } from '../../../../packages/vendor/busboy/index.mjs';
import cluster from 'cluster'
//import { performance, PerformanceObserver } from 'perf_hooks'
import { cpus } from 'os'
import { createHash } from 'crypto'

const numCPUs = cpus().length;
const MINCPUS = 2
const MAXCPUS = numCPUs - MINCPUS
const MAXCLIENTS = 4
const FORKIF = 70 // if clientscount > && fork if resourceUsage >

const HASHALG = 'sha256'

const cpuConsumptionPercent = () => JSON.parse(process.report.getReport()).resourceUsage.cpu.cpuConsumptionPercent

function getUsage(oldUsage) {
  let usage
  if (oldUsage && oldUsage._start) {
    usage = Object.assign({}, process.cpuUsage(oldUsage._start.cpuUsage));
    usage.time = Date.now() - oldUsage._start.time;
  } else {
    usage = Object.assign({}, process.cpuUsage());
    usage.time = process.uptime() * 1000; // s to ms
  }
  usage.percent = (usage.system + usage.user) / (usage.time * 10);
  usage._start =  {
      cpuUsage: process.cpuUsage(),
      time: Date.now()
    }
  
  return usage;
}

let cpuUsage = 0
let last
let current

function notify(message){
  //process.cpuUsage()
  if (process.send) process.send({ pid:process.pid, resourceUsage:cpuUsage, message})
  //if (process.send) process.send({ pid:process.pid, resourceUsage:JSON.parse(process.report.getReport()).resourceUsage, message });
  //  if (process.send) process.send({ pid:process.pid, resourceUsage:process.cpuUsage(), message });
  else console.log({resourceUsage:cpuUsage, message })
}
/*
const obs = new PerformanceObserver((list, observer) => {
  console.dir(list.getEntries()[0].duration);
  //observer.disconnect();
});
obs.observe({ entryTypes: ['measure'], buffered: true })
*/

const httpServer = createServer((req, res) => {
  //console.dir(req.method)
  if (req.method === 'PUT') {
    req.pause()
    //performance.mark('start')
    notify('start')
    const fields = []
    const busboy = new Busboy({ headers: req.headers });
    busboy.on('error', (error) => {
      console.log(`ERROR [${error}]: value: ${inspect(error)}`);
      console.error(error)
    });
    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
      //console.log(`File [${fieldname}]: filename: ${filename}, encoding: ${encoding}, mimetype: ${mimetype}`);
      const hasher = createHash('md5')
      file.on('data', (data) => {
        hasher.update(data)
        //console.log(`File [${fieldname}] got ${length} bytes`);
      });
      file.on('end', () => {
        //console.log(filename, hasher.digest('hex'))
        //console.log(`File [${fieldname}] Finished`);
      });
    });
    busboy.on('field', (fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) => {
      //console.log(`Field [${fieldname}]: value: ${inspect(val)}`);
      fields.push({fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype})
    });
    busboy.on('finish', () => {
      //console.log(process.pid +' Done parsing form!');
      res.writeHead(200, { Connection: 'close', Location: '/' });
      res.end();
      //performance.mark('end')
      //performance.measure('took', 'start', 'end');
      notify('end')

    });
    req.pipe(busboy);
    req.resume()
  } else if (req.method === 'GET') {
    res.writeHead(200, { Connection: 'close' });
    res.end('<html><head></head><body>\
               <form method="PUT" enctype="multipart/form-data">\
                <input type="text" name="textfield"><br />\
                <input type="file" name="filefield"><br />\
                <input type="submit">\
              </form>\
            </body></html>');
  }
})
httpServer.on('listening', () => {
  if (!process.send) console.log('running in single mode')
})

/*
httpServer.on('error', (e) => {
  if (!process.send) {
    console.error(e)
  }
  else {
    notify('error')
    console.log(e.code)
    console.dir(e)
    process.exit(e.code)
  }

})
*/

const workers = {}
if (cluster.isMaster && numCPUs > MINCPUS) {
  // start with single worker
  const worker = cluster.fork()
  workers[worker.process.pid] = {}
  workers[worker.process.pid].worker = worker
  workers[worker.process.pid].jobs = []

  cluster.on('error', (e) => {
    console.log('cluster error',e)
    console.error(e)
  })

  cluster.on('exit', (deadWorker, code, signal) => {
    var oldPID = deadWorker.process.pid;
    delete workers[oldPID]
    console.log(signal, code, oldPID+' worker exit ..', Object.keys(workers));
    if (Object.keys(workers).length === 0) {
      if (code > 0) {
        console.error('worker can not be started', code, signal)
        process.exit(1)
      }
      console.log('starting new worker, because none left')
      const worker = cluster.fork()
      workers[worker.process.pid] = {}
      workers[worker.process.pid].worker = worker
      workers[worker.process.pid].jobs = []
    }
  });
  cluster.on('listening', (worker, address) => {
    console.log(worker.process.pid,'listening',address)
  });
  cluster.on('message', (worker, message, handle) => {
    if (message.message === 'start') {
      if (workers[worker.process.pid].timer) clearTimeout(workers[worker.process.pid].timer)
      workers[worker.process.pid].jobs.push(message.resourceUsage)
      if(workers[worker.process.pid].jobs.length > MAXCLIENTS) {
      if (Object.keys(workers).length < MAXCPUS && message.resourceUsage > FORKIF){
          const wrkr = cluster.fork()
          workers[wrkr.process.pid] = {}
          workers[wrkr.process.pid].worker = wrkr
          workers[wrkr.process.pid].jobs = []
          console.log(wrkr.process.pid, 'new worker', Object.keys(workers))
      }
      }
    } else if (message.message === 'end') {
      workers[worker.process.pid].jobs.shift()
      console.log(message,workers[worker.process.pid].jobs.length)
      if (workers[worker.process.pid].jobs.length === 0 ) {
        if (Object.keys(workers).length > 1) {
        workers[worker.process.pid].timer = setTimeout(() => {
          if (Object.keys(workers).length > 1) {
            workers[worker.process.pid].worker.kill()
            console.log(worker.process.pid,'worker idle ..')
          }
        }, 1000);
        }
      }

    } else console.log(message)
  })

} else {
  // All the regular app code goes here
  last = getUsage()
  setInterval(()=>{
    last = getUsage(last)
    //console.dir(last)
    cpuUsage = last.percent
  },1000)
  try {
    httpServer.listen(8000)
  } catch (e) {
    console.log('catch ',e.message)
    //console.error(e)
    //notify(e.message)
  }
}
