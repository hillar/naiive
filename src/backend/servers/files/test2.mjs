import { createClusterHTTPServer } from '../../classes/base/clusterserver.mjs'
import { parseHostnamePort } from '../../utils/var.mjs'
import { PORT as FILESSERVERPORT } from '../../constants/files.mjs'
import { HASHFIELD, FILENAMEFIELD, FILESINDEX } from '../../constants/elastic.mjs'
import { Busboy } from '../../../../packages/vendor/busboy/index.mjs'
import { createHash } from 'crypto'
import { accessSync, constants, createWriteStream, renameSync, mkdirSync, unlinkSync, writeFileSync, readFileSync } from 'fs'
import { join, dirname } from 'path'
import { exec } from 'child_process'

const HASHALG = 'md5'

function isHash(hash,alg=HASHALG){
  switch (alg){
    case 'md5':
      return (/^[a-fA-F0-9]{32}$/).test(hash)
      break
    case 'sha256':
      return (/^[a-fA-F0-9]{64}$/).test(hash)
      break
    default:
      return false
  }
}


function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

// ? move to ...
// used to 'drain' broken .pipe( 's
function drainStream (stream) {
  stream.on('readable', stream.read.bind(stream))
}

function getmimetype(filename) {
  return new Promise ( (resolve) => {
    /*
    -i, --mime
        Causes the file command to output mime type strings rather than the more traditional human readable ones.  Thus it may say 'text/plain; charset=us-ascii' rather
        than ``ASCII text''.
    */

    const file = `file -i ${filename}`
    /*
    if (!canRead(filename)) {
      resolve(new Error('not exists '+filename))
      return
    }
    */
    exec(file, {stdio:['inherit','pipe','pipe']}, function(err, output, code) {
      if (!(err === null)) {
        console.error(err)
        throw err
        resolve ()
      } else if (output) {
        const m = output.split(':')[1].trim()
        const r = m ? m : 'application/octet-stream;'
        resolve(r)
      }
    })
  })
}

function getmimetypehuman(filename) {
  return new Promise ( (resolve) => {
    /*
   traditional human readable
    */

    const file = `file ${filename}`

    exec(file, {stdio:['inherit','pipe','pipe']}, function(err, output, code) {
      if (!(err === null)) {
        console.error(e)
        throw err
        resolve ()
      } else if (output) {
        const m = output.split(':')[1].trim()
        const r = m ? m : 'unknown'
        resolve(r)
      }
    })
  })
}

function exists (filename) {
  //console.dir(filename)
  try {
    accessSync(filename, constants.F_OK)
    return true
  } catch (e) {
    return false
  }
}

function doihaveit(paths, ...parts){
  for (const path of paths){

    if (exists(join(path, ...parts))) return path
  }
  return false
}

function update(filename,o) {
  let old = []
  if (exists(filename)) {
    try {
    old = JSON.parse(readFileSync(filename))
    if (!(Array.isArray(old))) old = [old]
    } catch (error) {
      throw new Error('update failed ' + error.message)
    }
  }
  old.push(o)
  try {
    writeFileSync(filename,JSON.stringify(old))
  } catch (error) {
    throw new Error('update failed '+ error.message)
  }
}

function whohasit(mates,hash) {
  for (const mate of mates) {

  }
  return false
}

const myserver = createClusterHTTPServer({
      about : async () => {
        // ela = await clusterhealt(myserver.elastics)
        return {hash_field_name:myserver.hash}
      },
      opts : {
               hash: p => {return p ? p.trim() : HASHFIELD },
               filename: p => {return p ? p.trim() : FILENAMEFIELD },
               paths: p => { return p ? p.split(',') : ['/files'] },
               mates: p => { return p ? p.split(',') : ['/files'] }


      },
      methods: {
        put: {
          fn:async (req, res, log) => {
            if (!(req.headers['content-type'] && req.headers['content-type'].startsWith('multipart/form-data'))) return
            req.pause()
            const fields = {}
            const files = []
            const busboy = new Busboy({ headers: req.headers });
            busboy.on('error', (error) => {
              //console.log(`ERROR [${error}]: value: ${inspect(error)}`);
              //console.error(error)
              res.emit('error',error)
              log.err({busboy:error.message})
              req.unpipe(busboy)
              drainStream(req)
              busboy.removeAllListeners()
            });
            busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
              //console.log(`File [${fieldname}]: filename: ${filename}, encoding: ${encoding}, mimetype: ${mimetype}`);
              const hasher = createHash(HASHALG)
              const created = JSON.stringify(new Date).replace(/"/g,'')
              const start = process.hrtime.bigint()
              const uid = guid()
              const spoolname = join(myserver.paths[0],uid)
              const spoolfile = createWriteStream(spoolname)
              spoolfile.on('error', (error) => {
                file.emit('error',error)
              })
              file.on('error', (error) => {
                log.err({fieldname, filename, encoding, mimetype,error})
                busboy.emit('error',error)
              })
              let chunks = 0
              let size = 0
              file.on('data', (data) => {
                hasher.update(data)
                chunks ++
                size += data.length
              })
              file.on('end', () => {
                const took = Number(process.hrtime.bigint() - start) //nanoseconds
                //const hash = hasher.digest('hex')
                //           bits / nano -> micro -> milli - sec
                // const rate = (size * 8) / (took / 1000 / 1000 / 1000)
                // const Mbs = rate / 1024 / 1024
                const tmp = {spoolname, uid, chunks, size, took, created}
                tmp[myserver.hash] = hasher.digest('hex')
                tmp[myserver.filename] = filename
                files.push(tmp)
              })
              file.pipe(spoolfile)
            });
            busboy.on('field', (fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) => {
              fields[fieldname] = val
            });
            busboy.on('finish', async () => {
              //log.info({fields})
              const path = myserver.paths[0]
              if (files.length > 0) { // we have at least one file
                for (const file of files) {

                  const cloned = Object.assign({}, fields)
                  for (const key of Object.keys(cloned)) {
                    if (cloned[key] === file[myserver.hash]) delete cloned[key]
                  }
                  cloned[myserver.hash] = file[myserver.hash] // overwrite always
                  cloned[myserver.filename] = file[myserver.filename] // ? more than one filename
                  //log.info({file, cloned})
                  const currentpath = doihaveit(myserver.paths,file[myserver.hash],file[myserver.hash])
                  if (currentpath) { //yes, it is here already
                    try {
                      unlinkSync(file.spoolname)
                    } catch (error) {
                      log.warning({unlinkSync:{file:file.spoolname,error}})
                      // res.writeHead(500)
                      // res.end()
                      // ? return
                      /*
                      [0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{8}
                      echo "leftovers $(ls -A | grep -v -e "[0-9a-f]\{32\}"| wc -l)"
                      ls -A | grep -v -e "[0-9a-f]\{32\}" | while read f; do rm "${f}" ; done
                      $(ls -A | grep -v -e "[0-9a-f]\{32\}" || wc -l) || echo 'error, still some leftovers'
                      */
                    }
                    file['name-hash'] = createHash(HASHALG).update(file[myserver.filename]).digest("hex")
                    //const nameHash = createHash(HASHALG).update(file[myserver.filename]).digest("hex")
                    //console.log('fields update',file[myserver.filename],currentpath,file[myserver.hash],FILESINDEX,nameHash)
                    console.log(currentpath,FILESINDEX,file['name-hash'])
                    const fieldsfilename =  join(currentpath,file[myserver.hash],FILESINDEX,file['name-hash']+'.json')
                    try {
                      update(fieldsfilename,cloned)
                      log.info({duplicate:{contentHash:file[myserver.hash],nameHash:file['name-hash'],cloned}})
                    } catch (error) {
                      log.err({filename:fieldsfilename,error})
                    }
                    //saveFields(join(path,file[myserver.hash],nameHash+'.json'),fields)
                  } else { // new or someone else has it ?
                    const someonehasit = whohasit(myserver.mates,file[myserver.hash])
                    if (someonehasit) {

                      //sendFields(someonehasit.hostname,someonehasit.port,file,fields)

                    } else { // no one has it, save new
                      const from = join(path,file.uid)
                      const to = join(path,file[myserver.hash],file[myserver.hash])
                      try {
                        mkdirSync(join(path,file[myserver.hash]))
                        renameSync(from,to)
                        res.writeHead(200)
                        res.write(file[myserver.hash]+'\n')
                      } catch (error) {
                        log.emerg({renameSync:{from,to,error}})
                        res.writeHead(500)
                        res.end()
                        return
                      }

                      const currentpath = dirname(to)
                      const metafilename = join(currentpath,'meta.json')

                      //file[myserver.hash] = file[myserver.hash]
                      file['content-type'] = await getmimetype(to)
                      file['content-type-human'] = await getmimetypehuman(to)
                      file['name-hash'] = createHash(HASHALG).update(file[myserver.filename]).digest("hex")
                      const fieldsfilename =  join(currentpath,FILESINDEX,file['name-hash']+'.json')
                      delete file.spoolname
                      delete file.uid
                      try {
                        writeFileSync(metafilename, JSON.stringify(file))
                        log.info({new:{file}})
                      } catch (error) {
                        log.err({writeFileSync:{filename:metafilename,error}})
                      }
                      try {
                          mkdirSync(join(currentpath,FILESINDEX))
                          //writeFileSync(fieldsfilename, JSON.stringify([cloned]))
                          update(fieldsfilename,cloned)
                          log.info({new:{fields}})
                      } catch (error) {
                        log.err({filename:fieldsfilename,error})
                      }
                      //console.log(file,cloned)
                      //console.log('fields first save',file[myserver.filename],currentpath,file[myserver.hash],FILESINDEX,file.namehash)
                    }
                  }
                }
              } else { // no files, just fields
                // check req.url
                const bittes = req.url.replace('//','/').split('/')
                const contentHash = bittes[1]
                if (isHash(contentHash)){
                  const nameHash = bittes[2] ? bittes[2].split('.')[0] : contentHash
                  if (!(isHash(nameHash))) {
                    log.warning({code:415,nothash:nameHash,content:contentHash})
                    res.writeHead(415)
                    res.end()
                    return
                  }
                  const path = doihaveit(myserver.paths,contentHash)
                  if (path) {
                    for (const key of Object.keys(fields)){
                      if (fields[key] === contentHash) delete fields[key]
                    }
                    fields[myserver.hash] = contentHash // overwrite always
                    try {
                      update(join(path,contentHash,FILESINDEX,nameHash+'.json'), fields)
                      log.info({patch:{contentHash,nameHash,fields}})
                    } catch (error) {
                      log.error({filename:join(path,contentHash,FILESINDEX,nameHash+'.json'),error})
                    }
                    // path/contentHash/FILESINDEX/nameHash
                    console.log('no files',path,contentHash,FILESINDEX,nameHash,fields)

                  } else { // not mine
                    const someonehasit = whohasit(myserver.mates,contentHash)
                    if (someonehasit) {

                    } else {
                      log.warning({code:404,notexist:contentHash})
                      res.writeHead(404)
                    }
                  }
                } else {
                  log.warning({code:415,nothash:contentHash})
                  res.writeHead(415)
                }
              }
              res.end()
            });
            req.pipe(busboy);
            req.resume()
          }
        }
      }
    })

myserver.listen(()=>{
  console.log('afasdvasdvasd vsdav sdAV SDV SDAV ASD VSDA')
})
