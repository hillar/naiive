SERVER=$(hostname)
PORT=6789

find ./ -type file| head -13 | while read f
do
  hash=$(md5sum "${f}" | awk '{print $1}')
  filename=$(basename "${f}")
  filenamehash=$(echo -en "${filename}" | md5sum | awk '{print $1}')
  echo " ${hash} ${filename} ${f}"

  curl --silent --write-out %{http_code} -XPUT \
  -F hash=${hash} \
  -F name="${filename}" \
  -F namehash="${filenamehash}" \
  -F fullpath="${f}" \
  -F data=@"${f}" \
   "http://${SERVER}:${PORT}"
  #"http://${SERVER}:${PORT}/${hash}/${filenamehash}"

  echo -e "\n\n $?"
done
