import { createServer } from "../../classes/base/server.mjs";
import { createCacheRoute } from "../../classes/routes/cache.mjs";
import { createStaticRoute } from "../../classes/routes/static.mjs";
import { createProxyRoute } from "../../classes/routes/proxy.mjs";
import { createClientsRoute } from "./clients.mjs"

// vue || svelte || .. dist directory
const __STATIC__ = "./dist"
const __VENDOR__ = "./cache/cdn"
const __CLIENTS__ = "./cache/clients"

const myserver = createServer({
  roles: "*",
  groups: "*",
  routes: {
    cache: createCacheRoute({ root: __VENDOR__ }),
    dist: createStaticRoute({ root: __STATIC__ }),
    clients: createClientsRoute({root:__CLIENTS__, startoff:true}),
    search: createProxyRoute({
      host: "127.0.0.1",
      port: 6200,
      methods: ["GET"]
    }),
    files: createProxyRoute({
      host: "127.0.0.1",
      port: 7200,
      methods: ["head", "put", "get"]
    }),
    status: {
      get: async (req, res, user, log) => {
        const [search, files] = await Promise.all([
          myserver.router.search.ping(),
          myserver.router.files.ping()
        ]);
        const status = {
          status: search && files ? true : false,
          backends: { search, files }
        };
        log.info({ returned: status });
        res.write(JSON.stringify({ status }));
      }
    },
    user: {
      get: async (req, res, user, log) => {
        log.info({ headers:req.headers,returned: user });
        res.write(JSON.stringify(user));
      }
    }
  }
});

myserver.router.default = "dist";
myserver.listen();
