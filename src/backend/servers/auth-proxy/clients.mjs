/*

prep desktop client download

*/

import { readFile, writeFileSync, realpathSync, access, accessSync, constants, createWriteStream, mkdirSync, unlinkSync, createReadStream } from 'fs'
import { parse, join, resolve as resolvename, dirname } from 'path'
import { hostname } from 'os'
import { exec } from 'child_process'
import { exists } from '../../utils/fs.mjs'
import { Route } from '../../classes/base/route.mjs'

const STATICROOT = './cache/client'
const NWJSVERSION =  '0.48.2'//'0.40.1'
const NWJSDL = 'https://dl.nwjs.io/v'
const NWJSSDK = '-sdk'
const KNOWNCLIENTS = ['osx','win','linux']
const CLIENTEXT = {'osx':'zip','win':'zip','linux':'tar.gz'}
const SDK = '-sdk' // ! keep in sync with prep-desktop-clients.sh !
const NAIIVE = 'naiive'+SDK

//const win = 'https://dl.nwjs.io/v0.40.1/nwjs-v0.40.1-win-x64.zip'



export class ClientsRoute extends Route {

  #root
  #default
  #nwjsversion
  #nwjsdl
  #startoff = false
  //#nwjsosx
  //#nwjswin
  //#nwjslinux

  constructor (logger, roles, groups, root = STATICROOT, defaultsite , nwjsversion = NWJSVERSION, nwjsdl = NWJSDL, startoff = false) {

    super(logger, roles, groups)
    this.startoff = startoff
    this.root = root
    this.default = defaultsite
    this.nwjsversion = nwjsversion
    this.nwjsdl = nwjsdl

    this.get = async (req, res, user, log) => {

      //const result = await new Promise( (resolve) => {
      return new Promise( async (resolve) => {

        if (!req.headers.referer) {
            this.log.notice({noreferer:{user}})
        }

        let pe = parse(decodeURIComponent(req.url))
        // if dir is / then use user agent
        if (pe.dir === '/' ) {
          const uo = req.headers['user-agent'].match(/(?<=\().*?(?=;)/)[0]
          if (uo.startsWith('Macintosh')) {
            pe.base = 'osx'
          } else if (uo.startsWith('Windows')) {
            pe.base = 'win'
          } else if (uo.startsWith('Linux')) {
            pe.base = 'linux'
          } else {
            this.logger.info({unknown:uo})
            res.writeHead(404)
            res.end()
            resolve()
          }
        }
        const os = pe.base.split('?').shift()
        if (!KNOWNCLIENTS.includes(os)){
          this.logger.info({unknown:os,KNOWNCLIENTS})
          res.writeHead(404)
          res.end()
          resolve()
        }
        // chop route from req path
        pe.dir = pe.dir.replace('/'+this.route,'')
        const filename = `${NAIIVE}-v${this.#nwjsversion}-${os}-x64.${CLIENTEXT[os]}`
        const fullname = join(this.path, pe.dir, filename)
        if (!exists(fullname)) {
          this.logger.warning({missing:fullname})
          res.writeHead(404)
          res.end()
          resolve()
        }
        if (exists(fullname)) {
          res.setHeader('Content-Disposition', 'attachment; filename=' + filename)
          res.setHeader('Content-Type', 'application/zip')
          // TODO catch pipe errors
          createReadStream(fullname).pipe(res)
          this.logger.info({fullname})
        } else {
          res.writeHead(500)
          res.end()
          resolve(false)
        }
      })

      //return result
    }

  }

  get default () { return this.#default }
  set default (server) {

    if (server) this.#default = server
    else return
    if (this.startoff)  {
      if (this.default && this.nwjsversion && this.root) {
        const prepdesktopclients = `./prep-desktop-clients.sh "${this.default}" "${this.root}" "${this.#nwjsversion}"`
        exec(prepdesktopclients, {stdio:['inherit','pipe','pipe']}, (err, output, code) => {
          if (!(err === null)) {
              this.log.err({code,err,output})
          } else {
              for (const line of output.split('\n'))
              this.log.notice({prepdesktopclients:line})
          }
        })
      } else {
        this.log.error({missing:{default:this.default, nwjsver:this.nwjsversion, root:this.root}})
      }
    }
  }

  get root () { return this.#root }
  get path () {
    if (this.root && this.route) {
      return join(this.root)
    } else {
      if (!this.root) throw new Error('no root directory')
      if (!this.route) throw new Error('no route')
    }
  }

  set root (root) {
    if (!root) root = STATICROOT
    if (!(Object.prototype.toString.call(root) === '[object String]')) throw new Error('root not a string ' + typeof root)
    root = root.trim()
    if (root === '/') throw new Error('can not serve /')
    if (!root) throw new Error('can not serve empty')
    try {
      const _root = realpathSync(root)
      if (_root === '/') throw new Error('can not serve /')
      else this.#root = _root
    } catch (err) {
      const fullname = resolvename(root)
      this.#root = fullname
      if (this.startoff) {
        try {
          mkdirSync(this.#root,{recursive:true})
          this.log.notice({created:this.#root})
        } catch (error) {
          this.logger.err({error,fullname:this.#root})
        }
      } else this.log.info({notexists:fullname})
    }
  }

  set nwjsversion (v) {
    if (v) this.#nwjsversion = v
    //this.#nwjsosx = `nwjs${NWJSSDK}-v${this.#nwjsversion}-osx-x64.zip`
    //this.#nwjswin = `nwjs${NWJSSDK}-v${this.#nwjsversion}-win-x64.zip`
    //this.#nwjslinux = `nwjs${NWJSSDK}-v${this.#nwjsversion}-linux-x64.tar.gz`
  }
  get nwjsversion () {return this.#nwjsversion}

  set nwjsdl (d) {
    if (d) this.#nwjsdl = d
  }
  get nwjsdl () {return this.#nwjsdl}

  set startoff (v) { if (!!v) this.#startoff = true; else this.#startoff = false }
  get startoff () {return this.#startoff}

  async ping () {
    const result = await new Promise((resolve)=>{
      const fullpath = resolvename(this.path)
      access(fullpath, constants.R_OK, (err) => {
        if (err) {
          this.log.err({ping:'failed',notexist:fullpath})
          resolve(false)
        } else {
          //this.log.info({ping:'ok',readable:fullpath})
          let ok = true
          for (const os of KNOWNCLIENTS){
            const filename = `${NAIIVE}-v${this.#nwjsversion}-${os}-x64.${CLIENTEXT[os]}`
            const fullname = join(this.path, filename)
            if (exists(fullname)) this.log.info({readable:fullname})
            else {
              ok = false
              this.log.err({ping:'failed',notreadable:fullname})
            }
          }
          resolve(ok)
        }
      })
    })
    return result
  }

}

export function createClientsRoute(options) {
  if (!options) throw new Error('no options')
  return new ClientsRoute(options.logger, options.roles, options.groups, options.root, options.defaultsite, options.nwjsversion,options.nwjsdl,options.startoff)
}
