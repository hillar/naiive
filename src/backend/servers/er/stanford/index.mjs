import { createClusterHTTPServer as createServer } from "../../../classes/base/clusterserver.mjs";

//import { parseHostnamePort } from "../../utils/var.mjs";
import { parseHostnamePort } from "../../../utils/var.mjs";
import { StanfordNER } from "../../../utils/parsers/ner/stanford/stanford-ner.mjs"

const stanfordServer = createServer({
  opts: {
    port: p => { return p ? Number(p) : 6789},
    ners: list => {
      return list && list.split
        ? list.split(",").map(hp => parseHostnamePort(hp, 9997))
        : [{ hostname: "localhost", port: 9997 }];
    }
  },
  methods: {
    put: {
      fn: async (req, res, logger) => {
        req.on('error', (error) => {
          logger.err({error})
          res.statusCode = 500
          res.end()
        })
        const chunks = []
        req.on("data", chunk => {
          chunks.push(chunk);
        });
        req.on('end',async () => {
          logger.info({ners:stanfordServer.ners})

          const entities = await StanfordNER(Buffer.concat(chunks).toString(), stanfordServer.ners,logger);

          if (entities && entities.entities && Object.keys(entities.entities).length) {
              res.end(JSON.stringify(entities))
          } else {
            if (entities) res.statusCode = 204
            else res.statusCode = 502
            res.end()
          }
        })
      }
    }
  }

})

stanfordServer.listen();
