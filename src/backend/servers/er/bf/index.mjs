import {createInterface as createReadLine} from 'readline'
import { createClusterHTTPServer as createServer } from "../../../classes/base/clusterserver.mjs";
//import { createSimpleHTTPServer as createServer } from "../../../classes/base/simpleserver.mjs";
import { BF as BFPORT } from "../../../constants/ports.mjs"



//import { parseHostnamePort } from "../../utils/var.mjs";
import { parseHostnamePort, isAsyncFunction, isFunction } from "../../../utils/var.mjs";
//import { BFNER } from "../../../utils/parsers/ner/bf/bf-ner.mjs"

function printProgress(...progress) {
  if (process.stdout.isTTY) {
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(progress.join(" "));
  } //else console.debug(progress.join(" "));
}


const MAYBE_TIME = /(\s|^|\W)(\d{4}|\d{1,2})(\/|-|\.)(\d{1,2}|\S{3})(\/|-|\.)(\d{4}|\d{1,2})(\s{1,}|:|T)(\d{1,2})(:)(\d{1,2})(:\d{1,2}(\.\d{3,})?(\s\+\d{4}|Z)?)?/
const END_OF_SEQUENCE = Symbol()


function* tokenize(chars) {
    let iterator = chars[Symbol.iterator]();
    let ch;
    do {
        ch = getNextItem(iterator);
        if (isWordChar(ch)) {
            let word = '';
            do {
                word += ch;
                ch = getNextItem(iterator);
             } while (isWordChar(ch))
             // strip \W
             word = word.replace(/^\W{1,}|\W{1,}$/,'')
             yield word
            // split on any non-word character, except .:
            if (/(?=[^.:])\W/.exec(word)) for (const w of word.split(/(?=[^.:])\W/)) if (w.length>1) yield w.replace(/"|'/g,'');
        }
    } while (ch !== END_OF_SEQUENCE);
}

function getNextItem(iterator) {
    let item = iterator.next();
    return item.done ? END_OF_SEQUENCE : item.value;
}
function isWordChar(ch) {
    return typeof ch === 'string' && /^\S$/.test(ch);
}

function trimArray(a) {
    return [ ...a].filter( i => (!(i === undefined)) )
}

function recognize(logger,txt, ...fns){
  return new Promise( async (resolve) => {
  const start = process.hrtime.bigint();
  const r = {}
  for (const tkn of tokenize(txt)){
    const pfns = []
    for (const fn of fns){
      pfns.push(new Promise( async (resolve) => {
        if (isFunction(fn)) {
          try {
            resolve(fn(tkn))
          } catch (error) {
            logger.emerg({recognize:error.message,fn:fn.name,error})
            resolve({})
          }
        }
        else if (isAsyncFunction(fn)) {
          let result = {}
          try {
            result = await fn(tkn)
          } catch (error) {
            logger.emerg({recognize:error.message,fn:fn.name,error})
          }
          resolve(result)
        } else logger.emerg({recognize:'not a function'})

      }))
    }
    const rr = trimArray(await Promise.all(pfns))
    if (rr.length > 0) {

      for (const rrr of rr){
        for (const k of Object.keys(rrr)){
          // uniq key and value(s)
          if (r[k]){
            if (Array.isArray(r[k])){
              if (!(r[k].includes(rrr[k]))) r[k].push(rrr[k])
            } else if (r[k] !== rrr[k]){
              r[k] = [r[k]]
              r[k].push(rrr[k])
            }
          } else {
            // array or single value ?
            r[k] = [rrr[k]]
          }
        }
      }
    }
  }
  const took = Number(process.hrtime.bigint() - start);
  resolve({size:Buffer.byteLength(txt),took,entities:r})
  })
}


const BFServer = createServer({
  opts: {
    port: p => { return p ? Number(p) : BFPORT},
    fns: list => {
      return list && list.split
        ? list.split(",")
        : ['ipv4','ipv6','uri','email','bitcoin','iban','estid','creditcard','phone','hash']
    }
  },
  methods: {
    put: {
      fn: async (req, res, logger) => {
        const start = process.hrtime.bigint();
        logger.info(req.headers)
        const entities = {}
        const timeline = []
        const objectlines = []
        let size = 0
        let previous = ''
        const body = []
        req.socket.setKeepAlive(true)
        req.on('data', async (chunk) => {
          size += chunk.length
          body.push(chunk)
          printProgress(size,chunk.length)
        })

        req.on('end',async () => {
          res.socket.setKeepAlive(true)
          const upload = Number(process.hrtime.bigint() - start) / 1e6
          const pstart = process.hrtime.bigint();
          let lines
          try {
            lines = Buffer.concat(body).toString().split(/\r?\n/g)
          } catch (error) {
            // 0x1fffffe8
            console.log('******* 0x1fffffe8',size, size > 0x1fffffe8, 0x1fffffe8 - size)

          }
          let done = 0
          for (const line of lines) {
            const ptime = Math.round((Number(process.hrtime.bigint() - start) / 1e6)/1000)
            const percent = Math.round((done/size)*100)
            printProgress(ptime,percent,done)
            if (percent > 20){
              if (ptime > percent){
                // we are to slow, tell client ..

              }

            }

            if (ptime > 100) {
              console.log('breaking', ptime)
              break
            }

            done += line.length
            const tmp = await recognize(logger,line.trim(),...BFServer.parsers)
            // TODO faster dedup and object line
            if (tmp.entities ){
              for (const key of Object.keys(tmp.entities)){
                if (!entities[key]) entities[key] = []
                for (let i in tmp.entities[key]){
                  // dedup recognizers results
                  // "hash20": ["EE522200221013264447"
                  // "iban": ["EE522200221013264447"
                  let found = false
                  for (const ekey of Object.keys(entities) ){
                    if (entities[ekey].includes(tmp.entities[key][i])) {
                      //console.log(ekey,key,tmp.entities[key][i])
                      found = true
                      break
                    }
                  }
                  if (!found) entities[key].push(tmp.entities[key][i])
                  else tmp.entities[key][i] = undefined
                }
              }

              const mt = MAYBE_TIME.exec(line)
              const timestamps = []
              if (mt  && mt[0]) {
                try {
                  let time = new Date(mt[0])
                  if (isNaN(time) && mt[7]) {
                    // Date() do not like nonspace between date and time
                    mt[0]=''
                    mt[1]=''
                    mt[7]=' '
                    mt[12]=''
                    mt[13]=''
                    time = new Date(mt.join(''))
                  }
                  if (!isNaN(time)) {
                    timestamps.push(time)
                    for (const key of Object.keys(tmp.entities)){
                      for (const e of tmp.entities[key]){
                        if (e) {
                          const tt = {}
                          tt.timestamp = time
                          tt[key.toUpperCase()] = e
                          timeline.push(tt)
                        }
                      }
                    }
                  }
                } catch (error) {
                  console.error('maybetime')
                  console.error(line)
                  console.error(mt)
                  console.error(error)
                }
              }
              // more than one entitie on same line
              const lt = {}
              for (const key of Object.keys(tmp.entities)){
                for (let i in tmp.entities[key]){
                  if (tmp.entities[key][i]) {
                    if (!lt[tmp.entities[key][i]]){
                      lt[tmp.entities[key][i]] = key
                    }
                  }
                }
              }
              if (Object.keys(lt).length > 1 ){

                const tl = {}
                for (const value of Object.keys(lt)) {
                  if (!tl[lt[value]]) tl[lt[value]] = []
                  tl[lt[value]].push(value)
                }
                objectlines.push({timestamp:timestamps.length?timestamps:undefined,...tl})

              }

            }
          }

          const parse = Number(process.hrtime.bigint() - pstart) / 1e6
          const took = Number(process.hrtime.bigint() - start) / 1e6
          if (entities && Object.keys(entities).length) {
              let et = {}
              for (const e of Object.keys(entities) ) et[e] = entities[e].length
              logger.info({took:{took, upload,parse},size,entities:et,timeline:timeline.length,objectlines:objectlines.length})
              res.end(JSON.stringify({took,entities,timeline,objectlines}))
          } else {
            res.statusCode = 204 // No Content
            res.end(JSON.stringify({took}))
          }
        })
        req.on('error', (error) => {
          logger.err({error})
          res.statusCode = 500
          res.end()
        })
      }
    }
  }

})

BFServer.listen(async () => {
  BFServer.server.setTimeout(1000*60*10)
  BFServer.parsers = []
  for (const fn of BFServer.fns){
    try {
      const module = await import(`./recognizers/${fn}.mjs`)
      BFServer.parsers.push(module.default)
    } catch (error) {
      BFServer.logger.err({fn,file:`./recognizers/${fn}.mjs`,error})
      process.exit(-1)
    }
  }
  BFServer.logger.info({recognizers:BFServer.fns})
});
