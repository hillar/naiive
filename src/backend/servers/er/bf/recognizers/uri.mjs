import parse from 'url'

export default function uri(s) {
  // TODO add validation
  // break down to parts

  const parts = /(([^:/?#]+):)?(\/\/([^/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?/.exec(s);

  if (parts && parts[2] && parts[4]) {
    try {
      const p = new URL(s)
      if (p.host) {
        const u = {}
        const hostname = p.hostname.replace(/^\W{1,}|\W{1,}$/,'')
        if (!s.replace(/\W{1,}$/,'').endsWith(hostname)) u.uri = s
        if (hostname.split('.').length>1){
          u.hostname = hostname
        }
        if (p.username) u.username = p.username
        if (p.password) u.password = p.password
        return u
      }
    } catch (e) {
      // noop
    }
  }
}
