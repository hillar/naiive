export default function iban(s){
    function likeIban(str){
      return /^([A-Z]{2}[ \-]?[0-9]{2})(?=(?:[ \-]?[A-Z0-9]){9,30}$)((?:[ \-]?[A-Z0-9]{3,5}){2,7})([ \-]?[A-Z0-9]{1,3})?$/.test(str);
    }

    function validateIbanChecksum(iban) {
      const ibanStripped = iban.replace(/[^A-Z0-9]+/gi,'').toUpperCase()
      const m = ibanStripped.match(/^([A-Z]{2})([0-9]{2})([A-Z0-9]{9,30})$/)
      if(!m) return false
      const numbericed = (m[3] + m[1] + m[2]).replace(/[A-Z]/g,function(ch){
          return (ch.charCodeAt(0)-55)
      })
      const mod97 = numbericed.match(/\d{1,7}/g).reduce(function(total, curr){ return Number(total + curr)%97},'')
      return (mod97 === 1)
    };
    if (likeIban(s)){
      if (validateIbanChecksum(s))
      return {'iban':s}
    }
}
