const LENGTHS = [128/8, 160/8, 192/8, 224/8, 256/8, 328/8, 384/8,  512/8]

function check(canditate) {
  const chars = {}
  const nums = {}
  for (const c of canditate) {
    if (isNaN(Number(c))) chars[c] = true
    else nums[c] = true
    if (Object.keys(chars).length>1 && Object.keys(nums).length>1) {
      return true
    }
  }
  return false
}

export default function hash(s){
  const length = s.length
  if (LENGTHS.includes(length)) {
    const re = new RegExp(`[a-f0-9]{${length}}`,'i')
    if (re.test(s)) {
      const tmp = {}
      tmp[`hash${length}`] = s
      if (check(s)) return tmp
    }
  }
}
