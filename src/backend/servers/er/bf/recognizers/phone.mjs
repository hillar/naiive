
import { request } from 'http'

const AUGMENTSERVER = process.env.AUGMENTSERVER
const AUGMENTPORT = process.env.AUGMENTPORT ? proccess.env.AUGMENTPORT: 6789

async function est(n){

  return new Promise( resolve => {
      const options = {
          hostname: AUGMENTSERVER,
          port: AUGMENTPORT,
          path: `/phone/372${n}`,
          method: 'GET',
          headers: {
            'Accept': 'application/json'
          }
      }
      const req = request(options, (res) => {
        let data = []
        res.on('data', (chunk) => {
          try {
            data.push(chunk)
          } catch (e) {
            //logger.err({gitlab:e})
            console.error(n)
            console.error(e)
            resolve({})
          }
        })
        res.on('end',() => {
              let l
              try {
                  l = JSON.parse(Buffer.concat(data).toString())
                  if (l && l.result && l.result.est && l.result.est.timestamp) delete l.result.est.timestamp
                  resolve(l.result.est)
              } catch (error) {
                console.error(n)
                console.error(error)
                resolve({})
              }
      })
        res.on('error', error => {
          console.error(error)
          //logger.err({gitlab:error})
          resolve({})
        })
      })
      req.on('error', (error) => {
        console.error(error)
        //logger.warning({gitlab:error})
        resolve({})
      })

      req.end()

    })
}


async function checkEST(s) {
/*

Eesti numeratsiooniplaan
https://www.riigiteataja.ee/akt/123052018005?leiaKehtiv

Numbrite, lühinumbrite, teenusnumbrite ja prefiksite paiknemine numeratsiooniväljas, nende pikkus ning kasutusotstarve
https://www.riigiteataja.ee/aktilisa/1260/2201/9009/MKM_m25_lisa.pdf#

*/


let isnumber = false
const length = s.length
const p2 = Number(s.substring(0,2))

/*
50–59 7 8 Mobiiltelefoninumber, mobiiltelefoniteenuse osutamiseks
Kehtivad käesoleva määruse § 20 lõike 1 erisused
*/
if (p2 > 49 && p2 < 60) {
  /*
  § 20 lõike 1 erisused
  (1) Enne 2005. aasta 1. jaanuari reserveeritud ja kasutusel olevate seitsmekohaliste mobiiltelefoninumbrite pikkus säilitatakse numbri broneeringu lõppemiseni järgmistes numeratsiooniväljades:
   1) 50;
   2) 510, 511, 512, 513, 514, 515, 516, 517, 518;
   3) 5195;
   4) 52;
   5) 550, 551, 552, 553, 554;
   6) 557, 558;
   7) 5640, 5641, 5642, 5643, 5644;
   8) 5651, 5652, 5653, 5654, 5655;
   9) 5658, 5659.
  */
  function isLegacy (n){
    if (n > 56 || n === 53 || n === 54) return false
    if (n === 50 || n === 52) return true
    else {
      const p3 = Number(s.substring(0,3))
      if (( p3 > 509  && p3 < 519 ) || (p3 > 549 && p3 < 555) || (p3 > 556 && p3 < 559)) return true
      else {
        const p4 = Number(s.substring(0,4))
        if (p4 === 5195) return true
        if (p4 > 5639 && p4 < 5645) return true
        if (p4 > 5650 && p4 < 5656) return true
        if (p4 > 5657 && p4 < 5660) return true
      }
    }
  }
  if (length === 7) {
    isnumber = isLegacy(p2)
  } else {
    isnumber = !isLegacy(p2)
  }
} else {
  /*
  81–84 8 8 Mobiiltelefoninumber, mobiiltelefoniteenuse osutamiseks
  */
  if (length === 8) {
    isnumber = (p2 > 80 && p2 < 85)
  } else {


    if (p2 === 69) {
      const p3 = Number(s.substring(0,3))
      // 697–699 7 7 Telefoninumber, telefoniteenuse osutamiseks
      if (p3 > 696 && p3 < 700) isnumber = true
      /* Enne määruse jõustumist broneeritud ja kasutusel olevate numeratsioonivälja 694–696 seitsmekohaliste telefoninumbrite kasutamiseotstarve säilitatakse numbri broneeringu lõppemiseni järgmistes numeratsiooniväljades:
   1) 6943300–6943399;
   2) 6969100–6969119;
   3) 6969129, 6969180, 6969199.
   */
      else {
        const p7 = Number(s)
        if (p7 > 6943299 && p7 < 6943400) isnumber = true
        if (p7 > 6969099 && p7 < 6969200) isnumber = true
        if (p7 === 6969129) isnumber = true
        if (p7 === 6969180) isnumber = true
        if (p7 === 6969199) isnumber = true
      }
    } else {
      // 88 7 7 Telefoninumber, telefoniteenuse osutamiseks
      // 38; 39 7 7 Telefoninumber, telefoniteenuse osutamiseks
      // 35 7 7 Telefoninumber, telefoniteenuse osutamiseks
      // 32; 33 7 7 Telefoninumber, telefoniteenuse osutamiseks
      if (p2 === 88 || p2 === 39 || p2 === 38 || p2 === 35 || p2 === 33 || p2 === 32 ) isnumber = true
      // 71–79 7 7 Telefoninumber,telefoniteenuse osutamiseks
      // 60–68 7 7 Telefoninumber, telefoniteenuse osutamiseks
      // 43–48 7 7 Telefoninumber, telefoniteenuse osutamiseks
      else {
          if (p2 > 70 && p2 < 80) isnumber = true
          else if (p2 > 59 && p2 < 69) isnumber = true
          else if (p2 > 42 && p2 < 49) isnumber = true
      }
    }
  }

}

//return isnumber && {'country':'EST',phone:'372'+s}

if (isnumber) {
  if (AUGMENTSERVER) {
    try {
      const result = await est(s)
      if (result && result.provider && result.provider.name) {
        return {phone:'372'+s, 'ORGANIZATION':result.provider.name+' ('+result.provider.code+')'}
      }
    } catch (error) {
      console.error(error)
      return isnumber && {phone:'372'+s}
    }
  } else return isnumber && {phone:'372'+s}
}

}

const codes = {'358':'FIN','371':'LAT','370':'LIT'}

let last = undefined
let pluss = false
let train = []
export default async function phone(p) {
  let s = p.replace(/\+|-|–|\(|\)|\./g,'')
  if (/^\d+$/.test(s)) {
    let length = s.length
    if (length < 7) {
      train.push(s)
      if (train.length > 3) train.shift()
      s = train.join('')
      length = s.length
    } else train = []
    /*
    if (last && length < 9) {
      train.push(s)
      s = train.join('')
      length = s.length
    }
    */
    if (length === 7 || length === 8 ) { train=[]; return checkEST(s)}
    else if (length === 10 || length === 11 ){
      train=[];
      if (s.startsWith('372')) return checkEST(s.substring(3))
      else {
        const code = s.substring(0,3)
        if (codes[code] && s.substring(4,1) !== 0) return {phone:s}
      }
    } else {
      //if (length === 3 && p.includes('+')) last = s
      if ( pluss || p.includes('+')) {last = s; pluss = false}
      else if (last && length < 7) last = s
      else last = undefined

    }
  } else if (p === '+'){
    pluss = true
    last = undefined
    train = []
  } else {
    pluss = false
    last = undefined
    train = []
  }

}

/*
const testString = '+3585514699 hillar.aarelaid@eesti.ee. (+372)50146948 . Kungla tn 2, 70411, Ülde küla, +372 5014694 Põhja-Sakala vald, Viljandi maakond, +3725014694 Eesti Vabariik ...'

for (const token of testString.split(' ')){
  const result = phonenumber(token)
  if (result !== undefined) console.dir({token,result})
}
*/
