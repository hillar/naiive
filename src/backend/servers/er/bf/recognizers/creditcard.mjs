export default function creditcard(s) {
  s = s.replace(/-/g,'')
  if (/\d{13,16}/.test(s)) {
    // American Express:
    if (/^3[47][0-9]{13}$/.test(s)) return {'AmericanExpress':s}
    // Visa:
    if (/^4[0-9]{12}(?:[0-9]{3})?$/.test(s)) return {'Visa':s}
    // Diners Club:
    if (/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/.test(s)) return {'DinersClub':s}
    // Discover:
    if (/^6(?:011|5[0-9]{2})[0-9]{12}$/.test(s)) return {'Discover':s}
    // JCB:
    if (/^(?:2131|1800|35\d{3})\d{11}$/.test(s)) return {'JCB':s}
    // MasterCard:
    if (/^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$/.test(s)) return {'MasterCard':s}
  }
}
