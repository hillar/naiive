// TODO http://data.iana.org/TLD/tlds-alpha-by-domain.txt
export default function email(s) {
  const tmp = s.toLowerCase()
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  if (re.test(tmp.toLowerCase())) return {'email':tmp}
}
