export default function bitcoin(s) {
  // mainnet only
  if (/^(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}$/.test(s)) return {'bitcoin':s}
  if (/^0x[a-fA-F0-9]{40}$/.test(s)) return {'ethereum':s}
  if (/^[LM3][a-km-zA-HJ-NP-Z1-9]{26,33}$/.test(s)) return {'litecoin':s}
}
