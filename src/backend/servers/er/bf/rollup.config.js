import commonjs from 'rollup-plugin-commonjs'
// https://github.com/acornjs/acorn-private-class-elements
// https://github.com/acornjs/acorn-class-fields
import classFields from 'acorn-class-fields'
// https://github.com/acornjs/acorn-static-class-features
import staticFields from 'acorn-static-class-features'
import copy from 'rollup-plugin-copy'

export default [{

    acornInjectPlugins: [
        staticFields, classFields
    ],
    plugins: [
      copy({
        targets: [
          { src: 'recognizers/**', dest: 'dist/recognizers' }
        ]
      }),
    commonjs({
      extensions: [ '.js' ],
      sourceMap: false,

    })
  ],
  output: {
    file: './dist/index.mjs',
    format: 'esm'
  },
  input: 'index.mjs'

}];
