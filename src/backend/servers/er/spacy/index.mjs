import { createClusterHTTPServer as createServer } from "../../../classes/base/clusterserver.mjs";

//import { parseHostnamePort } from "../../utils/var.mjs";
import { parseHostnamePort } from "../../../utils/var.mjs";
import { SpacyNER } from "../../../utils/parsers/ner/spacy/spacy-ner.mjs"

const spacyServer = createServer({
  opts: {
    port: p => { return p ? Number(p) : 6789},
    ners: list => {
      return list && list.split
        ? list.split(",").map(hp => parseHostnamePort(hp, 9996))
        : [{ hostname: "localhost", port: 9996 }];
    }
  },
  methods: {
    put: {
      fn: async (req, res, logger) => {
        req.on('error', (error) => {
          logger.err({error})
          res.statusCode = 500
          res.end()
        })
        const chunks = []
        req.on("data", chunk => {
          chunks.push(chunk);
        });
        req.on('end',async () => {
          logger.info({ners:spacyServer.ners})

          const entities = await SpacyNER(Buffer.concat(chunks).toString(),'en',spacyServer.ners,logger);

          if (entities && entities.entities && Object.keys(entities.entities).length) {
              res.end(JSON.stringify(entities))
          } else {
            if (entities) res.statusCode = 204
            else res.statusCode = 502
            res.end()
          }
        })
      }
    }
  }

})

spacyServer.listen();
