import { createClusterHTTPServer as createServer } from "../../classes/base/clusterserver.mjs";
import { ER as ERSERVERPORT } from "../../constants/ports.mjs"
import { parseHostnamePort } from "../../utils/var.mjs";
import { ENTITIES } from "../../utils/parsers/ner/entitie-names.mjs"
import { request } from 'http'


async function askERS(text,ers, drop,logger){
  const doers = []
  for (const er of ers) {
    doers.push(new Promise( resolve => {
      const options = {
        hostname: er.hostname,
        port: er.port,
        method: 'PUT'
      }
      const req = request(options, res => {
        res.on('error', (error) =>  {
          this.log.emerg({matesRequest:{error,mate}})
          resolve({statusCode:res.statusCode, er, error})
        })
        const chunks = [];
        res.on("data", chunk => {
          chunks.push(chunk);
        });
        res.on("end", () => {
          const took = Number(process.hrtime.bigint() - start) / 1e6
          let answer;
          try {
            answer = JSON.parse(Buffer.concat(chunks).toString());
          } catch (error) {
            logger.warning({ noterserver:er });
            resolve({er, error})
          }
          if (answer && answer.took) logger.info({took, wasted:(took-answer.took),er})
          if (answer && answer.entities && Object.keys(answer.entities).length) resolve({er,took, entities:answer.entities, timeline:answer.timeline})
          else resolve({er,took})
        })
      })
      req.on('error', error => {
        logger.warning({ er, error });
        resolve({er, error})
      })
      const start = process.hrtime.bigint();
      req.write(text);
      req.end();
    }))
  }
  const start = process.hrtime.bigint();
  const answers = await Promise.all(doers)
  const took = Number(process.hrtime.bigint() - start) / 1e6;
  logger.info({took,er:'ERS'})
  const entities = {}
  let timeline = []
  // TODO if value has more than one key !?
  for (const answer of answers){
    if (answer.entities  && Object.keys(answer.entities).length ){
      for (const key of Object.keys(answer.entities)){
        if (drop.includes(key)) continue
        if (!entities[key]) entities[key] = []
        for (const value of answer.entities[key]){
            if (!entities[key].includes(value)) entities[key].push(value)
        }
      }
      if (answer.timeline) timeline = [ ...timeline, ...answer.timeline]
    }
  }
  return {took,entities,timeline}
}

async function askNLPS(text,nlps,drop,logger){
  const doers = []
  for (const er of nlps) {
    doers.push(new Promise( resolve => {
      const options = {
        hostname: er.hostname,
        port: er.port,
        method: 'PUT'
      }
      const req = request(options, res => {
        res.on('error', (error) =>  {
          this.log.emerg({matesRequest:{error,mate}})
          resolve({statusCode:res.statusCode, er, error})
        })
        const chunks = [];
        res.on("data", chunk => {
          chunks.push(chunk);
        });
        res.on("end", () => {
          const took = Number(process.hrtime.bigint() - start) / 1e6;
          let answer;
          try {
            answer = JSON.parse(Buffer.concat(chunks).toString());
          } catch (error) {
            logger.notice({ noterserver:er });
            resolve({er, error})
          }
          if (answer && answer.took) logger.info({took, wasted:(took-answer.took),er})
          if (answer && answer.entities && Object.keys(answer.entities).length) resolve({er,took, entities:answer.entities})
          else resolve({er,took})
        })
      })
      req.on('error', error => {
        logger.warning({ er, error });
        resolve({er, error})
      })
      const start = process.hrtime.bigint();
      req.write(text);
      req.end();
    }))
  }
  const start = process.hrtime.bigint();
  const answers = await Promise.all(doers)
  const took = Number(process.hrtime.bigint() - start) / 1e6;
  logger.info({took,er:'NLPS'})
  let entities = {}
  // TODO if value has more than one key !?
  // TODO confidence
  const confidence = {}
  let maxConfidence = 0
  for (const answer of answers){
    if (answer.entities  && Object.keys(answer.entities).length ){
      for (const key of Object.keys(answer.entities)){
        if (drop.includes(key)) continue
        if (key.length < 3) continue
        if (!entities[key]) {
          entities[key] = []
          if (!ENTITIES.includes(key)) logger.info({unknown:key,er:answer.key})
        }
        for (const value of answer.entities[key]){
            if (value.length < 3) continue
            if (!entities[key].includes(value)) entities[key].push(value)
            if (!confidence[value]) confidence[value] = {}
            if (!confidence[value][key]) confidence[value][key] = 1
            else confidence[value][key] += 1
            maxConfidence = Math.max(maxConfidence,confidence[value][key])

        }
      }
    }
  }
  // none was recognized by more than one
  if (maxConfidence === 1 && answers.length > 1) entities = {}
  if (maxConfidence > 1 && answers.length > 1) {
    // drop key confidence < 2 || or keys count > 2
    entities = {}
    for (const value of Object.keys(confidence)){
      const keys = Object.keys(confidence[value])
      if (keys.length === 1 && confidence[value][keys[0]] > 1) {
        if (!entities[keys[0]]) entities[keys[0]] = []
        entities[keys[0]].push(value)
      } else if (keys.length === 2){
          if (confidence[value][keys[0]] > 1){
            if (!entities[keys[0]]) entities[keys[0]] = []
            entities[keys[0]].push(value)
          }
          if (confidence[value][keys[1]] > 1){
            if (!entities[keys[1]]) entities[keys[1]] = []
            entities[keys[1]].push(value)
          }
      } else {
        // dropping
        //console.log('dropping',value, keys)
      }
    }


  } else logger.info({nlp:'to few recognizers'})
  return {took,entities}

}


const hn =  'localhost'


const ERServer = createServer({
  opts: {
    port: p => { return p ? Number(p) : ERSERVERPORT},
    ers: list => {
      return list && list.split
        ? list.split(",").map(hp => parseHostnamePort(hp, 9984))
        : [{ hostname: hn, port: 9984 },{ hostname: hn, port: 9985 }];
    },
    nlps: list => {
      return list && list.split
        ? list.split(",").map(hp => parseHostnamePort(hp, 9987))
        : [{ hostname: hn, port: 9986 },{ hostname: hn, port: 9987 },{ hostname: hn, port: 9967 }];
    },
    drop: list => {
      return list && list.split
      ? list.split(",")
      : ['country','geo','PERCENT','MONEY','DATE','CARDINAL','GPE','ORDINAL'];
    }
  },
  methods: {
    put: {
      fn: async (req, res, logger) => {
        req.on('error', (error) => {
          logger.err({error})
          res.statusCode = 500
          res.end()
        })
        const chunks = []
        req.on("data", chunk => {
          chunks.push(chunk);
        });
        req.on('end',async () => {
           logger.info({ers:ERServer.ers,nlps:ERServer.nlps})
           const text = Buffer.concat(chunks).toString()
           const start = process.hrtime.bigint();
           const answers = await Promise.all([askERS(text, ERServer.ers,ERServer.drop,logger),askNLPS(text, ERServer.nlps,ERServer.drop,logger)])
           const took = Number(process.hrtime.bigint() - start) / 1e6;
           logger.info({took,er:'ALL'})
           const entities = {}
           let timeline = []
           for (const answer of answers){
             //if (answer.took) logger.info({took:answer.took})
             if (answer.entities && answer.entities && Object.keys(answer.entities).length ){
               for (const _key of Object.keys(answer.entities)){
                 const key = _key.toUpperCase()
                 if (!entities[key]) entities[key] = []
                 for (const value of answer.entities[_key]){
                     if (!entities[key].includes(value)) entities[key].push(value)
                 }
               }
               if (answer.timeline) timeline = [ ...timeline, ...answer.timeline]
             }
           }

          if (Object.keys(entities).length) {
              if (!timeline.length) timeline = undefined
              res.end(JSON.stringify({took,entities,timeline}))
          } else {
            if (entities) {
              res.statusCode = 204
              res.write(JSON.stringify({took}))
            }
            else res.statusCode = 502
            res.end()
          }
        })
      }
    }
  }

})

ERServer.listen();
