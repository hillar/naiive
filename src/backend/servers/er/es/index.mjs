import { createClusterHTTPServer as createServer } from "../../../classes/base/clusterserver.mjs";
import { parseHostnamePort } from "../../../utils/var.mjs";
import { ExtractorServiceNER } from "../../../utils/parsers/ner/ExtractorService/extractorservice.mjs"


const ESServer = createServer({
  opts: {
    port: p => { return p ? Number(p) : 6789},
    ners: list => {
      return list && list.split
        ? list.split(",").map(hp => parseHostnamePort(hp, 7000))
        : [{ hostname: "localhost", port: 7000 }];
    }
  },
  methods: {
    put: {
      fn: async (req, res, logger) => {
        req.on('error', (error) => {
          logger.err({error})
          res.statusCode = 500
          res.end()
        })
        const chunks = []
        req.on("data", chunk => {
          chunks.push(chunk);
        });
        req.on('end',async () => {
          logger.info({ners:ESServer.ners})

          const entities = await ExtractorServiceNER(Buffer.concat(chunks).toString(),30,  ESServer.ners,logger);

          if (entities && entities.entities && Object.keys(entities.entities).length) {

              res.end(JSON.stringify(entities))
          } else {
            if (entities) res.statusCode = 204 // No Content
            else res.statusCode = 502
            res.end()
          }
        })
      }
    }
  }

})

ESServer.listen();
