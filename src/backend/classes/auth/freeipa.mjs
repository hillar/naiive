import { default as Client } from '../vendor/ldapjs/lib/client/client.js'

import { AuthBase } from '../base/auth.mjs'
import { User } from '../base/user.mjs'

const SIZELIMIT = 1024
const PROTO = 'ldap'
const PORT = 389


/*
RFC 2255, The LDAP URL Format

<user>:<pass>@<hostname>:<port>/<base_dn>?<attributes>?<scope>?<filter>

*/
export function parseURL(url = 'manager:Secret123@ipa.demo1.freeipa.org/dc=demo1,dc=freeipa,dc=org'){
  if (!url.split) throw new Error('ldap url is not string')
  const bittes = url.split('@')
  if (!bittes[1]) throw new Error('ldap url has no user nor password')
  const [user, password] = bittes[0].split(':')
  if (!password) throw new Error('ldap url has no password')
  if (!user) throw new Error('ldap url has no user')
  const [hp,base] = bittes[1].split('/')
  if (!base) throw new Error('ldap url has no base')
  let [hostname, port] = hp.split(':')
  if (!port) port = 389
  else port = Number(port)
  if (!port) throw new Error('ldap port is not a number')
  if (!hostname) throw new Error('ldap has no hostname')
  return {user,password,hostname,port,base}
}

export function fetch_ldap(url,dn,password,base,filter,attributes = [],sizeLimit = SIZELIMIT,rejectUnauthorized) {
  const results = []
  return new Promise((resolve, reject) => {
    try {
      const client = new Client({
        url: url,
        tlsOptions: {rejectUnauthorized: rejectUnauthorized}
      })
      client.on('error',function(err){
           resolve(err)
         })
      client.bind(dn, password, (err)  => {
        if (err) {
          client.unbind()
          resolve(err)
        } else {
          try {
            const options = {
              filter: filter,
              scope: 'sub',
              attributes: attributes,
              sizeLimit: sizeLimit
            }
            client.search(base, options,(err, res) => {
                if (err) {
                  client.unbind()
                  resolve(err)
                } else {
                  res.on('searchEntry', function (entry) {
                    results.push(entry.object)
                  })
                  res.on('error', function (err) {
                    client.unbind()
                    resolve(err)
                  })
                  res.on('end', function (result) {
                    client.unbind()
                    resolve(results)
                  })
                }
              }
            )
          } catch (e) {
            client.unbind()
            reject(e)
          }
        }
      })
    } catch (e) {
      //client.unbind()
      reject(e)
    }
  })
}

const HOST = 'localhost'
const BASE = 'cn=accounts,dc=example,dc=org'
const BINDUSER = 'user'
const BINDPASS = 'password'
const FIELD = 'uid'
const SERIALFIELD = 'employeeNumber'

export class AuthFreeIPA extends AuthBase {

  #server
  #proto = PROTO
  #port = PORT
  #base
  #binduser
  #bindpass
  #field
  #serialfield
  #rejectUnauthorized

  constructor (server=HOST, base=BASE, binduser=BINDUSER, bindpass=BINDPASS, field=FIELD, serialfield=SERIALFIELD, rejectUnauthorized=false, directory,filename, time, logger) {
    super(logger,directory,filename, time)

    this.server = server
    this.proto = PROTO
    this.port = PORT
    this.base = base
    this.binduser = binduser
    this.bindpass = bindpass
    this.field = field
    this.serialfield = serialfield
    this.rejectUnauthorized = rejectUnauthorized
  }

  get url () {return  `${PROTO}://${this.#server}:${PORT}`}
  get server () { return  this.#server }
  get base () { return  this.#base }
  get binduser () { return  this.#binduser }
  // TODO check FREEIPA & AD doc's for cn=users
  get binddn () { return  `${this.field}=${this.binduser},cn=users,cn=accounts,${this.base}` }
  get bindpass () { return  this.#bindpass }
  get field () { return  this.#field }
  get serialfield() { return this.#serialfield}

  set proto (proto) {
    if (!proto) return
    if (!(Object.prototype.toString.call(proto) === '[object String]')) throw new Error(Object.getPrototypeOf(this).constructor.name + ' :: proto not string  ' + typeof proto)
    this.#proto = proto
  }
  set port (port) {
    if (!port) return
    if (isNaN(port)) throw new Error(Object.getPrototypeOf(this).constructor.name + 'port not a number')
    this.#port = port
  }
  get proto () {return this.#proto}
  get port () {return this.#port}

  set server (server) {
  	if (!(Object.prototype.toString.call(server) === '[object String]')) throw new Error(Object.getPrototypeOf(this).constructor.name + ' :: server not string  ' + typeof server)
  	if (!server) this.log.warning({empty:'server'})
  	else this.#server = server
  }
  set base (base) {
  	if (!(Object.prototype.toString.call(base) === '[object String]')) throw new Error(Object.getPrototypeOf(this).constructor.name + ' :: base not string  ' + typeof base)
  	if (!base) this.log.warning({empty:'base'})
  	else this.#base = base
  }
  set binduser (binduser) {
  	if (!(Object.prototype.toString.call(binduser) === '[object String]')) throw new Error(Object.getPrototypeOf(this).constructor.name + ' :: binduser not string  ' + typeof binduser)
  	if (!binduser) this.log.warning({empty:'binduser'})
  	else this.#binduser = binduser
  }
  set bindpass (bindpass) {
  	if (!(Object.prototype.toString.call(bindpass) === '[object String]')) throw new Error(Object.getPrototypeOf(this).constructor.name + ' :: bindpass not string  ' + typeof bindpass)
  	if (!bindpass) this.log.warning({empty:'bindpass'})
  	else this.#bindpass = bindpass
  }

  set field (field) {
  	if (!(Object.prototype.toString.call(field) === '[object String]')) throw new Error(Object.getPrototypeOf(this).constructor.name + ' :: field not string  ' + typeof field)
  	if (!field) this.log.warning({empty:'field'})
  	else this.#field = field
  }
  set serialfield (field) {
    if (!(Object.prototype.toString.call(field) === '[object String]')) throw new Error(Object.getPrototypeOf(this).constructor.name + ' :: serialfield not string  ' + typeof field)
    if (!field) this.log.warning({empty:'serialfield'})
    else this.#serialfield = field
  }

  async isUser(serial) {
    const filter = `(&(${this.serialfield}=${serial})(memberof=cn=ipausers,cn=groups,cn=accounts,${this.base}))`
    const attributes = 'krbPasswordExpiration'
    try {
      let ru = await fetch_ldap(this.url,this.binddn,this.bindpass,this.base,filter)
      if (ru instanceof Error) {
          if (u.message === 'No Such Object') {
            this.log.alert({'binddn':this.binddn,error:ru})
            return new Error('wrong bind user')
          } else if (u.message === 'Invalid Credentials') {
            this.log.alert({'binddn':this.binddn,error:ru})
            return new Error('wrong bind password')
          } else if (u.errno === 'ECONNREFUSED') {
            this.log.alert({'url':this.url,error:ru})
            return new Error('no auth backend')
          } else {
            // something went very wrong ;(
            this.log.emerg({FetchError:ru})
            return ru
          }
      } else {
        if (!(ru.length === 1)) {
          this.log.notice({'notuser':{serial}})
          return new Error('not found')
        } else {
          ru = ru[0]
          // see https://github.com/freeipa/freeipa/blob/master/install/ui/src/freeipa/datetime.js
          const e = ru.krbPasswordExpiration
          if (!e) return new Error('no krbPasswordExpiration')
          const ed = e.slice(0,4)+'-'+e.slice(4,6)+'-'+e.slice(6,8)+'T'+e.slice(8,10)+':'+e.slice(10,12)+':'+e.slice(12,14)+'Z'
          const passwordExpiration = new Date(ed)
          if (!(passwordExpiration instanceof Date) || isNaN(passwordExpiration)) {
            this.log.emerg({user:{username,msg:'krbPasswordExpiration not date',krbPasswordExpiration:e}})
            return new Error('krbPasswordExpiration not date ')
          } else {
            if (passwordExpiration < Date.now()){
              return new Error('password expired')
            } else {
              //copy out groups and roles
              ru.roles = []
              ru.groups = []
              for (const memberof of ru.memberOf) {
                if (memberof.endsWith('cn=accounts,'+this.base)) {
                  const tmp = memberof.split(',')
                  const what = tmp[1]
                  switch (tmp[1].split('=')[1]) {
                    case 'roles':
                                  ru.roles.push(tmp[0].split('=')[1])
                                  break
                    case 'groups':
                                  ru.groups.push(tmp[0].split('=')[1])
                                  break
                    default:
                            this.log.alert({ERROR:{shouldnothappen:tmp}})
                  }
                }
              }

              delete ru.memberOf

              const ssn = ru.employeeNumber || ''
              const ou = ru.ou || ''
              const manager = ru.manager || ''
              const emails = ru.mail || []
              const phones = ru.mobile || []
              const fu = new User(ru.uid, ru.roles, ru.groups, ssn, ru.givenName, ru.sn, ou , manager , emails , phones, this.log)
              return fu.toObj()
            }
          }
        }
      }
    } catch (error) {
      // something went very wrong ;(
      this.log.emerg({CatchBindError:error})
      return e
    }
  }

  async reallyVerify (username,password) {
    let filter = `(&(${this.field}=${username})(memberof=cn=ipausers,cn=groups,cn=accounts,${this.base}))`
    let attributes = 'krbPasswordExpiration'
    let u
    try {
      u = await fetch_ldap(this.url,this.binddn,this.bindpass,this.base,filter,attributes)
      if (u instanceof Error) {
        if (u.message === 'No Such Object') {
          this.log.alert({'binddn':this.binddn,error:u})
          return new Error('wrong bind user')
        }
        if (u.message === 'Invalid Credentials') {
          this.log.alert({'binddn':this.binddn,error:u})
          return new Error('wrong bind password')
        }
        if (u.errno === 'ECONNREFUSED') {
          this.log.alert({'url':this.url,error:u})
          return new Error('no auth backend')
        }
        // something went very wrong ;(
        this.log.emerg({BindError1:u})
        return u
      }
    } catch (error) {
      // something went very wrong ;(
      this.log.emerg({CatchBindError:error})
      return e
    }
    if (!(u.length === 1)) {
      this.log.notice({'notuser':{username,filter}})
      return new Error('not found')
    }
    u = u[0]
    // see https://github.com/freeipa/freeipa/blob/master/install/ui/src/freeipa/datetime.js
    let e = u.krbPasswordExpiration
    if (!e && this.server === 'ipa.demo1.freeipa.org')  e='20221231011529Z'
    if (!e) return new Error('no krbPasswordExpiration')
    const ed = e.slice(0,4)+'-'+e.slice(4,6)+'-'+e.slice(6,8)+'T'+e.slice(8,10)+':'+e.slice(10,12)+':'+e.slice(12,14)+'Z'
    const passwordExpiration = new Date(ed)
    if (!(passwordExpiration instanceof Date) || isNaN(passwordExpiration)) {
      this.log.emerg({user:{username,msg:'krbPasswordExpiration not date',krbPasswordExpiration:e}})
      return new Error('krbPasswordExpiration not date ')
    }
    if (passwordExpiration < Date.now()){
      return new Error('password expired')
    }
    // OK to check password
    let ru
    try {
      ru = await fetch_ldap(this.url,u.dn,password,this.base,filter)
      if (ru instanceof Error) {
        this.log.notice({authentication:ru.message.trim(),user:username,dn:u.dn})
        if (ru.message === 'Invalid Credentials') {
          return new Error('wrong user password')
        }
        if (ru.errno === 'ECONNREFUSED') {
          this.log.alert({'url':this.url,error:ru})
          return new Error('no auth backend')
        }
        // bad user bind ..
        //this.log.notice({authentication:ru.message.trim(),user:username,dn:u.dn})
        return ru
      }
    } catch (error) {
      // something went very wrong ;(
      this.log.emerg({CatchBindError:error})
      return e
    }
    if (!(ru.length === 1)) {
      this.log.alert({user:'to many' + username, users:ru})
      return new Error('to many users')
    }
    // password ok
    //copy out groups and roles
    ru = ru[0]
    ru.roles = []
    ru.groups = []
    for (const memberof of ru.memberOf) {
      if (memberof.endsWith('cn=accounts,'+this.base)) {
        const tmp = memberof.split(',')
        const what = tmp[1]
        switch (tmp[1].split('=')[1]) {
          case 'roles':
                        ru.roles.push(tmp[0].split('=')[1])
                        break
          case 'groups':
                        ru.groups.push(tmp[0].split('=')[1])
                        break
          default:
                  this.log.alert({ERROR:{shouldnothappen:tmp}})
        }
      }
    }

    delete ru.memberOf

    const ssn = ru.employeeNumber || ''
    const  ou = ru.ou || ''
    const manager = ru.manager || ''
    const emails = ru.mail || []
    const phones = ru.mobile || []
    const fu = new User(ru.uid, ru.roles, ru.groups, ssn, ru.givenName, ru.sn, ou , manager , emails , phones, this.log)
    return fu.toObj()
  }

  async ping () {
    const user = await this.verify(this.binduser,this.bindpass)
    if (user && user.uid && user.uid === this.binduser) {
      this.log.info({ping:'ok', server:this.url,bind:{dn:this.binddn}})
    } else {
      this.log.err({baduser:user})
      this.log.info({ping:'failed', server:this.url,bind:{dn:this.binddn}})
    }
    return user
  }
}

export function createAuthFreeIPA(options) {
  if (!options) throw new Error('no options')
  return new AuthFreeIPA(options.server, options.base, options.binduser, options.bindpass, options.field, options.serialfield, options.rejectUnauthorized, options.directory,options.filename, options.time, options.logger)
}
