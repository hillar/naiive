import { Base } from './base.mjs'
import { Check } from './check.mjs'

export class RolesAndGroups extends Base {

  #roles
  #groups

  constructor (roles, groups, logger) {
    super(logger)
    this.roles = roles
    this.groups =  groups
  }

  get roles () { return ( this.#roles && this.#roles.list) ? this.#roles.list : '' }
  set roles (roles) {
    //this.log.debug({self:this.prototypeof, roles})
    if (this.#roles) this.#roles.list = roles
    else this.#roles = new Check(roles,this.logger)
  }
  isinroles (t) {return this.#roles.inList(t)}

  get groups () { return (this.#groups && this.#groups.list) ? this.#groups.list : '' }
  set groups (groups) {
    if (this.#groups) this.#groups.list = groups
    else this.#groups = new Check(groups,this.logger)
  }
  isingroups (t) {return this.#groups.inList(t)}

  allowed (r = '', g = '') {
    return (this.isinroles(r) && this.isingroups(g))
  }

}
