import { ClusterHTTPServer } from './clusterserver.mjs'
import { parseHostnamePort, isFn } from '../../utils/var.mjs'
import { hostname } from 'os'
import { request as httpRequest, createServer as createHttpServer} from 'http'

const LOCALHOST = hostname()
const RETRYTIME = 3000
export const MATESTOTALAPI = '_cluster'
export const MATEAPI = '_node'
export const HEALTHAPI = 'health'
export const STATSAPI = 'stats'
export const SETTINGSAPI = 'settings'




class Mate extends ClusterHTTPServer {

  #mates = {}
  #shadow = {}
  #nodes = []
  #started = Date.now()
  mynames = [hostname()]

  constructor (logger, name=LOCALHOST, port, about='dummy',opts = [], methods = [], health, stats){
    super(logger,name, port, about, opts, methods)
    this.health = health
    this.stats = stats
  }

  set nodes (v) { this.#nodes = (v && v.split) ? v.split(',').map( hp => parseHostnamePort(hp,this.port)) : [] }
  get mates () {
    const ms = []
    for (const key of Object.keys(this.#mates)){
      const hostname = key.split(':')[0]
      const port = Number(key.split(':')[1])
      let tmp = {hostname, port}
      if (!(this.#mates[key])) tmp.status = 'unknown'
      if (this.#mates[key] instanceof Error) tmp.error = this.#mates[key]
      if (this.#mates[key] && this.#mates[key].ip) tmp = Object.assign(tmp,this.#mates[key])
      ms.push(tmp)
    }
    return ms
  }
  addMates (mates) {
    if (!(Array.isArray(mates))) mates = (mates && mates.split) ? mates.split(',').map( hp => parseHostnamePort(hp,this.port)) : []
    const newmates = []
    for (const mate of mates) {
      if (this.isMate(mate)) continue
      //if (this.mynames.includes(mate.hostname) && mate.port === this.port) return
      //this.#mates[mate.ip + ':' + mate.port] = undefined
      //this.log.notice({'NEWMATE':mate})
      //this.subscribe([mate])
      newmates.push(mate)
    }
    if (newmates.length) this.subscribe(newmates)
  }
  isMate(mate) {
    if (Object.keys(this.#mates).includes(mate.ip + ':' + mate.port)) return this.#mates[mate.ip + ':' + mate.port]
  }
  upMate(mate,others) {
    if (!this.isMate(mate)) {
      this.#mates[mate.ip + ':' + mate.port] = mate
      //if (!this.#mates[mate.ip + ':' + mate.port].names) this.#mates[mate.ip + ':' + mate.port].names = [this.#mates[mate.ip + ':' + mate.port].hostname]
      this.log.notice({NEWMATE:mate,others})
    } else if ( this.#mates[mate.ip + ':' + mate.port] instanceof Error){
      this.#mates[mate.ip + ':' + mate.port] = mate
      this.log.notice({UPMATE:mate,others})
    }
    else {
      const names = [ ...new Set([...this.#mates[mate.ip + ':' + mate.port].names, ...mate.names]) ]
      if (names.length !== this.#mates[mate.ip + ':' + mate.port].names.length){
        this.#mates[mate.ip + ':' + mate.port].names = names
        this.log.notice({MATENAMES:mate,names})
      }
    }
    this.addMates(others)
  }
  downMate(mate,reason) {
    if (this.isMate(mate)) {
      this.#mates[mate.ip + ':' + mate.port] = reason
      if ( reason instanceof Error) this.log.warning({DOWNMATE:mate,error:reason})
      else this.log.notice({DOWNMATE:mate,reason})
  }
  }
  //fullMate(mate) {this.#mates[mate.ip + ':' + mate.port] = null }
  rmMate(mate) { delete this.#mates[mate.ip + ':' + mate.port] }

  isMateReq(headers){
    if (headers['x-mate-hostnames'] && headers['x-mate-port']){
      return {hostname:JSON.parse(headers['x-mate-hostnames'])[0],port:Number(headers['x-mate-port']),ip:headers['x-mate-ip'], names:JSON.parse(headers['x-mate-hostnames'])}
    }
  }

  async subscribe(mates,counter=0){
    if (!mates) return
    const newmates = []
    const responses = await this.matesRequest('SUBSCRIBE',mates,this.mates)
    for (const response  of responses) {
      if (response.error ) { //&& response.error.code === 'ECONNREFUSED') {
        counter ++
        this.downMate(response.mate,response.error)
        if (counter < 2 || (counter % 100 === 0)) this.log.info({DOWN:{mate:response.mate,reason:response.error.message}})
        const to = setTimeout(async () => {
          this.subscribe([response.mate],counter)
        }, RETRYTIME)
      } else {
        this.upMate(response.mate,response.response)
        //this.addMates(response.response)
      }
    }
  }


  async matesRequest (method, mates, message, url='') {
    this.log.info({ASKING:{method, mates,message, url}})
    const domates = []
    for (const mate of mates) {
      if ((this.hostname === mate.hostname || this.server.address().address === mate.ip || this.mynames.includes(mate.hostname)) && (this.port === mate.port)) continue
      domates.push( new Promise( resolve => {
        const options = {
          hostname: mate.hostname,
          port: mate.port,
          method: method,
          path: '/'+url,
          headers: {'x-mate-hostnames':JSON.stringify(this.mynames),
                    'x-mate-port':this.port,
                    'x-mate-ip': this.listening.address,
                    'x-mate-yourname': mate.hostname,
                    'x-mate-message':JSON.stringify(message)}
        }
        const req = httpRequest(options, (res) => {
            let response
            try {
              response = JSON.parse(res.headers['x-mate-response'])
            } catch (error) {
              // noop
            }
            res.on('error', (error) =>  {
              this.log.emerg({matesRequest:{error,mate}})
              resolve({statusCode:res.statusCode, mate, error})
            })
            let chunks = ''
            res.on('data', (chunk) =>  chunks += chunk )
            res.on('end', () => {
              let data
              if (chunks) {
                try {
                  data = JSON.parse(chunks)
                } catch (error) {
                  this.log.emerg({matesRequest:{mate,error,method, url, chunks}})
                }
              }
              const realmate = this.isMateReq(res.headers)
              resolve({mate:realmate, statusCode:res.statusCode, response, data})
            })
        })
        req.on('error', error => {
          resolve({mate, error, method, url})
        })
        req.end()
      }))
    }
    const initalanswers = await Promise.all(domates)
    return initalanswers
  }


  async about () {
      if (this.server.listening)  {
        const mates = this.mates
        mates.push({hostname:this.hostname,port:this.server.address().port,ip:this.server.address().address,names:this.mynames})
        return {mates}
      }

  }

  async listen(fn) {

    if (!this.server) this.server = createHttpServer()

    this.server.on('request', async (request, response) => {
      if (response.finished) {
        // if it is not first in listeners queue, die
        console.error('response finished !?!?')
        process.exit(-1)
      }
      const mate = this.isMateReq(request.headers)
      //mate.ip = request.socket.remoteAddress
      if (request.headers['x-mate-yourname']) {
        if (!this.mynames.includes(request.headers['x-mate-yourname'])) this.mynames.push(request.headers['x-mate-yourname'])
      }
      if (!(mate && mate.hostname && mate.port)) {
        if (request.method === 'REPORT' && request.url.startsWith('/'+MATESTOTALAPI+'/')){
          const bittes = request.url.replace('//','/').split('/')
          const what = bittes[2]
          if (what && (what === HEALTHAPI || what === STATSAPI || what === SETTINGSAPI)) {
            const wr = await this.matesRequest('REPORT',this.mates,null, `${MATEAPI}/${what}`)
            let self = {}
            switch (what) {
              case HEALTHAPI:
                self[HEALTHAPI] = await this.health()
                break
              case STATSAPI:
                self[STATSAPI] = await this.stats()
                break
              case SETTINGSAPI:
                self[SETTINGSAPI] = this.defaults
                break
              default:
                self['error'] = 'not implemented ' + what
            }
            self.names = this.mynames
            wr.push({mate:{hostname:this.hostname,port:this.port,started:this.#started},data:self})
            response.write(JSON.stringify(wr))
            this.log.notice({ip:request.socket.remoteAddress,url:request.url,what,response:wr})
          } else { // is api, but bad endpoint
            this.log.notice({ip:request.socket.remoteAddress,url:request.url, error:501})
            response.statusCode = 501
            response.end()
          }
          response.end()
        } else { // not api, do noop, let next listeners do something ..
          //this.log.notice({ip:request.socket.remoteAddress,url:request.url, error:404})
          //response.statusCode = 404
          //response.end()
        }
      } else { // got from mate
        // this.log.info({mate,method:request.method,url:request.url})
        response.setHeader('x-mate-hostnames',JSON.stringify(this.mynames))
        response.setHeader('x-mate-port',this.server.address().port)
        response.setHeader('x-mate-ip',this.server.address().address)
        let message
        try {
          message = JSON.parse(request.headers['x-mate-message'])
        } catch (error) {
          this.log.err({shouldnothappen:error})
        }
        switch (request.method) {
          case 'UNSUBSCRIBE':
            this.log.info({UNSUBSCRIBE:mate})
            this.rmMate(mate)
            response.end()
            break
          case 'SUBSCRIBE':
            //this.log.info({SUBSCRIBE:{from:mate,mates:message}})
            this.upMate(mate,message)
            //this.addMates(message)
            response.setHeader('x-mate-response',JSON.stringify(this.mates))
            response.end()
            break
          case 'REPORT':
            const bittes = request.url.replace('//','/').split('/')
            if (bittes[1] === MATEAPI) {
                const what = bittes[2]
                switch (what) {
                  case STATSAPI:
                    const stats = await this.stats()
                    this.log.info({what,stats,replayto:mate})
                    response.write(JSON.stringify({stats,names:this.mynames}))
                    response.end()
                    break
                  case HEALTHAPI:
                    const health = await this.health()
                    this.log.info({what,health,replayto:mate})
                    response.write(JSON.stringify({health,names:this.mynames}))
                    break
                  case SETTINGSAPI:
                    const settings = this.defaults
                    const started = this.#started
                    this.log.info({what,settings,replayto:mate})
                    response.write(JSON.stringify({settings,started,names:this.mynames}))
                    break
                  default:
                    this.log.emerg({API:{what,replayto:mate,url:request.url}})
                    response.statusCode = 501
                }
            } else {
              this.log.emerg({API:{mate,notapi:request.url}})
              response.statusCode = 501
            }
            response.end()
            break
          default:
            return
        }
      }
    })

    super.listen(fn)

    this.server.on('listening', async () => {
      this.mynames.push(this.server.address().address)
      if (this.#nodes.length > 0) this.addMates(this.#nodes)
      else this.log.warning('NO NODES, WILL NOT BE PART OF ANY CLUSTER')
    })

  }
}

export function createMate(options){
  if (!options) throw new Error('no options')
  if (!options.health) throw new Error('no health')
  if (!options.stats) throw new Error('no stats')
  if (!options.methods) throw new Error('no methods')
  let methods = []
  if (Array.isArray(options.methods)) methods = options.methods
  else {
    for (const method of Object.keys(options.methods)){
      //if (isFn(options.methods[method])) {
        const tmp = {}
        tmp[method] = options.methods[method]
        methods.push(tmp)
      //} else throw new Error('no function for '+ method)
    }
  }
  let opts = []
  if (options.opts) {
    if (Array.isArray(options.opts)) opts = options.opts
    else {
      for (const opt of Object.keys(options.opts)){
        if (isFn(options.opts[opt])) {
          const tmp = {}
          tmp[opt] = options.opts[opt]
          opts.push(tmp)
        } else throw new Error('no function for '+ opts)
      }
    }
  }
  return new Mate(options.logger,options.ip,options.port,options.about,opts, methods,options.health,options.stats)

}
