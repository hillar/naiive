// for cluster see https://nodejs.org/api/cluster.html
import cluster from 'cluster'
import { cpus } from 'os'
import { createServer as createHttpServer, IncomingMessage } from 'http'
import { Socket } from 'net'
import { Base } from '../../classes/base/base.mjs'
import { is, isFn, isAsyncFunction, isObject } from '../../utils/var.mjs'
import { ip } from '../../utils/req.mjs'
import { ROUTEMETHODS as METHODS } from '../../constants/routemethods.mjs'
import { LOGMETHODS } from '../../constants/logmethods.mjs'
import { Command } from '../../../../packages/vendor/commander/index.mjs'

const PORT = 6789
const IP = '0.0.0.0'
const FORKIF = 100

function usageTotalNS(usage) {
  // microseconds to nanoseconds
  return (usage.user * 1000.0) + (usage.system * 1000.0)
}

function getUSage(startTime, startUsage) {
    const elapTime = process.hrtime.bigint() - startTime //bigint nanoseconds
    const tmp = process.cpuUsage(startUsage)
    const elapUsage = usageTotalNS(tmp) // nanoseconds
    const cpuPercent = (100.0 * elapUsage / Number(elapTime)).toFixed(1)
    //console.log(process.pid,cpuPercent,Number(elapTime),elapUsage,startUsage, tmp)
    return [ process.hrtime.bigint(), process.cpuUsage(), cpuPercent]
}

let time = process.hrtime.bigint()
let usage = process.cpuUsage()
let percent = 0


setInterval( async () => {
  [ time, usage, percent ] = getUSage(time, usage)

},1000)


async function notify(message, ...args){
  if (process.send) process.send({ pid:process.pid, resourceUsage:percent, message, args})
  //else console.log({resourceUsage:percent, message })
}

export class ClusterHTTPServer extends Base {
  #about
  #hostname = 'localhost'
  #port

  #methods = {}
  #configFile
  #mincpus
  #maxcpus
  server
  cluster
  listening


  constructor(logger, ip = IP, port, about='dummy',opts = [], methods = [] ) {
    if (!methods) throw new Error('no methods')
    super( logger, ...opts )

      for (const method of methods) {
        const name = Object.keys(method)[0].toLowerCase()
        if (name) {
          this.setMethod(name,method[name])
        } else throw new Error('not a method')
      }
    this.port = port
    this.#about = about
    this.hostname = ip

  }

  async about () {
  if (isFn(this.#about)) {
  if (isAsyncFunction(this.#about)){
  try {
  const r = await this.#about()
  return r
  } catch (error) {
  this.log.emerg('about afn',error)
  if (error instanceof ReferenceError || error instanceof SyntaxError ) {
            console.error(Object.getPrototypeOf( error ).constructor.name, this.prototypeof)
            console.error(error)
            process.exit(-1)
          }
  }
  } else {
  try {
  return this.#about()
  } catch (error) {
  this.log.emerg('about fn',error)
  if (error instanceof ReferenceError || error instanceof SyntaxError ) {
            console.error(Object.getPrototypeOf( error ).constructor.name, this.prototypeof)
            console.error(error)
            process.exit(-1)
          }
  }
  }
  } else return this.#about
  }

  get mincpus () { return this.#mincpus ? this.#mincpus : 2 }
  set mincpus (v) {Number(v) ? this.#mincpus = Number(v) : this.#mincpus = 2}
  get maxcpus () { return this.#maxcpus ? this.#maxcpus : cpus().length - this.mincpus}
  set maxcpus (v) { Number(v) ? this.#maxcpus = Math.min(Number(v),(cpus().length - this.mincpus)) : this.#maxcpus = cpus().length - this.mincpus}
  get hostname () {return this.#hostname}
  set hostname (n) {
  if (!n || !n.trim().length) throw new Error(this.typeof + ' no hostname')
  else this.#hostname = n
  }
  get port () { return this.#port}
  set port (p) {
  if (!p) throw new Error(this.typeof + ' no port')
  p = parseInt(p)
  if (!Number.isInteger(p)) throw new Error(this.typeof + ' port not a number')
  if (p > 65534) throw new Error(this.typeof + ' port must be less than 65535')
  this.#port = p
  }

  get methods () {
  return Object.keys(this.#methods)
  }

  setMethod(name,fn) {
    if (!METHODS.includes(name)) throw new Error('method name not allowed: ' + name)
    if (!fn) throw new Error('no function for' + name)
    let _fn
    let _ping
    if (fn) {
      if (fn.fn) _fn = fn.fn
      else _fn = fn
      if (fn.ping) _ping = fn.ping
    }
    if (!isFn(_fn)) throw new Error('not a function for ' + name)
    this.#methods[name] = {}
    if (isAsyncFunction(_fn)) {
      this.#methods[name].fn = async (req, res, logger) => {
      try {
        await _fn(req, res, logger)
      } catch (error) {
        this.log.emerg({asyncmethod:{name, error}})
        res.writeHead(500)
        if ((error instanceof ReferenceError)) process.exit(-1)
      }
      }
    } else {
      this.#methods[name].fn = (req, res, logger) => {
            return new Promise(async (resolve) => {
      try {
      _fn(req, res, logger)
      } catch (error) {
      this.log.emerg({syncmethod:{name, error}})
      res.writeHead('500')
      if ((error instanceof ReferenceError)) process.exit(-1)
      }
      resolve()
      })
      }
    }
    if (_ping) {
      if (isAsyncFunction(_fn)) {
      this.#methods[name].ping = async () => {
      try {
      await _ping(this.log)
      } catch (error) {
      this.log.emerg({method:name, error})
      if ((error instanceof ReferenceError)) process.exit(-1)
      }
      }
      } else {
      this.#methods[name].ping = () => {
      return new Promise(async (resolve) => {
      try {
      _ping(this.log)
      } catch (error) {
      this.log.emerg({method:name, error})
      if ((error instanceof ReferenceError)) process.exit(-1)
      }
      resolve()
      })
      }
      }
    }
  }

  async listen (onlistening) {

    // pre start

    const cliParams = new Command()
    cliParams
      .version('0.0.1')
      .usage('[options]')
      .option('-D, --docker','gen Dockerfile ARG and ENV skeleton')
      .option('-T, --dump-config','dump configuration')
      .option('-U, --dump-config-undefined','dump configuration with undefined')
      .option('-c, --config [file]', 'set configuration file','./config.js')
  .option('--ping', 'ping all internal methods',)
      for (const param of this.settings) {
        cliParams.option('--'+param+ ' ['+typeof this[param]+']','server '+param+ ' (default: '+this[param]+')')
      }
  cliParams.parse(process.argv)
  let configFile = cliParams.config || './config.js'
    this.#configFile = configFile
    try {
      configFile = realpathSync(configFile)
      this.#configFile = configFile
    } catch (e) {
      this.#configFile = undefined
      if (!(configFile === './config.js')) this.log.info({Config:'no file ' + configFile})
    }
    let conf = {}
    // load config file
    try {
      if (this.#configFile) {
        conf = JSON.parse(readFileSync(this.#configFile,"utf8"))
        //this.log.debug('conf',conf,CONFIG)
      }
    } catch (e) {
      this.log.info({Config:configFile,error:e.message})
      this.#configFile = undefined
    }
  // patch conf with command line params
  for (const param of this.settings) {
  if (!!cliParams[param] && cliParams[param] != conf[param]) conf[param] = cliParams[param]
  }
  // conf ready, apply now
  this.readConfig(conf)
  // just dump conf
  if (cliParams.dumpConfig) {
  process.stdout.write(['/* config dump  */\nmodule.exports = ',JSON.stringify(this.config,null,'\t'),'\n'].join('\n'))
  process.exit(0)
  }
  // dump conf with undefined as nulls
  if (cliParams.dumpConfigUndefined) {
  process.stdout.write(['/* config dump  with undefined as nulls */\nmodule.exports = ',JSON.stringify(this.config,function(k, v) { if (v === undefined) { return null; } return v; },'\t'),'\n'].join('\n'))
  process.exit(0)
  }
  // generate Dockerfile skeleton
  if (cliParams.docker) {
       let str = '\n'
  //str += "FROM node:13-alpine AS base\n"
  //str += "MAINTAINER 'no maintenance intended'\n"
  str += "\n"
  str += "# Declare args\n"
  for (const a of this.settings){
  str += `ARG ${a.toUpperCase()}\n` // =${flat[a]}
  }
  str += "\n"
  str += "# Declare envs vars for each arg\n"
  for (const a of this.settings) {
  str += `ENV ${a.toUpperCase()} $${a.toUpperCase()}\n`
  }
  str += "\n"

  process.stdout.write(str)
  process.exit(0)
  }
  // ping methods
  if (cliParams.ping) {
  for (const method of this.methods){
  this.log.info({ping:method})
  const req = new IncomingMessage()
  req.socket = new Socket()
  const res = {writeHead:()=>{},write:()=>{},end:()=>{}} //new http.ServerResponse()
  if (this.#methods[method].ping) await this.#methods[method].ping(req,res,this.log)
  else await this.#methods[method].fn(req,res,this.log)
  }
  process.exit(0)
  }

    // start it
    const workers = {}
    if (cluster.isMaster && cpus().length > this.mincpus) {

      process.on('exit', (code) => {
        this.log.notice({process:{exit:code}})
        cluster.on('exit', () => {})
        for (const id in cluster.workers) cluster.workers[id].kill()
      })
      process.on('SIGINT', () => {
        this.log.notice({process:'SIGINT'})
        cluster.on('exit', () => {})
        for (const id in cluster.workers) cluster.workers[id].kill()
        process.exit(0)
      })
      process.on('SIGTERM', () => {
        this.log.notice({process:'SIGTERM'})
        cluster.on('exit', () => {})
        for (const id in cluster.workers) cluster.workers[id].kill()
        process.exit(0)
      })
      process.on('SIGHUP', () => {
        this.log.notice({process:'SIGHUP',msg:'reloading workers'})
        for (const id in cluster.workers) cluster.workers[id].kill()
      })

      // start with single worker
      const worker = cluster.fork()
      workers[worker.process.pid] = {}
      workers[worker.process.pid].worker = worker
      workers[worker.process.pid].jobs = []
      this.log.notice({started:worker.process.pid})

      cluster.on('error', (error) => {
        this.log.emerg({cluster:error})
        console.log('cluster error',error)
        console.error(error)
      })

      cluster.on('exit', (deadWorker, code, signal) => {
        var oldPID = deadWorker.process.pid;
        delete workers[oldPID]
        //console.log(signal, code, oldPID+' worker exit ..', Object.keys(workers));
        this.log.notice({exit:{worker:oldPID},signal,code})
        if (Object.keys(workers).length === 0) {
          if (code > 0) {
            this.log.emerg({cluster:'can not run worker'})
            //console.error('worker can not be started', code, signal)
            process.exit(1)
          }
          //console.log('starting new worker, because none left')
          const worker = cluster.fork()
          workers[worker.process.pid] = {}
          workers[worker.process.pid].worker = worker
          workers[worker.process.pid].jobs = []
          this.log.notice({started:worker.process.pid})
        }
      });
      cluster.on('listening', (worker, address) => {
        //console.log(worker.process.pid,'listening',address)
        this.log.info({listening:worker.process.pid})
      });
      cluster.on('message', (worker, message, handle) => {
        if (message.message === 'start' && workers[worker.process.pid]) {
          if (workers[worker.process.pid].timer) clearTimeout(workers[worker.process.pid].timer)
          workers[worker.process.pid].jobs.push(message.resourceUsage)
          //if(workers[worker.process.pid].jobs.length > MAXCLIENTS) {
          //console.log(Object.keys(workers).length < this.maxcpus,Object.keys(workers).length,this.maxcpus,message.resourceUsage > FORKIF,message.resourceUsage)
          if (Object.keys(workers).length < this.maxcpus && message.resourceUsage > FORKIF){
              const wrkr = cluster.fork()
              workers[wrkr.process.pid] = {}
              workers[wrkr.process.pid].worker = wrkr
              workers[wrkr.process.pid].jobs = []
              //console.log(wrkr.process.pid, 'new worker', Object.keys(workers))
              this.log.notice({started:wrkr.process.pid,load:message.resourceUsage})
          //}
          }
        } else if (message.message === 'end' && workers[worker.process.pid]) {
          workers[worker.process.pid].jobs.shift()
          //console.log(message,workers[worker.process.pid].jobs.length)
          if (workers[worker.process.pid].jobs.length === 0 ) {
            if (Object.keys(workers).length > 1) {
            workers[worker.process.pid].timer = setTimeout(() => {
              if (Object.keys(workers).length > 1) {
                this.log.info({stoping:worker.process.pid})
                if (workers[worker.process.pid]) workers[worker.process.pid].worker.kill()
              }
            }, 1000);
            }
          }

        } else console.log(message)
      })
      this.cluster = cluster
    } else {
      // last = getUsage()
     //this.log.notice('worker ..') // worker

   if (!this.server) this.server = createHttpServer()

     this.server.on('request', async (req, res) => {

       if (res.finished) {
         //console.log(this.prototypeof, 'on request', 'res.finished')
         return
       }

       req.on('error', (error) => {
         this.log.err({req:error})
         this.server.emit('error',error)
       })

       res.on('error', (error) => {
         console.error(error)
         this.log.err({res:error})
         if (!res.finished) {
           res.statusCode = 500
           res.end()
         }
         //this.server.emit('error',error)
       })

  const method = req.method.toLowerCase()
  if (method==='get' && req.url === '/') {
  //this.log.debug('about',this.about)
  //res.write(JSON.stringify(this.about))
  const a = await this.about()
  //this.loga)
  if (a && Object.keys(a).length>0) res.write(JSON.stringify(a))
        res.end()
        this.log.info({about:req.socket.remoteAddress})

  } else {
  const logger = {}
  for (const logmethod of LOGMETHODS){
  logger[logmethod] = (...messages) => {
  let msg = []
  let ctx = {method,ip:ip(req)}
  for (const m of messages) {
  if (m instanceof Object) {
  ctx = Object.assign(ctx,m)
  } else msg.push(m)
  }
  if (msg.length > 0 ) ctx.messages = msg
  this.log[logmethod](ctx)
  }
  }
  if (this.methods.includes(method)){
  //this.log.info({url:req.url,headers:req.headers})
          notify('start')
          try {
            await this.#methods[method].fn(req,res,logger)
                 //if (!res.finished) res.end()
          } catch (error) {
            this.log.err({shouldnothappen:{method,error}})
  					res.writeHead(500)
            res.end()
          }
          notify('end')
    } else {
      //
      // not implemented
      //if (!res.finished)
      if (!(res.statusCode )) {
        this.log.notice({httperror:501})
        res.statusCode = 501
        res.end()
      }
      }
  }
      //console.dir(res)
  //if (!res.finished) res.end()
  })

  this.server.on('error', (err) => {
  this.log.err({server:err})
      //socket.end('HTTP/1.1 500 Server Error\r\n\r\n')
  })

  this.server.on('clientError', (err, socket) => {
  console.dir(err.toString())
  socket.end('HTTP/1.1 400 '+err.toString()+'\r\n\r\n')
  this.log.info({clientError:err})
  })

  this.server.on('listening', () => {
      if (!process.send) this.log.info({SINGLE:this.server.address()})
  else this.log.info({WORKER:this.server.address()})
  this.listening = this.server.address()
      if (onlistening) {
        this.log.info({onlistening:onlistening.toString()})
        try {
          onlistening()
        } catch (error) {
          this.log.err({onlistening:error})
        }
      }
  })

    process.on('exit', (code) => {
      this.server.close()
      this.log.notice({process:{exit:code}})
    })
    process.on('SIGTERM', () => {
      this.log.notice({process:'SIGTERM'})
      process.exit(0)
    })

    // go over all just to die early
  await this.about()
  for (const method of this.methods){
  const req = new IncomingMessage()
  req.socket = new Socket()
  const res = {writeHead:()=>{},write:()=>{},end:()=>{}} //new http.ServerResponse()
  await this.#methods[method].fn(req,res,this.log)
  }

    try {
      this.server.listen(this.port,this.hostname)
    } catch (error) {
      this.log.emerg({listen:error})
    }
  }
  } // end worker
}

export function createClusterHTTPServer(options) {
  if (!options) throw new Error("no options");
  if (!options.methods) throw new Error("no methods");
  let methods = [];
  if (Array.isArray(options.methods)) methods = options.methods;
  else {
    for (const method of Object.keys(options.methods)) {
      //if (isFn(options.methods[method])) {
      const tmp = {};
      tmp[method] = options.methods[method];
      methods.push(tmp);
      //} else throw new Error('no function for '+ method)
    }
  }
  let opts = [];
  if (Array.isArray(options.opts)) opts = options.opts;
  else {
    for (const opt of Object.keys(options.opts)) {
      if (isFn(options.opts[opt])) {
        const tmp = {};
        tmp[opt] = options.opts[opt];
        opts.push(tmp);
      } else throw new Error("no function for " + opts);
    }
  }
  return new ClusterHTTPServer(options.logger, options.ip, options.port, options.about, opts, methods);
}
