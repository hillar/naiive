
import { existsSync, accessSync, readFileSync, constants, writeFileSync  } from 'fs'
import { join, resolve as resolvename } from 'path'
import { isString } from '../../utils/var.mjs'
import { Base } from './base.mjs'
import { User } from './user.mjs'

const USERCACHEFILENAME = 'users.json'
const USERCACHEDIR = './cache'
const USERCACHETIME = 1000 * 60

export class AuthBase extends Base {

  #users = {}
  #cachetime
  #filecache = false
  #cachedir
  #cachefile

  constructor (logger,directory = USERCACHEDIR,filename = USERCACHEFILENAME, cachetime = USERCACHETIME) {
      super(logger)
      this.cachetime = cachetime
      this.cachedir = directory
      this.cachefile = filename
  }

  get cachetime () { return this.#cachetime }
  set cachetime (ms) { if (!isNaN(ms)) this.#cachetime = ms }

  get cachefullname () { if (this.filecache) return resolvename(join(this.cachedir,this.cachefile)) }

  get cachedir () {return this.#cachedir}
  set cachedir (cd) {
    if (!cd) cd = USERCACHEDIR
    if (!(isString(cd))) throw new Error(this.typeof + ' :: directory not string  ' + typeof cd)
    if (!existsSync(cd)) {
      this.filecache = false
      this.log.warning({notexists:resolvename(cd)})
      return
    } else this.#cachedir = cd
    if (this.cachefile) {
      this.log.info({'new users cache directory ': this.cachefullname})
      this.saveCache()
    }
  }
  get cachefile () {return this.#cachefile}
  set cachefile (cf) {
    if (!cf) cf = USERCACHEFILENAME
    if (!(isString(cf))) throw new Error(this.typeof + ' :: filename not string  ' + typeof cf)

    if (this.filecache) {
      if (existsSync(join(this.#cachedir,cf))) {
        try {
            accessSync(join(this.#cachedir,cf), constants.W_OK)
          } catch (err) {
            this.filecache = false
            return
          }
          this.#cachefile = cf
          this.#users = this.loadCache()
          return
      } else {
        if (this.#cachefile !== cf) this.log.info({'new users cache ':this.#cachedir+'/'+cf})
        this.#cachefile = cf
        this.saveCache()
      }
    }
  }

  async verify(username,password) {

    if (!(isString(username))) throw new Error(this.typeof + ' :: username not string  ' + typeof username)
    if (!(isString(password))) throw new Error(this.typeof + ' :: password not string  ' + typeof password)

    return new Promise(async (resolve) => {
      if (!username.trim()) {
        this.log.alert('no username')
        resolve(new Error('no username'))
      } else {
        if (!password.trim()) {
          this.log.alert({'no password': username})
          resolve(new Error('no password'))
        } else {
          if (this.#users[username]) {
            const now = Date.now()
            if (this.#users[username].lastVerify + this.cachetime > now) {
              resolve(this.#users[username])
            } else {
              this.#users[username] = await this.reallyVerify(username,password)
              if (this.#users[username] instanceof Error){
                resolve({})
                return
              } else if (this.#users[username].uid !== username) {
                this.log.notice({authentication:'username do not match uid',uid:this.#users[username].uid, username})
                resolve({})
                return
              } else {
              this.#users[username].lastVerify = Date.now()
              this.saveCache()
              resolve(this.#users[username])
              }
            }
          } else {
                  let user
                  try {
                    user  = await this.reallyVerify(username,password)
                  } catch (e){
                    this.log.emerg({reallyVerify:e.message})
                    resolve(e)
                    return
                    //throw e
                  }
                  if (user instanceof Error) {
                    resolve(user)
                    return
                  }
                  if (user.uid !== username) {
                    resolve(new Error('not original user'))
                  } else {
                    this.#users[username] = user
                    this.#users[username].lastVerify = Date.now()
                    this.#users[username].firstVerify = Date.now()
                    this.saveCache()
                    resolve(this.#users[username])
                  }
          }
        }
      }
    })
  }

  async reallyVerify (username,password) {
    if (!(Object.getPrototypeOf(this).constructor.name === 'AuthBase')) throw new Error('reallyVerify not implemented')
    //uid,ssn, fn,ln,ou,manager,emails,phones,roles,groups
    const user = new User(this.log,'dummy','1234567','firstname','lastname','org unit','manager','','','','')
    return new Promise((resolve) => {
            // Wait a bit
            setTimeout(() => {
                resolve(user.toObj())
            }, 100)
        })
  }

  loadCache () {
    if (this.filecache && this.cachefullname) {
      if (existsSync(this.cachefullname)){
        let u
        try {
          u = JSON.parse(readFileSync(this.cachefullname))
        } catch (e) {
          this.log.err(e)
          return {}
        }
        this.log.info({'loaded users cache from ':this.cachefullname})
        return u
      } else this.log.emerg({notexists:this.cachefullname})
    }
    return {}
  }
  saveCache () {
    if (this.filecache && this.cachefullname) {
      try {
        writeFileSync(this.cachefullname, JSON.stringify(this.#users))
      } catch (error) {
        this.log.err(error)
      }
    }
  }

}
