/*

Level	Description
emerg	System is unusable
alert	Action must be taken immediately
crit	Critical conditions
err	Error conditions
warn	Warning conditions
notice	Normal, but significant conditions
info	Informational messages
debug	Debugging messages





[%t]	timestamp for the message
[%l]	level of the message
[pid %p]	process pid
%F	source code file and line
%E	error status code and string
%a	client IP address and string
%M	the log message

Facilities ...

*/

/*
TODO
Error.prepareStackTrace = function(er, stack) {
    ...
 }
*/




const LOGGER = Object.prototype.toString.call(process.logger) === "[object Function]"

export class Event {
  constructor(message = {}, o = {}) {
    this.level = this.constructor.name.toLowerCase()
    this.timestamp = new Date()
    if (message instanceof Error) {
      this.message = message.message
      this.stack = message.stack.split("\n")
      this.stack.shift()
      Error.captureStackTrace(message, this.constructor)
      const tmp = message.stack.split("\n")
      tmp.shift()
      this.stack.push(...tmp)
      for (const key of Object.keys(message)) this[key] = message[key]
    } else {
      if (typeof message === "string") this.message = message
      else for (const key of Object.keys(message)) this[key] = message[key]
      for (const key of Object.keys(o)) this[key] = o[key]
      const tmp = {}
      Error.captureStackTrace(tmp, this.constructor)
      this.stack = tmp.stack.split("\n")
      this.stack.shift()
    }
    //if (process.stdout.isTTY) console.dir({ ...this })
    if (LOGGER) process.logger({ ...this })
  }
  get json() {
    return JSON.stringify({ ...this })
  }
}

export class LogEvent extends Event {}
export class ErrorEvent extends Event {}

export class Info extends LogEvent {}
export class Notice extends LogEvent {}
export class Warn extends LogEvent {}
export class Err extends ErrorEvent {}
export class Crit extends ErrorEvent {}
export class Alert extends ErrorEvent {}
export class Emerg extends ErrorEvent {}
