import { isFn, isString } from '../../utils/var.mjs'
import { settable } from './settable.mjs'
import { ROUTEMETHODS } from '../../constants/routemethods.mjs'
import { LOGMETHODS } from '../../constants/logmethods.mjs'
import { Logger } from './logger.mjs'

const __ignore_settings___ = [
	'logger',
	'checklist',
	'about',
	...ROUTEMETHODS
]


export class Base extends settable{
  #log = {}
	constructor( logger, ...opts ) {
		super( ...opts)
		if ( ! logger ) logger = new Logger()
		else if (logger instanceof Logger) {
				//this.#log = logger
				// noop
		} else {
			for ( const method of LOGMETHODS ) {
				if ( !logger[ method ] || !isFn( logger[ method ] )) throw new Error( this.prototype + '  logger has no method ' + method )
			}
			//this.#log = logger
		}
		for ( const method of LOGMETHODS ) {
			this.#log[method] =  ( ...msgs ) => {
				  const tmp = {ctx:this.prototypeof}
				  for (const m  of msgs) {
						if (!m) continue
						if (isString(m)) {
							if (!tmp['msg']) tmp['msg'] = []
							tmp['msg'].push(m)
						} else if (Object.keys(m).length ) {
							for (const k of Object.keys(m)) tmp[k] = m[k]
						}
					}
					logger[ method ](tmp )
			}
		}
	}

	// deprecated, use log()
	get logger() {	return this.#log }
	get log() {	return this.#log }

	// current values of all settings
	get config() {

		const conf = {}
		for ( const setting of this.settings ) {

			conf[ setting ] = this[ setting ]

		}
		return conf

	}

	// set config (conf) { <-- RangeError: Maximum call stack size exceeded
	// re-read the configuration
	readConfig( conf ) {

		if ( conf ) {

			//const settings = this.settings
			for ( const setting of this.settings ) {

				if ( conf[ setting ] && ( this[ setting ] !== conf[ setting ] ) ) {
					const configChanges = {}
					const tmp = this[ setting ]
					this[ setting ] = conf[ setting ]
					if (this[ setting ] !== tmp) {
						configChanges[ setting ] = { old: tmp }
						configChanges[ setting ]['new'] = this[ setting ]
						this.log.notice( { configChanges } )
					}


				}

			}

		}

	}

  /*
	ping() {

		throw new Error( 'no ping' )

	}
  */

}
