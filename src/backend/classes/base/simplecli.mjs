import { Base } from '../../classes/base/base.mjs'
import { is, isFn, isAsyncFunction, isObject } from '../../utils/var.mjs'
import { LOGMETHODS } from '../../constants/logmethods.mjs'
import { Command } from '../../../../packages/vendor/commander/index.mjs'


export class simpleCli extends Base {

	#configFile
	#fn

	constructor(logger, opts = [], fn) {
		if (!fn || !isFn(fn)) throw new Error('no function')
		super( logger, ...opts )
		this.#fn = fn
  }
	/*
  setMethod(_fn) {

		if (!isFn(_fn)) throw new Error('not a function')
		this.#methods = {}
    const name = 'run'
		if (isAsyncFunction(_fn)) {
			this.#methods[name] = {}
			this.#methods[name].fn = async (...o) => {
				try {
					await _fn(...o)
				} catch (error) {
					this.log.emerg({method:name, error})
					if ((error instanceof ReferenceError)) process.exit(-1)
				}
			}
		} else {
			this.#methods[name].fn = (...o) => {
	      return new Promise(async (resolve) => {
					try {
						_fn(...o)
					} catch (error) {
						this.log.emerg({method:name, error})
						if ((error instanceof ReferenceError)) process.exit(-1)
					}
					resolve()
				})
			}
		}
  }
	*/
  async prerun () {
		const cliParams = new Command()
    cliParams
      .version('0.0.1')
      .usage('[options]')
      .option('-D, --docker','gen Dockerfile ARG and ENV skeleton')
      .option('-T, --dump-config','dump configuration')
      .option('-U, --dump-config-undefined','dump configuration with undefined')
      .option('-c, --config [file]', 'set configuration file','./config.js')
      for (const param of this.settings) {
        cliParams.option('--'+param+ ' ['+typeof this[param]+']','server '+param+ ' (default: '+this[param]+')')
      }
		cliParams.parse(process.argv)
		let configFile = cliParams.config || './config.js'
    this.#configFile = configFile
    try {
      configFile = realpathSync(configFile)
      this.#configFile = configFile
    } catch (e) {
      this.#configFile = undefined
      if (!(configFile === './config.js')) this.log.info({Config:'no file ' + configFile})
    }
    let conf = {}
    // load config file
    try {
      if (this.#configFile) {
        conf = JSON.parse(readFileSync(this.#configFile,"utf8"))
        this.log.debug('conf',conf,CONFIG)
      }
    } catch (e) {
      this.log.info({Config:configFile,error:e.message})
      this.#configFile = undefined
    }
		// patch conf with command line params
		for (const param of this.settings) {
			if (!!cliParams[param] && cliParams[param] != conf[param]) conf[param] = cliParams[param]
		}
		// conf ready, apply now
		this.readConfig(conf)
		// just dump conf
		if (cliParams.dumpConfig) {
			process.stdout.write(['/* config dump  */\nmodule.exports = ',JSON.stringify(this.config,null,'\t'),'\n'].join('\n'))
			process.exit(0)
		}
		// dump conf with undefined as nulls
		if (cliParams.dumpConfigUndefined) {
			process.stdout.write(['/* config dump  with undefined as nulls */\nmodule.exports = ',JSON.stringify(this.config,function(k, v) { if (v === undefined) { return null; } return v; },'\t'),'\n'].join('\n'))
			process.exit(0)
		}
		// generate Dockerfile skeleton
		if (cliParams.docker) {
	     let str = '\n'
			//str += "FROM node:13-alpine AS base\n"
			//str += "MAINTAINER 'no maintenance intended'\n"
			str += "\n"
			str += "# Declare args\n"
			for (const a of this.settings){
				str += `ARG ${a.toUpperCase()}\n` // =${flat[a]}
			}
			str += "\n"
			str += "# Declare envs vars for each arg\n"
			for (const a of this.settings) {
				str += `ENV ${a.toUpperCase()} $${a.toUpperCase()}\n`
			}
			str += "\n"

			process.stdout.write(str)
			process.exit(0)
		}
		this.log.info('prerun ok')
  }
	async postrun(){
		this.log.info('postrun')
	}
	async _run(){
		if (isAsyncFunction(this.#fn)) {
			await this.#fn(this.log)
		} else {
			await new Promise( resolve => {
				this.#fn(this.log)
				resolve()
			})
		}
	}
	async run() {
		await this.prerun()
		await this._run()
		await this.postrun()
		process.exit(0)
  }
}

export function createSimpleCli (options) {
  if (!options) throw new Error('no options')
	let fn
	if (options.fn && isFn(options.fn)) fn = options.fn
	else if (isFn(options)) fn = options
	else throw new Error('no function')

	let opts = []
  if (options.opts) {
	if (Array.isArray(options.opts)) opts = options.opts
    else {
  		for (const opt of Object.keys(options.opts)){
  			//if (isFn(options.opts[opt])) {
  				const tmp = {}
  				tmp[opt] = options.opts[opt]
  				opts.push(tmp)
  			//} else throw new Error('no function for '+ opts)
  		}
  	}
  }
  return new simpleCli(options.logger,opts, fn)
}
