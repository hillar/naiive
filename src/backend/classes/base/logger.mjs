import { hostname } from 'os'
import { parse } from 'path'
import { LOGMETHODS } from '../../constants/logmethods.mjs'

const SEVERITYERROR = [
	'emerg',
	'alert',
	'crit',
	'err'
]

const SEVERITYLOG = [
	'warning',
	'notice',
	'info'
]


function nowAsJSON() {

	const now = new Date()
	return now.toJSON()

}

export class Logger {

	constructor() {

    // test if all LOGMETHODS exists here
    if ( ! ( LOGMETHODS.every( e => [ 'debug', ...SEVERITYLOG, ...SEVERITYERROR ].includes( e ) ) ) ) throw new Error( 'LOGMETHODS do not match' )


		if ( ! process.alias ) {

			let pe = parse( process.argv[ 1 ] )
			process.alias = pe.base.split('.').shift() === 'index' ? pe.dir.split( '/' ).pop() : pe.base
			if (process.alias === 'bin') process.alias = pe.dir.split( '/' )[pe.dir.split( '/' ).length-2]
		}
		// TODO rfc5424 6.3.  STRUCTURED-DATA
		for ( const method of SEVERITYLOG ) {

			this[ method.toLowerCase() ] = ( ctx, ...msg ) => console.log( nowAsJSON(), hostname(), '[', process.alias, ':', process.pid, ']', method.toUpperCase(), ':', JSON.stringify( ctx ), ...msg )

		}
		for ( const method of SEVERITYERROR ) {

			this[ method.toLowerCase() ] = ( ctx, ...msg ) => {
				console.error( nowAsJSON(), hostname(), '[', process.alias, ':', process.pid, ']', method.toUpperCase(), ':', JSON.stringify( ctx ), ...msg )
				for (const k  of Object.keys(ctx)) {
					if (ctx[k] instanceof Error) {
						console.error(k)
						console.error(Object.getPrototypeOf( ctx[k] ).constructor.name)
						console.error(ctx[k])
					} else if (ctx[k] instanceof Object){
						console.error(k)
						console.dir(ctx[k])
					}
				}
				for (const m of msg) {
					if (m instanceof Error) {
						console.error(Object.getPrototypeOf( error ).constructor.name)
						console.error(m)
					} else console.error(m)
				}
			}

		}
	}
	debug( ...o){
		console.log(`--- DEBUG ${o[0].ctx} ---`)
		for (const oo of o) {
			for (const ooo of Object.keys(oo)) {
				console.log(ooo)
				console.dir(oo[ooo])
			}
		}
		console.log('--- debug ---')
	}

}
