import { trimArrayOfStrings, isString } from '../../utils/var.mjs'
import { Base } from './base.mjs'

export class Check extends Base {

	#list

	constructor( checklist, logger ) {

		super( logger )

		this.inList = () => {

			return false

		}
/*
		Object.defineProperty( this, '_list', {
			enumerable: false,
			configurable: false,
			writable: true
		} )
*/
		this.list = checklist

	}


	get list () {

		if ( Array.isArray( this.#list ) && this.#list.length === 1 ) return this.#list[ 0 ]
		else return this.#list

	}

	set list( checklist ) {
		//this.log.debug({checklist})
		let orig = checklist
		//noop
		if ( checklist === null || checklist === undefined ) return
		//if not string or array, die
		if ( ! ( Array.isArray( checklist ) || isString( checklist ) ) ) throw new Error( Object.getPrototypeOf( this ).constructor.name + ' checklist :: not string nor array ' + typeof checklist )
		if ( checklist === '*' ) {

			this.inList = ( memberOf ) => {

				//if not string or array, die, even we allow all
				if ( ! ( Array.isArray( memberOf ) || isString( memberOf ) ) ) throw new Error( Object.getPrototypeOf( this ).constructor.name + ' inList :: not string nor array ' + typeof memberOf )
				return true

			}
			this.#list = '*'//'kõik on lubatud!'
			return

		}
		if ( isString( checklist ) ) checklist = [ checklist ]
		if ( Array.isArray( checklist ) ) checklist = trimArrayOfStrings( checklist )
		else throw new Error( Object.getPrototypeOf( this ).constructor.name + ' :: checklist not array' + typeof checklist )
		if ( Array.isArray( checklist ) && checklist.length > 0 ) {

			this.#list = checklist
			this.inList = ( memberOf ) => {

				//if not string or array, die
				if ( ! ( Array.isArray( memberOf ) || isString( memberOf ) ) ) throw new Error( Object.getPrototypeOf( this ).constructor.name + ' :: not string nor array ' + typeof memberOf )
				if ( ! Array.isArray( memberOf ) ) memberOf = [ memberOf ]
				return this.#list.some( ( v ) => {

					return memberOf.indexOf( v ) >= 0

				} )

			}

		} else {

			this.#list = undefined
			this.inList = () => {

				return false

			}
			this.log.alert( { 'empty checklist, deny all': orig } )

		}

	}

}
