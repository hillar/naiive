import { createServer as createHttpServer, IncomingMessage } from 'http'
import { Base } from '../../classes/base/base.mjs'
import { is, isFn, isAsyncFunction, isObject } from '../../utils/var.mjs'
import { ip } from '../../utils/req.mjs'
import { ROUTEMETHODS as METHODS } from '../../constants/routemethods.mjs'
import { LOGMETHODS } from '../../constants/logmethods.mjs'
import { Command } from '../../../../packages/vendor/commander/index.mjs'

const PORT = 9876
const IP = '0.0.0.0'



export class simpleHTTPServer extends Base {
  #about
	#hostname = 'localhost'
	#port
	#server
	#methods = {}
	#configFile

	constructor(logger, ip = IP, port, about='dummy',opts = [], methods = [] ) {
		if (!methods) throw new Error('no methods')
		super( logger, ...opts )

    for (const method of methods) {
      const name = Object.keys(method)[0].toLowerCase()
      if (name) {
        this.setMethod(name,method[name])
      } else throw new Error('not a method')
    }
		this.port = port
		this.#about = about
		this.hostname = ip
		//this.log.debug('constructor methods',this.methods)
  }

	async about () {
		if (isFn(this.#about)) {
			if (isAsyncFunction(this.#about)){
				try {
						const r = await this.#about()
						return r
				} catch (error) {
					this.log.emerg('about afn',error)
					if (error instanceof ReferenceError || error instanceof SyntaxError ) {
	          console.error(Object.getPrototypeOf( error ).constructor.name, this.prototypeof)
	          console.error(error)
	          process.exit(-1)
	        }
				}
			} else {
					try {
						return this.#about()
					} catch (error) {
						this.log.emerg('about fn',error)
						if (error instanceof ReferenceError || error instanceof SyntaxError ) {
		          console.error(Object.getPrototypeOf( error ).constructor.name, this.prototypeof)
		          console.error(error)
		          process.exit(-1)
		        }
					}
			}
		} else return this.#about
	}
	/*
	set about (m) {
		if (isObject(m)|| isFn(m)) this.#about = m
		else { this.#about = {about:m} }
	}
  */
	get hostname () {return this.#hostname}
	set hostname (n) {
		if (!n || !n.trim().length) throw new Error(this.typeof + ' no hostname')
		else this.#hostname = n
	}

	get port () { return this.#port}
	set port (p) {
		if (!p) throw new Error(this.typeof + ' no port')
		p = parseInt(p)
		if (!Number.isInteger(p)) throw new Error(this.typeof + ' port not a number')
		if (p > 65534) throw new Error(this.typeof + ' port must be less than 65535')
		this.#port = p
	}

	get methods () {
		return Object.keys(this.#methods)
	}

  setMethod(name,fn) {
		if (!METHODS.includes(name)) throw new Error('method name not allowed: ' + name)
		if (!fn) throw new Error('no function for' + name)
		let _fn
		let _ping
		if (fn) {
			if (fn.fn) _fn = fn.fn
			else _fn = fn
			if (fn.ping) _ping = fn.ping
		}
		if (!isFn(_fn)) throw new Error('not a function for ' + name)
		this.#methods[name] = {}
		if (isAsyncFunction(_fn)) {
			this.#methods[name].fn = async (req, res, logger) => {
				try {
					await _fn(req, res, logger)
				} catch (error) {
					this.log.emerg({method:name, error})
					res.writeHead('500')
					if ((error instanceof ReferenceError)) process.exit(-1)
				}
			}
		} else {
			this.#methods[name].fn = (req, res, logger) => {
	      return new Promise(async (resolve) => {
					try {
						_fn(req, res, logger)
					} catch (error) {
						this.log.emerg({method:name, error})
						res.writeHead('500')
						if ((error instanceof ReferenceError)) process.exit(-1)
					}
					resolve()
				})
			}
		}
		if (_ping) {
			if (isAsyncFunction(_fn)) {
				this.#methods[name].ping = async () => {
						try {
							await _ping(this.log)
						} catch (error) {
							this.log.emerg({method:name, error})
							if ((error instanceof ReferenceError)) process.exit(-1)
						}
				}
			} else {
			this.#methods[name].ping = () => {
				return new Promise(async (resolve) => {
					try {
						_ping(this.log)
					} catch (error) {
						this.log.emerg({method:name, error})
						if ((error instanceof ReferenceError)) process.exit(-1)
					}
					resolve()
				})
			}
			}
		}
		this.log.debug('method',name)
  }

  async listen (onListeningFunction) {
    if (onListeningFunction && isFn(onListeningFunction)) this.onListening = onListeningFunction
		const server = createHttpServer( async (req, res) => {
			const method = req.method.toLowerCase()
			if (method==='get' && req.url === '/') {
				//this.log.debug('about',this.about)
				//res.write(JSON.stringify(this.about))
				const a = await this.about()
				this.log.debug(a)
				if (a && Object.keys(a).length>0) res.write(JSON.stringify(a))
				res.end()
			} else {
				const logger = {}
				for (const logmethod of LOGMETHODS){
					logger[logmethod] = (...messages) => {
						let msg = []
						let ctx = {method,ip:ip(req)}
						for (const m of messages) {
							if (m instanceof Object) {
								ctx = Object.assign(ctx,m)
							} else msg.push(m)
						}
						if (msg.length > 0 ) ctx.messages = msg
						this.log[logmethod](ctx)
					}
				}
				if (this.methods.includes(method)){
					this.log.info({url:req.url,headers:req.headers})
          try {
					    await this.#methods[method].fn(req,res,logger)
          } catch (error) {
            error.who = simplehttpserver
            console.error(error)
          }
					//res.end()
				} else {
					// not implemented
					this.log.notice({httperror:501})
					res.writeHead(501)
					//res.end()
				}
			}
			//if (!res.finished) res.end()
		})
		server.on('error', (err) => {
			this.log.err(err)
		})

		server.on('clientError', (err, socket) => {
			socket.end('HTTP/1.1 400 Bad Request\r\n\r\n')
			this.log.err('clientError',err)
		})

		server.on('close', async () => {
			this.log.info(server.address(),'closing ...')
		})
		server.on('listening', () => {
			this.log.info({LISTENING:server.address()})
			this.listenig = server.address()
      if (this.onListening) this.onListening()
		})


		const cliParams = new Command()
    cliParams
      .version('0.0.1')
      .usage('[options]')
      .option('-D, --docker','gen Dockerfile ARG and ENV skeleton')
      .option('-T, --dump-config','dump configuration')
      .option('-U, --dump-config-undefined','dump configuration with undefined')
      .option('-c, --config [file]', 'set configuration file','./config.js')
			.option('--ping', 'ping all internal methods',)
      for (const param of this.settings) {
        cliParams.option('--'+param+ ' ['+typeof this[param]+']','server '+param+ ' (default: '+this[param]+')')
      }
		cliParams.parse(process.argv)
		let configFile = cliParams.config || './config.js'
    this.#configFile = configFile
    try {
      configFile = realpathSync(configFile)
      this.#configFile = configFile
    } catch (e) {
      this.#configFile = undefined
      if (!(configFile === './config.js')) this.log.info({Config:'no file ' + configFile})
    }
    let conf = {}
    // load config file
    try {
      if (this.#configFile) {
        conf = JSON.parse(readFileSync(this.#configFile,"utf8"))
        this.log.debug('conf',conf,CONFIG)
      }
    } catch (e) {
      this.log.info({Config:configFile,error:e.message})
      this.#configFile = undefined
    }
		// patch conf with command line params
		for (const param of this.settings) {
			if (!!cliParams[param] && cliParams[param] != conf[param]) conf[param] = cliParams[param]
		}
		// conf ready, apply now
		this.readConfig(conf)
		// just dump conf
		if (cliParams.dumpConfig) {
			process.stdout.write(['/* config dump  */\nmodule.exports = ',JSON.stringify(this.config,null,'\t'),'\n'].join('\n'))
			process.exit(0)
		}
		// dump conf with undefined as nulls
		if (cliParams.dumpConfigUndefined) {
			process.stdout.write(['/* config dump  with undefined as nulls */\nmodule.exports = ',JSON.stringify(this.config,function(k, v) { if (v === undefined) { return null; } return v; },'\t'),'\n'].join('\n'))
			process.exit(0)
		}
		// generate Dockerfile skeleton
		if (cliParams.docker) {
	     let str = '\n'
			//str += "FROM node:13-alpine AS base\n"
			//str += "MAINTAINER 'no maintenance intended'\n"
			str += "\n"
			str += "# Declare args\n"
			for (const a of this.settings){
				str += `ARG ${a.toUpperCase()}\n` // =${flat[a]}
			}
			str += "\n"
			str += "# Declare envs vars for each arg\n"
			for (const a of this.settings) {
				str += `ENV ${a.toUpperCase()} $${a.toUpperCase()}\n`
			}
			str += "\n"

			process.stdout.write(str)
			process.exit(0)
		}
		// ping methods
		if (cliParams.ping) {
			for (const method of this.methods){
				this.log.info({ping:method})
				const req = new IncomingMessage()
				const res = {writeHead:()=>{},write:()=>{},end:()=>{}} //new http.ServerResponse()
				if (this.#methods[method].ping) await this.#methods[method].ping(req,res,this.log)
				else await this.#methods[method].fn(req,res,this.log)
			}
			process.exit(0)
		}
		// go over all just to die early
		await this.about()
    /*
		for (const method of this.methods){
			const req = new IncomingMessage()
			const res = {writeHead:()=>{},write:()=>{},end:()=>{}} //new http.ServerResponse()
			await this.#methods[method].fn(req,res,this.log)
		}
    */
		this.log.debug('start listen',this.hostname,this.port)
    try {
		server.listen(this.port,this.hostname)
    } catch (error) {
      this.log.emerg({listen:error})
    }
  }
}

export function createSimpleHTTPServer (options) {
  if (!options) throw new Error('no options')
	if (!options.methods) throw new Error('no methods')
	let methods = []
	if (Array.isArray(options.methods)) methods = options.methods
  else {
		for (const method of Object.keys(options.methods)){
			//if (isFn(options.methods[method])) {
				const tmp = {}
				tmp[method] = options.methods[method]
				methods.push(tmp)
			//} else throw new Error('no function for '+ method)
		}
	}
	let opts = []
	if (Array.isArray(options.opts)) opts = options.opts
  else {
		for (const opt of Object.keys(options.opts)){
			if (isFn(options.opts[opt])) {
				const tmp = {}
				tmp[opt] = options.opts[opt]
				opts.push(tmp)
			} else throw new Error('no function for '+ opts)
		}
	}
  return new simpleHTTPServer(options.logger,options.ip,options.port,options.about,opts, methods)
}
