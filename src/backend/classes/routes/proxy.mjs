/*

sample Proxy

*/

import { parse, join } from 'path'
import { request } from 'http'
import { Route } from '../base/route.mjs'

export class ProxyRoute extends Route {

  constructor (logger, roles, groups, host, port, methodnames) {

    if (!methodnames || !Array.isArray(methodnames)) throw new Error('no methods to proxy')
    const methods = []
    let log
    for (const method of methodnames) {
      const tmp = {}

      tmp[method.toLowerCase()] = async (req, res, user) => {
          const result = await new Promise( (resolve) => {

            req.pause()
            const url = req.url.replace('/'+this.route,'/').replace('//','/') //join(pe.dir, pe.base)
            this.logger.notice({proxy:{method:req.method,host:this.host, port:this.port, url}})

            const headers = req.headers
            const ignore = ['accept','expect','authorization']
            for (const k of Object.keys(headers)) {
              if (ignore.includes(k))
              delete headers[k]
            }
            headers['x-proxy-for'] = req.socket.remoteAddress
            headers['x-proxy-user'] = user.uid
            const options = {
              hostname: this.host,
              port: this.port,
              path: url,
              method: req.method,
              headers: headers
            }
            const proxy = request(options, function (r) {
              res.writeHead(r.statusCode,r.headers)
              r.on('error', (e) => {
                log.info({r:e})
                res.writeHead(500)
                res.end()
                resolve(true)
              })
              r.on('abort', (e) => {
                log.info({abort:e})
                res.writeHead(500)
                res.end()
                resolve(true)
              })
              r.on('end', () => {
                resolve(true)
              })
              r.on('close', () => {
                log.info({url,status:res.statusCode})
                resolve(true)
              })
              r.pipe(res)
            })

            // handle errors
            proxy.on('error', (e) => {
              this.logger.info({proxy:e})
              res.writeHead(502)
              res.end()
              resolve(true)
            })
            res.on('error', (e) => {
              this.logger.info({res:e})
              res.writeHead(500)
              res.end()
              resolve(true)
            })
            req.on('error', (e) => {
              this.logger.info({req:e})
              res.writeHead(500)
              res.end()
              resolve(true)
            })

            req.pipe(proxy)

            req.resume()

          })
          return result
        }

      methods.push(tmp)
    }

    super(logger, roles, groups, ...methods)
    log = this.logger
    this.host = host
    this.port = port

  }

  get host () { return this._host }
  set host (host) { this._host = host}
  get port () { return this._port }
  set port (port) { this._port = port}

  async ping () {
    const log = this.log
    let finalresult = true
    for (const method of this.methods) {
        this.logger.info(method,this.host,this.port,this.path)
        const result = await new Promise((resolve)=>{
        const options = {
          hostname: this.host,
          port: this.port,
          path: this.path,
          method: method //'GET'
        }

        const proxy = request(options, (r) => {
          r.on('error', (e) => {
            this.logger.info({r:e})
            resolve(false)
          })
        })
        proxy.on('error', (e) => {
          this.logger.warning({proxy:{error:e, options}})
          resolve(false)
        })
        proxy.on('finish', (e) => {
          this.logger.info({proxy:options})

          resolve(true)
        })
        proxy.end()
      })
      if (!result) finalresult = false
    }
    return finalresult
  }

}

export function createProxyRoute(options) {
  if (!options) throw new Error('no options')
  return new ProxyRoute(options.logger, options.roles, options.groups, options.host, options.port,options.methods)
}
