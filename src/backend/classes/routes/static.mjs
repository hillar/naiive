/*

sample extended Route

*/

import { readFile, realpathSync, access, constants } from 'fs'
import { parse, join, resolve as resolvename } from 'path'
import { Route } from '../base/route.mjs'


const STATICROOT = './'
const DEFAULTFILE = 'index.html'

export class StaticRoute extends Route {

  #root
  #default

  constructor (logger, roles, groups, root = STATICROOT, defaultfile = DEFAULTFILE) {

    super(logger, roles, groups)
    this.root = root
    this.default = defaultfile

    this.get = async (req, res, user, log) => {
      //const result = await new Promise( (resolve) => {
      return new Promise( (resolve) => {
        let pe = parse(decodeURIComponent(req.url))
        // if dir is / then use default
        if (pe.dir === '/' && this.default) pe.base = this.default
        // chop route from req path
        pe.dir = pe.dir.replace('/'+this.route,'')
        let filename
        try {
           filename = realpathSync(join(this.path, pe.dir, pe.base.split('?').shift()))
        } catch (error) {
          this.log.warning({error,filename})
          res.writeHead(404)
          res.end()
          resolve()
          return
        }
        // M$ SEC7113 "CSS was ignored due to mime type mismatch"
        if (filename.endsWith('.css')) res.setHeader("Content-Type", "text/css")

        readFile(filename, (err,content) => {
          if (err) {
            this.logger.warning({notexists:filename})
            res.writeHead(404)
            res.end()
            resolve(false)
          } else {
            this.logger.notice({access:filename})
            res.write(content)
            res.end()
            resolve(true)
          }
        })
      })
      //return result
    }

  }

  get default () { return this.#default }
  set default (filename) { this.#default = filename}

  get root () { return this.#root }
  get path () {
    if (this.root && this.route) {
      return join(this.root)
    } else {
      if (!this.root) throw new Error('no root directory')
      if (!this.route) throw new Error('no route')
    }
  }

  set root (root) {
    if (!root) root = STATICROOT
    if (!(Object.prototype.toString.call(root) === '[object String]')) throw new Error('root not a string ' + typeof root)
    root = root.trim()
    if (root === '/') throw new Error('can not serve /')
    if (!root) throw new Error('can not serve empty')
    try {
      this.#root = realpathSync(root)
      if (this.#root === '/') throw new Error('can not serve /')
    } catch (err) {
      this.#root = root
      const fullname = resolvename(root)
      this.logger.warning({notexists:fullname})
    }
  }

  async ping () {
    const result = await new Promise((resolve)=>{
      const fullname = resolvename(this.path)
      access(fullname, constants.R_OK, (err) => {
        if (err) {
          this.logger.err({ping:'failed',notexist:fullname})
          resolve(false)
        } else {
          this.logger.info({ping:'ok',readable:fullname})
          resolve(true)
        }
      })
    })
    return result
  }

}

export function createStaticRoute(options) {
  if (!options) throw new Error('no options')
  return new StaticRoute(options.logger, options.roles, options.groups, options.root, options.defaultfile)
}
