/*

caches vendor files to local dir

*/

import { readFile, realpathSync, access, accessSync, constants, createWriteStream, mkdirSync, unlinkSync } from 'fs'
import { get } from 'http'
import { parse, join, resolve as resolvename, dirname } from 'path'
import { Route } from '../base/route.mjs'


function exists(filename) {
  try {
    accessSync(filename, constants.F_OK);
    return true;
  } catch (e) {
    return false;
  }
}


const STATICROOT = './'
const DEFAULTFILE = 'index.html'

export class CacheRoute extends Route {

  #root
  #default

  constructor (logger, roles, groups, root = STATICROOT, defaultfile = DEFAULTFILE) {

    super(logger, roles, groups)
    this.root = root
    this.default = defaultfile

    this.get = async (req, res, user, log) => {
      //const result = await new Promise( (resolve) => {
      return new Promise( async (resolve) => {

        let pe = parse(decodeURIComponent(req.url))
        // if dir is / then use default
        if (pe.dir === '/' && this.default) pe.base = this.default
        // chop route from req path
        pe.dir = pe.dir.replace('/'+this.route,'')

        const filename = join(this.path, pe.dir, pe.base.split('?').shift())
        // M$ SEC7113 "CSS was ignored due to mime type mismatch"
        if (filename.endsWith('.css')) res.setHeader("Content-Type", "text/css")

        if (!exists(filename)) {
          const dname = dirname(filename)
          if (!exists(dname)) {
            try {
              mkdirSync(dname,{recursive:true})
            } catch (error) {
              this.logger.err({error,dname})
              res.writeHead(500)
              res.end()
              resolve(false)
            }
            this.log.notice({downloading:req.url.replace(this.route,'')})
            const file = createWriteStream(filename)
            await new Promise( resolve => {
              const request = get('http://'+req.url.replace(this.route,''), function(response) {
                response.pipe(file);
                response.on('end', () => resolve())
                response.on('error', (error) => {
                  this.logger.warning({error})
                  res.writeHead(503)
                  res.end()
                  try {
                      unlinkSync(filename)
                  } catch (e) {
                    console.error(e)
                  }
                  return
                })
              })
              request.on('error', (error) => {
                this.logger.emerg({error})
                res.writeHead(500)
                res.end()
                try {
                    unlinkSync(filename)
                } catch (e) {
                  console.error(e)
                }
                return
              })
            })
          }
        }
        if (exists(filename)) {
          readFile(filename, (error,content) => {
            if (error) {
              this.logger.err({error,filename})
              res.writeHead(500)
              res.end()
              resolve(false)
            } else {
              this.logger.info({filename})
              res.write(content)
              res.end()
              resolve(true)
            }
          })
        } else {
          res.writeHead(500)
          res.end()
          resolve(false)
        }
      })

      //return result
    }

  }

  get default () { return this.#default }
  set default (filename) { this.#default = filename}

  get root () { return this.#root }
  get path () {
    if (this.root && this.route) {
      return join(this.root)
    } else {
      if (!this.root) throw new Error('no root directory')
      if (!this.route) throw new Error('no route')
    }
  }

  set root (root) {
    if (!root) root = STATICROOT
    if (!(Object.prototype.toString.call(root) === '[object String]')) throw new Error('root not a string ' + typeof root)
    root = root.trim()
    if (root === '/') throw new Error('can not serve /')
    if (!root) throw new Error('can not serve empty')
    try {
      this.#root = realpathSync(root)
      if (this.#root === '/') throw new Error('can not serve /')
    } catch (err) {
      this.#root = root
      const fullname = resolvename(root)
      this.log.warning({notexists:fullname})
    }
  }

  async ping () {
    const result = await new Promise((resolve)=>{
      const fullpath = resolvename(this.path)
      access(fullpath, constants.R_OK, (err) => {
        if (err) {
          this.log.err({ping:'failed',notexist:fullpath})
          resolve(false)
        } else {
          this.log.info({ping:'ok',readable:fullpath})
          resolve(true)
        }
      })
    })
    return result
  }

}

export function createCacheRoute(options) {
  if (!options) throw new Error('no options')
  return new CacheRoute(options.logger, options.roles, options.groups, options.root, options.defaultfile)
}
