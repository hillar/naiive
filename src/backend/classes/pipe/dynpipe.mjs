/*

If some stage takes (or may take) much longer than the others, and cannot be
sped up, the designer can provide two or more processing elements to carry out
that task in parallel, with a single input buffer and a single output buffer.
As each element finishes processing its current data item, it delivers it to
the common output buffer, and takes the next data item from the common input
 buffer. This concept of "non-linear" or "dynamic" pipeline is exemplified by
 shops or banks that have two or more cashiers serving clients from a single
 waiting queue.

*/
import { Event } from '../base/event-levels.mjs'
import { Transform, Readable } from "stream";
import { inspect } from "util";

import { createHash } from 'crypto'

const DEFAULTALGORITHM = 'md5'
function digestString(str, algorithm = DEFAULTALGORITHM, encoding = 'hex') {
  return createHash(algorithm).update(str).digest(encoding)
}

export function wait(ms) {
  if (!ms) ms = 10
  //console.dir({wait:ms})
  //ms = Math.round(ms);
  return new Promise(resolve => {
    setTimeout(() => {
      resolve();
    }, ms);
  });
}

export function printProgress(...progress) {
  if (process.stdout.isTTY) {
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write((new Date()).toLocaleTimeString('it-IT')+' '+progress.join(" "));
  } else console.debug(progress.join(" "));
}

export class StringStream extends Readable {
  constructor (str, encoding) {
    super()
    this._str = str
    this._encoding = encoding || 'utf8'
  }

  _read () {
    if (!this.ended) {
      process.nextTick(() => {
        this.push(Buffer.from(this._str, this._encoding))
        this.push(null)
      })
      this.ended = true
    }
  }
}

// pool: array of strings of hostname:port pairs
// tti : amount of time in which it takes for the server to become functional
// tries: how many times to make connection attempts to unresponsive server
// fails if all servers in pool are not 'connectable' in (tti * tries) OR  if tries 0 then try forever

export class Ask extends Transform {
  /*
  name = "";
  pool = [];
  tti = 1000 * 3;
  tries = 3;
  counter = 0;
  tooked = 0;
  average = undefined;
  _askState = {
    running: [],
    down: [],
    flushing: false
  };
  */
  constructor(options) {

    if (!options || !options.pool || !Array.isArray(options.pool) || !options.pool.length) throw new Error("No pool. Required array of hostname:port pairs");
    if (!options.idField || !options.idField.length) throw new Error('No id fieldname')
    options.objectMode = true;
    options.highWaterMark = 1;
    super(options);
    this.name = "";
    this.pool = [];
    this.inpool = []
    this.tti = 1000 * 3;
    this.tries = 3;
    this.counter = 0;
    this.tooked = 0;
    this.average = undefined;
    this.killers = {}
    this._askState = {
      running: [],
      down: [],
      flushing: false
    };
    this._metrix = {}
    for (const server of options.pool) {
      // TODO make it prettier
      let hostname, port
      if (server.hostname && server.port) {hostname = server.hostname; port = server.port}
      else { let bittes = server.split(":"); hostname = bittes[0]; if (bittes[1]) port = bittes[1] }
      if (!port) port = options.defaultport;
      if (hostname.trim().length && port) {
        this.pool.push({ hostname, port });
        this._metrix[hostname+':'+port] = {worked:0}
      } else throw new Error("Missing hostname:port pair");
    }
    if (typeof options.ask === "function") this._ask = options.ask;
    if (typeof options.ping === "function") this._ping = options.ping
    if (typeof options.sendmetrix === 'function') this.sendmetrix = options.sendmetrix
    if (options.tti) this.tti = options.tti*1000;
    if (options.tries !== undefined) this.tries = options.tries;
    if (options.name) this.name = options.name;
    else this.name = this.constructor.name;
    this.idField = options.idField
  }

  push(something) {
    if (!this.destroyed && !this.ended && !this.errored) {
      try {
        super.push(something)
      } catch (error) {
        console.dir('--- push error --')
        console.dir(this)
        console.error(error)

        this.destroy(error)
      }
    } else console.error(this.name +' destroyed, no more push ;(')
  }

  _ask(hostname, port, data, abort) {
    console.error(this.name + ": _ask not implemented");
    process.exit(-1);
  }

  get waittime() {
    return this.average ? Math.round(this.average / this.pool.length) : Math.round(this.tti / this.pool.length);
  }

  get alldown() {
    if (this.tries === 0 && this._askState.down.length === this.pool.length && this._askState.down.reduce( (a,b) => a+b,0) > this.pool.length * 3) {
      this.push({__critical:{service:this.name,alldown:this.pool}})
    }
    if (this.tries) return this._askState.down.filter(v => v > this.tries).length === this.pool.length;
    else return false // if tries === 0 try forever
  }

  get somerunning() {
    for (const i in this._askState.running) {
        if (this._askState.running[i]) {
          const state = inspect(this._askState.running[i]);
          if (state === "Promise { <pending> }") return true;
        }
    }
    return false;
  }

  sendmetrix(m) {
    //console.dir(m);
  }

  async _transform(current, encoding, next) {
    if (this.counter === 0 ) {this.counter++ ;await this.pingsome()}
    if (current instanceof Error || current instanceof Event) {
      this.push(current) // just pass trough errors
    } else {
      if (Object.keys(current).filter(k=>!k.startsWith('__')).length){
        await this.waitforslot(current);
        this.counter++;
        printProgress('transform',this.name,this.counter,current[this.idField])
      } else this.push(current) // just pass trough if only '__foo*'
    }
    next();
  }

  async _flush(last) {
    // clean up pings
    for (const i in this._askState.down) {
      delete this._askState.running[i];
    }
    // wait for slots do finish
    const start = process.hrtime.bigint();
    while (this.somerunning && !this.destroyed) {
      const waited = Number(process.hrtime.bigint() - start) / 1e6;
      if (waited > (this.average * this.pool.length * this.tries + 120 * 1000)) {
        //this.destroy(new Error(this.name + " flush timeout "));
        this.destroyed = true
        console.error(new Error(this.name + " flush timeout "))
        break
      }
      printProgress('flushing', this.name, Math.round(waited/1000),this._askState.running.length)
      await wait(this.waittime);
    }
    this.push({__done__:{service:this.name,count:this.counter,average:this.average,servers:this._metrix}})
    return void last();
  }

  async waitforslot(data) {
    let waited = 0;
    while (!this.destroyed) {  // <-----------
      for (const i in this.pool) {
        if (this._askState.running[i]) continue;
        this._askState.running[i] = this.ask(data, i, waited);
        this.inpool[i] = data[this.idField]
        return;
      }
      const waittime = this.waittime;
      await wait(waittime);
      waited += waittime;
      printProgress('waitforslot', this.name, Math.round(waited/1000), this.inpool)
    } // <---------------------------------------
  }

  async ask(data, slot, waited) {
    const start = process.hrtime.bigint();
    const { hostname, port } = this.pool[slot];
    const server = hostname + ":" + port;
    let answer;
    try {
      answer = await this._ask(hostname, port, data, this);
      this._askState.running[slot] = undefined;
      const took = Number(process.hrtime.bigint() - start) / 1e6;
      if (Object.keys(answer).length && !this.destroyed) this.push(answer);
      this.tooked += took;
      this.average = this.tooked / this.counter;
      this._metrix[hostname+':'+port].worked += took
      this.sendmetrix({ waited, took, server, service: this.name, i: data._id });
      printProgress('ok',this.name,data[this.idField])
    } catch (error) {
      // die on syntax etc.. errors
      if (error instanceof SyntaxError || error instanceof ReferenceError || error instanceof RangeError) {
        console.error(error)
        console.error('pekkis')
        process.exit(-1)
      // TODO decrease servers pool
      // } else if (ErrorEvent){
      // try again
      } else {

        //this.push({__failed__:{error, timestamp:new Date(), server, service:this.name}})
        this.inpool[slot] = 'ping'
        this._askState.running[slot] = this.ping(slot, hostname, port);
        if (!this._metrix[hostname+':'+port].failures) this._metrix[hostname+':'+port].failures = []
        this._metrix[hostname+':'+port].failures.push({error,timestamp:new Date()})
        // TODO check is it really  killing or was it 'dead' before
        let hash
        try {
          hash = digestString(JSON.stringify(data[this.idField]))
        } catch (error) {
          console.error(error)
          console.dir(data)
          console.dir(this)
          process.exit(-1)
        }
        error.id = data[this.idField]
        if (!this.killers[hash]) this.killers[hash] = []
        this.killers[hash].push(error)
        if (this.tries && this.killers[hash].length > this.tries) {
          this.push({__killer:{o:Object.keys(data),pathname:data.pathname,basename:data.basename,kills:this.name,server,errors:this.killers[hash]}})
        } else await this.waitforslot(data);
      }
    }
  }

  async ping(slot, hostname, port) {
    let answer;
    const start = process.hrtime.bigint();
    const server = hostname+':'+port
    if (!this._ping) this._ping = this._ask
    while (!answer && !this.destroyed) {
      try {
        answer = await this._ping(hostname, port, {}, this);
        this._askState.running[slot] = undefined;
        this._askState.down[slot] = undefined;
        const downtime = Number(process.hrtime.bigint() - start) / 1e6;
        //this.push({__up__:{downtime,timestamp:new Date(),server, service:this.name,w:answer._warning}})
        this.sendmetrix({ downtime, server, service: this.name });
        this.inpool[slot] = 'up'
      } catch (error) {
        if (error instanceof ReferenceError || error instanceof SyntaxError){
          console.error(error)
          process.exit(-1)
        }
        if (!this._askState.down[slot]) this._askState.down[slot] = 0;
        this._askState.down[slot]++;
        if (this.alldown) this.destroy(new Error(this.name + " ALL DOWN"+JSON.stringify(this.pool)+"\n"+error+"\n"+JSON.stringify(this._metrix)))
        else await wait(this.tti);
      }
    }
    this._askState.running[slot] = undefined;
    this._askState.down[slot] = undefined;
  }

  async pingsome(all=false){
    let pingok = false
    const updown = {up:[],down:[]}
    for (const {hostname, port} of this.pool){
      if (all) printProgress(this.name,'pinging',hostname,port)
      try {
        const a = await this._ping(hostname,port)
        pingok = true
        updown.up.push({hostname, port})
      } catch (error) {
        updown.down.push({hostname, port})
        //noop
      }
      if (pingok && !all) break
    }
    if (!pingok && this.tries) {
      this.destroy(new Error(this.name+' sorry, ALL DOWN '+ JSON.stringify(this.pool)+"\n"+JSON.stringify(this._metrix)))
    } else {
      if (all) this.push({__pinged:{name:this.name,all, up:updown.up,down:updown.down, timestamp:new Date()}})
    }
  }

}
