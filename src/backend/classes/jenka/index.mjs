import { Polka } from './modules/polka/index.mjs'

export class Jenka extends Polka {
  constructor(opts = {port:3000}) {
    super(opts)
    /*
    console.dir(this.prototypeof)
    console.dir(this.port)
    console.dir(this.defaults)
    console.dir(this.settings)
    */
  }
  listen(options,callback) {


    super.listen(this.port,callback)
  }
}

export const polka = (opts) => new Jenka(opts)
