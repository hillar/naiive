export const every = (arr, cb) => {
  let i = 0
  const len = arr.length

  for (; i < len; i++) {
    if (!cb(arr[i], i, arr)) {
      return false
    }
  }

  return true
}
