import { request } from 'http'

const ELASTICSERVER = 'localhost'
const ELASTICPORT = 9200
const ELASTICSERVERS = [{hostname:ELASTICSERVER,port:ELASTICPORT}]

function getRandomI(max) {
    return Math.floor(Math.random() * (max + 1))
}



// https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-exists.html
export function indexExists(index,servers=ELASTICSERVERS,log) {
  return new Promise ( resolve => {
    // TODO roundrobin and query another if fails
    const hostname = servers[0].hostname
    const port = servers[0].port
    const options = {
      hostname,
      port,
      path: index,
      method: 'HEAD'
    }
    try {
      const reqHead = request(options, r => {
        resolve(r.statusCode === 200)
      })
      reqHead.on('error', (error) => {
        log.warning({indexExists:{hostname,port,error}})
        resolve(null)
      })
      reqHead.end()
    } catch (error) {
      log.emerg({indexExists:{hostname,port,options,error}})
      //console.error(error)
      resolve(null)
    }

  })
}


export function put2Elastic(doc,path,servers=ELASTICSERVERS,log){
  return new Promise ( resolve => {
    // TODO roundrobin and query another if fails
    const hostname = servers[0].hostname
    const port = servers[0].port

    const options = {
      hostname: hostname,
      port: port,
      path: `${path}`,
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
    }
    try {
    const req = request(options, (res) => {
      let data = []
      res.on('data', (chunk) => {
        try {
          data.push(chunk)
        } catch (e) {
          console.error(id)
          console.error(e)
          resolve()
        }
      })
      res.on('end',() => {
        let r
        try {
          r = JSON.parse(Buffer.concat(data).toString())
          if (res.statusCode > 299) {
            console.error(path,hostname,port)
            console.error(r)
            console.dir(doc)
          }
          //console.dir(r)
          resolve(r)
        } catch (e) {
          resolve()
        }
      })
    })
    req.on('error', (error) => {
      resolve()
    })
    req.write(doc,'utf-8')
    req.end()
  } catch (error) {
    log.emerg({put2Elastic:{hostname,port,options,error}})
    //console.error(error)
    resolve(null)
  }
  })
}


/*

POST twitter/_delete_by_query
{
  "query": {
    "match": {
      "message": "some message"
    }
  }
}

*/

/*

curl -XPUT "localhost:9200/your_example_index/_settings" -H 'Content-Type: application/json' -d'
{
    "index" : {
        "index.highlight.max_analyzed_offset" : 100000
    }
}

*/

export function putDoc2Elastic(doc,id,index,servers=ELASTICSERVERS,log){
  //return new Promise( (resolve) => {
    log.info({'put':{id,index}})
    const path = `/${index}/_doc/${id}`
    return put2Elastic(doc,path,servers,log)
    /*
    const options = {
      hostname: server,
      port: port,
      path: path,
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
    }
    const req = request(options, (res) => {
      debug(id,index,`PUT statusCode: ${res.statusCode}`)
      let data = []
      res.on('data', (chunk) => {
        try {
          data.push(chunk)
        } catch (e) {
          console.error(id)
          console.error(e)
          resolve()
        }
      })
      res.on('end',() => {
        let r
        try {
          r = JSON.parse(Buffer.concat(data).toString())
          if (res.statusCode > 299) {
            console.error(id)
            console.error(r)
            console.dir(doc)
          }
          debug(id,r)
          resolve(r)

        } catch (e) {
          debug(id,e)
          resolve()
        }
      })
    })
    req.on('error', (error) => {
      debug(error)
      resolve()
    })
    req.write(doc,'utf-8')
    req.end()
    */

  //})
}

export function updateDoc2Elastic(doc,id,index,servers=ELASTICSERVERS,log){
  //return new Promise( (resolve) => {
    log.info({'update':{id,index}})
    const path = `/${index}/_update/${id}`
    return update2Elastic(doc,path,servers,log)
}

// update2Elastic
function update2Elastic(doc,path,servers=ELASTICSERVERS,log){
  return new Promise ( resolve => {
    // TODO roundrobin and query another if fails
    const hostname = servers[0].hostname
    const port = servers[0].port

    const options = {
      hostname: hostname,
      port: port,
      path: `${path}`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
    }
    try {
    const req = request(options, (res) => {
      let data = []
      res.on('data', (chunk) => {
        try {
          data.push(chunk)
        } catch (e) {
          console.error(id)
          console.error(e)
          resolve()
        }
      })
      res.on('end',() => {
        let r
        try {
          r = JSON.parse(Buffer.concat(data).toString())
          if (res.statusCode > 299) {
            console.error(path,hostname,port)
            console.error(r)
            console.dir(doc)
          }
          //console.dir(r)
          resolve(r)
        } catch (e) {
          resolve()
        }
      })
    })
    req.on('error', (error) => {
      resolve()
    })
    req.write(JSON.stringify({doc}),'utf-8')
    req.end()
  } catch (error) {
    log.emerg({put2Elastic:{hostname,port,options,error}})
    //console.error(error)
    resolve(null)
  }
  })
}

export function docExistInElastic(id,index,servers=ELASTICSERVERS){
  return new Promise( (resolve) => {
    // TODO roundrobin and query another if fails
    const hostname = servers[0].hostname
    const port = servers[0].port
    const options = {
      hostname: hostname,
      port: port,
      path: `/${index}/_doc/${id}`,
      method: 'HEAD'
    }
    const req = request(options, (res) => {
      resolve( (res.statusCode === 200) )
    })
    req.on('error', (error) => {
      resolve()
    })
    req.end()
  })
}


function _getDoc(id,fields,index,server,port){
  return new Promise( (resolve) => {
    const options = {
      hostname: server,
      port: port,
      path: `/${index}/_doc/${id}/_source?_source_includes=${fields}`,
      method: 'GET'
    }
    const req = request(options, (res) => {

      let data = []
      res.on('data', (chunk) => {
        try {
          data.push(chunk)
        } catch (e) {
          console.error(e)
          console.error(query)
          resolve({})
        }
      })
      res.on('end',() => {
        let r
        try {
          r = JSON.parse(Buffer.concat(data).toString())
          resolve(r)
        } catch (e) {
          resolve({})
        }
      })

    })
    req.on('error', (error) => {
      resolve()
    })
    req.end()
  })
}

export async function getFieldFromDoc(id,field,index,servers,logger){

  const i = getRandomI(servers.length-1)
  const hostname = servers[i].hostname
  const port = servers[i].port
  const r = await _getDoc(id,field,index,hostname,port)
  if (r === false) {
    if (logger) logger.warning({elastic:{hostname,port,status:'down'}})
    if (servers.length > 1) {
      const tmp = [ ...servers]
      tmp.splice(i,1)
      return getFieldFromDoc(id,field,index,tmp,logger)
    } else {
      if (logger) logger.emerg('no live elasticsearch servers')
      else console.error('no live elasticsearch servers');
      //process.exit(-1)
    }
  }
  return r

 }

 export function getDoc(id,index,servers,logger){

   return getFieldFromDoc(id,'*',index,servers,logger)

 }


/*

Elasticsearch supports the following special characters in query string:
– + : AND operation
– | : OR operation
– - : negates a single token
– " : wraps a number of tokens to signify a phrase for searching (e.g: “java sample approach”)
– * : at the end of a term to signigy a prefix query (e.g: java*)
– ( and ) : signify precedence

*/

export async function elasticSimpleQuery(query,filter,index,from,size,servers,logger){
  const i = getRandomI(servers.length-1)
  const hostname = servers[i].hostname
  const port = servers[i].port
  const r = await _elasticSimpleQuery(query,filter,index,from,size,hostname,port,logger)
  if (r === false) {
    if (logger) logger.warning({elastic:{hostname,port,status:'down'}})
    if (servers.length > 1) {
      const tmp = [ ...servers]
      tmp.splice(i,1)
      return elasticSimpleQuery(query,filter,index,from,size,tmp,logger)
    } else {
      if (logger) logger.emerg('no live elasticsearch servers')
      else console.error('no live elasticsearch servers');
      //process.exit(-1)
    }
  }
  return r //=== false ? {} : r
}

function _elasticSimpleQuery(query,filter,index,from,size,hostname,port,logger){
  //console.log({query,filter,index,from,size,hostname,port})
  return new Promise( (resolve) => {
    const qq = {
          "_source": {
            "includes": [ "*" ],
            "excludes": [ "content" ]
          },
          "query": {
            bool:{must:{"simple_query_string" : {
                "query": query,
                "fields": ["content","fields.tag*","fields.note*","entities.*"],
                "default_operator": "and"
            }}},
          },
          "highlight": {
                  "fields": {
                      "*": {"require_field_match": false}
                  }
          },
          "sort": ["_score"],
          "from": from,
          "size": size

        }
        if (filter.length>0) {
          qq.query.bool.filter = filter
        }

    const options = {
      hostname: hostname,
      port: port,
      path: `/${index}/_search`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
    }
    const req = request(options, (res) => {
      let data = []
      res.on('data', (chunk) => {
        try {
          data.push(chunk)
        } catch (e) {
          console.error(e)
          console.error(query)
          resolve({})
        }
      })
      res.on('end',() => {
        let r
        try {
          r = JSON.parse(Buffer.concat(data).toString())
          if (r.hits.total.value && !r.hits.hits.length) logger.warning({r})
          if (r.hits) resolve(r.hits)
          else {
            console.error(r.error)
            resolve({})
          }
        } catch (e) {
          resolve({})
        }
      })
    })
    req.on('error', (error) => {
      resolve(false)
    })
    console.log(JSON.stringify(qq,null,4))
    req.write(JSON.stringify(qq),'utf-8')
    req.end()
  })
}

export async function elasticQuery(query,field,index,servers,logger){
  const i = getRandomI(servers.length-1)
  const hostname = servers[i].hostname
  const port = servers[i].port
  const r = await _elasticQuery(query,field,index,hostname,port,logger)
  if (r === false) {
    if (logger) logger.warning({elastic:{hostname,port,status:'down'}})
    if (servers.length > 1) {
      const tmp = [ ...servers]
      tmp.splice(i,1)
      return elasticQuery(query,field,index,tmp,logger)
    } else {
      if (logger) logger.emerg('no live elasticsearch servers')
      else console.error('no live elasticsearch servers');
      //process.exit(-1)
    }
  }
  return r === false ? {} : r
}
function _elasticQuery(query,field,index,hostname,port){
  console.log(query,field,index)
  return new Promise( (resolve) => {

    const options = {
      hostname: hostname,
      port: port,
      path: `/${index}/_search?q=${field}:${query}&df=_index`,
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
    }
    const req = request(options, (res) => {
      let data = []
      res.on('data', (chunk) => {
        try {
          data.push(chunk)
        } catch (e) {
          console.error(e)
          console.error(query)
          resolve({})
        }
      })
      res.on('end',() => {
        let r
        try {
          r = JSON.parse(Buffer.concat(data).toString())
          if (r.hits) resolve(r.hits)
          else resolve({})

        } catch (e) {
          resolve({})
        }
      })
    })
    req.on('error', (error) => {
      resolve(false)
    })

    req.end()
  })
}



export async function elasticMLT(index,id,from,size,servers,logger){
  const i = getRandomI(servers.length-1)
  const hostname = servers[i].hostname
  const port = servers[i].port
  const r = await _elasticMLT(index,id,from,size,hostname,port)
  if (r === false) {
    if (logger) logger.warning({elastic:{hostname,port,status:'down'}})
    if (servers.length > 1) {
      const tmp = [ ...servers]
      tmp.splice(i,1)
      return elasticMLT(index,id,from,size,tmp,logger)
    } else {
      if (logger) logger.emerg('no live elasticsearch servers')
      else console.error('no live elasticsearch servers');
      //process.exit(-1)
    }
  }
  return r //=== false ? {} : r
}

function _elasticMLT(index,id,from,size,hostname,port){
  // TODO param field, freq, length und terms
  return new Promise( (resolve) => {
    const qq = {
      "_source": {
        "includes": [ "*" ],
        "excludes": [ "content" ]
      },
      "from": from,
      "size": size,
      "query": {
        "more_like_this" : {
          "fields" : ["content"],
          "like" : {"_id" : id},
          "min_term_freq" : 1,
          "min_word_length": 4,
          "max_query_terms" : 32
          }
        }
      }
    const options = {
      hostname: hostname,
      port: port,
      path: `/${index}/_search`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
    }
    const req = request(options, (res) => {
      let data = []
      res.on('data', (chunk) => {
        try {
          data.push(chunk)
        } catch (e) {
          console.error(e)
          console.error(query)
          resolve({})
        }
      })
      res.on('end',() => {
        let r
        try {
          r = JSON.parse(Buffer.concat(data).toString())
          if (r.hits)resolve(r.hits)
          else {
            console.error(r.error)
            resolve({})
          }
        } catch (e) {
          resolve({})
        }
      })
    })
    req.on('error', (error) => {
      resolve(false)
    })
    console.log(JSON.stringify(qq,null,4))
    req.write(JSON.stringify(qq))
    req.end()
  })

}



// ---------
/*
GET /c3￣content￣aju2-watch‾7200/_termvectors/1295c0722be3b16a366ba350c73bf238?fields=content
{
      "term_statistics" : true,
    "field_statistics" : true,
    "payloads": false,
    "positions": false,
    "offsets": false,
   "filter" : {
      "max_num_terms":3,
      "min_word_length": 4,
      "max_word_length": 12,
      "max_term_freq": 1,
      "max_doc_freq": 1
    }

}
*/
export async function elasticTermVector(id,field,filter,index,servers,logger){
  const i = getRandomI(servers.length-1)
  const hostname = servers[i].hostname
  const port = servers[i].port
  const r = await _elasticTermVector(id,field,filter,index,hostname,port,logger)
  if (r === false) {
    if (logger) logger.warning({elastic:{hostname,port,status:'down'}})
    if (servers.length > 1) {
      const tmp = [ ...servers]
      tmp.splice(i,1)
      return elasticTermVector(id,field,filter,index,tmp,logger)
    } else {
      if (logger) logger.emerg('no live elasticsearch servers')
      else console.error('no live elasticsearch servers');
      //process.exit(-1)
    }
  }
  return r === false ? {} : r
}

function _elasticTermVector(id,field,filter,index,hostname,port,logger){

  return new Promise( (resolve) => {
    // GET /c3￣content￣aju2-watch‾7200/_termvectors/1295c0722be3b16a366ba350c73bf238?fields=content

    const options = {
      hostname: hostname,
      port: port,
      path: `/${index}/_termvectors/${id}/?fields=${field}`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
    }
    const req = request(options, (res) => {
      let data = []
      res.on('data', (chunk) => {
        try {
          data.push(chunk)
        } catch (e) {
          console.error(e)
          console.error(query)
          resolve({})
        }
      })
      res.on('end',() => {
        let r
        try {
          r = JSON.parse(Buffer.concat(data).toString())
          //console.log('termv',r)
          if (r.term_vectors && Object.keys(r.term_vectors).length) {
            if (Object.keys(r.term_vectors).length === 1) resolve(r.term_vectors[field])
            else {
              // TODO
              logger.emerg({notimplemented:'term vector multi field'})
              throw new Error('not implemented: term vector multi field')
            }
          } else resolve({})

        } catch (e) {
          resolve({})
        }
      })
    })
    req.on('error', (error) => {
      resolve(false)
    })

    req.end(JSON.stringify({"term_statistics" : true,
                              "field_statistics" : true,
                              "payloads": false,
                              "positions": false,
                              "offsets": false,
                              filter}),'utf-8')
    //req.end()
  })
}
