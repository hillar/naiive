import { Logger } from "../classes/base/logger.mjs";
import { getRandomI } from "./var.mjs"
import { request } from "http";




export async function Languagezer(text, servers, logger) {
  const i = getRandomI(servers.length - 1);
  const hostname = servers[i].hostname;
  const port = servers[i].port;
  const r = await _Languagezer(text, hostname, port, (logger = new Logger()));
  if (r === false) {
    if (logger) logger.warning({ Languagezer: { hostname, port, status: "down" } });
    if (servers.length > 1) {
      const tmp = [...servers];
      tmp.splice(i, 1);
      return Languagezer(text, tmp, logger);
    } else {
      logger.emerg("no live Languagezer servers");
    }
  }
  return r;
}

function _Languagezer(text, hostname, port, logger) {
  return new Promise(resolve => {

    const options = {
      hostname,
      port,
      //path: model,
      method: "PUT",
      headers: {
        "Content-Length": Buffer.byteLength(text)
      }
    };

    const req = request(options, res => {
      if (res.statusCode === 404) resolve({})
      else if (res.statusCode !== 200 ) {
        if (res.statusCode > 499) logger.crit({ hostname, port, status: res.statusCode, headers: res.headers })
        else logger.warning({ hostname, port, status: res.statusCode, headers: res.headers });
        resolve(false);
        return
      }
      const chunks = [];
      res.on("data", chunk => {
        chunks.push(chunk);
      });
      res.on("end", () => {
        const took = Number(process.hrtime.bigint() - start);
        let data;
        try {
          data = JSON.parse(Buffer.concat(chunks).toString());
        } catch (error) {
          logger.err({ hostname, port, fault: data });
          resolve(false);
        }

        if (data) {
          if (data.languages && Object.keys(data.languages).length) {
            logger.info({ hostname, port, took, wasted:(took - data.took) });
            resolve({ languages:data.languages });
          } else {
            logger.info({ hostname, port, message: "no languages" });
            resolve(false);
          }
        } else {
          logger.err({ hostname, port, message: "unknown response" });
          resolve(false);
        }
      });
    });

    req.on("error", e => {
      logger.warning({ hostname, port, message: e.message });
      resolve(false);
    });
    const start = process.hrtime.bigint();
    req.write(text);
    req.end();
  });
}
