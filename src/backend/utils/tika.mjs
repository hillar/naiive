import { request } from 'http'
import { createReadStream } from 'fs'
import { getHeapStatistics, getHeapSpaceStatistics } from 'v8'

const TIKASERVER = 'localhost'
const TIKAPORT = 9998
const TIKASERVERS = [{hostname:TIKASERVER,port:TIKAPORT}]
const TIKAMAXSIZE =  getHeapStatistics().heap_size_limit / 3//314572800 // 300 Mb

const renameKeys = (keysMap, obj) => Object
    .keys(obj)
    .reduce((acc, key) => ({
        ...acc,
        ...{ [keysMap[key] || key]: obj[key] }
    }), {})



function getRandomI(max) {
    return Math.floor(Math.random() * (max + 1))
}



export async function tikaRMeta(filename, tikaservers=TIKASERVERS,log) {
  log.info({tikar:filename})
  const i = getRandomI(tikaservers.length-1)
  const hostname = tikaservers[i].hostname
  const port = tikaservers[i].port
  const r = await _tikaRMeta(filename, hostname, port,log)
  if (r === false) {
    if (tikaservers.length > 1) {
      const tmp = [ ...tikaservers]
      tmp.splice(i,1)
      return tikaRMeta(filename,tmp,log)
    } else {
      console.error('no live tika servers');
      process.exit(-1)
    }
  }
  return r === false ? {} : r
}

function _tikaRMeta(filename, hostname,port,log) {
  return new Promise ( async (resolve) => {
    const options = {
      hostname: hostname,
      port: port,
      path: '/rmeta/text',
      method: 'PUT',
      headers: {
        'Accept': 'application/json'
      }
    }
    const req = request(options, (res) => {
      let data = ''//[]
      let body = []
      let counter = 0
      let total = 0
      res.on('data', (chunk) => {
        //console.dir(''+d)
        counter ++
        total += chunk.length

        //if (total % (1024 * 1024 * 10) === 0) //debug('data',total)
        if (total > TIKAMAXSIZE) {
          console.log('to biiiig',filename)
          console.log(body.length,total, total/body.length, TIKAMAXSIZE)
          console.log(getHeapSpaceStatistics())
          console.log(getHeapStatistics())
          console.log(process.memoryUsage())

          body = [Buffer.from(`[{"error":"tika result is more than max ${TIKAMAXSIZE}"}]`)]
          console.error('to big',total,TIKAMAXSIZE,filename)
          log.notice({tikalimit:{total,TIKAMAXSIZE},filename})
          resolve({internalerror:413})
          res.destroy()
          return
          //throw new Error('to biiiiiiiig')
        } else {

        //data.push(d)
            body.push(chunk)
            //console.dir(''+chunk)

        }
      })
      res.on('error', (e) => {
        console.error('res on error', filename)
        console.error(e)
        log.notice({tika:{error:e}})
        resolve({})
      })
      res.on('end',() => {
        let rr = {}
        let r = {}
        /*
        console.log(body.length,total, total/body.length, TIKAMAXSIZE,filename)
        console.log(getHeapSpaceStatistics())
        console.log(getHeapStatistics())
        console.log(process.memoryUsage())
        */
        /*
        /hdd/sdc/7300/b5d6521c60b4dddb685c637b52fbcc4b/b5d6521c60b4dddb685c637b52fbcc4b
        /hdd/sdc/7300/68afd0466dff8ff3d9e6712e36d4a4be/68afd0466dff8ff3d9e6712e36d4a4be
        to biiiig /hdd/sdc/7200/a9653d6081628031579f27ae3382d782/a9653d6081628031579f27ae3382d782
27007 508985344 18846.422927389194 508969974
[ { space_name: 'read_only_space',
    space_size: 524288,
    space_used_size: 33520,
    space_available_size: 482064,
    physical_space_size: 42224 },
  { space_name: 'new_space',
    space_size: 8388608,
    space_used_size: 1375672,
    space_available_size: 2749000,
    physical_space_size: 6023488 },
  { space_name: 'old_space',
    space_size: 11735040,
    space_used_size: 9482656,
    space_available_size: 1778688,
    physical_space_size: 10396744 },
  { space_name: 'code_space',
    space_size: 1572864,
    space_used_size: 1141472,
    space_available_size: 369952,
    physical_space_size: 1556096 },
  { space_name: 'map_space',
    space_size: 1060864,
    space_used_size: 409120,
    space_available_size: 625632,
    physical_space_size: 609392 },
  { space_name: 'large_object_space',
    space_size: 1572864,
    space_used_size: 249120,
    space_available_size: 1501863424,
    physical_space_size: 1572864 },
  { space_name: 'new_large_object_space',
    space_size: 0,
    space_used_size: 0,
    space_available_size: 0,
    physical_space_size: 0 } ]
{ total_heap_size: 24854528,
  total_heap_size_executable: 3145728,
  total_physical_size: 20201552,
  total_available_size: 1507833024,
  used_heap_size: 12728040,
  heap_size_limit: 1526909922,
  malloced_memory: 8192,
  peak_malloced_memory: 4876256,
  does_zap_garbage: 0 }
{ rss: 564633600,
  heapTotal: 24854528,
  heapUsed: 12745448,
  external: 509069672 }
to big 508985344 508969974 /hdd/sdc/7200/a9653d6081628031579f27ae3382d782/a9653d6081628031579f27ae3382d782
1 508985344 508985344 508969974
[ { space_name: 'read_only_space',
    space_size: 524288,
    space_used_size: 33520,
    space_available_size: 482064,
    physical_space_size: 42224 },
  { space_name: 'new_space',
    space_size: 8388608,
    space_used_size: 1456352,
    space_available_size: 2668320,
    physical_space_size: 6023488 },
  { space_name: 'old_space',
    space_size: 11735040,
    space_used_size: 9488768,
    space_available_size: 1778688,
    physical_space_size: 10402856 },
  { space_name: 'code_space',
    space_size: 1572864,
    space_used_size: 1141472,
    space_available_size: 369952,
    physical_space_size: 1556096 },
  { space_name: 'map_space',
    space_size: 1060864,
    space_used_size: 416720,
    space_available_size: 617952,
    physical_space_size: 609392 },
  { space_name: 'large_object_space',
    space_size: 1572864,
    space_used_size: 249120,
    space_available_size: 1501863424,
    physical_space_size: 1572864 },
  { space_name: 'new_large_object_space',
    space_size: 0,
    space_used_size: 0,
    space_available_size: 0,
    physical_space_size: 0 } ]
{ total_heap_size: 24854528,
  total_heap_size_executable: 3145728,
  total_physical_size: 20207208,
  total_available_size: 1507744664,
  used_heap_size: 12821976,
  heap_size_limit: 1526909922,
  malloced_memory: 8192,
  peak_malloced_memory: 4876256,
  does_zap_garbage: 0 }
{ rss: 564633600,
  heapTotal: 24854528,
  heapUsed: 12830744,
  external: 509069672 }

        */
        try {
          //console.log('',data.length)
          //debug('data',data.length,total)
          try {
            //r = JSON.parse(Buffer.concat(data).toString())
            r = JSON.parse(Buffer.concat(body).toString())
          } catch (e) {
            //console.error('TIKA BAD',hostname,port,filename,counter,total,body.length,res.statusCode)
            //console.dir(Buffer.concat(body).toString())

            //console.error(Buffer.concat(data).toString())
            //console.error(e)
            //if (counter > 1) process.exit(-1)
            log.notice({tika:{status:res.statusCode},filename})
            resolve({tikaerror:res.statusCode})
            return
          }
          //debug('r',r.length)
          if (r && r[0]) {
            rr = r.shift()
            //debug('r',Object.keys(rr))
            const regex = /\W/gi;
            const keysMap = {}
             for (const key of Object.keys(rr)){
               // rename tika fields
               if (key.includes(':') || key.includes(' ')) {
                 keysMap[key] = key.replace(regex,'-').toLowerCase()
                 // use 'content' for 'X-TIKA:content'
                 if (key==='X-TIKA:content') keysMap[key] = 'content'
               }
             }
             rr = renameKeys(keysMap, rr)
             //debug(keysMap)
             if (r.length > 0) {
               //debug('rrr',r.length)

               while (r.length > 0) {
                 let rrr = r.shift()
                 if (rrr['X-TIKA:content'] && rrr['X-TIKA:content'].length) {
                   if (!rr.attachments) rr.attachments = []
                   //debug('rrr',Object.keys(rrr))
                   const regex = /\W/gi;
                   const keysMap = {}
                    for (const key of Object.keys(rrr)){
                      // rename tika fields
                      if (key.includes(':') || key.includes(' ')) {
                        keysMap[key] = key.replace(regex,'-').toLowerCase()
                        // use 'content' for 'X-TIKA:content'
                        if (key==='X-TIKA:content') keysMap[key] = 'content'
                      }
                    }
                    rrr = renameKeys(keysMap, rrr)
                    rr.attachments.push(rrr)
                  }
               }

             }

          }
          ////debug('content length',r.content.length)

          resolve(rr)
        } catch (e) {
          //debug('catch',e)
          log.notice({tika:{error:e},filename})
          resolve({})
        }
      })
    })
    req.on('error', (error) => {
      //debug('req.on',error)
      log.notice({tika:{error},filename})
      resolve(false)
    })
    try {
      const rs = createReadStream(filename)
      rs.on('error',(e)=>{
        console.error(e)
        log.warning({tika:{error:e},filename})
        resolve({error:{filename,message:e.message}})
      })
      rs.pipe(req)
    } catch (error) {
      log.notice({tika:{error},filename})
      resolve(false)
    }
  })
}
