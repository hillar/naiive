//import { default as ss } from 'ssdeep'

/*
let ss
try {
  ss = import('ssdeep')
} catch (error) {
  console.log('no ssdeep')
  const n = () => {}
  ss = {hash_from_file:n, hash_from_str:n,hash_compare:n}
}
*/
console.log('no ssdeep')
const n = () => {}
const ss = {hash_from_file:n, hash_from_str:n,hash_compare:n}

// https://www.virusbulletin.com/virusbulletin/2015/11/optimizing-ssdeep-use-scale/

export function hash_from_file (fn) {
  return new Promise( resolve => {
    try {
      const hash = ss.hash_from_file(fn)
      resolve(hash)
    } catch (e) {
      resolve ()
    }
  })
}

export function hash_from_str (str) {
  return new Promise( resolve => {
    try {
      const hash = ss.hash(str)
      resolve(hash)
    } catch (e) {
      resolve ()
    }
  })
}

export function hash_compare (h1,h2) {
  return ss.compare(h1,h2)
}

export function hash_split (ss) {
  if (!ss) {
    let [chunksize, chunk, double_chunk] = []
    return { chunksize, chunk, double_chunk }
  }
  let [chunksize, chunk, double_chunk] = ss.split(':')
  chunksize = Number(chunksize)
  return { chunksize, chunk, double_chunk }
}

export function findSimilar (id,index,elasticserver,threshold_grade=75) {
  return new Promise( async (resolve) => {
      const result = []
      const ssdeep = await elasticGetFieldFromDoc(id,'ssdeep',index,elasticserver)
      if (ssdeep ) {
          let {chunksize, chunk, double_chunk } = hash_split(ssdeep)
          const q = `
"query": {
    "bool": {
        "must": [
            {
                "terms": {
                    "chunksize": [${chunksize}, ${chunksize * 2}, ${Math.round(chunksize / 2)}]
                }
            },
            {
                "bool": {
                    "should": [
                        {
                            "match": {
                                "chunk": {
                                    "query": ${chunk}
                                }
                            }
                        },
                        {
                            "match": {
                                "double_chunk": {
                                    "query": ${double_chunk}
                                }
                            }
                        }
                    ],
                    "minimum_should_match": 1
                }
            }
        ]
    }
}
`
        const results = await elasticQuery(q,index,elasticserver)
        for (const record of results['hits']['hits']) {
          const record_ssdeep = record['_source']['ssdeep']
          if (record_ssdeep) {
            const ssdeep_grade = compare(record_ssdeep, ssdeep)
            if (ssdeep_grade >= threshold_grade)
            result.push(record['_id'])
          }
        }


      } else {
        debug('no ssdeep',id,index)

      }
      resolve(result)
  })
}
