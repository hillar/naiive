import {createHash } from 'crypto'
import { readdirSync, readFileSync, watch, createReadStream } from 'fs'
import { join } from 'path'
import { digestFile, isHash, DEFAULTALGORITHM as HASHALGORITHM } from './hasher.mjs'
import { tikaRMeta } from './tika.mjs'
import { putDoc2Elastic, docExistInElastic, getDoc, indexExists, put2Elastic, updateDoc2Elastic } from './elastic.mjs'
import { isEstonianIDCode } from './parsers/est-id.mjs'
import { hash_from_str, hash_split } from './ssdeep.mjs'

import { HASHFIELD, CONTENTINDEX, FILESINDEX, METAINDEX, ENTITYINDEX, ALLINDEX, MAXPREVIEWSIZE, indexName } from '../constants/elastic.mjs'
import { PORT as FILESSERVERPORT, SEPARATOR as PORTSEPARATOR } from '../constants/files.mjs'
import { trimArrayOfStrings } from './var.mjs'


const MINCONTENTLENGTH2FIND = 11
const MAXFIELDS = 5000 // total_fields.limit
/*
const CONTENT = 'content'
const FILES = 'files'
const META = 'meta'
const FINDINGS = 'findings'
*/



const END_OF_SEQUENCE = Symbol()

function* tokenize(chars) {
    let iterator = chars[Symbol.iterator]();
    let ch;
    do {
        ch = getNextItem(iterator);
        if (isWordChar(ch)) {
            let word = '';
            do {
                word += ch;
                ch = getNextItem(iterator);
            } while (isWordChar(ch));
            yield word;
        }
    } while (ch !== END_OF_SEQUENCE);
}

function getNextItem(iterator) {
    let item = iterator.next();
    return item.done ? END_OF_SEQUENCE : item.value;
}
function isWordChar(ch) {
    return typeof ch === 'string' && /^\S$/.test(ch);
}

function trimArray(a) {
    return [ ...a].filter( i => (!(i === undefined)) )
}

async function pp(str, ...fns){
  const r = {}
  for (const tkn of tokenize(str)){
    const pfns = []
    for (const fn of fns){
      pfns.push(new Promise( resolve => {
        resolve(fn(tkn))
      }))
    }
    const rr = trimArray(await Promise.all(pfns))
    if (rr.length > 0) {
      for (const rrr of rr){
        for (const k of Object.keys(rrr)){
          // uniq key and value(s)
          if (r[k]){
            if (Array.isArray(r[k])){
              if (!(r[k].includes(rrr[k]))) r[k].push(rrr[k])
            } else if (r[k] !== rrr[k]){
              r[k] = [r[k]]
              r[k].push(rrr[k])
            }
          } else {
            // array or single value ?
            r[k] = [rrr[k]]
          }
        }
      }
    }
  }

  return r
}

// TODO http://data.iana.org/TLD/tlds-alpha-by-domain.txt
const email = (s) => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  if (re.test(s.toLowerCase())) return {'email':s}
}

function estID(s){
  const re = /^[1-6][0-9]{2}[0,1][0-9][0-9]{2}[0-9]{4}$/
  if (re.test(s)) {
    if (isEstonianIDCode(s)) {
      return {'estID':s}
    }
  }
}

function ip4(s){
  const re = /^(25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)$/
  if (re.test(s)) return {'ipv4':s}
}

function ip6(s){
  const re = /^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$/
  if (re.test(s)) return {'ipv6':s}
}

function iban(s){
    function likeIban(str){
      return /^([A-Z]{2}[ \-]?[0-9]{2})(?=(?:[ \-]?[A-Z0-9]){9,30}$)((?:[ \-]?[A-Z0-9]{3,5}){2,7})([ \-]?[A-Z0-9]{1,3})?$/.test(str);
    }

    function validateIbanChecksum(iban) {
      const ibanStripped = iban.replace(/[^A-Z0-9]+/gi,'').toUpperCase()
      const m = ibanStripped.match(/^([A-Z]{2})([0-9]{2})([A-Z0-9]{9,30})$/)
      if(!m) return false
      const numbericed = (m[3] + m[1] + m[2]).replace(/[A-Z]/g,function(ch){
          return (ch.charCodeAt(0)-55)
      })
      const mod97 = numbericed.match(/\d{1,7}/g).reduce(function(total, curr){ return Number(total + curr)%97},'')
      return (mod97 === 1)
    };
    if (likeIban(s)){
      if (validateIbanChecksum(s))
      return {'iban':s}
    }
}



function pprocess(str) {
  return pp(str, email,ip4,ip6,iban,estID)
}


function getMetaFiles(dir){
  const metaFiles = []
  const files = readdirSync(dir)
  for (const f of files) {
    if (f.endsWith('.json')) metaFiles.push(f)
  }
  return metaFiles
}

function flatten (target, opts) {
  opts = opts || {}

  var delimiter = opts.delimiter || '.'
  var maxDepth = opts.maxDepth
  var output = {}

  function step (object, prev, currentDepth) {
    currentDepth = currentDepth || 1
    Object.keys(object).forEach(function (key) {
      var value = object[key]
      var isarray = opts.safe && Array.isArray(value)
      var type = Object.prototype.toString.call(value)

      var isobject = (
        type === '[object Object]' ||
        type === '[object Array]'
      )

      var newKey = prev
        ? prev + delimiter + key
        : key

      if (!isarray  && isobject && Object.keys(value).length &&
        (!opts.maxDepth || currentDepth < maxDepth)) {
        return step(value, newKey, currentDepth + 1)
      }

      output[newKey] = value
    })
  }

  step(target)

  return output
}

function cleanup(obj){
  if (obj) {
    if (typeof obj === 'object') {
      if (Array.isArray(obj)) {
        if (typeof obj[0] === 'object') {
          const r = {}
          for (const i in obj) {
            const tmp = cleanup(obj[i])
            if (tmp) {
              for (const key of Object.keys(tmp)) {
                if (tmp[key]) {

                  if (!r[key]) r[key] = tmp[key]
                  else {
                    if (Array.isArray(r[key])) {
                      r[key] = trimArrayOfStrings([r[key],tmp[key]].flat())
                    } else {
                      r[key] = trimArrayOfStrings([r[key],tmp[key]].flat())
                    }
                  }
                }
              }
            }
          }
          if (Object.keys(r).length) return r
          else return
        } else return obj.flat()
      } else {
        const r = {}
        for (const key of Object.keys(obj)) {
          const tmp = cleanup(obj[key])
          if (tmp) r[key] = tmp
        }
        if (Object.keys(r).length) return r
        else return
      }
    } else return obj
  } else return
}

function __cleanup(obj){
  if (typeof obj !== 'object') return obj
  // look into arrays
  if (Array.isArray(obj) && obj.length > 0 ) {
    const a = []
    for (const o of obj) {
      const v = cleanup(o)
      if (v) {
         if (typeof v === 'object') a.push(v)
         else {
           if (!a.includes(v)) a.push(v)
         }
       }
    }
    if (a.length > 0) {
      if (typeof a[0] === 'object') {
        const r = {}
        for (const i in a) {
          for (const key of Object.keys(a[i])){
            if (r[key]) {
              if (Array.isArray(r[key])) r[key].push(a[i][key])
              else {
                r[key] = [r[key]]
                r[key].push(a[i][key])
              }
            } else r[key] = a[i][key]
          }
        }
        /*
        const keys = Object.keys(a[0])
        for (const item of a) {
          for (const key of keys){
            if (typeof item[key] === 'object'){
              r[key] = flatten(item[key])
            } else {
              if (!r[key]) r[key] = []

              r[key].push(item[key])
            }
          }
        }
        */
        return r
      } else return a
    } else return
  }
  if (Object.keys(obj).length === 0 ) return
  const ret = {}
  for (const o of Object.keys(obj)) {
    const v = cleanup(obj[o])
    if (v) ret[o] = v
  }
  return ret
}

/*
const ismd5 = function (inputString) {
	return (/[a-fA-F0-9]{32}/).test(inputString);
}
*/

export async function processDir(contentHash,archiveDirectory, tikaservers, elasticservers,prefix,suffix,content,field,files,entities,meta,log){

    if (!isHash(contentHash,HASHALGORITHM)) {
      log.warning({processDir:{error:'not '+HASHALGORITHM,contentHash}})
      return new Error('not a '+HASHALGORITHM+' hash '+contentHash)
    }

    const contenIndex = indexName(prefix,content,suffix)
    const filesIndex = indexName(prefix,files,suffix)
    const entityIndex = indexName(prefix,entities,suffix)
    const metaIndex = indexName(prefix,meta,suffix)
    const allIndex = indexName(prefix,ALLINDEX,suffix)
    // check index and creatre if not exists
    //const commonSettings = {settings:{"number_of_replicas":0, "index.refresh_interval": "10s"}}
    const contentExists = await indexExists(contenIndex,elasticservers,log)
    if (!(contentExists === true)) {

      /*
      "Caused by: java.lang.IllegalArgumentException: The length of [content.keyword] field of [cca0a02dabfb09ded1489af66cac4916] doc of [test1￣content￣fs01] index has exceeded [1000000] - maximum allowed to be analyzed for highlighting.
      This maximum can be set by changing the [index.highlight.max_analyzed_offset] index level setting.
      For large texts, indexing with offsets or term vectors is recommended!"
      type: 'illegal_argument_exception',
    reason: 'unknown setting [index.indices.query.bool.max_clause_count] please check that any required plugins are installed, or check the breaking changes documentation for removed settings'
      */
        const contentSettings = { "settings": {
          "number_of_replicas":0,
          "index.refresh_interval": "10s",
          "index.query.default_field": ["content"],
          "index.highlight.max_analyzed_offset": 100000000,
          //"indices.query.bool.max_clause_count":4096,
          "analysis": {
            "analyzer": {
              "ssdeep_analyzer": {
                "tokenizer": "ssdeep_tokenizer"
              },
              "fulltext_analyzer": {
                "type": "custom",
                "tokenizer": "standard",
                "filter": [
                  "lowercase",
                  "type_as_payload"
                ]
              }
            },
            "tokenizer": {
              "ssdeep_tokenizer": {
                "type": "ngram",
                "min_gram": 7,
                "max_gram": 7
              }
            }
          }
        },
        "mappings": {
            "dynamic": "strict",
            "properties": {
              "content": {
                "type": "text",
                "term_vector": "with_positions_offsets_payloads",
                "store" : true,
                "analyzer" : "fulltext_analyzer"
              },
              "chunksize": {
                "type": "integer"
              },
              "chunk": {
                "analyzer": "ssdeep_analyzer",
                "type": "text"
              },
              "double_chunk": {
                "analyzer": "ssdeep_analyzer",
                "type": "text"
              },
              "ssdeep": {
                "type": "keyword"
              }
            }
          }
        }
        const createdContent = await put2Elastic(JSON.stringify(contentSettings),contenIndex,elasticservers,log)
    }

    const filesExists = await indexExists(filesIndex,elasticservers,log)
    if (!(filesExists === true)) {
      const createdFiles = await put2Elastic(JSON.stringify({settings:{"index.mapping.ignore_malformed": true,"number_of_replicas":0, "index.refresh_interval": "10s"}}),filesIndex,elasticservers,log)

    }
    const metaExists = await indexExists(metaIndex,elasticservers,log)
    if (!(metaExists === true)) {
      const createdMeta = await put2Elastic(JSON.stringify({settings:{"index.mapping.ignore_malformed": true,"index.mapping.total_fields.limit":MAXFIELDS,"number_of_replicas":0, "index.refresh_interval": "10s"}}),metaIndex,elasticservers,log)

    }
    const findingsExists = await indexExists(entityIndex,elasticservers,log)
    if (!(findingsExists === true)) {
      const createdFindings = await put2Elastic(JSON.stringify({settings:{"index.mapping.ignore_malformed": true,"number_of_replicas":0, "index.refresh_interval": "10s"}}),entityIndex,elasticservers,log)
    }
    const allExists = await indexExists(allIndex,elasticservers,log)
    if (!(allExists === true)) {
      const createdAll = await put2Elastic(JSON.stringify(
        {settings: {
            "index.mapping.total_fields.limit":MAXFIELDS*2,
            "index.mapping.ignore_malformed": true,
            "number_of_replicas":0,
            "index.refresh_interval": "10s",
            "index.query.default_field": ["content"],
            "index.highlight.max_analyzed_offset": 100000000,
            "analysis": {
              "analyzer": {
                "ssdeep_analyzer": {
                  "tokenizer": "ssdeep_tokenizer"
                },
                "fulltext_analyzer": {
                  "type": "custom",
                  "tokenizer": "standard",
                  "filter": [
                    "lowercase",
                    "type_as_payload"
                  ]
                }
              },
              "tokenizer": {
                "ssdeep_tokenizer": {
                  "type": "ngram",
                  "min_gram": 7,
                  "max_gram": 7
                }
              }
            }
          },
          "mappings": {
              //"dynamic": "strict",
              "properties": {
                "content": {
                  "type": "text",
                  "term_vector": "with_positions_offsets_payloads",
                  "store" : true,
                  "analyzer" : "fulltext_analyzer",
                  "index_options": "positions",
                  "index_phrases": true

                },
                "chunksize": {
                  "type": "integer"
                },
                "chunk": {
                  "analyzer": "ssdeep_analyzer",
                  "type": "text"
                },
                "double_chunk": {
                  "analyzer": "ssdeep_analyzer",
                  "type": "text"
                },
                "ssdeep": {
                  "type": "keyword"
                }
              }
            }
        }
        ),
        allIndex,
        elasticservers,log
      )
    }


    // ok to proccess
    const dir = join(archiveDirectory,contentHash)
    const contentFile = join(dir,contentHash)

    const ff = []
    let isnew = false
    let isupdated = false

    // isdir ?
    /*
    files server writes to
    mkdirSync(join(currentpath,FILESINDEX))
    */
    let fields = []
    for (const f of readdirSync(join(dir,files))) {

      if (f.endsWith('.json')) {

        const filename = join(dir,files,f)
        //log.debug({filename})
         try {
           const id = await digestFile(filename)
           const exists = await docExistInElastic(id,filesIndex,elasticservers)
           if (exists === false) {
             // TODO delete old journals
             // deleteDocs({journalname:filename},'files',elasticserver)
             //let doc
             //doc.journalname = filename
             let doc = cleanup(JSON.parse(readFileSync(filename)))
             doc = cleanup(doc)
             fields.push(doc)
             //doc.__journalname__ = filename
             //debug('cleaned raw',raw)
             //if (Array.isArray(raw)) doc.journal = raw
             //else doc.journal = [ raw ]
             await putDoc2Elastic(JSON.stringify(doc),id,filesIndex,elasticservers,log)

             isupdated = true
           } else {
             //ff.push({isnew:false,id})
             //const old = await getDoc(id,filesIndex,elasticserver)
             //debug('old',old, contentHash === old.contentHash)

           }
         } catch (error) {
           console.error(error)
           log.warning({processDir:{error}})
         }
      }
    }

    const exists = await docExistInElastic(contentHash,allIndex,elasticservers)
    //debug('exists',exists)
    if (exists === false ) {
      isnew = true
      //debug('wait for tika')
      let rmeta = await tikaRMeta(contentFile,tikaservers)
      let content = rmeta.content ? rmeta.content.replace(new RegExp('(\n){3,}', 'gim') , '\n') : ''
      delete rmeta.content
      const attachments = rmeta.attachments
      //delete rmeta.attachments
      if (attachments) {
        for (const attachment  of attachments) {
          if (attachment.content && attachment.content.length) {
            content += '\n' + attachment.content.replace(new RegExp('(\n){3,}', 'gim') , '\n')
            delete attachment.content
            //log.debug({contentHash,attachment})
          }
        }
      }
      //debug(content)
      let findigs
      rmeta = cleanup(rmeta)
      await putDoc2Elastic(JSON.stringify(rmeta),contentHash,metaIndex,elasticservers,log)
      if (content && content.length) {

        if (content.length && content.length > MINCONTENTLENGTH2FIND) findigs = await pprocess(content)
        if (findigs && Object.keys(findigs).length>0) {
          //debug(Object.keys(findigs).length)
          const ff = await putDoc2Elastic(JSON.stringify(findigs),contentHash,entityIndex,elasticservers,log)
          //debug(ff)
        }
        const ssdeep = await hash_from_str(content)
        //log.debug({ssdeep})
        const {chunksize, chunk, double_chunk } = hash_split(ssdeep)

        const r = await putDoc2Elastic(JSON.stringify({content,ssdeep,chunksize, chunk, double_chunk}),contentHash,contenIndex,elasticservers,log)

      } else {
        //debug('no content',contentHash)
        const r = await putDoc2Elastic(JSON.stringify({}),contentHash,contenIndex,elasticservers,log)

      }
      fields = cleanup(fields)

      //const tmp = {content,meta:rmeta,fields:fields,entities:findigs}
      const all = await putDoc2Elastic(JSON.stringify({content,meta:rmeta,fields:fields,entities:findigs}),contentHash,allIndex,elasticservers,log)
      //console.dir(flatten({all,tmp}))
    } else {

      if (isupdated) {
          log.info({update:{id:contentHash,index:allIndex}})
          updateDoc2Elastic({fields:cleanup(fields)},contentHash,allIndex,elasticservers,log)
      }
    }
    return {contentHash,isnew,ff}

}
