export function hilbert(something) {

    function index2xy(index, size) {

        function last2bits(x) {
            return x & 3
        }

        const positions = [
            [0, 0],
            [0, 1],
            [1, 1],
            [1, 0]
        ]
        let tmp = positions[last2bits(index)]
        index = index >>> 2
        let x = tmp[0]
        let y = tmp[1]
        for (let n = 4; n <= size; n *= 2) {
            const n2 = n / 2
            switch (last2bits(index)) {
                case 0 : // left-bottom
                    tmp = x
                    x = y
                    y = tmp
                    break
                case 1 : // left-upper
                    x = x
                    y = y + n2
                    break
                case 2 : // right-upper
                    x = x + n2
                    y = y + n2
                    break
                case 3 : // right-bottom
                    tmp = y
                    y = n2 - 1 - x
                    x = n2 - 1 - tmp
                    x = x + n2
                    break
            }
            index = index >>> 2
        }
        return { x, y }
    }

    if (typeof something[Symbol.iterator] !== "function") throw new Error("not iterable")
    if (!something.length) throw new Error("no length")

    const size = Math.pow(2, Math.ceil(Math.log(Math.sqrt(something.length))/ Math.log(2))) // next pow 2
    const matrix = []
    for (let i in something) {
        const { x, y } = index2xy(i, size)
        if (!matrix[x]) matrix[x] = []
        matrix[x][y] = something[i]
    }
    if (typeof something === "string") {
        for (const x in matrix) {
            matrix[x] = matrix[x].join("").replace(/\n/g, "\\")
        }
        return matrix.join("\n")
    } else return matrix
}
