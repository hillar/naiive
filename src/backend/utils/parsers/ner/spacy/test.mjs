
import { readFileSync } from "fs";
import { SpacyNER } from "./spacy-ner.mjs"

(async () => {
  const inputfile = "../stanford/9505040.txt";
  //const txt = 'sample text with some entitites like: Tallinn, Estonia \n Hillar Aarelaid hillar@aarelaid.net'
  const txt = readFileSync(inputfile, { encoding: "utf-8" });
  const hostname = "localhost";
  const port = 9996;

  try {
    const entities = await SpacyNER(txt, 'en',[{ hostname, port }]);
    console.log("---------------");
    console.dir(entities);
    console.dir(Object.keys(entities.entities))
  } catch (e) {
    console.error(e);
  }
})();
