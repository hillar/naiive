import { Logger } from "../../../../classes/base/logger.mjs";
import { getRandomI } from "../../../var.mjs"
import { request } from "http";
import { ENTITIES, SHORT  } from '../entitie-names.mjs'

const PATH = "/ent";
const MODEL = 'en';


export async function SpacyNER(text, model = MODEL, servers, logger) {
  const i = getRandomI(servers.length - 1);
  const hostname = servers[i].hostname;
  const port = servers[i].port;
  const r = await _SpacyNER(text, model,  hostname, port, (logger = new Logger()));
  if (r === false) {
    if (logger) logger.warning({ Spacy: { hostname, port, status: "down" } });
    if (servers.length > 1) {
      const tmp = [...servers];
      tmp.splice(i, 1);
      return SpacyNER(text, model, tmp, logger);
    } else {
      logger.emerg("no live Spacy servers");
    }
  }
  return r;
}

function _SpacyNER(text, model, hostname, port, logger) {
  return new Promise(resolve => {
    const postData = JSON.stringify({text,model});

    const options = {
      hostname,
      port,
      path: PATH,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Content-Length": Buffer.byteLength(postData)
      }
    };

    const start = process.hrtime.bigint();
    const req = request(options, res => {
      if (res.statusCode !== 200) {
        if (res.statusCode === 500) logger.crit({ hostname, port, status: res.statusCode, headers: res.headers })
        else logger.warning({ hostname, port, status: res.statusCode, headers: res.headers });
        resolve(false);
      }
      const chunks = [];
      res.on("data", chunk => {
        chunks.push(chunk);
      });
      res.on("end", () => {
        const took = Number(process.hrtime.bigint() - start) / 1e6;
        let data;
        try {
          data = JSON.parse(Buffer.concat(chunks).toString());
        } catch (error) {
          logger.err({ hostname, port, fault: data });
          resolve(false);
        }
        if (data) {
          if (Array.isArray(data) && data.length) {
            const entities = {};
            for (const result of data) {
              if (result.type && result.text) {
                const key = SHORT[result.type] ? SHORT[result.type] : result.type
                if (!entities[key]) {
                  entities[key] = []
                  if (!ENTITIES.includes(key)) logger.notice({unknown:key,hostname,port})
                }
                if (!entities[key].includes(result.text)) entities[key].push(result.text.replace(/\n/,' '))
                //entities.push(tmp);
              } else {
                logger.info({ hostname, port, message: "not result", result });
              }
            }
            resolve({ entities, took, size: Buffer.byteLength(text), total:data.length });
          } else {
            logger.err({ hostname, port, message: "unknown response", data });
            resolve(false);
          }
        } else {
          logger.err({ hostname, port, message: "no data" });
          resolve(false);
        }
      });
    });

    req.on("error", e => {
      logger.warning({ hostname, port, message: e.message });
      resolve(false);
    });
    req.write(postData);
    req.end();
  });
}
