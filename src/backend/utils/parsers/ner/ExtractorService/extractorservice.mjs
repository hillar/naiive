import { Logger } from "../../../../classes/base/logger.mjs";
import { getRandomI } from "../../../var.mjs"
import { ENTITIES, SHORT  } from '../entitie-names.mjs'
import { request } from "http";

const PATH = "/ExtractorService/jsonwsp";
const CONFIDENCE = 30;


export async function ExtractorServiceNER(text, confidence = CONFIDENCE, servers, logger) {
  const i = getRandomI(servers.length - 1);
  const hostname = servers[i].hostname;
  const port = servers[i].port;
  const r = await _ExtractorServiceNER(text, confidence, hostname, port, (logger = new Logger()));
  if (r === false) {
    if (logger) logger.warning({ ExtractorService: { hostname, port, status: "down" } });
    if (servers.length > 1) {
      const tmp = [...servers];
      tmp.splice(i, 1);
      return ExtractorServiceNER(text, confidence, tmp, logger);
    } else {
      logger.emerg("no live ExtractorService servers");
    }
  }
  return r;
}

function _ExtractorServiceNER(text, confidence, hostname, port, logger) {
  return new Promise(resolve => {
    /*
    {
    "args":{
      "unstructured_text":"foo bar",
      "confidence_threshold":"30"
    },
    "methodname":"extract",
    "type":"jsonwsp/request",
    "version":"1.0"
    }

    */
    const postData = JSON.stringify({
      methodname: "extract",
      args: {
        confidence_threshold: `${confidence}`,
        unstructured_text: text
      }
    });

    const options = {
      hostname,
      port,
      path: PATH,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Content-Length": Buffer.byteLength(postData)
      }
    };

    const start = process.hrtime.bigint();
    const req = request(options, res => {
      if (res.statusCode !== 200) {
        logger.warning({ hostname, port, status: res.statusCode, headers: res.headers });
        resolve(false);
      }
      const chunks = [];
      res.on("data", chunk => {
        chunks.push(chunk);
      });
      res.on("end", () => {
        const took = Number(process.hrtime.bigint() - start) / 1e6;
        let data;
        try {
          data = JSON.parse(Buffer.concat(chunks).toString());
        } catch (error) {
          logger.err({ hostname, port, fault: 'not json' });
          resolve(false);
          return
        }
        if (data) {
          if (data.fault) {
            if (data.fault.detail) {
              if (data.fault.detail.join().includes("Traceback")) {
                throw new Error(`${data.fault.hint} \n ${data.fault.detail.join("\n")}`);
              } else {
                logger.crit({ hostname, port, fault: data.fault });
                resolve(false);
              }
            }
          } else if (data.result && Array.isArray(data.result.extractions)) {
            const entities = {};
            for (const result of data.result.extractions) {
              if (result.data_type && result.data_value) {
                //const key = result.data_type.upperCase()
                // TODO should key be uppercase !?!?
                let key = (result.data_sub_type && result.data_sub_type !== "Unknown")
                    ? result.data_sub_type.toUpperCase()
                    : result.data_type.toUpperCase()
                if (key.includes(' ')) key = key.split(' ')[0]
                key = SHORT[key] ? SHORT[key] : key
                if (!entities[key]) entities[key] = []
                //if (!entities[key].includes(result.data_value)) entities[key].push(result.data_value)

                const tmp = {};
                if (result.data_value.startsWith("{") && result.data_value.endsWith("}")) {
                  try {
                    tmp[key] = JSON.parse(result.data_value);
                  } catch (error) {
                    logger.err({ hostname, port, message: "bad data value", type: result.data_type, data: result.data_value });
                  }
                } else tmp[key] = result.data_value;
                if (tmp[key]) {
                  if (tmp[key].offset) {
                    delete tmp[key].length;
                    delete tmp[key].offset;
                  }
                  //if (result.data_sub_type) console.dir(result);
                  if (!(typeof tmp[key] === 'object')) {
                    if (!entities[key].includes(tmp[key])) entities[key].push(tmp[key])
                  } else entities[key].push(tmp[key]);
                } else logger.warning({ hostname, port, message: "empty result", type: result.data_type, result });

              } else {
                logger.info({ hostname, port, message: "not result", result });
              }
            }
            resolve({ entities, took, size: Buffer.byteLength(text), total: data.result.length });
          } else {
            logger.err({ hostname, port, message: "unknown response", data });
            resolve(false);
          }
        } else {
          logger.err({ hostname, port, message: "no data" });
          resolve(false);
        }
      });
    });

    req.on("error", e => {
      logger.warning({ hostname, port, message: e.message });
      resolve(false);
    });
    req.write(postData);
    req.end();
  });
}
