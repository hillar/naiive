import { Logger } from "../../../../classes/base/logger.mjs";
import { getRandomI } from "../../../var.mjs"
import { ENTITIES, SHORT  } from '../entitie-names.mjs'
import { Socket } from 'net';


// see https://nlp.stanford.edu/software/CRF-NER.shtml
// 7 class:	Location, Person, Organization, Money, Percent, Date, Time
// handle IOB format https://arxiv.org/pdf/cmp-lg/9505040.pdf

export async function StanfordNER(text,  servers, logger) {
  const i = getRandomI(servers.length - 1);
  const hostname = servers[i].hostname;
  const port = servers[i].port;
  const r = await _StanfordNER(text, hostname, port, (logger = new Logger()));
  if (r === false) {
    if (logger) logger.warning({ Stanford: { hostname, port, status: "down" } });
    if (servers.length > 1) {
      const tmp = [...servers];
      tmp.splice(i, 1);
      return StanfordNER(text,  tmp, logger);
    } else {
      logger.emerg("no live Stanford servers");
    }
  }
  return r;
}

function _StanfordNER(text, hostname, port, logger) {
  return new Promise(resolve => {
    const socket = new Socket();
    socket.on('error', error => {
			logger.warning({hostname,port,error})
      resolve(false)
		});
    const chunks = []
    socket.on('data', chunk => chunks.push(chunk))
    socket.on('end', () => {
      const took = Number(process.hrtime.bigint() - start) / 1e6 ;
      //const entities = []
			const re = /([^\s]+)\/([A-Z][A-Z]+\b)\s/  // /<([A-Za-z-]+?)>(.+?)<\/\1>/g;
			const str = Buffer.concat(chunks).toString()
      //console.dir(str)
      const entities = parse(str,logger,hostname,port)
      //console.dir(es)
      resolve({ entities, took, size: Buffer.byteLength(text), total: entities.length });
    })
    socket.on('error', (error) => {
        logger.err({ hostname, port, error });
    })

    const start = process.hrtime.bigint();
    socket.connect(port, hostname, () => {
      logger.info({hostname,port,sending:text.length})
			socket.setNoDelay(true);
		  // üõöä should 'break' PERSON followed by ORGANIZATION
      socket.write(`${text.replace(/\r?\n|\r/g, ' üõöä ').replace(/\t|\//g, ' ')}\n`)
		});
  });
}

function parse(slashtags, logger,hostname,port) {

  // extract the neighbors into one entity
  const entities = {};
  let prevEntity = false;
  let last
  let lasts = []
  const entityBuffer = [];
  for (const token of slashtags.split(/\s/gmi)){

    const parts = new RegExp('(.+)\/([A-Z]-?[A-Z]+)','g').exec(token);
    if (parts) {
      let key
      let value
      // handle B- I-
      const bi = parts[2].split('-')
      if (bi[1]) {
        key = SHORT[bi[1]] ? SHORT[bi[1]] : bi[1]
        value = parts[1]
      } else {
        // no B- I-
        key = parts[2]
        value = parts[1]
      }
      if (last === key) {
        if (value !== 'üõöä') lasts.push(value)
      } else {
        if (last && lasts.length) {
          if (!entities[last]) {
            if (!ENTITIES.includes(last)) logger.notice({unknown:last,hostname,port})
            entities[last] = []
          }
          const joined = lasts.join(' ')
          if (!entities[last].includes(joined)) entities[last].push(joined)
        }
        last = null
        lasts = []
        if (value !== 'üõöä') lasts.push(value)
      }
      last = key

    } else {
      if (last && lasts.length){
        if (!entities[last]) {
          if (!ENTITIES.includes(last)) logger.notice({unknown:last,hostname,port})
          entities[last] = []
        }
        const joined = lasts.join(' ')
        if (!entities[last].includes(joined)) entities[last].push(joined)
      }
      last = null
      lasts = []
    }
  }
	return entities;

}
