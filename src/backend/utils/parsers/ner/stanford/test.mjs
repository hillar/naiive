
import { readFileSync } from "fs";
import { StanfordNER } from "./stanford-ner.mjs"

(async () => {
  const inputfile = "../stanford/9505040.txt";
  const txt = 'sample textwith some/B-ABC entitites like: Tallinn, Estonia \n Hillar Aarelaid hillar@aarelaid.net'
  //const txt = readFileSync(inputfile, { encoding: "utf-8" });
  const confidence = 10;
  const hostname = "localhost";
  const port = 9997;

  try {
    const entities = await StanfordNER(txt, [{ hostname, port }]);
    console.log("---------------");
    console.dir(entities);
    console.dir(Object.keys(entities.entities))
  } catch (e) {
    console.error(e);
  }
})();
