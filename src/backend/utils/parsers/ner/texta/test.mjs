
import { readFileSync } from "fs";
import { TextaMLP } from "./texta-mlp.mjs"

(async () => {
  const inputfile = "../stanford/9505040.txt";
  //const txt = 'sample text with some entitites like: Tallinn, Estonia \n Hillar Aarelaid hillar@aarelaid.net'
  const txt = readFileSync(inputfile, { encoding: "utf-8" });
  const hostname = "texta-mlp.lab";
  const port = 5000;

  try {
    const entities = await TextaMLP(txt, 'en',[{ hostname, port }]);
    console.log("---------------");
    console.dir(entities);
    console.dir(Object.keys(entities.entities))
  } catch (e) {
    console.error(e);
  }
})();
