import { Logger } from "../../../../classes/base/logger.mjs";
import { getRandomI } from "../../../var.mjs"
import { request } from "http";
import { ENTITIES, SHORT  } from '../entitie-names.mjs'

const PATH = "/mlp";
const MODEL = 'en';


export async function TextaMLP(text, servers, logger) {
  const i = getRandomI(servers.length - 1);
  const hostname = servers[i].hostname;
  const port = servers[i].port;
  const r = await _TextaMLP(text, hostname, port, (logger = new Logger()));
  if (r === false) {
    if (logger) logger.warning({ Texta: { hostname, port, status: "down" } });
    if (servers.length > 1) {
      const tmp = [...servers];
      tmp.splice(i, 1);
      return TextaMLP(text, tmp, logger);
    } else {
      logger.emerg("no live Texta servers");
    }
  }
  return r;
}

function _TextaMLP(text, hostname, port, logger) {
  return new Promise(resolve => {
    const postData = text //JSON.stringify({text,model});

    const options = {
      hostname,
      port,
      path: PATH,
      method: "POST",
      headers: {
        "Output-Type": "facts",
        "Content-Type": "text/plain",
        "Content-Length": Buffer.byteLength(postData)
      }
    };

    const start = process.hrtime.bigint();
    const req = request(options, res => {
      if (res.statusCode !== 200) {
        if (res.statusCode === 500) logger.crit({ hostname, port, status: res.statusCode, headers: res.headers })
        else logger.warning({ hostname, port, status: res.statusCode, headers: res.headers });
        resolve(false);
      }
      const chunks = [];
      res.on("data", chunk => {
        chunks.push(chunk);
      });
      res.on("end", () => {
        const took = Number(process.hrtime.bigint() - start) / 1e6;
        let data;
        try {
          data = JSON.parse(Buffer.concat(chunks).toString());
          //console.log(JSON.string(_data,null,4))
          //data = _data.texta_facts
        } catch (error) {
          logger.err({ hostname, port, fault: data });
          resolve(false);
        }
        if (data) {
          if (Object.keys(data).length) {
            const entities = {};
            for (const _key of Object.keys(data)){
              const key = SHORT[_key] ? SHORT[_key] : _key
              entities[key] = data[_key]
              if (_key=== 'EML') {
                entities[key] =entities[key].map(s=>s.replace(/ /g,''))
              }
              if (!ENTITIES.includes(key)) logger.notice({unknown:key,hostname,port})
            }
            /*
            for (const result of data) {
              if (result.fact && result.str_val) {
                const key = SHORT[result.fact] ? SHORT[result.fact] : result.fact
                if (!entities[key]) {
                  entities[key] = []
                  if (!ENTITIES.includes(key)) logger.notice({unknown:key,hostname,port})
                }
                if (!entities[key].includes(result.str_val)) entities[key].push(result.str_val.replace(/\n/,' '))
                //entities.push(tmp);
              } else {
                logger.info({ hostname, port, message: "not result", result });
              }
            }
            */
            resolve({ entities, took, size: Buffer.byteLength(text), total:data.length });
          } else {
            logger.err({ hostname, port, message: "unknown response", data });
            resolve(false);
          }
        } else {
          logger.err({ hostname, port, message: "no data" });
          resolve(false);
        }
      });
    });

    req.on("error", e => {
      logger.warning({ hostname, port, message: e.message });
      resolve(false);
    });
    req.write(postData);
    req.end();
  });
}
