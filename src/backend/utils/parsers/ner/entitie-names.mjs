export const ENTITIES = [
  "LOCATION",
  "PERSON",
  "ORGANIZATION",
  "MONEY",
  "PERCENT",
  "DATE",
  "TIME",
  "MISCELLANEOUS",
  "EVENT",
  "PRODUCT",
  "LANGUAGE",
  "EMAIL",
  "PHONE",
  "BITCOIN",
  "CREDITCARD",
  "ESTID",
  "HASH",
  "IBAN",
  "IPV4",
  "IPV6",
  "URI"

]
// normalize 'aliases'
export const SHORT = { 'MISC':'MISCELLANEOUS',
                'PER':'PERSON',
                'ORG':'ORGANIZATION',
                'LOC':'LOCATION',
                'GEO':'LOCATION',
                'EML':'EMAIL',
                'ID_CODE': 'ESTID'
              }

/*

[
      'DATE',      'ORG',
       'LAW',   'PERSON',
       'GPE',  'PERCENT',
  'CARDINAL',     'NORP',
  'LANGUAGE',  'PRODUCT',
   'ORDINAL', 'QUANTITY',
       'FAC',    'EVENT'
]

*/
