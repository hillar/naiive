import { accessSync, constants } from 'fs'

export function exists(filename) {
  try {
    accessSync(filename, constants.F_OK);
    return true;
  } catch (e) {
    return false;
  }
}
