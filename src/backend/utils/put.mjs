import { createReadStream } from 'fs'
import { request, globalAgent, STATUS_CODES } from 'http'
import { kStringMaxLength as STRINGMAXLENGTH} from 'buffer'
import { Notice, Warn, Err, Crit } from '../classes/base/event-levels.mjs'

const REQUESTTIMEOUT = 30000 // network.http.pipelining.read-timeout


// TODO debug globalagent not reusing sockets
//globalAgent.keepAlive = true
//globalAgent.maxSockets = 64
//globalAgent.keepAliveMsecs = 1000 * 30


// httpAgent: new http.Agent({ keepAlive: true, keepAliveMsecs: 5000, maxSockets: 40 }),

/*

http.globalAgent.maxSockets = 10000;

// http.globalAgent.maxSockets = Number.POSITIVE_INFINITY;
const request = () => new Promise((resolve, reject) => {
  http.get('http://localhost:8080', (res) => {
    let buffer = '';
    res.setEncoding('utf-8');
    res.on('readable', (chunk) => {
      buffer += res.read();
    });
    res.on('end', () => {
      resolve(buffer);
    });
    res.on('error', (e) => {
      reject(e);
    });
  });
});

*/


/*

resolve answer || error => done || do not try again that
reject error => try again later

see dynpipe.mjs ask loop

*/



export function putfile(pathname, hostname, port, path ='/' ,timeout = REQUESTTIMEOUT){
  return new Promise( async(resolve,reject)=>{

    let rs = createReadStream(pathname)

    const options = {
      hostname,
      port,
      //timeout,
      path,
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'connection': 'keep-alive',
      }
    }

    const req = request(options, (res) => {
      if (res.statusCode > 299 && res.statusCode < 500) {
        // we did someting wrong ;(
        resolve(new Crit(STATUS_CODES[res.statusCode],{statuscode: res.statusCode, hostname,port}))
      } else if (res.statusCode > 499) {
         // the server is aware that it has encountered an error
         resolve(new Notice(STATUS_CODES[res.statusCode], { statuscode: res.statusCode,hostname, port }));
      } else {
        let body = []
        let size = 0
        let endevent = 'normal'
        res.on('timeout', () => {
          endevent = 'timeout'
          resolve(new Notice('Response timeout',{hostname,port,pathname}))
        })
        res.on('aborted', () => {
          endevent = 'aborted'
          resolve(new Notice('Response aborted',{hostname,port,pathname}))
        })
        res.on('error', (error) => {
          endevent = 'error'
          error.pathname = pathname
          resolve(new Notice(error))
        })
        res.on('data', (chunk) => {
           body.push(chunk)
           size += chunk.length
           if (size > STRINGMAXLENGTH) {
             resolve(new Err("Response to big", { hostname, port, pathname }))
             rs.unpipe(req)
             req.abort()
           }
        })
        res.on('end', () => {
          if (endevent!== 'normal') return
          let answer
          const str = Buffer.concat(body).toString()
          const took = Number(process.hrtime.bigint() - start)/1e+6;
          try {
            answer = JSON.parse(str)
            resolve({took,answer})
          } catch (error) {
            if (str.trim().length) {
              resolve(new Warn('Bad response',{hostname,port,pathname}))
            } else {
              if (res.statusCode === 204) resolve({took,answer:{}})
              else resolve(new Warn('Empty response',{hostname,port,pathname,statuscode:res.statusCode}))
            }
          }
        })
      }
    })

    req.on('error', (error) => {
      rs.unpipe(req)
      error.pathname = pathname
      resolve(new Notice(error))
    })
    req.on('timeout', () => {
      rs.unpipe(req)
      req.abort()
      resolve( new Notice('Request timeout',{hostname,port,pathname}))
    })

    //req.setSocketKeepAlive(true,1000)

    req.on('socket',(socket )=>{
      socket.setKeepAlive(true,1000)
      /*
      // even keepalive probes are ok, it will still be 'inactivity' ;(
      socket.setTimeout(REQUESTTIMEOUT, ()=>{
        socket.end();
        req.emit('timeout')
      });
      */
    })

    let stotal = 0
    rs.on('data', chunk => {
      stotal += chunk.length
    })

    rs.on('error', (error) => {
        rs.unpipe(req)
        req.abort()
        resolve(new Err(error)) // it is 'local' issue, not server fault
      })

    const start = process.hrtime.bigint();

    rs.pipe(req)
  })
}

export function putstr(str, hostname, port, path = '/',timeout = REQUESTTIMEOUT) {
  return _postputstr(str, hostname, port, path, timeout, 'PUT')
}

export function poststr(str, hostname, port, path = '/',timeout = REQUESTTIMEOUT) {
  return _postputstr(str, hostname, port, path, timeout, 'POST')
}

function _postputstr(str, hostname, port, path, timeout, method) {
  let type
  if (typeof str === 'string' || str instanceof String) {
    if (str.startsWith('{')  && (str.endsWith('}\n') || str.endsWith('}'))) type = 'application/json; charset=utf-8'
    else type = 'text/plain; charset=utf-8'
  } else {
    str = JSON.stringify(str)
    type = 'application/json; charset=utf-8'
  }
  const buff = Buffer.from(str)
  return new Promise(async (resolve, reject) => {
    const options = {
      hostname,
      port,
      path,
      //timeout,
      method,
      headers: {
        'connection': 'keep-alive',
        Accept: "application/json",
        'content-length' : buff.length,
        'content-type' : type
      }

    };
    // custom long timeout
    let longrun
    let reportprogress = false
    if (timeout > REQUESTTIMEOUT) {
      longrun = setTimeout(()=>{
          reportprogress = true
      },REQUESTTIMEOUT)
    }

    const req = request(options, (res) => {

      if (res.statusCode > 299 && res.statusCode < 500) {
        // we did someting wrong ;(
        resolve(new Crit(STATUS_CODES[res.statusCode],{statuscode: res.statusCode, message:res.statusMessage, hostname,port}))
      } else if (res.statusCode > 499) {
         // server is aware that it is on error
         resolve(new Notice(STATUS_CODES[res.statusCode], { statuscode: res.statusCode,message:res.statusMessage,hostname, port }));
      } else {
        let size = 0
        let body = [];
        let endevent = "normal";
        res.on("timeout", () => {
          endevent = "timeout";
          const error = new Warn("Response timeout", { hostname, port });
          resolve(new Notice("Response timeout", { hostname, port }));
        });
        res.on("aborted", () => {
          endevent = "aborted";
          resolve(new Notice("Response aborted", { hostname, port }));
        });
        res.on("error", (error) => {
          endevent = "error";
          resolve(new Notice(error));
        });
        res.on("data", (chunk) => {

          body.push(chunk);
          size += chunk.length
          if (size > STRINGMAXLENGTH) {
            resolve(new Err("Response to big", { hostname, port }))
            req.abort()
          }
        });
        res.on("end", () => {
          if (longrun){
            clearTimeout(longrun)
          }
          if (endevent !== "normal") return;
          const took = Number(process.hrtime.bigint() - start) / 1e6;
          let answer;
          const str = Buffer.concat(body).toString();

          try {
            answer = JSON.parse(str);
            resolve({answer,took});
          } catch (error) {
            if (str.trim().length) {
               resolve(new Warn("Bad response", { hostname, port }))
            } else {
              if (res.statusCode === 204) resolve({took,answer:{}})
              else resolve(new Warn("Empty response", { hostname, port }))
            }
          }
        })
      }
    })

    req.on("error", (error) => {
      resolve(new Notice(error));
    })

    req.on("timeout", () => {
      req.abort();
      resolve(new Notice("Request timeout", { hostname, port }));
    })

    req.on('connect',(res, socket, head)=>{
      socket.setKeepAlive(true,1000)
      /*
      socket.setTimeout(REQUESTTIMEOUT);
      socket.on('timeout', () => {
        console.log('socket timeout');
        socket.end();
        req.emit('timeout')
      });
      */
    })

    const start = process.hrtime.bigint();
    req.end(buff);
  })
}

export function get(hostname, port, path = '/',timeout = REQUESTTIMEOUT) {
  return new Promise(async (resolve, reject) => {
    const options = {
      hostname,
      port,
      path,
      timeout,
      method: "GET",
      headers: {
        'connection': 'keep-alive',
        Accept: "application/json",
      }
    };

    const req = request(options, (res) => {
      if (res.statusCode > 299 && res.statusCode < 500) {
        // we did someting wrong ;(
        resolve(new Crit(STATUS_CODES[res.statusCode],{hostname,port}))
      } else {
        let size = 0
        let body = [];
        let endevent = "normal";
        res.on("timeout", () => {
          endevent = "timeout";
          const error = new Warn("Response timeout", { hostname, port });
          resolve(new Err("Response timeout", { hostname, port }));
        });
        res.on("aborted", () => {
          endevent = "aborted";
          resolve(new Err("Response aborted", { hostname, port }));
        });
        res.on("error", (error) => {
          endevent = "error";
          resolve(new Err(error));
        });
        res.on("data", (chunk) => {
          body.push(chunk);
          size += chunk.length
          if (size > STRINGMAXLENGTH) {
            resolve(new Notice("Response to big", { hostname, port }))
            req.abort()
          }
        });
        res.on("end", () => {
          if (endevent !== "normal") return;
          let answer;
          const str = Buffer.concat(body).toString();
          const took = Number(process.hrtime.bigint() - start) / 1e6;
          try {
            answer = JSON.parse(str);
            resolve({answer,took});
          } catch (error) {
            if (str.trim().length) {
              resolve(new Err("Bad response", { hostname, port }))
            } else {
              if (res.statusCode === 204) resolve({took,answer:{}})
              else resolve(new Warn('Empty response',{hostname,port,pathname,h:res.statusCode}))
            }
          }
        })
      }
    })

    req.on("error", (error) => {
      resolve(new Err(error));
    })

    req.on("timeout", () => {
      req.abort();
      resolve(new Warn("Request timeout", { hostname, port }));
    })

    const start = process.hrtime.bigint();
    req.end();
  })
}

/*

(async function() {
  try {
    // const r = await putfile('put.mjs','localhost',9998,'/2:kxVkrYfBfLpaOKRvGckzLpaOKRvG6fKf0WF9L79:kHkspf9aO2vGce9aO2vGbf0WF9N&24:9vqSEytc1sp2KdkIs7uUMqXx6wJuyBK/H2urmt2BjzBN8Fl1tPn:9tiWhUMUV5qmQBvH8FTFn')

    //const r = await get('localhost',9997,'/2:kxVkrYfBfLpaOKRvGckzLpaOKRvG6fKf0WF9L79:kHkspf9aO2vGce9aO2vGbf0WF9N&24:9vqSEytc1sp2KdkIs7uUMqXx6wJuyBK/H2urmt2BjzBN8Fl1tPn:9tiWhUMUV5qmQBvH8FTFn')
    const s = '{"index:{}"}\n{"kala":"maja"}\n'
    const r = await putstr(s,'localhost',9200,'/deleteme/_bulk')
    console.dir({r})
    console.dir(r.answer)
  } catch (e){
    console.dir({e})
  }

}());

*/
