
import { readFileSync } from "fs";
import { EntitieRecognizer } from "./er.mjs"

(async () => {
  const inputfile = "./test-er.mjs";
  //const txt = 'sample text with some entitites like: Tallinn, Estonia \n Hillar Aarelaid hillar@aarelaid.net'
  const txt = readFileSync(inputfile, { encoding: "utf-8" });
  const hostname = 'staging-ain' //"localhost";
  const port = 9980;

  try {
    const entities = await EntitieRecognizer(txt, 'en',[{ hostname, port }]);
    console.log("---------------");
    console.dir(entities);
    console.dir(Object.keys(entities.entities))
  } catch (e) {
    console.error(e);
  }
})();
