import { get as gets } from 'https'
import { createWriteStream, unlinkSync } from 'fs'

export async function download(url,fn,hash,alg){
  return new Promise( resolve => {
    if (exists(fn)) resolve(true)
    let file = createWriteStream(fn)
    file.on('error', error => {
      try {
        unlinkSync(fn)
      } catch (e) {
        console.error(e)
      }
      resolve(error)
    })
    try {
    // TODO add plain http
    const request = gets(url, (response) => {
      if (response.statusCode === 200) {
        response.pipe(file)
        response.on('end', () => {
          // TODO check hash with alg after download
          console.dir({url,fn})
          resolve(true)
        })
        response.on('error', (error) => {
          try {
              unlinkSync(fn)
          } catch (e) {
            console.error(e)
          }
          resolve(error)
        })
      } else {
        resolve(new Error('server error '+ response.statusCode + ' ' + url))
      }
    })
    request.on('error', (error) => {
      try {
          unlinkSync(fn)
      } catch (e) {
        console.error(e)
      }
      resolve(error)
    })
  } catch (e) {
    resolve(e)
  }
  })
}
