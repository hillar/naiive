
import { createHash } from 'crypto'
import { createReadStream } from 'fs'


/*

$ uname
Linux


$ nodejs -v
v11.5.0


## Algorithms supported

> console.log(require('crypto').getHashes())
[ 'RSA-MD4',
  'RSA-MD5',
  'RSA-MDC2',
  'RSA-RIPEMD160',
  'RSA-SHA1',
  'RSA-SHA1-2',
  'RSA-SHA224',
  'RSA-SHA256',
  'RSA-SHA384',
  'RSA-SHA512',
  'blake2b512',
  'blake2s256',
  'md4',
  'md4WithRSAEncryption',
  'md5',
  'md5-sha1',
  'md5WithRSAEncryption',
  'mdc2',
  'mdc2WithRSA',
  'ripemd',
  'ripemd160',
  'ripemd160WithRSA',
  'rmd160',
  'sha1',
  'sha1WithRSAEncryption',
  'sha224',
  'sha224WithRSAEncryption',
  'sha256',
  'sha256WithRSAEncryption',
  'sha384',
  'sha384WithRSAEncryption',
  'sha512',
  'sha512WithRSAEncryption',
  'ssl3-md5',
  'ssl3-sha1',
  'whirlpool' ]

## Character encodings

'ascii' - For 7-bit ASCII data only. This encoding is fast and will strip the high bit if set.

'utf8' - Multibyte encoded Unicode characters. Many web pages and other document formats use UTF-8.

'utf16le' - 2 or 4 bytes, little-endian encoded Unicode characters. Surrogate pairs (U+10000 to U+10FFFF) are supported.

'ucs2' - Alias of 'utf16le'.

'base64' - Base64 encoding. When creating a Buffer from a string, this encoding will also correctly accept "URL and Filename Safe Alphabet" as specified in RFC 4648, Section 5.

'latin1' - A way of encoding the Buffer into a one-byte encoded string (as defined by the IANA in RFC 1345, page 63, to be the Latin-1 supplement block and C0/C1 control codes).

'binary' - Alias for 'latin1'.

'hex' - Encode each byte as two hexadecimal characters.
*/

export const DEFAULTALGORITHM = 'md5'

export function digestFile( path, algorithm = DEFAULTALGORITHM, encoding = 'hex' ) {
  return new Promise( resolve =>
    createReadStream(path)
      .on('error', e => {
        resolve()
      })
      .pipe(createHash(algorithm)
      .setEncoding(encoding))
      .once('finish',  function() {
        resolve( this.read() )
      })
  )
}

export function digestString(str, algorithm = DEFAULTALGORITHM, encoding = 'hex') {
  return createHash(algorithm).update(str).digest(encoding)
}

/*
let algs = require('crypto').getHashes()
let lengths = {}
for (const alg of algs) {
    if (alg.includes('With')) continue
    const tmp = require('crypto').createHash(alg).update('kala').digest('hex')
    //console.dir({alg,l:tmp.length})
    lengths[alg] = tmp.length
}
console.log(JSON.stringify(lengths,null,4))

*/

export const SUPPORTED = {
    "RSA-MD4": 32,
    "RSA-MD5": 32,
    "RSA-MDC2": 32,
    "RSA-RIPEMD160": 40,
    "RSA-SHA1": 40,
    "RSA-SHA1-2": 40,
    "RSA-SHA224": 56,
    "RSA-SHA256": 64,
    "RSA-SHA3-224": 56,
    "RSA-SHA3-256": 64,
    "RSA-SHA3-384": 96,
    "RSA-SHA3-512": 128,
    "RSA-SHA384": 96,
    "RSA-SHA512": 128,
    "RSA-SHA512/224": 56,
    "RSA-SHA512/256": 64,
    "RSA-SM3": 64,
    "blake2b512": 128,
    "blake2s256": 64,
    "md4": 32,
    "md5": 32,
    "md5-sha1": 72,
    "mdc2": 32,
    "ripemd": 40,
    "ripemd160": 40,
    "rmd160": 40,
    "sha1": 40,
    "sha224": 56,
    "sha256": 64,
    "sha3-224": 56,
    "sha3-256": 64,
    "sha3-384": 96,
    "sha3-512": 128,
    "sha384": 96,
    "sha512": 128,
    "sha512-224": 56,
    "sha512-256": 64,
    "shake128": 32,
    "shake256": 64,
    "sm3": 64,
    "ssl3-md5": 32,
    "ssl3-sha1": 40,
    "whirlpool": 128
}

export function isHash(str,algorithm = DEFAULTALGORITHM){
  if (!SUPPORTED[algorithm]) throw new Error('not supported '+ algorithm)
  //return (/[a-fA-F0-9]{SUPPORTED[algorithm]}/).test(str);
  if (str.length !== SUPPORTED[algorithm]) return false
  else return (/[a-fA-F0-9]/).test(str)
}
