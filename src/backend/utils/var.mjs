import { Socket as netSocket} from 'net'
import { accessSync,  constants } from 'fs'
import { HASHFUNCTION as HASHALG } from '../constants/files.mjs'


export function isString(s) {
  return Object.prototype.toString.call( s ) === '[object String]'
}


export function isFunction(fn) {
  return (Object.prototype.toString.call(fn) === '[object Function]')
}

export function isAsyncFunction(fn) {
  return (Object.prototype.toString.call(fn) === '[object AsyncFunction]')
}

export function isFn(fn) {
  return (Object.prototype.toString.call(fn) === '[object Function]' || Object.prototype.toString.call(fn) === '[object AsyncFunction]')
}


export function isObject (o) {
  return (Object.prototype.toString.call(o) === '[object Object]')
}


export function isHash(hash, alg = HASHALG) {
  switch (alg) {
    case "md5":
      return /^[a-fA-F0-9]{32}$/.test(hash);
      break;
    case "sha256":
      return /^[a-fA-F0-9]{64}$/.test(hash);
      break;
    default:
      console.error('not supported: '+alg)
      process.exit(-1)
      return false;
  }
}

export function isReadable(name){
  try {
    accessSync(name, constants.R_OK);
    return true
  } catch (error) {
    return false
  }
}

/*
Returns a name property of the Object constructor function **that created the instance object**.
*/

export function creatorName(that) {
  return Object.getPrototypeOf(that).constructor.name
}

/*
? standardised way to tell the type of a javascript object
*/

export function objectType(that) {
  return Object.prototype.toString.call(that)
}

export function is(that) {
  return [Object.getPrototypeOf(that).constructor.name,Object.prototype.toString.call(that)].join(':')
}

export function parseHostnamePort(hostnameport,defaultport){
    const hostname = hostnameport.split(':')[0].trim()
    if (!hostname) throw new Error('no hostname')
    let port
    try {
      port = hostnameport.split(':')[1]
    } catch (e) {
      if (defaultport) port = ''+defaultport
      else throw new Error('no port')
    }
    if (!port && defaultport) port = ''+defaultport
    port = Number(port)
    if (!port) throw new Error('port not number')
    if (port > 65535) throw new Error('port should be >= 0 and < 65536')
    return { hostname, port }
}

export function probePort(address, port, timeout = 500) {
  return new Promise( resolve => {
    const s = new netSocket()
      s.connect(port, address, function() {
          s.destroy()
          resolve(true)
      })
      s.on('error', function(e) {
          s.destroy()
          resolve(false)
      })
      s.setTimeout(timeout, function() {
          s.destroy()
          resolve(false)
      })
  })
}

export function trimArrayOfStrings(a) {
    return [ ...( new Set( a.map( s => ( s.trim().length > 0 ) ? s.trim() : undefined ) ) ) ].filter( i => (!(i === undefined)) )
}

export function getRandomI(max) {
  return Math.floor(Math.random() * (max + 1));
}
