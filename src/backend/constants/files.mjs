import { FILES } from './ports.mjs'
export const PORT = FILES
export const PATH = '/files'
export const SEPARATOR = '‾'
export const HASHFUNCTION = 'md5'
export const FILESPATH = 'files'
