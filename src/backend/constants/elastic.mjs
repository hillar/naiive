/*

https://github.com/elastic/elasticsearch/blob/7b44743ccc2402d79e347882dc74623e04883488/server/src/main/java/org/elasticsearch/index/search/QueryParserHelper.java#L133

if (acceptMetadataField == false && fieldType.name().startsWith("_")) {
                // Ignore metadata fields
                continue;
            }

*/

export const IDFIELD = '__id__'
export const HASHFIELD = '__contenthash__' //'journal.ondisk.contentHash'
export const FILENAMEFIELD = '__filename__'
export const MIMETYPEFIELD = '__mimetype__'
export const MIMEHUMANFIELD = '__mimehuman__'
export const SSDEEPFIELDPREFIX = '__ssdeep_'
export const CONTENTFIELD = 'content'
export const CONTENTINDEX = 'content'
export const ENTITYINDEX = 'entities'
export const TIMELINEINDEX = 'timeline'
export const FILESINDEX = 'files'
export const METAINDEX = 'meta'
export const TIMEINDEX = 'time'
export const LINEINDEX = 'line'
export const ALLINDEX = 'denormalized'
export const MAXPREVIEWSIZE = 1024 * 10
export const SEPARATOR = '￣'
export const indexName = (prefix,middle,suffix) => {
  let name = ''
  if (prefix) name += prefix + SEPARATOR
  if (middle) name += middle
  if (suffix) name += SEPARATOR+suffix
  if (name.length === 0) {
    console.error('cant make index name')
    throw new Error('no prefix,middle,suffix. Index name can not be empty')
 }
  return encodeURIComponent(name.toLowerCase())
}
// TODO ticket elasticsearch to get supported langueges list from API
export const LANGUAGES = ['ARABIC', 'ARMENIAN', 'BASQUE', 'BENGALI', 'BRAZILIAN', 'BULGARIAN', 'CATALAN', 'CJK', 'CZECH', 'DANISH', 'DUTCH', 'ENGLISH', 'ESTONIAN', 'FINNISH', 'FRENCH', 'GALICIAN', 'GERMAN', 'GREEK', 'HINDI', 'HUNGARIAN', 'INDONESIAN', 'IRISH', 'ITALIAN', 'LATVIAN', 'LITHUANIAN', 'NORWEGIAN', 'PERSIAN', 'PORTUGUESE', 'ROMANIAN', 'RUSSIAN', 'SORANI', 'SPANISH', 'SWEDISH', 'TURKISH', 'THAI']
