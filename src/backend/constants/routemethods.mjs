export const ROUTEMETHODS = [
	'head',
	'get',
	'post',
	'put',
	'patch',
	'delete'
]
