import { createSimpleCli } from "../../classes/base/simplecli.mjs";
import {
  HASHFIELD,
  CONTENTINDEX,
  FILESINDEX,
  METAINDEX,
  ENTITYINDEX,
  MAXPREVIEWSIZE,
  indexName
} from "../../constants/elastic.mjs";
import {
  PORT as FILESSERVERPORT,
  SEPARATOR as PORTSEPARATOR
} from "../../constants/files.mjs";
import {
  elasticSimpleQuery,
  elasticQuery,
  getDoc
} from "../../utils/elastic.mjs";
import { parseHostnamePort } from "../../utils/var.mjs";
import { hostname } from "os";
import { resolve } from "path";
import { readdirSync, accessSync, constants } from "fs";
import { processDir } from "../../utils/postprocess-er.mjs";

function printProgress(progress) {
  process.stdout.clearLine();
  process.stdout.cursorTo(0);
  process.stdout.write("" + progress);
}

const glob = createSimpleCli({
  opts: {
    filespath: p => {
      const d = p ? resolve(p) : resolve("");
      try {
        accessSync(d, constants.R_OK);
      } catch (error) {
        glob.log.emerg({ notreadable: d });
        process.exit(-1);
      }
      return d;
    },
    tikas: list => {
      return list && list.split
        ? list.split(",").map(hp => parseHostnamePort(hp, 9998))
        : [{ hostname: "localhost", port: 9998 }];
    },
    elastics: list => {
      return list && list.split
        ? list.split(",").map(hp => parseHostnamePort(hp, 9200))
        : [{ hostname: "localhost", port: 9200 }];
    },
    ers: list => {
      return list && list.split
        ? list.split(",").map(hp => parseHostnamePort(hp, 9980))
        : [{ hostname: "localhost", port: 9980 }];
    },
    prefix: p => {
      return p ? p.toLowerCase() : "";
    },
    // https://www.elastic.co/guide/en/elasticsearch/reference/current/breaking-changes-7.0.html#_literal_literal_is_no_longer_allowed_in_index_name
    // : is no longer allowed in index name
    suffix: p => {
      return p
        ? p.toLowerCase()
        : hostname()
            .split(".")
            .shift() +
            PORTSEPARATOR +
            FILESSERVERPORT;
    },
    content: p => {
      return p ? p.toLowerCase() : CONTENTINDEX;
    },
    field: p => {
      return p ? p : HASHFIELD;
    }, // <- case sensitive !
    files: p => {
      return p ? p.toLowerCase() : FILESINDEX;
    },
    entities: p => {
      return p ? p.toLowerCase() : ENTITYINDEX;
    },
    meta: p => {
      return p ? p.toLowerCase() : METAINDEX;
    }
  },
  fn: async logger => {
    logger.info({ startinwith: glob.defaults });
    let count = 0;
    //try {
    for (const dir of readdirSync(glob.filespath, { withFileTypes: true })) {
      if (dir.isDirectory()) {
        //console.dir(dir)
        count++;
        //if (((count % 10) === 0) && process.stdout.isTTY ) printProgress(count)
        printProgress(count + "  " + dir.name);
        await processDir(
          dir.name,
          glob.filespath,
          glob.tikas,
          glob.elastics,
          glob.ers,
          glob.prefix,
          glob.suffix,
          glob.content,
          glob.field,
          glob.files,
          glob.entities,
          glob.meta,
          logger
        );
      }
    }
    /*} catch (error) {
      console.dir(error)
      logger.emerg('readdirSync',error)
    }
    */
  }
});

glob.run();
