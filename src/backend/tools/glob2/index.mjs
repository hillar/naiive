import { createSimpleCli } from "../../classes/base/simplecli.mjs";

import { opts } from "../../servers/watch2/opts.mjs"
import { runPipeline } from "../../servers/watch2/pipeline.mjs"

import { readdirSync, accessSync, constants } from "fs"
import {  Readable } from 'stream'
import { resolve as host } from 'dns'
import { resolve as absolute } from 'path'


function name2ip(name){
  return new Promise( resolve => {
    host(name, (err, addresses) => {
      if (err) resolve(name)
      else {
        resolve(addresses[0])
      }
    } )
  })
}

function* dirReader(dirname){
  for (const basename of readdirSync(dirname)) {
    yield {basename:encodeURIComponent(basename)}
  }
}

const state = {}

const glob = createSimpleCli({
  opts,
  fn: async (logger) => {
    logger.info({ startinwith: glob.defaults });
    // resolve names to ip's
    const resolved = glob.defaults
    for (const key of Object.keys(resolved)){
      if (Array.isArray(resolved[key]) && resolved[key][0] && resolved[key][0].hostname ) {
        for (const i in resolved[key])  {
          resolved[key][i].hostname = await name2ip(resolved[key][i].hostname)
        }
      }
    }
    logger.info({ resolved });
    const readable = Readable.from(dirReader(glob.filespath), {objectMode: true,highWaterMark:1});
    const result = await runPipeline(state,logger, readable, resolved)
  }
});

glob.run();
