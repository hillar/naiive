import 'net';
import { accessSync, constants, lstatSync, readlinkSync, realpathSync as realpathSync$1, statSync, createReadStream, readdirSync, readFileSync as readFileSync$1 } from 'fs';
import { hostname } from 'os';
import { parse, basename as basename$1, extname, dirname, join, resolve } from 'path';
import { EventEmitter } from 'events';
import { spawn } from 'child_process';
import { createHash } from 'crypto';
import { Readable, Transform, pipeline, Writable } from 'stream';
import { inspect, promisify } from 'util';
import { request, STATUS_CODES } from 'http';
import { kStringMaxLength } from 'buffer';
import { resolve as resolve$1 } from 'dns';

const FILES = 7200;
const LD = 7400;
const ER = 7500;
const INFLUXDB = 8089;
const ELASTIC = 9200;
const TIKA = 9998;
const SSDEEP = 9997;

const PORT = FILES;
const SEPARATOR$1 = '‾';
const HASHFUNCTION = 'md5';
const FILESPATH = 'files';

function isString(s) {
  return Object.prototype.toString.call( s ) === '[object String]'
}

function isAsyncFunction(fn) {
  return (Object.prototype.toString.call(fn) === '[object AsyncFunction]')
}

function isFn(fn) {
  return (Object.prototype.toString.call(fn) === '[object Function]' || Object.prototype.toString.call(fn) === '[object AsyncFunction]')
}


function isHash(hash, alg = HASHFUNCTION) {
  switch (alg) {
    case "md5":
      return /^[a-fA-F0-9]{32}$/.test(hash);
    case "sha256":
      return /^[a-fA-F0-9]{64}$/.test(hash);
    default:
      console.error('not supported: '+alg);
      process.exit(-1);
      return false;
  }
}

function isReadable(name){
  try {
    accessSync(name, constants.R_OK);
    return true
  } catch (error) {
    return false
  }
}

function parseHostnamePort(hostnameport,defaultport){
    const hostname = hostnameport.split(':')[0].trim();
    if (!hostname) throw new Error('no hostname')
    let port;
    try {
      port = hostnameport.split(':')[1];
    } catch (e) {
      if (defaultport) port = ''+defaultport;
      else throw new Error('no port')
    }
    if (!port && defaultport) port = ''+defaultport;
    port = Number(port);
    if (!port) throw new Error('port not number')
    if (port > 65535) throw new Error('port should be >= 0 and < 65536')
    return { hostname, port }
}

function trimArrayOfStrings(a) {
    return [ ...( new Set( a.map( s => ( s.trim().length > 0 ) ? s.trim() : undefined ) ) ) ].filter( i => (!(i === undefined)) )
}

class NotImplementedError extends Error {
  constructor ( ...args ) {
    super ( ...args );
  }
}

class CoerseError extends Error {
  constructor ( ...args ) {
    super ( ...args );
  }
}

class settable {

  constructor( ...settings) {
    let kala = [...settings];
    // convert sinlge object to array 
    if (settings.length === 1 && Object.keys(settings[0]).length > 1 ) {
      kala = [];
      for (const key of Object.keys(settings[0])) {
        const tmp = {};
        tmp[key] = settings[0][key];
        kala.push(tmp);
      }
    }
    for (const setting of kala) {

      // there has to be name, setting can not be anonymous
      let name = Object.keys(setting);
      if (!name[0]) throw new Error(this.prototypeof + ' no name for setting')
      name = name[0];


      /*
      The coercion function should accept one argument,
      and should return always some value
      or throw an error.
      If the function throws, the error will be treated as
      a kinda validation failure and CoerseError is thrown

      { hostname: (v) => { return v.trim().length > 0 ? v.toLowerCase() : 'localhost'} }

      { port: (v) => {
          const port = Number(v)
          if (!port) throw new Error('port not number')
          if (port > 65535 || port < 0) throw new Error('port should be >= 0 and < 65536')}
          return port
      }


      If just value is passed, then typeof is used for coersion function
      and value itself as default

      { hostname: 'localhost' }
      { port: 9200 }
      { verbose: true}

      */
      let fn = setting[name];
      let _default;
      try {
        _default = fn();
      } catch (e) {
        //console.log('catch',Object.getPrototypeOf( e ).constructor.name, this.prototypeof, n)
        if (e instanceof ReferenceError || e instanceof SyntaxError ) {
          console.error(Object.getPrototypeOf( e ).constructor.name, this.prototypeof, name);
          console.error(e);
          process.exit(-1);
        } else if (e.message === 'fn is not a function') {
          _default = fn;
          switch (typeof fn) {
            case 'string':
              fn = String;
              break
            case 'number':
              fn = Number;
              break
            case 'boolean':
              fn = Boolean;
              break
            default:
              throw new NotImplementedError(this.prototypeof + '.' + name + ' ' + typeof fn + ' default coerse function is not implemented (yet)')
          }
        } else throw new CoerseError(this.prototypeof + '.' + name + ' : '+ e.message)
      }
      if (_default === undefined) throw new Error(this.prototypeof + '.' + name + ' : no default returned by coerse function')
      Object.defineProperty( this, '__'+name, {
        enumerable: false,
        writable: true,
        value:  _default
      });
      Object.defineProperty (Object.getPrototypeOf(this) ,name, {
        get: () => { return this['__'+name] },
        set: (v) => {
            try {
              this['__'+name] = fn(v);
            } catch (error) {
              error.prototypeof = this.prototypeof;
              error.settable = name;
              console.error(error);
              process.exit(-1);
            }
        },
        enumerable: false,
        configurable: true
      });
    }

  }

  // deprecated, please us prototypeof
  get typeof () {
    console.error(this.prototypeof+' typeof is deprecated, please us prototypeof');
    return Object.getPrototypeOf( this ).constructor.name
  }
  get prototypeof () { return Object.getPrototypeOf( this ).constructor.name}

  /*
    return all class and it parents settable names
  */
  get settings(){
  	function dig( i ) {
  		let _self = [];
			const proto = Object.getPrototypeOf( i );
			for ( const name of Object.getOwnPropertyNames( proto ) ) {
        if (! name.startsWith( '__') ) { // '__proto__' && ..
				   const desc = Object.getOwnPropertyDescriptor( proto, name );
    		   if ( desc && typeof desc.set === 'function' ) _self.push( name );
        }
			}
			const parent = Object.getPrototypeOf( i );
			if ( parent && parent.constructor.name !== 'Object' ) return [ ...new Set([ ..._self, ...dig( parent ) ])]
			else return _self
	   }
		return dig( this )
  }


  /*
    return all class settables with default values
  */
  get defaults () {
    const d = {};
    for (const name of this.settings) if (!(this[name] === undefined)) d[name] = this[name];
    return d
  }

}

const LOGMETHODS = [
	'emerg',
	'alert',
	'crit',
	'err',
	'warning',
	'notice',
	'info',
	'debug'
];

const SEVERITYERROR = [
	'emerg',
	'alert',
	'crit',
	'err'
];

const SEVERITYLOG = [
	'warning',
	'notice',
	'info'
];


function nowAsJSON() {

	const now = new Date();
	return now.toJSON()

}

class Logger {

	constructor() {

    // test if all LOGMETHODS exists here
    if ( ! ( LOGMETHODS.every( e => [ 'debug', ...SEVERITYLOG, ...SEVERITYERROR ].includes( e ) ) ) ) throw new Error( 'LOGMETHODS do not match' )


		if ( ! process.alias ) {

			let pe = parse( process.argv[ 1 ] );
			process.alias = pe.base.split('.').shift() === 'index' ? pe.dir.split( '/' ).pop() : pe.base;
			if (process.alias === 'bin') process.alias = pe.dir.split( '/' )[pe.dir.split( '/' ).length-2];
		}
		// TODO rfc5424 6.3.  STRUCTURED-DATA
		for ( const method of SEVERITYLOG ) {

			this[ method.toLowerCase() ] = ( ctx, ...msg ) => console.log( nowAsJSON(), hostname(), '[', process.alias, ':', process.pid, ']', method.toUpperCase(), ':', JSON.stringify( ctx ), ...msg );

		}
		for ( const method of SEVERITYERROR ) {

			this[ method.toLowerCase() ] = ( ctx, ...msg ) => {
				console.error( nowAsJSON(), hostname(), '[', process.alias, ':', process.pid, ']', method.toUpperCase(), ':', JSON.stringify( ctx ), ...msg );
				for (const k  of Object.keys(ctx)) {
					if (ctx[k] instanceof Error) {
						console.error(k);
						console.error(Object.getPrototypeOf( ctx[k] ).constructor.name);
						console.error(ctx[k]);
					} else if (ctx[k] instanceof Object){
						console.error(k);
						console.dir(ctx[k]);
					}
				}
				for (const m of msg) {
					if (m instanceof Error) {
						console.error(Object.getPrototypeOf( error ).constructor.name);
						console.error(m);
					} else console.error(m);
				}
			};

		}
	}
	debug( ...o){
		console.log(`--- DEBUG ${o[0].ctx} ---`);
		for (const oo of o) {
			for (const ooo of Object.keys(oo)) {
				console.log(ooo);
				console.dir(oo[ooo]);
			}
		}
		console.log('--- debug ---');
	}

}

class Base extends settable{
  #log = {}
	constructor( logger, ...opts ) {
		super( ...opts);
		if ( ! logger ) logger = new Logger();
		else if (logger instanceof Logger) ; else {
			for ( const method of LOGMETHODS ) {
				if ( !logger[ method ] || !isFn( logger[ method ] )) throw new Error( this.prototype + '  logger has no method ' + method )
			}
			//this.#log = logger
		}
		for ( const method of LOGMETHODS ) {
			this.#log[method] =  ( ...msgs ) => {
				  const tmp = {ctx:this.prototypeof};
				  for (const m  of msgs) {
						if (!m) continue
						if (isString(m)) {
							if (!tmp['msg']) tmp['msg'] = [];
							tmp['msg'].push(m);
						} else if (Object.keys(m).length ) {
							for (const k of Object.keys(m)) tmp[k] = m[k];
						}
					}
					logger[ method ](tmp );
			};
		}
	}

	// deprecated, use log()
	get logger() {	return this.#log }
	get log() {	return this.#log }

	// current values of all settings
	get config() {

		const conf = {};
		for ( const setting of this.settings ) {

			conf[ setting ] = this[ setting ];

		}
		return conf

	}

	// set config (conf) { <-- RangeError: Maximum call stack size exceeded
	// re-read the configuration
	readConfig( conf ) {

		if ( conf ) {

			//const settings = this.settings
			for ( const setting of this.settings ) {

				if ( conf[ setting ] && ( this[ setting ] !== conf[ setting ] ) ) {
					const configChanges = {};
					const tmp = this[ setting ];
					this[ setting ] = conf[ setting ];
					if (this[ setting ] !== tmp) {
						configChanges[ setting ] = { old: tmp };
						configChanges[ setting ]['new'] = this[ setting ];
						this.log.notice( { configChanges } );
					}


				}

			}

		}

	}

  /*
	ping() {

		throw new Error( 'no ping' )

	}
  */

}

/*

partial convert from es5 to es6

see https://github.com/tj/commander.js/blob/713db77d5744a9274ee3812400243ffaae3b7abc/index.js


(The MIT License)

Copyright (c) 2011 TJ Holowaychuk <tj@vision-media.ca>
Copyright (c) 2018 Hillar Aarelaid <hillar@aarelaid.net>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/



/**
 * Initialize a new `Option` with the given `flags` and `description`.
 *
 * @param {String} flags
 * @param {String} description
 * @api public
 */

class Option {
  constructor(flags, description) {
    this.flags = flags;
    this.required = flags.indexOf('<') >= 0;
    this.optional = flags.indexOf('[') >= 0;
    this.bool = flags.indexOf('-no-') === -1;
    flags = flags.split(/[ ,|]+/);
    if (flags.length > 1 && !/^[[<]/.test(flags[1])) this.short = flags.shift();
    this.long = flags.shift();
    this.description = description || '';
  }

  /**
   * Return option name.
   *
   * @return {String}
   * @api private
   */

  name() {
    return this.long
      .replace('--', '')
      .replace('no-', '');
  }

  /**
   * Return option name, in a camelcase format that can be used
   * as a object attribute key.
   *
   * @return {String}
   * @api private
   */

  attributeName() {
    return camelcase(this.name());
  }


  /**
   * Check if `arg` matches the short or long flag.
   *
   * @param {String} arg
   * @return {Boolean}
   * @api private
   */

  is(arg) {
    return this.short === arg || this.long === arg;
  }
}

/**
 * Initialize a new `Command`.
 *
 * @param {String} name
 * @api public
 */

class Command  extends EventEmitter {
  constructor(name) {
    super();
    this.commands = [];
    this.options = [];
    this._execs = {};
    this._allowUnknownOption = false;
    this._args = [];
    this._name = name || '';
  }

  /**
   * Add command `name`.
   *
   * The `.action()` callback is invoked when the
   * command `name` is specified via __ARGV__,
   * and the remaining arguments are applied to the
   * function for access.
   *
   * When the `name` is "*" an un-matched command
   * will be passed as the first arg, followed by
   * the rest of __ARGV__ remaining.
   *
   * Examples:
   *
   *      program
   *        .version('0.0.1')
   *        .option('-C, --chdir <path>', 'change the working directory')
   *        .option('-c, --config <path>', 'set config path. defaults to ./deploy.conf')
   *        .option('-T, --no-tests', 'ignore test hook')
   *
   *      program
   *        .command('setup')
   *        .description('run remote setup commands')
   *        .action(function() {
   *          console.log('setup');
   *        });
   *
   *      program
   *        .command('exec <cmd>')
   *        .description('run the given remote command')
   *        .action(function(cmd) {
   *          console.log('exec "%s"', cmd);
   *        });
   *
   *      program
   *        .command('teardown <dir> [otherDirs...]')
   *        .description('run teardown commands')
   *        .action(function(dir, otherDirs) {
   *          console.log('dir "%s"', dir);
   *          if (otherDirs) {
   *            otherDirs.forEach(function (oDir) {
   *              console.log('dir "%s"', oDir);
   *            });
   *          }
   *        });
   *
   *      program
   *        .command('*')
   *        .description('deploy the given env')
   *        .action(function(env) {
   *          console.log('deploying "%s"', env);
   *        });
   *
   *      program.parse(process.argv);
    *
   * @param {String} name
   * @param {String} [desc] for git-style sub-commands
   * @return {Command} the new command
   * @api public
   */

  command(name, desc, opts) {
    if (typeof desc === 'object' && desc !== null) {
      opts = desc;
      desc = null;
    }
    opts = opts || {};
    const args = name.split(/ +/);
    const cmd = new Command(args.shift());

    if (desc) {
      cmd.description(desc);
      this.executables = true;
      this._execs[cmd._name] = true;
      if (opts.isDefault) this.defaultExecutable = cmd._name;
    }
    cmd._noHelp = !!opts.noHelp;
    this.commands.push(cmd);
    cmd.parseExpectedArgs(args);
    cmd.parent = this;

    if (desc) return this;
    return cmd;
  }

  /**
   * Define argument syntax for the top-level command.
   *
   * @api public
   */

  arguments(desc) {
    return this.parseExpectedArgs(desc.split(/ +/));
  }

  /**
   * Add an implicit `help [cmd]` subcommand
   * which invokes `--help` for the given command.
   *
   * @api private
   */

  addImplicitHelpCommand() {
    this.command('help [cmd]', 'display help for [cmd]');
  }

  /**
   * Parse expected `args`.
   *
   * For example `["[type]"]` becomes `[{ required: false, name: 'type' }]`.
   *
   * @param {Array} args
   * @return {Command} for chaining
   * @api public
   */

  parseExpectedArgs(args) {
    if (!args.length) return;
    const self = this;
    args.forEach(arg => {
      const argDetails = {
        required: false,
        name: '',
        variadic: false
      };

      switch (arg[0]) {
        case '<':
          argDetails.required = true;
          argDetails.name = arg.slice(1, -1);
          break;
        case '[':
          argDetails.name = arg.slice(1, -1);
          break;
      }

      if (argDetails.name.length > 3 && argDetails.name.slice(-3) === '...') {
        argDetails.variadic = true;
        argDetails.name = argDetails.name.slice(0, -3);
      }
      if (argDetails.name) {
        self._args.push(argDetails);
      }
    });
    return this;
  }

  /**
   * Register callback `fn` for the command.
   *
   * Examples:
   *
   *      program
   *        .command('help')
   *        .description('display verbose help')
   *        .action(function() {
   *           // output help here
   *        });
   *
   * @param {Function} fn
   * @return {Command} for chaining
   * @api public
   */

  action(fn) {

    const self = this;
    const listener = (args, unknown) => {
      // Parse any so-far unknown options
      args = args || [];
      unknown = unknown || [];

      const parsed = self.parseOptions(unknown);

      // Output help if necessary
      outputHelpIfNecessary(self, parsed.unknown);

      // If there are still any unknown options, then we simply
      // die, unless someone asked for help, in which case we give it
      // to them, and then we die.
      if (parsed.unknown.length > 0) {
        self.unknownOption(parsed.unknown[0]);
      }

      // Leftover arguments need to be pushed back. Fixes issue #56
      if (parsed.args.length) args = parsed.args.concat(args);

      self._args.forEach((arg, i) => {
        console.log('my arg',arg,args[i]);
        if (arg.required && args[i] == null) {
          self.missingArgument(arg.name);
        } else if (arg.variadic) {
          if (i !== self._args.length - 1) {
            self.variadicArgNotLast(arg.name);
          }

          args[i] = args.splice(i);
        }
      });

      // Always append ourselves to the end of the arguments,
      // to make sure we match the number of arguments the user
      // expects
      if (self._args.length) {
        args[self._args.length] = self;
      } else {
        args.push(self);
      }

      fn.apply(self, args);
    };
    const parent = this.parent || this;
    const name = parent === this ? '*' : this._name;
    parent.on(`command:${name}`, listener);
    if (this._alias) parent.on(`command:${this._alias}`, listener);
    return this;
  }

  /**
   * Define option with `flags`, `description` and optional
   * coercion `fn`.
   *
   * The `flags` string should contain both the short and long flags,
   * separated by comma, a pipe or space. The following are all valid
   * all will output this way when `--help` is used.
   *
   *    "-p, --pepper"
   *    "-p|--pepper"
   *    "-p --pepper"
   *
   * Examples:
   *
   *     // simple boolean defaulting to false
   *     program.option('-p, --pepper', 'add pepper');
   *
   *     --pepper
   *     program.pepper
   *     // => Boolean
   *
   *     // simple boolean defaulting to true
   *     program.option('-C, --no-cheese', 'remove cheese');
   *
   *     program.cheese
   *     // => true
   *
   *     --no-cheese
   *     program.cheese
   *     // => false
   *
   *     // required argument
   *     program.option('-C, --chdir <path>', 'change the working directory');
   *
   *     --chdir /tmp
   *     program.chdir
   *     // => "/tmp"
   *
   *     // optional argument
   *     program.option('-c, --cheese [type]', 'add cheese [marble]');
   *
   * @param {String} flags
   * @param {String} description
   * @param {Function|*} [fn] or default
   * @param {*} [defaultValue]
   * @return {Command} for chaining
   * @api public
   */

  option(flags, description, fn, defaultValue) {
    const self = this, option = new Option(flags, description), oname = option.name(), name = option.attributeName();



    // default as 3rd arg
    if (typeof fn !== 'function') {
      if (fn instanceof RegExp) {
        const regex = fn;
        fn = (val, def) => {
          const m = regex.exec(val);
          return m ? m[0] : def;
        };
      } else {
        defaultValue = fn;
        fn = null;
      }
    }

    // preassign default value only for --no-*, [optional], or <required>
    if (!option.bool || option.optional || option.required) {
      // when --no-* we make sure default is true
      if (!option.bool) defaultValue = true;
      // preassign only if we have a default
      if (defaultValue !== undefined) {
        self[name] = defaultValue;
        option.defaultValue = defaultValue;
      }
    }

    // register the option
    this.options.push(option);

    // when it's passed assign the value
    // and conditionally invoke the callback
    this.on(`option:${oname}`, val => {
      // coercion
      //console.log(`on option:${oname}`)
      if (val !== null && fn) {
        try {
          val = fn(val, self[name] === undefined ? defaultValue : self[name]);
        } catch (e) {
          console.error();
          console.error(`  error: failed to parse option ${oname} (env ${oname.toUpperCase()}) :: ${e.message} (bad value: "${val}")\n`);
          console.error();
          //if (option.required)
          process.exit(-1);
        }
      }

      // unassigned or bool
      if (typeof self[name] === 'boolean' || typeof self[name] === 'undefined') {
        // if no value, bool true, and we have a default, then use it!
        if (val == null) {
          self[name] = option.bool
            ? defaultValue || true
            : false;
        } else {
          self[name] = val;
        }
      } else if (val !== null) {
        // reassign
        self[name] = val;
      }
    });

    return this;
  }

  /**
   * Allow unknown options on the command line.
   *
   * @param {Boolean} arg if `true` or omitted, no error will be thrown
   * for unknown options.
   * @api public
   */
  allowUnknownOption(arg) {
    this._allowUnknownOption = arguments.length === 0 || arg;
    return this;
  }

  /**
   * Parse `argv`, settings options and invoking commands when defined.
   *
   * @param {Array} argv
   * @return {Command} for chaining
   * @api public
   */

  parse(argv) {
    // implicit help
    if (this.executables) this.addImplicitHelpCommand();

    // store raw args
    this.rawArgs = argv;

    // guess name
    this._name = this._name || basename$1(argv[1], '.js');

    // github-style sub-commands with no sub-command
    if (this.executables && argv.length < 3 && !this.defaultExecutable) {
      // this user needs help
      argv.push('--help');
    }

    // process argv
    const parsed = this.parseOptions(this.normalize(argv.slice(2)));

    const args = this.args = parsed.args;

    const result = this.parseArgs(this.args, parsed.unknown);


    // executable sub-commands
    const name = result.args[0];

    let aliasCommand = null;
    // check alias of sub commands
    if (name) {
      aliasCommand = this.commands.filter(command => command.alias() === name)[0];
    }

    if (this._execs[name] && typeof this._execs[name] !== 'function') {
      return this.executeSubCommand(argv, args, parsed.unknown);
    } else if (aliasCommand) {
      // is alias of a subCommand
      args[0] = aliasCommand._name;
      return this.executeSubCommand(argv, args, parsed.unknown);
    } else if (this.defaultExecutable) {
      // use the default subcommand
      args.unshift(this.defaultExecutable);
      return this.executeSubCommand(argv, args, parsed.unknown);
    }
    return result;
  }

  /**
   * Execute a sub-command executable.
   *
   * @param {Array} argv
   * @param {Array} args
   * @param {Array} unknown
   * @api private
   */

  executeSubCommand(argv, args, unknown) {
    args = args.concat(unknown);

    if (!args.length) this.help();
    if (args[0] === 'help' && args.length === 1) this.help();

    // <cmd> --help
    if (args[0] === 'help') {
      args[0] = args[1];
      args[1] = '--help';
    }

    // executable
    const f = argv[1];
    // name of the subcommand, link `pm-install`
    let bin = `${basename$1(f, extname(f))}-${args[0]}`;

    // In case of globally installed, get the base dir where executable
    //  subcommand file should be located at
    let baseDir, link = lstatSync(f).isSymbolicLink() ? readlinkSync(f) : f;

    // when symbolink is relative path
    if (link !== f && link.charAt(0) !== '/') {
      link = path.join(dirname(f), link);
    }
    baseDir = dirname(realpathSync$1(link));

    // prefer local `./<bin>` to bin in the $PATH
    const localBin = join(baseDir, bin);

    // whether bin file is a js script with explicit `.js` or `.ts` extension
    let isExplicitJS = false;
    if (exists(`${localBin}.js`)) {
      bin = `${localBin}.js`;
      isExplicitJS = true;
    } else if (exists(`${localBin}.ts`)) {
      bin = `${localBin}.ts`;
      isExplicitJS = true;
    } else if (exists(localBin + '.mjs')) {
      bin = localBin + '.mjs';
      isExplicitJS = true;
    } else if (exists(localBin)) {
      bin = localBin;
    }

    args = args.slice(1);

    let proc;
    if (process.platform !== 'win32') {
      if (isExplicitJS) {
        args.unshift(bin);
        // add executable arguments to spawn
        args = (process.execArgv || []).concat(args);

        proc = spawn(process.argv[0], args, { stdio: 'inherit', customFds: [0, 1, 2] });
      } else {
        proc = spawn(bin, args, { stdio: 'inherit', customFds: [0, 1, 2] });
      }
    } else {
      args.unshift(bin);
      proc = spawn(process.execPath, args, { stdio: 'inherit' });
    }

    const signals = ['SIGUSR1', 'SIGUSR2', 'SIGTERM', 'SIGINT', 'SIGHUP'];
    signals.forEach(signal => {
      process.on(signal, () => {
        if (proc.killed === false && proc.exitCode === null) {
          proc.kill(signal);
        }
      });
    });
    proc.on('close', process.exit.bind(process));
    proc.on('error', ({code}) => {
      if (code === 'ENOENT') {
        console.error('%s(1) does not exist, try --help', bin);
      } else if (code === 'EACCES') {
        console.error('%s(1) not executable. try chmod or run with root', bin);
      }
      process.exit(1);
    });

    // Store the reference to the child process
    this.runningCommand = proc;
  }

  /**
   * Normalize `args`, splitting joined short flags. For example
   * the arg "-abc" is equivalent to "-a -b -c".
   * This also normalizes equal sign and splits "--abc=def" into "--abc def".
   *
   * @param {Array} args
   * @return {Array}
   * @api private
   */

  normalize(args) {
    let ret = [], arg, lastOpt, index;

    for (let i = 0, len = args.length; i < len; ++i) {
      arg = args[i];
      if (i > 0) {
        lastOpt = this.optionFor(args[i - 1]);
      }

      if (arg === '--') {
        // Honor option terminator
        ret = ret.concat(args.slice(i));
        break;
      } else if (lastOpt && lastOpt.required) {
        ret.push(arg);
      } else if (arg.length > 1 && arg[0] === '-' && arg[1] !== '-' && !arg.includes('=')) {
        arg.slice(1).split('').forEach(c => {
          ret.push(`-${c}`);
        });
      } else if (/^--/.test(arg) && ~(index = arg.indexOf('='))) {
        ret.push(arg.slice(0, index), arg.slice(index + 1));
      } else {
        ret.push(arg);
      }
    }
    // hack to get ENV in
    for (const o of this.options) {
        if ( (o.short !== '-h' && o.short !== '-V')&& !(ret.includes(o.short) || ret.includes(o.long))){
          const ename = o.name().toUpperCase().replace('-','_');
          if (process.env[ename]){
            ret.push(o.long);
            ret.push(process.env[ename]);
          }
        }
    }
    return ret;
  }

  /**
   * Parse command `args`.
   *
   * When listener(s) are available those
   * callbacks are invoked, otherwise the "*"
   * event is emitted and those actions are invoked.
   *
   * @param {Array} args
   * @return {Command} for chaining
   * @api private
   */

  parseArgs(args, unknown) {
    let name;

    if (args.length) {
      name = args[0];
      if (this.listeners(`command:${name}`).length) {
        this.emit(`command:${args.shift()}`, args, unknown);
      } else {
        this.emit('command:*', args);
      }
    } else {
      outputHelpIfNecessary(this, unknown);

      // If there were no args and we have unknown options,
      // then they are extraneous and we need to error.
      if (unknown.length > 0) {
        this.unknownOption(unknown[0]);
      }
      if (this.commands.length === 0 &&
          this._args.filter(({required}) => required).length === 0) {
        this.emit('command:*');
      }
    }
    // look for missing required options
    for (const o of this.options) {
        if (o.required) {
          if (!this[o.name()]) {
            console.error();
            console.error(`  error: missing required option ${o.flags} (env ${o.name().toUpperCase().replace('-','_')})`);
            console.error();
            process.exit(1);
          }
        }
    }
    return this;
  }

  /**
   * Return an option matching `arg` if any.
   *
   * @param {String} arg
   * @return {Option}
   * @api private
   */

  optionFor(arg) {
    for (let i = 0, len = this.options.length; i < len; ++i) {
      if (this.options[i].is(arg)) {
        return this.options[i];
      }
    }
  }

  /**
   * Parse options from `argv` returning `argv`
   * void of these options.
   *
   * @param {Array} argv
   * @return {Array}
   * @api public
   */

  parseOptions(argv) {
    const args = [];
    const len = argv.length;
    let literal;
    let option;
    let arg;

    const unknownOptions = [];

    // parse options
    for (let i = 0; i < len; ++i) {
      //console.log('arg',argv[i])
      arg = argv[i];

      // literal args after --
      if (literal) {
        args.push(arg);
        continue;
      }

      if (arg === '--') {
        literal = true;
        continue;
      }

      // find matching Option
      option = this.optionFor(arg);

      // option is defined
      if (option) {
        // requires arg
        if (option.required) {
          arg = argv[++i];
          if (arg == null) return this.optionMissingArgument(option);
          this.emit(`option:${option.name()}`, arg);
        // optional arg
        } else if (option.optional) {
          arg = argv[i + 1];
          if (arg == null || (arg[0] === '-' && arg !== '-')) {
            arg = null;
          } else {
            ++i;
          }
          this.emit(`option:${option.name()}`, arg);
        // bool
        } else {
          this.emit(`option:${option.name()}`);
        }
        continue;
      }

      // looks like an option
      if (arg.length > 1 && arg[0] === '-') {
        unknownOptions.push(arg);

        // If the next argument looks like it might be
        // an argument for this option, we pass it on.
        // If it isn't, then it'll simply be ignored
        if ((i + 1) < argv.length && argv[i + 1][0] !== '-') {
          unknownOptions.push(argv[++i]);
        }
        continue;
      }

      // arg
      args.push(arg);
    }
    //console.log('parseOptions',args,unknownOptions)
    return { args, unknown: unknownOptions };
  }

  /**
   * Return an object containing options as key-value pairs
   *
   * @return {Object}
   * @api public
   */
  opts() {
    const result = {}, len = this.options.length;

    for (let i = 0; i < len; i++) {
      const key = this.options[i].attributeName();
      result[key] = key === this._versionOptionName ? this._version : this[key];
    }
    return result;
  }

  /**
   * Argument `name` is missing.
   *
   * @param {String} name
   * @api private
   */

  missingArgument(name) {
    console.error();
    console.error("  error: missing required argument `%s'", name);
    console.error();
    process.exit(1);
  }

  /**
   * `Option` is missing an argument, but received `flag` or nothing.
   *
   * @param {String} option
   * @param {String} flag
   * @api private
   */

  optionMissingArgument({flags}, flag) {
    console.error();
    if (flag) {
      console.error("  error: option `%s' argument missing, got `%s'", flags, flag);
    } else {
      console.error("  error: option `%s' argument missing", flags);
    }
    console.error();
    process.exit(1);
  }

  /**
   * Unknown option `flag`.
   *
   * @param {String} flag
   * @api private
   */

  unknownOption(flag) {
    if (this._allowUnknownOption) return;
    console.error();
    console.error("  error: unknown option `%s'", flag);
    console.error();
    process.exit(1);
  }

  /**
   * Variadic argument with `name` is not the last argument as required.
   *
   * @param {String} name
   * @api private
   */

  variadicArgNotLast(name) {
    console.error();
    console.error("  error: variadic arguments must be last `%s'", name);
    console.error();
    process.exit(1);
  }

  /**
   * Set the program version to `str`.
   *
   * This method auto-registers the "-V, --version" flag
   * which will print the version number when passed.
   *
   * @param {String} str
   * @param {String} [flags]
   * @return {Command} for chaining
   * @api public
   */

  version(str, flags) {
    if (arguments.length === 0) return this._version;
    this._version = str;
    flags = flags || '-V, --version';
    const versionOption = new Option(flags, 'output the version number');
    this._versionOptionName = versionOption.long.substr(2) || 'version';
    this.options.push(versionOption);
    this.on(`option:${this._versionOptionName}`, () => {
      process.stdout.write(`${str}\n`);
      process.exit(0);
    });
    return this;
  }

  /**
   * Set the description to `str`.
   *
   * @param {String} str
   * @param {Object} argsDescription
   * @return {String|Command}
   * @api public
   */

  description(str, argsDescription) {
    if (arguments.length === 0) return this._description;
    this._description = str;
    this._argsDescription = argsDescription;
    return this;
  }

  /**
   * Set an alias for the command
   *
   * @param {String} alias
   * @return {String|Command}
   * @api public
   */

  alias(alias) {
    let command = this;
    if (this.commands.length !== 0) {
      command = this.commands[this.commands.length - 1];
    }

    if (arguments.length === 0) return command._alias;

    if (alias === command._name) throw new Error('Command alias can\'t be the same as its name');

    command._alias = alias;
    return this;
  }

  /**
   * Set / get the command usage `str`.
   *
   * @param {String} str
   * @return {String|Command}
   * @api public
   */

  usage(str) {
    const args = this._args.map(arg => humanReadableArgName(arg));

    const usage = `[options]${this.commands.length ? ' [command]' : ''}${this._args.length ? ` ${args.join(' ')}` : ''}`;

    if (arguments.length === 0) return this._usage || usage;
    this._usage = str;

    return this;
  }

  /**
   * Get or set the name of the command
   *
   * @param {String} str
   * @return {String|Command}
   * @api public
   */

  name(str) {
    if (arguments.length === 0) return this._name;
    this._name = str;
    return this;
  }

  /**
   * Return prepared commands.
   *
   * @return {Array}
   * @api private
   */

  prepareCommands() {
    return this.commands.filter(({_noHelp}) => !_noHelp).map(cmd => {
      const args = cmd._args.map(arg => humanReadableArgName(arg)).join(' ');

      return [
        cmd._name +
          (cmd._alias ? `|${cmd._alias}` : '') +
          (cmd.options.length ? ' [options]' : '') +
          (args ? ` ${args}` : ''),
        cmd._description
      ];
    });
  }

  /**
   * Return the largest command length.
   *
   * @return {Number}
   * @api private
   */

  largestCommandLength() {
    const commands = this.prepareCommands();
    return commands.reduce((max, command) => Math.max(max, command[0].length), 0);
  }

  /**
   * Return the largest option length.
   *
   * @return {Number}
   * @api private
   */

  largestOptionLength() {
    const options = [].slice.call(this.options);
    options.push({
      flags: '-h, --help'
    });
    return options.reduce((max, {flags}) => Math.max(max, flags.length), 0);
  }

  /**
   * Return the largest arg length.
   *
   * @return {Number}
   * @api private
   */

  largestArgLength() {
    return this._args.reduce((max, {name}) => Math.max(max, name.length), 0);
  }

  /**
   * Return the pad width.
   *
   * @return {Number}
   * @api private
   */

  padWidth() {
    let width = this.largestOptionLength();
    if (this._argsDescription && this._args.length) {
      if (this.largestArgLength() > width) {
        width = this.largestArgLength();
      }
    }

    if (this.commands && this.commands.length) {
      if (this.largestCommandLength() > width) {
        width = this.largestCommandLength();
      }
    }

    return width;
  }

  /**
   * Return help for options.
   *
   * @return {String}
   * @api private
   */

  optionHelp() {
    const width = this.padWidth();

    // Append the help information
    return this.options.map(({flags, description, bool, defaultValue}) => `${pad(flags, width)}  ${description}${(bool && defaultValue !== undefined) ? ` (default: ${defaultValue})` : ''}`).concat([`${pad('-h, --help', width)}  output usage information`])
      .join('\n');
  }

  /**
   * Return command help documentation.
   *
   * @return {String}
   * @api private
   */

  commandHelp() {
    if (!this.commands.length) return '';

    const commands = this.prepareCommands();
    const width = this.padWidth();

    return [
      'Commands:',
      '',
      commands.map(cmd => {
        const desc = cmd[1] ? `  ${cmd[1]}` : '';
        return (desc ? pad(cmd[0], width) : cmd[0]) + desc;
      }).join('\n').replace(/^/gm, '  '),
      ''
    ].join('\n');
  }

  /**
   * Return program help documentation.
   *
   * @return {String}
   * @api private
   */

  helpInformation() {
    let desc = [];
    if (this._description) {
      desc = [
        this._description,
        ''
      ];

      const argsDescription = this._argsDescription;
      if (argsDescription && this._args.length) {
        const width = this.padWidth();
        desc.push('Arguments:');
        desc.push('');
        this._args.forEach(({name}) => {
          desc.push(`  ${pad(name, width)}  ${argsDescription[name]}`);
        });
        desc.push('');
      }
    }

    let cmdName = this._name;
    if (this._alias) {
      cmdName = `${cmdName}|${this._alias}`;
    }
    const usage = [
      `Usage: ${cmdName} ${this.usage()}`,
      ''
    ];

    let cmds = [];
    const commandHelp = this.commandHelp();
    if (commandHelp) cmds = [commandHelp];

    const options = [
      'Options:',
      '',
      `${this.optionHelp().replace(/^/gm, '  ')}`,
      ''
    ];

    return usage
      .concat(desc)
      .concat(options)
      .concat(cmds)
      .join('\n');
  }

  /**
   * Output help information for this command
   *
   * @api public
   */

  outputHelp(cb) {
    if (!cb) {
      cb = passthru => passthru;
    }
    process.stdout.write(cb(this.helpInformation()));
    this.emit('--help');
  }

  /**
   * Output help information and exit.
   *
   * @api public
   */

  help(cb) {
    this.outputHelp(cb);
    process.exit();
  }
}

/**
 * Camel-case the given `flag`
 *
 * @param {String} flag
 * @return {String}
 * @api private
 */

function camelcase(flag) {
  return flag.split('-').reduce((str, word) => str + word[0].toUpperCase() + word.slice(1));
}

/**
 * Pad `str` to `width`.
 *
 * @param {String} str
 * @param {Number} width
 * @return {String}
 * @api private
 */

function pad(str, width) {
  const len = Math.max(0, width - str.length);
  return str + Array(len + 1).join(' ');
}

/**
 * Output help information if necessary
 *
 * @param {Command} command to output help for
 * @param {Array} array of options to search for -h or --help
 * @api private
 */

function outputHelpIfNecessary(cmd, options = []) {
  for (let i = 0; i < options.length; i++) {
    if (options[i] === '--help' || options[i] === '-h') {
      cmd.outputHelp();
      process.exit(0);
    }
  }
}

/**
 * Takes an argument an returns its human readable equivalent for help usage.
 *
 * @param {Object} arg
 * @return {String}
 * @api private
 */

function humanReadableArgName({name, variadic, required}) {
  const nameOutput = name + (variadic === true ? '...' : '');

  return required
    ? `<${nameOutput}>`
    : `[${nameOutput}]`;
}

// for versions before node v0.8 when there weren't `fs.existsSync`
function exists(file) {
  try {
    if (statSync(file).isFile()) {
      return true;
    }
  } catch (e) {
    return false;
  }
}

class simpleCli extends Base {

	#configFile
	#fn

	constructor(logger, opts = [], fn) {
		if (!fn || !isFn(fn)) throw new Error('no function')
		super( logger, ...opts );
		this.#fn = fn;
  }
	/*
  setMethod(_fn) {

		if (!isFn(_fn)) throw new Error('not a function')
		this.#methods = {}
    const name = 'run'
		if (isAsyncFunction(_fn)) {
			this.#methods[name] = {}
			this.#methods[name].fn = async (...o) => {
				try {
					await _fn(...o)
				} catch (error) {
					this.log.emerg({method:name, error})
					if ((error instanceof ReferenceError)) process.exit(-1)
				}
			}
		} else {
			this.#methods[name].fn = (...o) => {
	      return new Promise(async (resolve) => {
					try {
						_fn(...o)
					} catch (error) {
						this.log.emerg({method:name, error})
						if ((error instanceof ReferenceError)) process.exit(-1)
					}
					resolve()
				})
			}
		}
  }
	*/
  async prerun () {
		const cliParams = new Command();
    cliParams
      .version('0.0.1')
      .usage('[options]')
      .option('-D, --docker','gen Dockerfile ARG and ENV skeleton')
      .option('-T, --dump-config','dump configuration')
      .option('-U, --dump-config-undefined','dump configuration with undefined')
      .option('-c, --config [file]', 'set configuration file','./config.js');
      for (const param of this.settings) {
        cliParams.option('--'+param+ ' ['+typeof this[param]+']','server '+param+ ' (default: '+this[param]+')');
      }
		cliParams.parse(process.argv);
		let configFile = cliParams.config || './config.js';
    this.#configFile = configFile;
    try {
      configFile = realpathSync(configFile);
      this.#configFile = configFile;
    } catch (e) {
      this.#configFile = undefined;
      if (!(configFile === './config.js')) this.log.info({Config:'no file ' + configFile});
    }
    let conf = {};
    // load config file
    try {
      if (this.#configFile) {
        conf = JSON.parse(readFileSync(this.#configFile,"utf8"));
        this.log.debug('conf',conf,CONFIG);
      }
    } catch (e) {
      this.log.info({Config:configFile,error:e.message});
      this.#configFile = undefined;
    }
		// patch conf with command line params
		for (const param of this.settings) {
			if (!!cliParams[param] && cliParams[param] != conf[param]) conf[param] = cliParams[param];
		}
		// conf ready, apply now
		this.readConfig(conf);
		// just dump conf
		if (cliParams.dumpConfig) {
			process.stdout.write(['/* config dump  */\nmodule.exports = ',JSON.stringify(this.config,null,'\t'),'\n'].join('\n'));
			process.exit(0);
		}
		// dump conf with undefined as nulls
		if (cliParams.dumpConfigUndefined) {
			process.stdout.write(['/* config dump  with undefined as nulls */\nmodule.exports = ',JSON.stringify(this.config,function(k, v) { if (v === undefined) { return null; } return v; },'\t'),'\n'].join('\n'));
			process.exit(0);
		}
		// generate Dockerfile skeleton
		if (cliParams.docker) {
	     let str = '\n';
			//str += "FROM node:13-alpine AS base\n"
			//str += "MAINTAINER 'no maintenance intended'\n"
			str += "\n";
			str += "# Declare args\n";
			for (const a of this.settings){
				str += `ARG ${a.toUpperCase()}\n`; // =${flat[a]}
			}
			str += "\n";
			str += "# Declare envs vars for each arg\n";
			for (const a of this.settings) {
				str += `ENV ${a.toUpperCase()} $${a.toUpperCase()}\n`;
			}
			str += "\n";

			process.stdout.write(str);
			process.exit(0);
		}
		this.log.info('prerun ok');
  }
	async postrun(){
		this.log.info('postrun');
	}
	async _run(){
		if (isAsyncFunction(this.#fn)) {
			await this.#fn(this.log);
		} else {
			await new Promise( resolve => {
				this.#fn(this.log);
				resolve();
			});
		}
	}
	async run() {
		await this.prerun();
		await this._run();
		await this.postrun();
		process.exit(0);
  }
}

function createSimpleCli (options) {
  if (!options) throw new Error('no options')
	let fn;
	if (options.fn && isFn(options.fn)) fn = options.fn;
	else if (isFn(options)) fn = options;
	else throw new Error('no function')

	let opts = [];
  if (options.opts) {
	if (Array.isArray(options.opts)) opts = options.opts;
    else {
  		for (const opt of Object.keys(options.opts)){
  			//if (isFn(options.opts[opt])) {
  				const tmp = {};
  				tmp[opt] = options.opts[opt];
  				opts.push(tmp);
  			//} else throw new Error('no function for '+ opts)
  		}
  	}
  }
  return new simpleCli(options.logger,opts, fn)
}

/*

https://github.com/elastic/elasticsearch/blob/7b44743ccc2402d79e347882dc74623e04883488/server/src/main/java/org/elasticsearch/index/search/QueryParserHelper.java#L133

if (acceptMetadataField == false && fieldType.name().startsWith("_")) {
                // Ignore metadata fields
                continue;
            }

*/

const IDFIELD = '__id__';
const HASHFIELD = '__contenthash__'; //'journal.ondisk.contentHash'
const SSDEEPFIELDPREFIX = '__ssdeep_';
const CONTENTINDEX = 'content';
const ENTITYINDEX = 'entities';
const TIMELINEINDEX = 'timeline';
const FILESINDEX = 'files';
const METAINDEX = 'meta';
const LINEINDEX = 'line';
const SEPARATOR = '￣';
const indexName = (prefix,middle,suffix) => {
  let name = '';
  if (prefix) name += prefix + SEPARATOR;
  if (middle) name += middle;
  if (suffix) name += SEPARATOR+suffix;
  if (name.length === 0) {
    console.error('cant make index name');
    throw new Error('no prefix,middle,suffix. Index name can not be empty')
 }
  return encodeURIComponent(name.toLowerCase())
};
// TODO ticket elasticsearch to get supported langueges list from API
const LANGUAGES$1 = ['ARABIC', 'ARMENIAN', 'BASQUE', 'BENGALI', 'BRAZILIAN', 'BULGARIAN', 'CATALAN', 'CJK', 'CZECH', 'DANISH', 'DUTCH', 'ENGLISH', 'ESTONIAN', 'FINNISH', 'FRENCH', 'GALICIAN', 'GERMAN', 'GREEK', 'HINDI', 'HUNGARIAN', 'INDONESIAN', 'IRISH', 'ITALIAN', 'LATVIAN', 'LITHUANIAN', 'NORWEGIAN', 'PERSIAN', 'PORTUGUESE', 'ROMANIAN', 'RUSSIAN', 'SORANI', 'SPANISH', 'SWEDISH', 'TURKISH', 'THAI'];

/*

options for glob'er and spot'er


*/


const opts = {
  filespath: p => {
    const d = p ? resolve(p) : resolve("");
    try {
      accessSync(d, constants.R_OK);
    } catch (error) {
      throw new Error('not readable: ' + d)
    }
    return d;
  },
  influxdb: list => {
    return list && list.split ? list.split(",").map(hp => parseHostnamePort(hp, INFLUXDB)) : [{ hostname: "localhost", port: INFLUXDB }];
  },
  tikas: list => {
    return list && list.split ? list.split(",").map(hp => parseHostnamePort(hp, TIKA)) : [{ hostname: "localhost", port: TIKA }];
  },
  ssdeeps: list => {
    return list && list.split ? list.split(",").map(hp => parseHostnamePort(hp, SSDEEP)) : [{ hostname: "localhost", port: SSDEEP }];
  },
  elastics: list => {
    return list && list.split ? list.split(",").map(hp => parseHostnamePort(hp, ELASTIC)) : [{ hostname: "localhost", port: ELASTIC }];
  },
  clustername: p => {
    return p ? p.toLowerCase() : "singlehost-cluster";
  },
  ers: list => {
    return list && list.split ? list.split(",").map(hp => parseHostnamePort(hp, ER)) : [{ hostname: "localhost", port: ER }];
  },
  lds: list => {
    return list && list.split ? list.split(",").map(hp => parseHostnamePort(hp, LD)) : [{ hostname: "localhost", port: LD }];
  },
  prefix: p => {
    return p ? p.toLowerCase() : "DELETEME";
  },
  // https://www.elastic.co/guide/en/elasticsearch/reference/current/breaking-changes-7.0.html#_literal_literal_is_no_longer_allowed_in_index_name
  // : is no longer allowed in index name
  suffix: p => {
    return p
      ? p.toLowerCase()
      : hostname()
          .split(".")
          .shift() +
          SEPARATOR$1 +
          PORT;
  },
  content: p => {
    return p ? p.toLowerCase() : CONTENTINDEX;
  },
  field: p => {
    return p ? p : HASHFIELD;
  }, // <- case sensitive !
  files: p => {
    return p ? p.toLowerCase() : FILESINDEX;
  },
  entities: p => {
    return p ? p.toLowerCase() : ENTITYINDEX;
  },
  timeline: p => {
    return p ? p.toLowerCase() : TIMELINEINDEX;
  },
  line: p => {
    return p ? p.toLowerCase() : LINEINDEX;
  },
  meta: p => {
    return p ? p.toLowerCase() : METAINDEX;
  }
};

/*

Level	Description
emerg	System is unusable
alert	Action must be taken immediately
crit	Critical conditions
err	Error conditions
warn	Warning conditions
notice	Normal, but significant conditions
info	Informational messages
debug	Debugging messages





[%t]	timestamp for the message
[%l]	level of the message
[pid %p]	process pid
%F	source code file and line
%E	error status code and string
%a	client IP address and string
%M	the log message

Facilities ...

*/

/*
TODO
Error.prepareStackTrace = function(er, stack) {
    ...
 }
*/




const LOGGER = Object.prototype.toString.call(process.logger) === "[object Function]";

class Event {
  constructor(message = {}, o = {}) {
    this.level = this.constructor.name.toLowerCase();
    this.timestamp = new Date();
    if (message instanceof Error) {
      this.message = message.message;
      this.stack = message.stack.split("\n");
      this.stack.shift();
      Error.captureStackTrace(message, this.constructor);
      const tmp = message.stack.split("\n");
      tmp.shift();
      this.stack.push(...tmp);
      for (const key of Object.keys(message)) this[key] = message[key];
    } else {
      if (typeof message === "string") this.message = message;
      else for (const key of Object.keys(message)) this[key] = message[key];
      for (const key of Object.keys(o)) this[key] = o[key];
      const tmp = {};
      Error.captureStackTrace(tmp, this.constructor);
      this.stack = tmp.stack.split("\n");
      this.stack.shift();
    }
    //if (process.stdout.isTTY) console.dir({ ...this })
    if (LOGGER) process.logger({ ...this });
  }
  get json() {
    return JSON.stringify({ ...this })
  }
}

class LogEvent extends Event {}
class ErrorEvent extends Event {}
class Notice extends LogEvent {}
class Warn extends LogEvent {}
class Err extends ErrorEvent {}
class Crit extends ErrorEvent {}

/*

$ uname
Linux


$ nodejs -v
v11.5.0


## Algorithms supported

> console.log(require('crypto').getHashes())
[ 'RSA-MD4',
  'RSA-MD5',
  'RSA-MDC2',
  'RSA-RIPEMD160',
  'RSA-SHA1',
  'RSA-SHA1-2',
  'RSA-SHA224',
  'RSA-SHA256',
  'RSA-SHA384',
  'RSA-SHA512',
  'blake2b512',
  'blake2s256',
  'md4',
  'md4WithRSAEncryption',
  'md5',
  'md5-sha1',
  'md5WithRSAEncryption',
  'mdc2',
  'mdc2WithRSA',
  'ripemd',
  'ripemd160',
  'ripemd160WithRSA',
  'rmd160',
  'sha1',
  'sha1WithRSAEncryption',
  'sha224',
  'sha224WithRSAEncryption',
  'sha256',
  'sha256WithRSAEncryption',
  'sha384',
  'sha384WithRSAEncryption',
  'sha512',
  'sha512WithRSAEncryption',
  'ssl3-md5',
  'ssl3-sha1',
  'whirlpool' ]

## Character encodings

'ascii' - For 7-bit ASCII data only. This encoding is fast and will strip the high bit if set.

'utf8' - Multibyte encoded Unicode characters. Many web pages and other document formats use UTF-8.

'utf16le' - 2 or 4 bytes, little-endian encoded Unicode characters. Surrogate pairs (U+10000 to U+10FFFF) are supported.

'ucs2' - Alias of 'utf16le'.

'base64' - Base64 encoding. When creating a Buffer from a string, this encoding will also correctly accept "URL and Filename Safe Alphabet" as specified in RFC 4648, Section 5.

'latin1' - A way of encoding the Buffer into a one-byte encoded string (as defined by the IANA in RFC 1345, page 63, to be the Latin-1 supplement block and C0/C1 control codes).

'binary' - Alias for 'latin1'.

'hex' - Encode each byte as two hexadecimal characters.
*/

const DEFAULTALGORITHM$2 = 'md5';

function digestFile( path, algorithm = DEFAULTALGORITHM$2, encoding = 'hex' ) {
  return new Promise( resolve =>
    createReadStream(path)
      .on('error', e => {
        resolve();
      })
      .pipe(createHash(algorithm)
      .setEncoding(encoding))
      .once('finish',  function() {
        resolve( this.read() );
      })
  )
}

/*

If some stage takes (or may take) much longer than the others, and cannot be
sped up, the designer can provide two or more processing elements to carry out
that task in parallel, with a single input buffer and a single output buffer.
As each element finishes processing its current data item, it delivers it to
the common output buffer, and takes the next data item from the common input
 buffer. This concept of "non-linear" or "dynamic" pipeline is exemplified by
 shops or banks that have two or more cashiers serving clients from a single
 waiting queue.

*/

const DEFAULTALGORITHM$1 = 'md5';
function digestString$1(str, algorithm = DEFAULTALGORITHM$1, encoding = 'hex') {
  return createHash(algorithm).update(str).digest(encoding)
}

function wait(ms) {
  if (!ms) ms = 10;
  //console.dir({wait:ms})
  //ms = Math.round(ms);
  return new Promise(resolve => {
    setTimeout(() => {
      resolve();
    }, ms);
  });
}

function printProgress(...progress) {
  if (process.stdout.isTTY) {
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write((new Date()).toLocaleTimeString('it-IT')+' '+progress.join(" "));
  } else console.debug(progress.join(" "));
}

class StringStream extends Readable {
  constructor (str, encoding) {
    super();
    this._str = str;
    this._encoding = encoding || 'utf8';
  }

  _read () {
    if (!this.ended) {
      process.nextTick(() => {
        this.push(Buffer.from(this._str, this._encoding));
        this.push(null);
      });
      this.ended = true;
    }
  }
}

// pool: array of strings of hostname:port pairs
// tti : amount of time in which it takes for the server to become functional
// tries: how many times to make connection attempts to unresponsive server
// fails if all servers in pool are not 'connectable' in (tti * tries) OR  if tries 0 then try forever

class Ask extends Transform {
  /*
  name = "";
  pool = [];
  tti = 1000 * 3;
  tries = 3;
  counter = 0;
  tooked = 0;
  average = undefined;
  _askState = {
    running: [],
    down: [],
    flushing: false
  };
  */
  constructor(options) {

    if (!options || !options.pool || !Array.isArray(options.pool) || !options.pool.length) throw new Error("No pool. Required array of hostname:port pairs");
    if (!options.idField || !options.idField.length) throw new Error('No id fieldname')
    options.objectMode = true;
    options.highWaterMark = 1;
    super(options);
    this.name = "";
    this.pool = [];
    this.inpool = [];
    this.tti = 1000 * 3;
    this.tries = 3;
    this.counter = 0;
    this.tooked = 0;
    this.average = undefined;
    this.killers = {};
    this._askState = {
      running: [],
      down: [],
      flushing: false
    };
    this._metrix = {};
    for (const server of options.pool) {
      // TODO make it prettier
      let hostname, port;
      if (server.hostname && server.port) {hostname = server.hostname; port = server.port;}
      else { let bittes = server.split(":"); hostname = bittes[0]; if (bittes[1]) port = bittes[1]; }
      if (!port) port = options.defaultport;
      if (hostname.trim().length && port) {
        this.pool.push({ hostname, port });
        this._metrix[hostname+':'+port] = {worked:0};
      } else throw new Error("Missing hostname:port pair");
    }
    if (typeof options.ask === "function") this._ask = options.ask;
    if (typeof options.ping === "function") this._ping = options.ping;
    if (typeof options.sendmetrix === 'function') this.sendmetrix = options.sendmetrix;
    if (options.tti) this.tti = options.tti*1000;
    if (options.tries !== undefined) this.tries = options.tries;
    if (options.name) this.name = options.name;
    else this.name = this.constructor.name;
    this.idField = options.idField;
  }

  push(something) {
    if (!this.destroyed && !this.ended && !this.errored) {
      try {
        super.push(something);
      } catch (error) {
        console.dir('--- push error --');
        console.dir(this);
        console.error(error);

        this.destroy(error);
      }
    } else console.error(this.name +' destroyed, no more push ;(');
  }

  _ask(hostname, port, data, abort) {
    console.error(this.name + ": _ask not implemented");
    process.exit(-1);
  }

  get waittime() {
    return this.average ? Math.round(this.average / this.pool.length) : Math.round(this.tti / this.pool.length);
  }

  get alldown() {
    if (this.tries === 0 && this._askState.down.length === this.pool.length && this._askState.down.reduce( (a,b) => a+b,0) > this.pool.length * 3) {
      this.push({__critical:{service:this.name,alldown:this.pool}});
    }
    if (this.tries) return this._askState.down.filter(v => v > this.tries).length === this.pool.length;
    else return false // if tries === 0 try forever
  }

  get somerunning() {
    for (const i in this._askState.running) {
        if (this._askState.running[i]) {
          const state = inspect(this._askState.running[i]);
          if (state === "Promise { <pending> }") return true;
        }
    }
    return false;
  }

  sendmetrix(m) {
    //console.dir(m);
  }

  async _transform(current, encoding, next) {
    if (this.counter === 0 ) {this.counter++ ;await this.pingsome();}
    if (current instanceof Error || current instanceof Event) {
      this.push(current); // just pass trough errors
    } else {
      if (Object.keys(current).filter(k=>!k.startsWith('__')).length){
        await this.waitforslot(current);
        this.counter++;
        printProgress('transform',this.name,this.counter,current[this.idField]);
      } else this.push(current); // just pass trough if only '__foo*'
    }
    next();
  }

  async _flush(last) {
    // clean up pings
    for (const i in this._askState.down) {
      delete this._askState.running[i];
    }
    // wait for slots do finish
    const start = process.hrtime.bigint();
    while (this.somerunning && !this.destroyed) {
      const waited = Number(process.hrtime.bigint() - start) / 1e6;
      if (waited > (this.average * this.pool.length * this.tries + 120 * 1000)) {
        //this.destroy(new Error(this.name + " flush timeout "));
        this.destroyed = true;
        console.error(new Error(this.name + " flush timeout "));
        break
      }
      printProgress('flushing', this.name, Math.round(waited/1000),this._askState.running.length);
      await wait(this.waittime);
    }
    this.push({__done__:{service:this.name,count:this.counter,average:this.average,servers:this._metrix}});
    return void last();
  }

  async waitforslot(data) {
    let waited = 0;
    while (!this.destroyed) {  // <-----------
      for (const i in this.pool) {
        if (this._askState.running[i]) continue;
        this._askState.running[i] = this.ask(data, i, waited);
        this.inpool[i] = data[this.idField];
        return;
      }
      const waittime = this.waittime;
      await wait(waittime);
      waited += waittime;
      printProgress('waitforslot', this.name, Math.round(waited/1000), this.inpool);
    } // <---------------------------------------
  }

  async ask(data, slot, waited) {
    const start = process.hrtime.bigint();
    const { hostname, port } = this.pool[slot];
    const server = hostname + ":" + port;
    let answer;
    try {
      answer = await this._ask(hostname, port, data, this);
      this._askState.running[slot] = undefined;
      const took = Number(process.hrtime.bigint() - start) / 1e6;
      if (Object.keys(answer).length && !this.destroyed) this.push(answer);
      this.tooked += took;
      this.average = this.tooked / this.counter;
      this._metrix[hostname+':'+port].worked += took;
      this.sendmetrix({ waited, took, server, service: this.name, i: data._id });
      printProgress('ok',this.name,data[this.idField]);
    } catch (error) {
      // die on syntax etc.. errors
      if (error instanceof SyntaxError || error instanceof ReferenceError || error instanceof RangeError) {
        console.error(error);
        console.error('pekkis');
        process.exit(-1);
      // TODO decrease servers pool
      // } else if (ErrorEvent){
      // try again
      } else {

        //this.push({__failed__:{error, timestamp:new Date(), server, service:this.name}})
        this.inpool[slot] = 'ping';
        this._askState.running[slot] = this.ping(slot, hostname, port);
        if (!this._metrix[hostname+':'+port].failures) this._metrix[hostname+':'+port].failures = [];
        this._metrix[hostname+':'+port].failures.push({error,timestamp:new Date()});
        // TODO check is it really  killing or was it 'dead' before
        let hash;
        try {
          hash = digestString$1(JSON.stringify(data[this.idField]));
        } catch (error) {
          console.error(error);
          console.dir(data);
          console.dir(this);
          process.exit(-1);
        }
        error.id = data[this.idField];
        if (!this.killers[hash]) this.killers[hash] = [];
        this.killers[hash].push(error);
        if (this.tries && this.killers[hash].length > this.tries) {
          this.push({__killer:{o:Object.keys(data),pathname:data.pathname,basename:data.basename,kills:this.name,server,errors:this.killers[hash]}});
        } else await this.waitforslot(data);
      }
    }
  }

  async ping(slot, hostname, port) {
    let answer;
    const start = process.hrtime.bigint();
    const server = hostname+':'+port;
    if (!this._ping) this._ping = this._ask;
    while (!answer && !this.destroyed) {
      try {
        answer = await this._ping(hostname, port, {}, this);
        this._askState.running[slot] = undefined;
        this._askState.down[slot] = undefined;
        const downtime = Number(process.hrtime.bigint() - start) / 1e6;
        //this.push({__up__:{downtime,timestamp:new Date(),server, service:this.name,w:answer._warning}})
        this.sendmetrix({ downtime, server, service: this.name });
        this.inpool[slot] = 'up';
      } catch (error) {
        if (error instanceof ReferenceError || error instanceof SyntaxError){
          console.error(error);
          process.exit(-1);
        }
        if (!this._askState.down[slot]) this._askState.down[slot] = 0;
        this._askState.down[slot]++;
        if (this.alldown) this.destroy(new Error(this.name + " ALL DOWN"+JSON.stringify(this.pool)+"\n"+error+"\n"+JSON.stringify(this._metrix)));
        else await wait(this.tti);
      }
    }
    this._askState.running[slot] = undefined;
    this._askState.down[slot] = undefined;
  }

  async pingsome(all=false){
    let pingok = false;
    const updown = {up:[],down:[]};
    for (const {hostname, port} of this.pool){
      if (all) printProgress(this.name,'pinging',hostname,port);
      try {
        const a = await this._ping(hostname,port);
        pingok = true;
        updown.up.push({hostname, port});
      } catch (error) {
        updown.down.push({hostname, port});
        //noop
      }
      if (pingok && !all) break
    }
    if (!pingok && this.tries) {
      this.destroy(new Error(this.name+' sorry, ALL DOWN '+ JSON.stringify(this.pool)+"\n"+JSON.stringify(this._metrix)));
    } else {
      if (all) this.push({__pinged:{name:this.name,all, up:updown.up,down:updown.down, timestamp:new Date()}});
    }
  }

}

const REQUESTTIMEOUT$2 = 30000; // network.http.pipelining.read-timeout


// TODO debug globalagent not reusing sockets
//globalAgent.keepAlive = true
//globalAgent.maxSockets = 64
//globalAgent.keepAliveMsecs = 1000 * 30


// httpAgent: new http.Agent({ keepAlive: true, keepAliveMsecs: 5000, maxSockets: 40 }),

/*

http.globalAgent.maxSockets = 10000;

// http.globalAgent.maxSockets = Number.POSITIVE_INFINITY;
const request = () => new Promise((resolve, reject) => {
  http.get('http://localhost:8080', (res) => {
    let buffer = '';
    res.setEncoding('utf-8');
    res.on('readable', (chunk) => {
      buffer += res.read();
    });
    res.on('end', () => {
      resolve(buffer);
    });
    res.on('error', (e) => {
      reject(e);
    });
  });
});

*/


/*

resolve answer || error => done || do not try again that
reject error => try again later

see dynpipe.mjs ask loop

*/



function putfile(pathname, hostname, port, path ='/' ,timeout = REQUESTTIMEOUT$2){
  return new Promise( async(resolve,reject)=>{

    let rs = createReadStream(pathname);

    const options = {
      hostname,
      port,
      //timeout,
      path,
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'connection': 'keep-alive',
      }
    };

    const req = request(options, (res) => {
      if (res.statusCode > 299 && res.statusCode < 500) {
        // we did someting wrong ;(
        resolve(new Crit(STATUS_CODES[res.statusCode],{statuscode: res.statusCode, hostname,port}));
      } else if (res.statusCode > 499) {
         // the server is aware that it has encountered an error
         resolve(new Notice(STATUS_CODES[res.statusCode], { statuscode: res.statusCode,hostname, port }));
      } else {
        let body = [];
        let size = 0;
        let endevent = 'normal';
        res.on('timeout', () => {
          endevent = 'timeout';
          resolve(new Notice('Response timeout',{hostname,port,pathname}));
        });
        res.on('aborted', () => {
          endevent = 'aborted';
          resolve(new Notice('Response aborted',{hostname,port,pathname}));
        });
        res.on('error', (error) => {
          endevent = 'error';
          error.pathname = pathname;
          resolve(new Notice(error));
        });
        res.on('data', (chunk) => {
           body.push(chunk);
           size += chunk.length;
           if (size > kStringMaxLength) {
             resolve(new Err("Response to big", { hostname, port, pathname }));
             rs.unpipe(req);
             req.abort();
           }
        });
        res.on('end', () => {
          if (endevent!== 'normal') return
          let answer;
          const str = Buffer.concat(body).toString();
          const took = Number(process.hrtime.bigint() - start)/1e+6;
          try {
            answer = JSON.parse(str);
            resolve({took,answer});
          } catch (error) {
            if (str.trim().length) {
              resolve(new Warn('Bad response',{hostname,port,pathname}));
            } else {
              if (res.statusCode === 204) resolve({took,answer:{}});
              else resolve(new Warn('Empty response',{hostname,port,pathname,statuscode:res.statusCode}));
            }
          }
        });
      }
    });

    req.on('error', (error) => {
      rs.unpipe(req);
      error.pathname = pathname;
      resolve(new Notice(error));
    });
    req.on('timeout', () => {
      rs.unpipe(req);
      req.abort();
      resolve( new Notice('Request timeout',{hostname,port,pathname}));
    });

    //req.setSocketKeepAlive(true,1000)

    req.on('socket',(socket )=>{
      socket.setKeepAlive(true,1000);
      /*
      // even keepalive probes are ok, it will still be 'inactivity' ;(
      socket.setTimeout(REQUESTTIMEOUT, ()=>{
        socket.end();
        req.emit('timeout')
      });
      */
    });

    let stotal = 0;
    rs.on('data', chunk => {
      stotal += chunk.length;
    });

    rs.on('error', (error) => {
        rs.unpipe(req);
        req.abort();
        resolve(new Err(error)); // it is 'local' issue, not server fault
      });

    const start = process.hrtime.bigint();

    rs.pipe(req);
  })
}

function putstr(str, hostname, port, path = '/',timeout = REQUESTTIMEOUT$2) {
  return _postputstr(str, hostname, port, path, timeout, 'PUT')
}

function poststr(str, hostname, port, path = '/',timeout = REQUESTTIMEOUT$2) {
  return _postputstr(str, hostname, port, path, timeout, 'POST')
}

function _postputstr(str, hostname, port, path, timeout, method) {
  let type;
  if (typeof str === 'string' || str instanceof String) {
    if (str.startsWith('{')  && (str.endsWith('}\n') || str.endsWith('}'))) type = 'application/json; charset=utf-8';
    else type = 'text/plain; charset=utf-8';
  } else {
    str = JSON.stringify(str);
    type = 'application/json; charset=utf-8';
  }
  const buff = Buffer.from(str);
  return new Promise(async (resolve, reject) => {
    const options = {
      hostname,
      port,
      path,
      //timeout,
      method,
      headers: {
        'connection': 'keep-alive',
        Accept: "application/json",
        'content-length' : buff.length,
        'content-type' : type
      }

    };
    // custom long timeout
    let longrun;
    if (timeout > REQUESTTIMEOUT$2) {
      longrun = setTimeout(()=>{
      },REQUESTTIMEOUT$2);
    }

    const req = request(options, (res) => {

      if (res.statusCode > 299 && res.statusCode < 500) {
        // we did someting wrong ;(
        resolve(new Crit(STATUS_CODES[res.statusCode],{statuscode: res.statusCode, message:res.statusMessage, hostname,port}));
      } else if (res.statusCode > 499) {
         // server is aware that it is on error
         resolve(new Notice(STATUS_CODES[res.statusCode], { statuscode: res.statusCode,message:res.statusMessage,hostname, port }));
      } else {
        let size = 0;
        let body = [];
        let endevent = "normal";
        res.on("timeout", () => {
          endevent = "timeout";
          new Warn("Response timeout", { hostname, port });
          resolve(new Notice("Response timeout", { hostname, port }));
        });
        res.on("aborted", () => {
          endevent = "aborted";
          resolve(new Notice("Response aborted", { hostname, port }));
        });
        res.on("error", (error) => {
          endevent = "error";
          resolve(new Notice(error));
        });
        res.on("data", (chunk) => {

          body.push(chunk);
          size += chunk.length;
          if (size > kStringMaxLength) {
            resolve(new Err("Response to big", { hostname, port }));
            req.abort();
          }
        });
        res.on("end", () => {
          if (longrun){
            clearTimeout(longrun);
          }
          if (endevent !== "normal") return;
          const took = Number(process.hrtime.bigint() - start) / 1e6;
          let answer;
          const str = Buffer.concat(body).toString();

          try {
            answer = JSON.parse(str);
            resolve({answer,took});
          } catch (error) {
            if (str.trim().length) {
               resolve(new Warn("Bad response", { hostname, port }));
            } else {
              if (res.statusCode === 204) resolve({took,answer:{}});
              else resolve(new Warn("Empty response", { hostname, port }));
            }
          }
        });
      }
    });

    req.on("error", (error) => {
      resolve(new Notice(error));
    });

    req.on("timeout", () => {
      req.abort();
      resolve(new Notice("Request timeout", { hostname, port }));
    });

    req.on('connect',(res, socket, head)=>{
      socket.setKeepAlive(true,1000);
      /*
      socket.setTimeout(REQUESTTIMEOUT);
      socket.on('timeout', () => {
        console.log('socket timeout');
        socket.end();
        req.emit('timeout')
      });
      */
    });

    const start = process.hrtime.bigint();
    req.end(buff);
  })
}

/*

(async function() {
  try {
    // const r = await putfile('put.mjs','localhost',9998,'/2:kxVkrYfBfLpaOKRvGckzLpaOKRvG6fKf0WF9L79:kHkspf9aO2vGce9aO2vGbf0WF9N&24:9vqSEytc1sp2KdkIs7uUMqXx6wJuyBK/H2urmt2BjzBN8Fl1tPn:9tiWhUMUV5qmQBvH8FTFn')

    //const r = await get('localhost',9997,'/2:kxVkrYfBfLpaOKRvGckzLpaOKRvG6fKf0WF9L79:kHkspf9aO2vGce9aO2vGbf0WF9N&24:9vqSEytc1sp2KdkIs7uUMqXx6wJuyBK/H2urmt2BjzBN8Fl1tPn:9tiWhUMUV5qmQBvH8FTFn')
    const s = '{"index:{}"}\n{"kala":"maja"}\n'
    const r = await putstr(s,'localhost',9200,'/deleteme/_bulk')
    console.dir({r})
    console.dir(r.answer)
  } catch (e){
    console.dir({e})
  }

}());

*/

const LANGUAGES = [...LANGUAGES$1];
const NUMBEROFREPLICAS = 2;

//globalAgent.keepAlive = true

// TODO move to separate module
const DEFAULTALGORITHM = 'md5';
function digestString(str, algorithm = DEFAULTALGORITHM, encoding = 'hex') {
  if (!str) str = Math.random()+Date.now(); // TODO better way to survive random disk reading errors
  return createHash(algorithm).update(str).digest(encoding)
}


/*

curl -X POST "localhost:9200/_bulk?pretty" -H 'Content-Type: application/json' -d'
{ "index" : { "_index" : "test", "_id" : "1" } }
{ "field1" : "value1" }
{ "delete" : { "_index" : "test", "_id" : "2" } }
{ "create" : { "_index" : "test", "_id" : "3" } }
{ "field1" : "value3" }
{ "update" : {"_id" : "1", "_index" : "test"} }
{ "doc" : {"field2" : "value2"} }
'


POST /<index>/_bulk

The index and create actions expect a source on the next line, and have the same semantics as the op_type parameter in the standard index API: create fails if a document with the same name already exists in the index, index adds or replaces a document as necessary.

Response body

took
(integer) How long, in milliseconds, it took to process the bulk request.
errors
(boolean) If true, one or more of the operations in the bulk request did not complete successfully.
items
(array of objects) Contains the result of each operation in the bulk request, in the order they were submitted.


!!!

When using the HTTP API, make sure that the client does not send HTTP chunks, as this will slow things down.


-------

Sending a 'Content-Length' header will disable the default chunked encoding.


If I remember correctly Node.js behaviour is:

Use chunked transfer encoding by default if res.write() is used.
Do not use chunked transfer encoding by default if only res.end() is used.

*/

const CHUNKSIZE = 1024 * 1024 * 99; // elastics accepts 100Mb

function _bulk2Elastic(id,arr,index,hostname, port){
  const path = `/${index}/_bulk`;
  return new Promise ( async (resolve, reject) => {
    let chunk = '';
    let counter = 0;
    for (const item of arr) {
      const tmp = {};
      tmp[IDFIELD] = id;
      const current = '{"index":{}}\n'+JSON.stringify({...tmp,...item})+'\n';
      const length = chunk.length + current.length;
      if (length > CHUNKSIZE) {
        const result = await putstr(chunk,hostname,port,path);
        if (result.answer) {
          if (result.answer.errors && result.answer.errors.length > counter/2){
            reject(new Notice('more than half are errors',{count:counter,errors:result.answer.errors}));
            return
          } else {
             resolve(true);
          }
        } else if (result instanceof ErrorEvent) {
          resolve(result); // do not try again, if error
        } else if (result instanceof LogEvent) {
          reject(result); // try again else {
        } else {
          console.dir({badresult:result});
          resolve({});
        }
          chunk = current;
          counter = 1;

      } else {
        counter ++;
        chunk += current;
      }
    }
    // put leftovers
    if (chunk.length) {
      const result = await putstr(chunk,hostname,port,path);
      if (result.answer) {
        if (result.answer.errors && result.answer.errors.length > counter/2){
          console.dir(result.answer);
          reject(new Notice('more than half are errors',{count:counter,errors:result.answer.errors}));
          return
        } else {
           resolve(true);
        }
      } else if (result instanceof ErrorEvent) {
        resolve(result); // do not try again, if error
      } else if (result instanceof LogEvent) {
        reject(result); // try again else {
      } else {
        console.dir({badresult:result});
        resolve({});
      }
    } else {
      resolve(true);
    }


  })
}

function _put2Elastic(doc,path,hostname, port){

  return new Promise ( (resolve, reject) => {
    const options = {
      hostname: hostname,
      port: port,
      path: `${path}`,
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
    };
    try {
      const req = request(options, (res) => {
        let data = [];
        res.on('data', (chunk) => {
            data.push(chunk);
        });
        res.on('end',() => {
          let r;
          try {
            r = JSON.parse(Buffer.concat(data).toString());

            if (res.statusCode > 299) {
              const error = new Error(hostname+':'+port);
              error.elasticsearch = r;
              error.elasticsearch.path = path;
              error.elasticsearch.doc = doc;
              reject(error);
            } else resolve(r);
          } catch (error) {

            error.badresponse = Buffer.concat(data).toString();
            error.docsize = doc.length;
            error.dochead = doc.substring(0,1024);
            error.doctail = doc.substring(-1024);
            if (!error.badresponse.length) {
              resolve(new Warn(error));
            } else {
              reject(new Notice(error));
            }

          }
        });
      });
      req.on('error', (error) => {
        reject(error);
      });
      req.write(doc,'utf-8');
      req.end();
    } catch (error) {
      reject(error);
    }
  })
}

function putDoc2Elastic(doc,id,index,hostname,port){
    const path = `/${index}/_doc/${id}`;
    return _put2Elastic(doc,path,hostname,port)
}

function _existElastic(path,hostname,port){
  return new Promise( (resolve, reject) => {
    const options = {
      hostname: hostname,
      port: port,
      path: path,
      method: 'HEAD'
    };
    const req = request(options, (res) => {
      resolve( (res.statusCode === 200) );
    });
    req.on('error', (error) => {
      reject(new Notice(error));
    });
    req.end();
  })
}

function docExist(id,index,hostname,port){
    const path =  `/${index}/_doc/${id}`;
    return _existElastic(path,hostname,port)
}

function indexExist(index,hostname,port){
    const path =  `/${index}`;
    return _existElastic(path,hostname,port)
}


function add2Elastic(obj,id,index,hostname,port){
  return new Promise( async (resolve, reject) => {
    const doc = JSON.stringify(obj);
    const _id = id || digestString(doc);
    try {
      const exists = await docExist(_id,index,hostname,port);
      if (exists) resolve(_id);
      else {
        try {
          const r = await putDoc2Elastic(doc,_id,index,hostname,port);
          resolve(_id);
        } catch (error) {
          console.error(obj.basename);
          console.error(error);
          reject(error);
        }
      }
    } catch (error) {
      reject(error);
    }
  })
}




class save2Elastic extends Ask{
  constructor(options){
    options.tti = options.tti || 20;
    if (!options.idField) options.idField = IDFIELD; //TODO param
    super(options);
    this._askState.ready = false;
    this.cluster = options.cluster;
    if (!this.cluster) throw new Error('no cluster')
    this.index = options.index;
    if (!this.index) throw new Error('no index')

    this.settings = options.settings || {settings:{"index.mapping.ignore_malformed": true,"number_of_replicas":NUMBEROFREPLICAS, "index.refresh_interval": "10s"}};

  }

  _ping(hostname,port,data,self){
    return new Promise( async(resolve,reject)=>{
      const options = {
        hostname: hostname,
        port: port,
        path: '/_cat/health',
        method: 'GET',
        headers: {
          'Accept': 'application/json'
        }
      };
      const req = request(options, (res) => {
        const body = [];
        res.on('data', (chunk) => {body.push(chunk);});
        res.on('end', async () => {
          let answer;
          try {
            answer = JSON.parse(Buffer.concat(body).toString());
          } catch (error) {
            reject(error);
          }
          if (answer && answer[0] && answer[0].status && answer[0].status !== 'red' ) {
            if (answer[0].cluster !== this.cluster) this.destroy(new Error('wrong cluster, expected '+this.cluster + ' got '+answer[0].cluster));
            else {
              try {
                const ie = await indexExist(this.index,hostname,port);
                if (!ie){
                  try {
                    const a = await _put2Elastic(JSON.stringify(this.settings),this.index,hostname,port);

                    resolve(true);
                  } catch (error) {
                    console.dir(error);
                    if (error.elasticsearch.error.type === 'resource_already_exists_exception') resolve(true);
                    else reject(new Warn(error));
                  }
                } else resolve(true);
              } catch (error) {
                reject(new Notice(error));
              }
            }
          } else {
            const error = new Error(hostname+':'+port);
            error.elasticsearch = answer;
            reject(new Notice(error));
          }
        });
      });
      req.on('error', (error) => {
        reject(new Notice(error));
      });
      req.end();
    })
  }
  _ask(hostname,port,data,self){
    if (!this._elaask) {
      console.error(this.name + ": _elaask not implemented");
      process.exit(-1);
    }
    return new Promise( async (resolve,reject) => {
      try {
        const answer = await this._elaask(hostname,port,data,self);
        resolve(answer);
      } catch (error) {
        //console.error('should not happen')
        //console.error(error)
        reject(error);
      }
    })
  }
}




class saveEntities extends save2Elastic{
  constructor(options){
    options.settings = options.settings || {settings: {
            "index.mapping.ignore_malformed": true,
            "number_of_replicas":NUMBEROFREPLICAS,
            "index.refresh_interval":"10s"
          },

          "mappings": {
              "dynamic": true,
              "dynamic_templates": [
                {
                  "ip": {
                    "match": "ipv*",
                    "match_mapping_type": "string",
                    "mapping": {
                      "type": "ip"
                    }
                  }
                }
              ]
            }

        };
    super(options);
    this.entitiesfield = options.field || '__entities';
    //this.idField = options.id || '__contenthash__'
  }
  _elaask(hostname,port,data,self){
    return new Promise( async ( resolve,reject) => {
      if (!data[this.entitiesfield]) resolve(data);
      else {
        try {

           await add2Elastic(data[this.entitiesfield],data[this.idField],this.index,hostname,port);
           //delete data[this.entitiesfield]
          resolve(data);
        } catch (error) {

          reject(error);
        }
      }
    })
  }
}


class saveLine extends save2Elastic{
  constructor(options){
    options.settings = options.settings || {settings: {
            "index.mapping.ignore_malformed": true,
            "number_of_replicas":NUMBEROFREPLICAS,
            "index.refresh_interval":"10s"
          },

          "mappings": {
              "dynamic": true,
              "dynamic_templates": [
                {
                  "ip": {
                    "match": "ipv*",
                    "match_mapping_type": "string",
                    "mapping": {
                      "type": "ip"
                    }
                  }
                },
                {
                  "ip": {
                    "match": "IPV*",
                    "match_mapping_type": "string",
                    "mapping": {
                      "type": "ip"
                    }
                  }
                }
              ]
            }

        };

    super(options);
    this.linefield = options.field || '__line';
    //this.idField = options.id || '__contenthash__'
  }
  _elaask(hostname,port,data,self){
    return new Promise( async ( resolve,reject) => {

      if (!data[this.linefield]) resolve(data);
      else {
          try {
            const result = await _bulk2Elastic(data[this.idField],data[this.linefield],this.index,hostname,port);
            delete data[this.linefield];
            resolve(data);
          } catch (error) {
            reject(error); // try again
          }
      }
    })
  }
}

class saveTimeline extends save2Elastic{
  constructor(options){
    options.settings = options.settings || {settings: {
            "index.mapping.ignore_malformed": true,
            "number_of_replicas":NUMBEROFREPLICAS,
            "index.refresh_interval":"10s"
          },

          "mappings": {
              "dynamic": true,
              "dynamic_templates": [
                {
                  "ip": {
                    "match": "ipv*",
                    "match_mapping_type": "string",
                    "mapping": {
                      "type": "ip"
                    }
                  }
                },
                {
                  "ip": {
                    "match": "IPV*",
                    "match_mapping_type": "string",
                    "mapping": {
                      "type": "ip"
                    }
                  }
                }
              ]
            }

        };

    super(options);
    this.timelinefield = options.field || '__timeline';
    //this.idField = options.id || '__contenthash__'
  }
  _elaask(hostname,port,data,self){
    return new Promise( async ( resolve,reject) => {
      //console.dir(data)
      if (!data[this.timelinefield]) resolve(data);
      else {
          try {
            const result = await _bulk2Elastic(data[this.idField],data[this.timelinefield],this.index,hostname,port);
            delete data[this.timelinefield];
            resolve(data);
          } catch (error) {
            reject(error); // try again
          }


        /*
        try {
            const tmp = {}
            tmp[this.idField] = data[this.idField]

            for (const i of data[this.timelinefield]) {
              await add2Elastic({...tmp, ...i},null,this.index,hostname,port)
          }
          delete data[this.timelinefield]
          resolve(data)
        } catch (error) {
          console.error(error)
          reject(error)
        }
        */

      }
    })
  }
}


// just strip away tika field names
function o2t(o){
  let t = '';
  for (const k of Object.keys(o)) {
    if (o[k].length > 2 && !o[k].toString().startsWith('org.apache.tika.') && !t.includes(o[k].toString()))
    t+=o[k].toString()+'\n';
  }
  return t
}

class saveMeta extends save2Elastic{
  constructor(options){
    options.settings = options.settings || {settings: {
            "index.mapping.ignore_malformed": true,
            "number_of_replicas":NUMBEROFREPLICAS,
            "index.refresh_interval":"10s"
          },
          "mappings": {
            "properties": {
                "location": {
                  "type": "geo_point"
                }
              }
            }
        };

    super(options);
    this.datetimefields = ['modified','created','last-modified','last-printed','creation-date','last-save-date','date','save-date','metadatadate','date-stamp-mode','date-time','date-time-digitized','date-time-original','file-modified-date','datetimeoriginal','print-date','profile-date-time'];
    this.metafields = [
      'md5',
      'basename',
      'long','lat','gps-altitude','gps-altitude-ref','gps-date-stamp','gps-latitude','gps-latitude-ref','gps-longitude','gps-longitude-ref','gps-time-stamp','gps-version-id',
      'content-type','resourcename',
      'author','last-author', 'subject','description',
      'message-from','from-name','message-to','to-email','to','from','to-name','x-originating-ip','message-cc'
    ];
  }
  _elaask(hostname,port,data,self){
    return new Promise( async ( resolve,reject) => {

      const _data = {};
      const carryon = Object.keys(data).filter(i => i.startsWith('__'));
      for (const key of carryon){
        _data[key] = data[key];
        if (key !== this.idField) delete data[key];
      }
      _data.content = data.content;
      delete data.content;

    const id = {};
    id[this.idField] = data[this.idField];
    const meta = {...id,json:JSON.stringify(data),all:o2t(data)};


    for (const field of this.metafields){
      if (data[field]) meta[field] = data[field];
    }
    for (const field of this.datetimefields){
      if (data[field]) {
        if (!Array.isArray(data[field])) data[field] = [data[field]];
        const okDates = [];
        for (const test of data[field]) {
          const tested = new Date(test);
          if (!isNaN(tested)) okDates.push(tested);
        }
        if (okDates.length) meta[field] = okDates;
      }
    }
    // fix iPhone bad Date
    if (meta['gps-date-stamp']) meta['gps-date-stamp'] = meta['gps-date-stamp'].replace(/\:/g,'/');
    // lat, long
    if (meta.long && meta.lat) {
      meta.location = {lat:Number(meta.lat),lon:Number(meta.long)};
      if (meta['gps-date-stamp'] && meta['gps-time-stamp']){
          meta.location_timestamp = new Date(meta['gps-date-stamp'] +' '+ meta['gps-time-stamp']);
      }
    }

    try {
      meta.id = await add2Elastic(meta,data[this.idField],this.index,hostname,port);
      _data.__meta = meta;
      resolve(_data);
    } catch (error) {
      console.error(error);
      reject(error);
    }
    })
  }
}





// TODO TODO chicken egg savelang checkExists

class checkExists extends save2Elastic{
  constructor(options){

    options.settings = {
        "settings": {
                "index.mapping.ignore_malformed": true,
                "number_of_replicas":NUMBEROFREPLICAS,
                "index.refresh_interval": "10s",
                //"index.highlight.max_analyzed_offset": 10000000,
                "analysis": {
                  "filter": {
                    "length_3_to_32_char": {
                      "type": "length",
                      "min": 3,
                      "max": 32
                    }
                  },
                  "analyzer": {
                    "standard_lowercase_asciifolding": {
              			     "tokenizer": "standard",
                         //  TODO add more langs to stopwords filter
                  			"filter": ["lowercase", "asciifolding","stop", "length_3_to_32_char"]
              			},
                    "ssdeep_analyzer": {
                      "tokenizer": "ssdeep_tokenizer"
                    },
                    "fulltext_analyzer": {
                      "type": "custom",
                      "tokenizer": "standard",
                      "filter": [
                        "lowercase",
                        "type_as_payload"
                      ]
                    }
                  },
                  "tokenizer": {
                    "ssdeep_tokenizer": {
                      "type": "ngram",
                      "min_gram": 7,
                      "max_gram": 7
                    }
                  }
                }
              },
              "mappings": {
                  "dynamic": true,
                  "dynamic_templates": [ ],
                  "properties": {

                    "content": {
                      "type": "text",
                      "term_vector": "with_positions_offsets_payloads",
                      "store" : true,
                      "analyzer" : "fulltext_analyzer",
                      "index_options": "positions",
                      "index_phrases": true,
                      "fields": {
                        "keyword": {
                          "store":false,
                          "type": "text",
      			              "analyzer": "standard_lowercase_asciifolding",
      			              "fielddata": true,
                        }
                      }
                    },

                    "__all__": {
                      "type": "text",
                      //"term_vector": "with_positions_offsets_payloads",
                      "term_vector": "yes",
                      "store" : false,
                      "analyzer" : "fulltext_analyzer",
                      "fields": {
                        "keyword": {
                          "store":false,
                          "type": "text",
      			              "analyzer": "standard_lowercase_asciifolding",
      			              "fielddata": true,
                        }
                      }
                    },
                    "__meta.all":{
                      "copy_to": "__all__",
                      "type": "text",
                      "index": false
                    },
                    "__meta.json":{
                      "type":"object",
                      "enabled":false
                    }
                  }
                }
            };

            //options.settings.mappings.dynamic_templates = []
            for (const language of LANGUAGES) {
              const mapping = {};
              mapping[language] = {
                            "path_match" : "*-"+language,
                            "match_mapping_type": "string",
                            "mapping": {
                              "type": "text",
                              "store": true,
                              "analyzer" : language.toLowerCase(),
                              "term_vector": "with_positions_offsets_payloads",
                              "index_options": "positions",
                              "index_phrases": true
                            }
                          };
              options.settings.mappings.dynamic_templates.push(mapping);
            }

            options.settings.mappings.dynamic_templates.push({'unknown':{
                          "path_match" : "*-unknown",
                          "match_mapping_type": "string",
                          "mapping": {
                            "type": "text",
                            "term_vector": "with_positions_offsets_payloads",
                            "store" : true,
                            "analyzer" : "fulltext_analyzer",
                            "index_options": "positions",
                            "index_phrases": true,
                          }
                        }});

            options.settings.mappings.dynamic_templates.push({"chunk_": {
              "match": "chunk_*",
              "mapping": {
                "type": "text",
                "store" : false,
                "analyzer": "ssdeep_analyzer",
              }
            }});
            options.settings.mappings.dynamic_templates.push({"double_chunk_": {
              "match": "double_chunk_*",
              "mapping": {
                "store" : false,
                "type": "text",
                "analyzer": "ssdeep_analyzer"
              }
            }});
            options.settings.mappings.dynamic_templates.push({"__all__": {
              "match": ["__fields.*","__entities.*"],
              "mapping": {
                "type": "text",
                "copy_to": "__all__",
                "index": false
              }
            }});


    super(options);
    this.langsfield = options.field || '__languages';
    //this.idField = options.id || '__contenthash__'


  }
  // TODO   HASHFIELD param !!

  _elaask(hostname,port,data,self){
    return new Promise( async ( resolve,reject) => {
      const { basename, pathname} = data;
      let exists = false;
        //try {
          //const exists = await docExist(basename,this.index,hostname,port)
          const query =  `{
                        "query": {
                            "bool": {
                                "must": {
                                    "term": {"${HASHFIELD}": "${basename}"}
                            }
                        }
                      },
                      "size": 0
                    }`;
          const response = await poststr(query,hostname,port,'/'+this.index+'/_search');

          exists = response?.answer?.hits?.total?.value;

          if (!exists) {
            if (response instanceof Event) reject(response);
            else resolve(data);
          } else resolve({});
        //} catch (error) {
          //reject(error)
        //}

    })
  }
}


class saveLangs extends checkExists{
  constructor(options){
    super(options);
    /*
    options.settings =  {
      "settings": {
                "index.mapping.ignore_malformed": true,
                "number_of_replicas":NUMBEROFREPLICAS,
                "index.refresh_interval": "10s",
                //"index.highlight.max_analyzed_offset": 10000000,
                "analysis": {
                  "filter": {
                    "length_3_to_32_char": {
                      "type": "length",
                      "min": 3,
                      "max": 32
                    }
                  },
                  "analyzer": {
                    "standard_lowercase_asciifolding": {
              			     "tokenizer": "standard",
                         //  TODO add more langs to stopwords filter
                  			"filter": ["lowercase", "asciifolding","stop", "length_3_to_32_char"]
              			},
                    "ssdeep_analyzer": {
                      "tokenizer": "ssdeep_tokenizer"
                    },
                    "fulltext_analyzer": {
                      "type": "custom",
                      "tokenizer": "standard",
                      "filter": [
                        "lowercase",
                        "type_as_payload"
                      ]
                    }
                  },
                  "tokenizer": {
                    "ssdeep_tokenizer": {
                      "type": "ngram",
                      "min_gram": 7,
                      "max_gram": 7
                    }
                  }
                }
              },
              "mappings": {
                  "dynamic": true,
                  "dynamic_templates": [ ],
                  "properties": {

                    "content": {
                      "type": "text",
                      "term_vector": "with_positions_offsets_payloads",
                      "store" : false,
                      "analyzer" : "fulltext_analyzer",
                      "index_options": "positions",
                      "index_phrases": true,
                    },

                    "__all__": {
                      "type": "text",
                      //"term_vector": "with_positions_offsets_payloads",
                      "term_vector": "yes",
                      "store" : false,
                      "analyzer" : "fulltext_analyzer",
                      "fields": {
                        "keyword": {
                          "store":false,
                          "type": "text",
      			              "analyzer": "standard_lowercase_asciifolding",
      			              "fielddata": true,
                        }
                      }
                    },
                    "__meta.all":{
                      "copy_to": "__all__",
                      "type": "text",
                      "analyzer" : "fulltext_analyzer",
                      "term_vector": "with_positions_offsets_payloads",
                      "store" : false,
                      "index_options": "positions",
                      "index_phrases": true
                    }
                  }
                }
            }


            options.settings.mappings.dynamic_templates = []
            for (const language of LANGUAGES) {
              const mapping = {}
              mapping['content-'+language] = {
                            //"match_pattern": "regex",
                            //"match": '__languages.content-'+language,
                            "path_match" : "*.content-"+language,
                            "match_mapping_type": "string",
                            "mapping": {
                              "type": "text",
                              "analyzer" : language.toLowerCase(),
                              "term_vector": "with_positions_offsets_payloads",
                              "store" : false,
                              "index_options": "positions",
                              "index_phrases": true
                            }
                          }
              options.settings.mappings.dynamic_templates.push(mapping)
            }
            const mapping = {}
            mapping['content-unknown'] = {
                          //"match_pattern": "regex",
                          //"match": '__languages.content-'+language,
                          "path_match" : "*.content-unknown",
                          "match_mapping_type": "string",
                          "mapping": {
                            "type": "text",
                            "term_vector": "with_positions_offsets_payloads",
                            "store" : false,
                            "analyzer" : "fulltext_analyzer",
                            "index_options": "positions",
                            "index_phrases": true,
                          }
                        }
            mapping['content-all'] = {
              "match_mapping_type": "string",
                "mapping": {
                  "type": "text",
                  "copy_to": "__all__"
                }
              }

            options.settings.mappings.dynamic_templates.push(mapping)


    super(options)
    this.langsfield = options.field || '__languages'
    //this.idField = options.id || '__contenthash__'
    */
  }
  _elaask(hostname,port,data,self){
    return new Promise( async ( resolve,reject) => {
      if (!data[this.idField]) resolve(data);
      else {

        try {

          //let id = null
            let ssdeep = {};
            //if (data.__embedded_no === '0') id = data[this.idField]
            if (data.ssdeep) {
              let [chunksize, chunk, double_chunk] = data.ssdeep.split(':');
              ssdeep = {chunksize, chunk, double_chunk, ssdeep:data.ssdeep};
            }
            const ssdeeps = {};
            for (const key of Object.keys(data).filter( i => i.startsWith(SSDEEPFIELDPREFIX))){
              const name = key.replace(SSDEEPFIELDPREFIX,'');

              let [chunksize, chunk, double_chunk] = data[key].split(':');
              ssdeeps['ssdeep_'+name] = data[key];
              ssdeeps['chunksize_'+name] = chunksize;
              ssdeeps['chunk_'+name] = chunk;
              ssdeeps['double_chunk_'+name] = double_chunk;
            }
            //const doc = {basename, ...ssdeep, ...ssdeeps,...data[this.langsfield], 'content-raw':data.content}
            if (data.content) { //data.content = '' // TODO
              const sizes = { content: data.content.length};
              if (data.__languages) for (const key of Object.keys(data.__languages)) {
                sizes[key] = data.__languages[key].join('\n').length;
              }
              if (sizes.content > 1000000) {
                // ? TODO split here
              }

              const doc = {...data, ...ssdeeps, sizes};

              const kala = await add2Elastic(doc, data[this.idField], this.index, hostname, port);
              delete data[this.langsfield];
            } else console.dir(data);

            resolve(data);
        } catch (error) {
          console.error(error);
          reject(error);
        }
      }
    })
  }
}


class checkFields extends save2Elastic{
  constructor(options){
    super(options);

  }
  _elaask(hostname,port,data,self){
    return new Promise( async ( resolve,reject) => {
      const { basename, pathname, __fields } = data;
      for (const f of __fields){
        // TODO delete old versions
        const id = f.__fieldscontenthash__;
        try {
          const exists = await docExist(id,this.index,hostname,port);
          if (!exists) {
            try {
              await putDoc2Elastic(JSON.stringify(f),id,this.index,hostname,port);
            } catch (error) {
              error.id = data[this.idField];
              error.name = 'checkFields.save';
              resolve(new Warn(error));
            }
          }
        } catch (error) {
          error.id = data[this.idField];
          error.name = 'checkFields.exists';
          reject(new Notice(error));
        }
      }
      resolve(data);
    })
  }
}

const REQUESTTIMEOUT$1 = 1000 * 130; // tambov !?
const APIPATH$1 = '/rmeta/text';

class Tika extends Ask {
  constructor(hashFieldame, options) {
    options.idField = hashFieldame;
    super(options);
    this.hashFieldame = options.idField;
    //if (hashFieldame) this.hashFieldame = hashFieldame
    //else throw new Error('missing hashFieldame')
  }

  _ping(hostname, port) {
    return new Promise(async (resolve, reject) => {
      const result = await putstr('ping', hostname, port, APIPATH$1, REQUESTTIMEOUT$1);
      if (result instanceof Event) {
        reject(result);
      } else if (result.answer) {
        resolve(true);
      }
    })
  }

  _ask(hostname, port, data, parent) {
    return new Promise(async (resolve, reject) => {
      if (data.pathname) {
        if (!Array.isArray(data.__fields.__mimetype__)) data.__fields.__mimetype__ = [data.__fields.__mimetype__];
        else data.__fields.__mimetype__ = data.__fields.__mimetype__.filter((x) => x !== 'inode/x-empty; charset=binary');
        let [mimetype, charset] = data.__fields.__mimetype__[0].split(';');
        if (!Array.isArray(data.__fields.__mimehuman__)) data.__fields.__mimehuman__ = [data.__fields.__mimehuman__];
        const istext = data.__fields.__mimehuman__[0].endsWith(' text') ? true : mimetype.startsWith('text/') ? true : false;

        if (istext) {
          const highWaterMark = Math.round(kStringMaxLength/1024) -1024;
          const encoding = 'utf-8';

          try {
            const stat = statSync(data.pathname);
            const totalchunks = Math.round(stat.size / highWaterMark + 0.5);
            const rtf = createReadStream(data.pathname, { highWaterMark, encoding });
            const wrtbl = this._readableState.pipes[0];
            let chunks = 0;
            let last = '';
            class F2D extends Transform {
              constructor() {
                super({ objectMode: true, highWaterMark: 1 });
              }
              _transform(chunk, _, next) {
                chunks++;
                const tmp = (last + chunk).split(/\r\n|\n|\r/);
                last = tmp.pop();
                this.push({ ...data, content: tmp.join('\n'), __chunk_no: chunks, __chunks_total: totalchunks });
                next();
              }
            }
            const f2d = new F2D();
            function close(e) {
              if (e) {
                this.push(new Error(this.constructor.name + ' ' + error.toString()));
              }
              f2d.unpipe(wrtbl);
              rtf.unpipe(f2d);
              resolve(true);
            }
            rtf.on('error', close);
            rtf.on('close', close);
            rtf.on('end', close);
            rtf.pipe(f2d);
            f2d.pipe(wrtbl);
            /*
            for await (const chunk of createReadStream(data.pathname,{ highWaterMark, encoding })) {
                chunks ++
                // TODO something more effective
                // TODO send last after end
                const tmp = (last + chunk).split(/\r\n|\n|\r/)
                last = tmp.pop()
                //const text = tmp.join('\n')
                //size += text.length
                //data.content = text
                //data.__chunk_no = chunks//counter++ + '-'+ totalchunks + '-' + stat.size
                console.dir(this)
                this.push({...data, content: tmp.join('\n'), __chunk_no: chunks, __chunks_total: totalchunks})
                console.dir(this._readableState.pipes[0])

            }
            */
          } catch (error) {
            error.processor = this.constructor.name;
            console.error(error);
            this.push(error);
            resolve(true);
          }
        } else {
          resolve(true);
          /*
          const result = await putfile(data.pathname,hostname,port,APIPATH,REQUESTTIMEOUT)
          if (result.answer && Array.isArray(result.answer ) && result.answer.length ) {
            const carryon = Object.keys(data).filter(i => i.startsWith('__'))
            for (const i in result.answer) {
              result.answer[i].__embedded_no = i
              for (const key of carryon){
                result.answer[i][key] = data[key]
              }
              //item[this.hashFieldame] = data[this.hashFieldame]
              //item.fields = data.fields
            }

            resolve(result.answer)
          } else if (result instanceof ErrorEvent) {
            resolve(result) // do not try again, if error
          } else if (result instanceof LogEvent) {
            reject(result) // try again
          } else {
            console.dir({badresult:result})
            resolve({})
          }
          */
        }
      } else {
        //throw new Error('no '+this.fieldname)
        resolve(data);
      }
    })
  }
}

function hilbert(something) {

    function index2xy(index, size) {

        function last2bits(x) {
            return x & 3
        }

        const positions = [
            [0, 0],
            [0, 1],
            [1, 1],
            [1, 0]
        ];
        let tmp = positions[last2bits(index)];
        index = index >>> 2;
        let x = tmp[0];
        let y = tmp[1];
        for (let n = 4; n <= size; n *= 2) {
            const n2 = n / 2;
            switch (last2bits(index)) {
                case 0 : // left-bottom
                    tmp = x;
                    x = y;
                    y = tmp;
                    break
                case 1 : // left-upper
                    x = x;
                    y = y + n2;
                    break
                case 2 : // right-upper
                    x = x + n2;
                    y = y + n2;
                    break
                case 3 : // right-bottom
                    tmp = y;
                    y = n2 - 1 - x;
                    x = n2 - 1 - tmp;
                    x = x + n2;
                    break
            }
            index = index >>> 2;
        }
        return { x, y }
    }

    if (typeof something[Symbol.iterator] !== "function") throw new Error("not iterable")
    if (!something.length) throw new Error("no length")

    const size = Math.pow(2, Math.ceil(Math.log(Math.sqrt(something.length))/ Math.log(2))); // next pow 2
    const matrix = [];
    for (let i in something) {
        const { x, y } = index2xy(i, size);
        if (!matrix[x]) matrix[x] = [];
        matrix[x][y] = something[i];
    }
    if (typeof something === "string") {
        for (const x in matrix) {
            matrix[x] = matrix[x].join("").replace(/\n/g, "\\");
        }
        return matrix.join("\n")
    } else return matrix
}

const PREFIX = SSDEEPFIELDPREFIX;

class STR2SSDeep extends Ask{
  constructor(options){
    if (!options.idField) options.idField = '__contenthash__';
    super(options);
    if (options.fieldname) this.fieldname = options.fieldname;
    else throw new Error('missing source fieldname for ssdeep')
  }
  get hashFieldame(){
    return PREFIX+this.fieldname
  }


  _ping(hostname,port,data,self){
    return new Promise( async(resolve,reject)=>{
      const result = await putstr('ping',hostname,port);
      if (result instanceof Event) {
        reject(data);
      } else if (result.answer) {
        resolve(true);
      }
    })
  }

  _ask(hostname,port,data,self) {
    return new Promise( async ( resolve,reject) => {
      //console.log(this.hashFieldame, this.fieldname)
      if (data[this.fieldname]) {
          const result = await putstr(data[this.fieldname],hostname,port);
          if (result.answer) {
            data[this.hashFieldame] = result.answer.ssdeep;
            resolve(data);
          } else if (result instanceof ErrorEvent) {
            resolve(result);
          } else if (result instanceof LogEvent) {
            reject(result);
          } else {
            console.dir({badresult:result});
            console.dir(result);
            resolve({});
          }
      } else {
        //throw new Error('no '+this.fieldname)
        resolve(data);
      }
    })
  }
}
class File2SSDeep extends Ask{
  constructor(options){
    if (!options.fieldname) throw new Error('missing source filename field for ssdeep')
    options.idField = options.fieldname;
    super(options);
    this.fieldname = options.fieldname;

  }
  get hashFieldame(){
    return PREFIX+'binary'
  }


  _ping(hostname,port,data,self){
    return new Promise( async(resolve,reject)=>{
      const result = await putstr('ping',hostname,port);
      if (result instanceof Event) {
        reject(data);
      } else if (result.answer) {
        resolve(true);
      }
    })
  }

  _ask(hostname,port,data,self) {
    return new Promise( async ( resolve,reject) => {
      //console.log(this.hashFieldame, this.fieldname)
      if (data[this.fieldname]) {
          const result = await putfile(data[this.fieldname],hostname,port);
          if (result.answer) {
            data[this.hashFieldame] = result.answer.ssdeep;
            resolve(data);
          } else if (result instanceof ErrorEvent) {
            resolve(result);
          } else if (result instanceof LogEvent) {
            reject(result);
          } else {
            console.dir({badresult:result});
            console.dir(result);
            resolve({});
          }
      } else {
        //throw new Error('no '+this.fieldname)
        resolve(data);
      }
    })
  }
}

const APIPATH = '/';
const REQUESTTIMEOUT = 120 * 1000;

class EntityReqognizer extends Ask{

  constructor(options){
    if (!options.idField) options.idField = IDFIELD;
    super(options);
    this.minlength = options.minlength || 11;
    this.fieldname = options.fieldname;
    if (!this.fieldname || !this.fieldname.trim()) throw new Error("missing fieldname")
  }

  _ping(hostname,port){
    return new Promise( async(resolve,reject)=>{
      const result = await putstr('',hostname,port);
      if (result instanceof Event) {
        reject(result);
      } else if (result.answer) {
        resolve(true);
      }
    })
  }

  _ask(hostname,port,data,self) {
    return new Promise( async ( resolve,reject) => {

      if (data[this.fieldname] && data[this.fieldname].length > this.minlength){
        const result = await putstr(data[this.fieldname],hostname,port,APIPATH,REQUESTTIMEOUT);
        if (result.answer) {
          if (result.answer.entities) {
            data.__entities = result.answer.entities;
            if (Array.isArray(result.answer.timeline)) data.__timeline = result.answer.timeline;
            if (Array.isArray(result.answer.objectlines)) data.__line = result.answer.objectlines;
          }
          resolve(data);
        } else if (result instanceof ErrorEvent) {
          resolve(result); // do not try again, if error
        } else if (result instanceof LogEvent) {
          reject(result); // try again
        } else {
          console.dir({badresult:result});
          resolve({});
        }
      } else resolve(data);
    })
  }
}

function chunkString(str, length = (1000000-1)) {
  /*
  The length of [content-ENGLISH] field of [8024d875347b1db54476514352f3c1e4] doc
  of [deleteme￣content￣macbook-pro‾7200] index has exceeded [1000000] -
  maximum allowed to be analyzed for highlighting.
  */
  // /(.|[\r\n]){1,n}/g
  return str.match(new RegExp('(.|[\r\n]){1,'+length+'}', 'g'));
}

class LanguageDetect extends Ask{
  constructor(options){
    if (!options.idField) options.idField = '__contenthash__';
    super(options);
    this.minlength = options.minlength || 32;
    this.fieldname = options.fieldname || 'content';
  }
  _ping(hostname,port,data,self){
    return new Promise( async(resolve,reject)=>{
      const options = {
        hostname: hostname,
        port: port,
        path: '/',
        method: 'PUT',
        headers: {
          'Accept': 'application/json'
        }
      };
      const req = request(options, (res) => {
        res.on('data', () => {});
        res.on('end', () => {resolve(true);} );
      });
      req.on('error', (error) => {
        reject(error);
      });
      req.end();
    })
  }
  _ask(hostname,port,data,self) {
    return new Promise( async ( resolve,reject) => {
      if (data[this.fieldname] && data[this.fieldname].length > this.minlength){
        data.__plines = data[this.fieldname].substring(8,1024*4).replace(/[^\d\s\W]/gi,'a').replace(/\d/gi,'0');
        data.__snake = hilbert(data.__plines);
        const options = {
          hostname: hostname,
          port: port,
          path: '/',
          method: 'PUT',
          headers: {
            'Accept': 'application/json'
          }
        };
        const req = request(options, (res) => {
          const body = [];
          let total = 0;
          res.on('data', (chunk) => {
            total += chunk.length;
            //if (total > 1024 *1024 *1024)
            //printProgress(hostname,port,basename, '<-',total)
            if (total + 65536 > kStringMaxLength) {
              const error = new Warning('Response is to big',{pathname, basename,this:this.name});
              resolve({__warning:error});
              res.destroy(error);

            } else body.push(chunk);
            //printProgress(this.name,total)

          });
          res.on('end', () => {

            let answer;
            try {
              answer = JSON.parse(Buffer.concat(body).toString());
            } catch (error) {
              answer = {__warning:new Error('Response is not valid json.')};
            }
            if (answer && answer.languages) {
              data.__languages = {};
              for (const language of Object.keys(answer.languages)){
                  //data[language] = answer.languages[language]
                  let sl;
                  if (LANGUAGES$1.includes(language)) sl = 'content-'+language;
                  else sl = 'content-unknown';
                  data.__languages[sl] = [];
                  for (const item of answer.languages[language]) {
                    for ( const chunk  of chunkString(item))
                    if (chunk.trim().length) data.__languages[sl].push(chunk);
                  }
              }
            }
            resolve(data);
          } );
        });
        req.on('error', (error) => {
          reject(error);
        });
        new StringStream(data[this.fieldname]).pipe(req);
        //req.end(data[this.fieldname])
      } else resolve(data);
    })
  }
}

/*

glob'er and spot'er use same pipelein with different starting point

what must emit 'basename'

*/





class filterBasename extends Transform {
  constructor(options) {
    options = options || {};
    options.objectMode = true;
    options.highWaterMark = 1;
    super(options);
  }
  _transform(data,e,next) {
      if (isHash(data.basename)) this.push(data);
      //else console.error('not hash:',data.basename)
      next();
  }
}


class Warning$1 extends Error {
  constructor(message,o={}) {
    super(message);
    this.msg = message;
    //this.name = this.constructor.name;
    for (const key of Object.keys(o)) this[key] = o[key];
    Error.captureStackTrace(this, this.constructor);
  }
}

function cleanup(obj){
  if (obj) {
    if (typeof obj === 'object') {
      if (Array.isArray(obj)) {
        if (typeof obj[0] === 'object') {
          const r = {};
          for (const i in obj) {
            const tmp = cleanup(obj[i]);
            if (tmp) {
              for (const key of Object.keys(tmp)) {
                if (tmp[key]) {

                  if (!r[key]) r[key] = tmp[key];
                  else {
                    if (Array.isArray(r[key])) {
                      r[key] = trimArrayOfStrings([r[key],tmp[key]].flat());
                    } else {
                      r[key] = trimArrayOfStrings([r[key],tmp[key]].flat());
                    }
                    if (Array.isArray(r[key]) && r[key].length === 1 ) r[key] = r[key][0];
                  }
                }
              }
            }
          }
          if (Object.keys(r).length) return r
          else return
        } else return obj.flat()
      } else {
        const r = {};
        for (const key of Object.keys(obj)) {
          const tmp = cleanup(obj[key]);
          if (tmp) r[key] = tmp;
        }
        if (Object.keys(r).length) return r
        else return
      }
    } else return obj
  } else return
}

class checkFieldsFile extends Transform {
  constructor(logger,dirname,hashFieldame,options) {
    options = options || {};
    options.objectMode = true;
    options.highWaterMark = 1;
    super(options);
    this.dirname = dirname;
    if (hashFieldame) this.hashFieldame = hashFieldame;
    else throw new Error('missing hashFieldame')
  }
  async _transform(data,e,next) {
      const pathname = join(this.dirname,data.basename,FILESPATH);
      if ( isReadable(pathname) ) {
        data.__fields = [];
        for (const f of readdirSync(pathname)) {
          if (f.endsWith('.json')) {
            const filename = join(pathname,f);
             try {
               const id = await digestFile(filename);
               let doc = cleanup(JSON.parse(readFileSync$1(filename)));
               doc = cleanup(doc);
               doc.__fieldscontenthash__ = id;
               data.__fields.push(doc);
             } catch (error) {
               error.filename = filename;
               error.basename = data.basename;
               console.error(error);
               this.push(error);
             }
          }
        }

        data.pathname = join(this.dirname,data.basename,data.basename);
        data[this.hashFieldame] = data.basename;
        if (data.__fields.length < 1) this.push(new Warning$1('missing fields file(s)',{pathname, basename:data.basename}));
        else this.push(data);
      } else {
        this.push(new Warning$1('not readable',{pathname, basename:data.basename}));
      }
      //else console.error('not hash:',data.basename)
      next();
  }
}


class deArrayfy extends Transform {
  constructor(masterhashFieldName,hashFieldame,options) {
    options = options || {};
    options.objectMode = true;
    options.highWaterMark = 1;
    super(options);
    if (masterhashFieldName) this.masterhashFieldame = masterhashFieldName;
    else throw new Error('missing master (main) hash Fieldame')
    if (hashFieldame) this.hashFieldame = hashFieldame;
    else throw new Error('missing hash Fieldame')
  }
  _transform(data,e,next) {
      //if (data instanceof Error || data instanceof Event) this.push(data)
      //else {
        if (Array.isArray(data)){
          const length = data.length;
          for (const i in data) {
            if (!data[i][this.masterhashFieldame]) {
              console.dir(data[i]);
              process.exit(-1);
            }
            if (!data[i][this.hashFieldame]){
              data[i][this.hashFieldame] = data[i][this.masterhashFieldame] + '-' + i + '-' + length;
            }
            this.push(data[i]);
          }
        } else {
          //if (!data[this.hashFieldame]){
            data[this.hashFieldame] = data[this.masterhashFieldame] + '-' + data.__chunk_no + '-' + data.__chunks_total;
          //} else {
            //console.dir(data[this.hashFieldame])
          //}
          this.push(data);
        }
      //}
      next();
  }
}

class unFlatten extends Transform {
  // 'X-TIKA:digest:MD5'-> 'md5'
  constructor(hashFieldame,options) {
    options = options || {};
    options.objectMode = true;
    options.highWaterMark = 1;
    super(options);
    if (hashFieldame) this.hashFieldame = hashFieldame;
    else throw new Error('missing hashFieldame')
    this.separator = ':';
    this.map = {};
  }
  _transform(data,e,next) {
      if (data instanceof Error || data instanceof Event) this.push(data);
      else {
        const unflattened = {};
        for (const key of Object.keys(data)){
          if (key.startsWith('__')) {
            unflattened[key] = data[key];
          } else if (data[key] && !key.includes('EXCEPTION')) {
            const bittes = key.split(this.separator);
            const i = bittes.length -1;
            const newname = bittes[i].replace(/\W/gi,'-').toLowerCase();

            if (!this.map[key]) this.map[key] = 0;
            this.map[key] += 1;
            if (!unflattened[newname]) unflattened[newname] = data[key];
            else {
              if (!Array.isArray(unflattened[newname])) unflattened[newname] = [unflattened[newname]];
              if (!unflattened[newname].includes(data[key])) unflattened[newname].push(data[key]);
            }
          }
        }
        this.push(unflattened);
      }
      next();
  }
  _flush(last){
    //this.push({__map__:this.map})
    last();
  }
}







function runPipeline(state, logger, first, params, tries = 3) {

  logger.info({PIPELINE:params});

  class delSomething extends Transform {
    constructor(fieldname,options) {
      options = options || {};
      options.objectMode = true;
      options.highWaterMark = 1;
      super(options);
      this.field2delete = fieldname;

    }
    _transform(data,e,next) {
        delete data[this.field2delete];
        this.push(data);
        next();
    }
  }

  class allpySomething extends Transform {
    constructor(fieldname,fn,options) {
      options = options || {};
      options.objectMode = true;
      options.highWaterMark = 1;
      super(options);
      this.field2delete = fieldname;
      this.fn = fn;

    }
    _transform(data,e,next) {
        data[this.field2delete] = this.fn(data[this.field2delete]);
        this.push(data);
        next();
    }
  }


  class Debug extends Transform {
    constructor(name,options) {
      options = options || {};
      options.objectMode = true;
      options.highWaterMark = 1;
      super(options);
      this.name = name;
      this.counter = 0;
      this.errors = 0;
      this.events = 0;
      this.messages = 0;
      state[this.name] = {};
      state[this.name].counters = {};
    }

    _transform(data,e,next) {
        //console.log(this.name,JSON.stringify(data))
        this.counter ++;
        if (data instanceof Error) this.errors++;
        else if (data instanceof Event) this.events++;
        else if (Object.keys(data).filter(k=>!k.startsWith('__')).length===0) this.messages ++;
        //printProgress(this.name,this.counter,data.basename)
        state[this.name].counters = { total:this.counter,
          data:(this.counter-this.errors-this.messages),
          messages:this.messages,
          events:this.events,
          errors:this.errors};

        this.push(data);

        next();
    }
  }
  return new Promise( async (resolve) => {

  const sendmetrix = ()=>{};
  const checkfields = new checkFields({tries,pool:params.elastics, cluster:params.clustername, index:indexName(params.prefix,params.files,params.suffix),sendmetrix});
  const checkexists = new checkExists({tries,pool:params.elastics, cluster:params.clustername, index:indexName(params.prefix,params.content,params.suffix),sendmetrix});
  const tika = new Tika(params.field,{tries,pool:params.tikas,sendmetrix});
  new File2SSDeep({tries,pool:params.ssdeeps,fieldname:'pathname',sendmetrix});
  const content2ssdeep = new STR2SSDeep({tries,pool:params.ssdeeps,fieldname:params.content,sendmetrix});
  const snake2ssdeep = new STR2SSDeep({tries,pool:params.ssdeeps,fieldname:'__snake',sendmetrix});
  const plines2ssdeep = new STR2SSDeep({tries,pool:params.ssdeeps,fieldname:'__plines',sendmetrix});
  const erbf = new EntityReqognizer({tries,fieldname:params.content,pool:params.ers,sendmetrix});
  const st = new saveTimeline({tries,pool:params.elastics, cluster:params.clustername, index:indexName(params.prefix,params.timeline,params.suffix),sendmetrix});
  new saveLine({tries,pool:params.elastics, cluster:params.clustername, index:indexName(params.prefix,params.line,params.suffix),sendmetrix});

  const se = new saveEntities({tries,pool:params.elastics, cluster:params.clustername, index:indexName(params.prefix,params.entities,params.suffix),sendmetrix});
  const sm = new saveMeta({tries,pool:params.elastics, cluster:params.clustername, index:indexName(params.prefix,params.meta,params.suffix),sendmetrix});
  const ld = new LanguageDetect({tries,pool:params.lds,sendmetrix});
  //const ldsdeep = new Ask({name:'ldssdeep',tries,ask:askssdeepstr, ping:pingssdeep, pool:params.ssdeeps,sendmetrix})
  const sl = new saveLangs({tries,pool:params.elastics, cluster:params.clustername, index:indexName(params.prefix,params.content,params.suffix),sendmetrix});


    try {
      let counter = 0;
      const pipeline$1 = promisify(pipeline);
      const pp = await pipeline$1(
        first,
        new Debug('first'),
        new filterBasename(),
        new Debug('filterBasename'),
        new checkFieldsFile(logger, params.filespath, params.field ),
        new Debug('checkfieldsfile'),
        checkfields,
        new allpySomething('__fields',cleanup),
        new Debug('checkfields'),
        checkexists,
        new Debug('checkexists'),
        //basessdeep,
        //new Debug('ssdeep'),
        tika,
        new Debug('tika'),
        new deArrayfy(params.field,IDFIELD),
        new Debug('deArrayfy'),
        new unFlatten(params.field),
        new Debug('unFlatten'),
        sm,
        new Debug('sm'),
        erbf,
        new Debug('erbf'),
        st,
        new Debug('st'),
        //sline,
        //new Debug('sline'),
        se,
        new Debug('se'),
        content2ssdeep,
        ld,
        new Debug('ld'),
        snake2ssdeep,
        new delSomething('__snake'),
        plines2ssdeep,
        new delSomething('__plines'),
        //ldsdeep,
        new Debug('ldssdeep'),
        sl,
        new Debug('sl'),

        new Writable({objectMode: true, write: (d,e,next) => {
            //console.dir(d)
            if (d instanceof Error) {
              logger.notice(d);
            } else {
              if (Object.keys(d).filter(k=>!k.startsWith('__')).length){
                counter ++;
                //printProgress('done',counter,d.basename)
              } else {
                //console.dir(JSON.stringify(d,null,4))
              }
            }

            next();
          }
        })
      );
      console.log('result last -----');
      //console.dir(state)
      console.log(JSON.stringify(state,null,4));
      resolve(true);
    } catch (error){
      console.log('error last -------');
      console.error(error);
      resolve(error);
    }
  })

}

function name2ip(name){
  return new Promise( resolve => {
    resolve$1(name, (err, addresses) => {
      if (err) resolve(name);
      else {
        resolve(addresses[0]);
      }
    } );
  })
}

function* dirReader(dirname){
  for (const basename of readdirSync(dirname)) {
    yield {basename:encodeURIComponent(basename)};
  }
}

const state = {};

const glob = createSimpleCli({
  opts,
  fn: async (logger) => {
    logger.info({ startinwith: glob.defaults });
    // resolve names to ip's
    const resolved = glob.defaults;
    for (const key of Object.keys(resolved)){
      if (Array.isArray(resolved[key]) && resolved[key][0] && resolved[key][0].hostname ) {
        for (const i in resolved[key])  {
          resolved[key][i].hostname = await name2ip(resolved[key][i].hostname);
        }
      }
    }
    logger.info({ resolved });
    const readable = Readable.from(dirReader(glob.filespath), {objectMode: true,highWaterMark:1});
    await runPipeline(state,logger, readable, resolved);
  }
});

glob.run();
