# ER aka Entitie Recognizer

TLDR;

```
$# docker-compose up -d

$# curl -s -XPUT -T some.txt localhost:9980 | jq .

{
  "took": 55976700,
  "entities": {
    "EMAIL": [
      "ramshaw@polar.bowdoin.edu",
      "mitch@linc.cis.upenn.edu"
    ],
    "PERSON": [
      "Eric Brill"
    ],
    "ORGANIZATION": [
      "Mitchell P. Marcus Computer and Information Science Dept. University of Pennsylvania"
    ],
    "LOCATION": [
      "Philadelphia"
    ]
  }
}

```

![birdview](birdview-er.png)


## combine: drop if less than 2

```
~/gitlab.com/naiive/docker/er(master)$ docker-compose ps
        Name                      Command               State           Ports         
--------------------------------------------------------------------------------------
er                     /sbin/tini -- node --no-wa ...   Up      0.0.0.0:9980->6789/tcp
ner-bf                 /sbin/tini -- node --no-wa ...   Up      0.0.0.0:9984->6789/tcp
spacy                  bash /app/start.sh               Up      0.0.0.0:9996->80/tcp  
spacy-proxy            /sbin/tini -- node --no-wa ...   Up      0.0.0.0:9986->6789/tcp
stanford-conll         /sbin/tini -- /bin/sh -c j ...   Up      0.0.0.0:9977->9997/tcp
stanford-muc7          /sbin/tini -- /bin/sh -c j ...   Up      0.0.0.0:9997->9997/tcp
stanford-proxy-conll   /sbin/tini -- node --no-wa ...   Up      0.0.0.0:9967->6789/tcp
stanford-proxy-muc     /sbin/tini -- node --no-wa ...   Up      0.0.0.0:9987->6789/tcp
```

```
for port in 9980 9967 9987 9986 9984; do echo $port; curl -s -XPUT -T some.txt localhost:{$port} | jq . ; done
```
### combined

```
{
  "took": 55976700,
  "entities": {
    "EMAIL": [
      "ramshaw@polar.bowdoin.edu",
      "mitch@linc.cis.upenn.edu"
    ],
    "PERSON": [
      "Eric Brill"
    ],
    "ORGANIZATION": [
      "Mitchell P. Marcus Computer and Information Science Dept. University of Pennsylvania"
    ],
    "LOCATION": [
      "Philadelphia"
    ]
  }
}
```

### stanford-conll

```
{
  "entities": {
    "MISCELLANEOUS": [
      "Transformation-Based Learning"
    ],
    "PERSON": [
      "Lance A. Ramshaw",
      "Eric Brill"
    ],
    "ORGANIZATION": [
      "Computer Science Bowdoin College",
      "Mitchell P. Marcus Computer and Information Science Dept. University of Pennsylvania",
      "PA"
    ],
    "LOCATION": [
      "Brunswick",
      "USA",
      "Philadelphia"
    ]
  },
  "took": 25934000,
  "size": 1056
}
```

### stanford-muc7

```
{
  "entities": {
    "ORGANIZATION": [
      "Lance A. Ramshaw Dept. of Computer Science Bowdoin College Brunswick",
      "Mitchell P. Marcus Computer and Information Science Dept. University of Pennsylvania",
      "PA"
    ],
    "LOCATION": [
      "Philadelphia"
    ],
    "PERSON": [
      "Eric Brill"
    ],
    "PERCENT": [
      "92 %",
      "88 %"
    ]
  },
  "took": 42537000,
  "size": 1056
}
```
### Spacy
```
{
  "entities": {
    "ORGANIZATION": [
      "Text Chunking",
      "Computer Science",
      "Bowdoin College",
      "Computer and Information Science Dept",
      "University of Pennsylvania",
      "Treebank"
    ],
    "LAW": [
      "Transformation-Based Learning "
    ],
    "PERSON": [
      "Lance A. Ramshaw Dept",
      "Brunswick",
      "Mitchell P. Marcus",
      "Eric Brill"
    ],
    "GPE": [
      "Philadelphia"
    ],
    "PERCENT": [
      "roughly 92%",
      "88%"
    ]
  },
  "took": 33406500,
  "size": 1056,
  "total": 14
}
```

### BF

```
{
  "size": 1056,
  "took": 2832700,
  "entities": {
    "email": [
      "ramshaw@polar.bowdoin.edu",
      "mitch@linc.cis.upenn.edu"
    ]
  }
}

```
