FROM node:13-alpine AS base

FROM base AS builder
COPY --chown=node:node ./ /
RUN cd /src/backend/servers/er/spacy && yarn && yarn run build && cd dist && pwd && ls * && node --experimental-modules index.mjs -T

FROM base
USER root
RUN apk add --no-cache tini
RUN mkdir -p /opt/spacy && chown node:node /opt/spacy
USER node
COPY --from=builder --chown=node:node /src/backend/servers/er/spacy/dist/index.mjs /opt/spacy/index.mjs

# Declare args
ARG MINCPUS
ARG MAXCPUS
ARG HOSTNAME
ARG PORT
ARG NERS

# Declare envs vars for each arg
ENV MINCPUS $MINCPUS
ENV MAXCPUS $MAXCPUS
ENV HOSTNAME $HOSTNAME
ENV PORT $PORT
ENV NERS $NERS

EXPOSE $PORT
WORKDIR /opt/spacy
ENTRYPOINT ["/sbin/tini", "--"]
CMD ["node", "--no-warnings", "--experimental-modules","--max_old_space_size=8192", "/opt/spacy/index.mjs"]
