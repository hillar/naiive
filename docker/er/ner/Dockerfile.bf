FROM node:14-alpine AS base

FROM base AS builder
COPY --chown=node:node ./ /
RUN cd /src/backend/servers/er/bf && npm install && npm install rollup-plugin-copy  && ./node_modules/.bin/rollup -c && cd dist && pwd && ls * && node --experimental-modules index.mjs -T

FROM base
USER root
RUN apk add --no-cache tini && mkdir -p /opt/bf/recognizers && chown -R node:node /opt/bf
USER node
COPY --from=builder --chown=node:node /src/backend/servers/er/bf/dist/index.mjs /opt/bf/index.mjs
COPY --from=builder --chown=node:node /src/backend/servers/er/bf/dist/recognizers /opt/bf/recognizers

# Declare args
ARG MINCPUS
ARG MAXCPUS
ARG HOSTNAME
ARG PORT
ARG FNS
ARG AUGMENTSERVER
ARG AUGMENTPORT

# Declare envs vars for each arg
ENV MINCPUS $MINCPUS
ENV MAXCPUS $MAXCPUS
ENV HOSTNAME $HOSTNAME
ENV PORT $PORT
ENV FNS $FNS
ENV AUGMENTSERVER $AUGMENTSERVER
ENV AUGMENTPORT $AUGMENTPORT

EXPOSE $PORT
WORKDIR /opt/bf
ENTRYPOINT ["/sbin/tini", "--"]
CMD ["node", "--no-warnings", "--experimental-modules","--max_old_space_size=8192", "/opt/bf/index.mjs"]
