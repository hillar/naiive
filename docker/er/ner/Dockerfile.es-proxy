FROM node:13-alpine AS base

FROM base AS builder
COPY --chown=node:node ./ /
RUN cd /src/backend/servers/er/es && yarn && yarn run build && cd dist && pwd && ls * && node --experimental-modules index.mjs -T

FROM base
USER root
RUN apk add --no-cache tini
RUN mkdir -p /opt/es && chown node:node /opt/es
USER node
COPY --from=builder --chown=node:node /src/backend/servers/er/es/dist/index.mjs /opt/es/index.mjs

# Declare args
ARG MINCPUS
ARG MAXCPUS
ARG HOSTNAME
ARG PORT
ARG NERS

# Declare envs vars for each arg
ENV MINCPUS $MINCPUS
ENV MAXCPUS $MAXCPUS
ENV HOSTNAME $HOSTNAME
ENV PORT $PORT
ENV NERS $NERS

EXPOSE $PORT
WORKDIR /opt/es
ENTRYPOINT ["/sbin/tini", "--"]
CMD ["node", "--no-warnings", "--experimental-modules","--max_old_space_size=8192", "/opt/es/index.mjs"]
