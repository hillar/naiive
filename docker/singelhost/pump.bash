#!/bin/bash

find ./ -type f | while read fullname 
do
    basename=$(basename "${fullname}")
    echo "$(date) ${fullname}"
    curl -s -XPUT -F fullname="${fullname}" -F basename="${basename}" -F filename="@${fullname}" http://localhost:7200
done
