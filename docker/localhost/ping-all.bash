# Name                     Command               State                Ports
# ---------------------------------------------------------------------------------------------
# localhost-elastic   /elastic-entrypoint.sh ela ...   Up      0.0.0.0:9201->9200/tcp, 9300/tcp
# localhost-er        /sbin/tini -- node --no-wa ...   Up      0.0.0.0:9980->6789/tcp
# localhost-files     /sbin/tini -- node --no-wa ...   Up      0.0.0.0:7201->7200/tcp
# localhost-kibana    /docker-entrypoint.sh kibana     Up      0.0.0.0:5601->5601/tcp
# localhost-ld        /sbin/tini -- node --no-wa ...   Up      0.0.0.0:8081->6789/tcp
# localhost-ner-bf    /sbin/tini -- node --no-wa ...   Up      0.0.0.0:9984->6789/tcp
# localhost-proxy     /sbin/tini -- node --no-wa ...   Up      0.0.0.0:5201->4040/tcp
# localhost-search    /sbin/tini -- node --no-wa ...   Up      0.0.0.0:6201->6200/tcp
# localhost-tika      /sbin/tini -- /bin/sh -c j ...   Up      0.0.0.0:9998->9998/tcp


# files
curl -s -v -XPUT -T docker-compose.yml localhost:7201

# tika
curl -s -v -XPUT -T docker-compose.yml localhost:9998/rmeta/text | jq .

# LD
curl -s -v -XPUT -T docker-compose.yml localhost:8081 | jq .

# NER-BF
curl -s -v -XPUT -T docker-compose.yml localhost:9984 | jq .

# ER
curl -s -v -XPUT -T docker-compose.yml localhost:9980 || echo '{"failed":"ER"}' && echo '{"ok":"ER"}' | jq .
