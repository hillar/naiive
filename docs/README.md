# About name


name `naïve`
 - it written in javascript
 - is created by a person who lacks the formal education and training  
 - childlike simplicity and frankness
 - not following rules, etc.. (see Kimmo Pohjonen)

----

 - JavaScript

 > JavaScript is a great choice for **kids** because almost everything now runs on this.
 The -Script suffix suggests that it is **not a real** programming language. It lacks the discipline **to be a serious** software engineering language, thanks to loose typing and freewheeling coercions, and their wildly inconsistent semantics..


# About naïve

`naïve` was started, because previous solution did not scale ..
