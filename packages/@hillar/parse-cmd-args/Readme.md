
```
import { parseCmdArgs } from 'parse-args.mjs'

const argv = parseCmdArgs(args, opts={})

```

Return an argument object populated with the array arguments from args.

If no args provided then process.argv.slice(2) is used.

**Boolean-looking** (true,false,yes,no) arguments will be returned as booleans unless opts.string is set for that argument name.

**Numeric-looking** arguments will be returned as numbers unless opts.string or opts.boolean is set for that argument name.

`_` contains all the arguments that didn't have an option associated with them.

Any arguments after '--' will not be parsed and will end up in `--`

options can be:

* opts.string - a string or array of strings argument names to always treat as strings

* opts.boolean - a boolean, string or array of strings to always treat as booleans. if true will treat all double hyphenated arguments without equal signs as boolean (e.g. affects --foo, not -f or --foo=bar)

* opts.alias - an object mapping string names to strings or arrays of string argument names to use as aliases

* opts.default - an object mapping string argument names to default values

* opts.stopEarly - when true, populate argv._ with everything after the first non-option

* opts.unknown - a function which is invoked with a command line parameter not defined in the opts configuration object. If the function returns false, the unknown option is not added to argv.

* opts['--'] - when true, populate argv._ with everything before the -- and argv['--'] with everything after the --. Here's an example:

```
 parseCmdArgs('one two three -- four five --six'.split(' '), { '--': true })
{
  _: [ 'one', 'two', 'three' ],
  '--': [ 'four', 'five', '--six' ]
}
```
Note that with opts['--'] set, parsing for arguments still stops after the --.
