/*

This software is released under the MIT license:

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Original work Copyright (c) 2015 James Halliday
Modified work Copyright (c) 2020 Hillar Aarelaid

*/

/*
CHANGES:

1) args defaults to process.argv.slice(2)
2) true,false,yes,no will be returned as booleans

*/


/*

> import { parseArgs } from './parse-args.mjs'
> const argv = parseArgs(args, opts={})

Return an argument object populated with the array arguments from args.

If no args provided then process.argv.slice(2) is used.

argv._ contains all the arguments that didn't have an option associated with them.

Boolean-looking (true,false,yes,no) arguments will be returned as booleans unless opts.string is set for that argument name.

Numeric-looking arguments will be returned as numbers unless opts.string or opts.boolean is set for that argument name.

Any arguments after '--' will not be parsed and will end up in argv._.

options can be:

opts.string - a string or array of strings argument names to always treat as strings

opts.boolean - a boolean, string or array of strings to always treat as booleans. if true will treat all double hyphenated arguments without equal signs as boolean (e.g. affects --foo, not -f or --foo=bar)

opts.alias - an object mapping string names to strings or arrays of string argument names to use as aliases

opts.default - an object mapping string argument names to default values

opts.stopEarly - when true, populate argv._ with everything after the first non-option

opts['--'] - when true, populate argv._ with everything before the -- and argv['--'] with everything after the --. Here's an example:

> parseArgs('one two three -- four five --six'.split(' '), { '--': true })
{ _: [ 'one', 'two', 'three' ],
  '--': [ 'four', 'five', '--six' ] }
Note that with opts['--'] set, parsing for arguments still stops after the --.

opts.unknown - a function which is invoked with a command line parameter not defined in the opts configuration object. If the function returns false, the unknown option is not added to argv.

*/


import { isNumber } from '../is/is.mjs'

export function parseCmdArgs(args, opts) {
  if (!args) args = process.argv.slice(2);
  if (!opts) opts = {};

  const flags = { bools: {}, strings: {}, unknownFn: null };

  if (typeof opts["unknown"] === "function") {
    flags.unknownFn = opts["unknown"];
  }

  if (typeof opts["boolean"] === "boolean" && opts["boolean"]) {
    flags.allBools = true;
  } else {
    []
      .concat(opts["boolean"])
      .filter(Boolean)
      .forEach(key => {
        flags.bools[key] = true;
      });
  }

  const aliases = {};
  Object.keys(opts.alias || {}).forEach(key => {
    aliases[key] = [].concat(opts.alias[key]);
    aliases[key].forEach(x => {
      aliases[x] = [key].concat(aliases[key].filter(y => x !== y));
    });
  });

  []
    .concat(opts.string)
    .filter(Boolean)
    .forEach(key => {
      flags.strings[key] = true;
      if (aliases[key]) {
        flags.strings[aliases[key]] = true;
      }
    });

  const defaults = opts["default"] || {};

  const argv = { _: [] };
  Object.keys(flags.bools).forEach(key => {
    setArg(key, defaults[key] === undefined ? false : defaults[key]);
  });

  let notFlags = [];

  if (args.includes("--")) {
    notFlags = args.slice(args.indexOf("--") + 1);
    args = args.slice(0, args.indexOf("--"));
  }

  function argDefined(key, arg) {
    return (
      (flags.allBools && /^--[^=]+$/.test(arg)) ||
      flags.strings[key] ||
      flags.bools[key] ||
      aliases[key]
    );
  }

  function setArg(key, val, arg) {
    if (arg && flags.unknownFn && !argDefined(key, arg)) {
      if (flags.unknownFn(arg) === false) return;
    }

    const value = !flags.strings[key] && isNumber(val) ? Number(val) : val;
    setKey(argv, key.split("."), value);

    (aliases[key] || []).forEach(x => {
      setKey(argv, x.split("."), value);
    });
  }

  function setKey(obj, keys, value) {
    let o = obj;
    keys.slice(0, -1).forEach(key => {
      if (o[key] === undefined) o[key] = {};
      o = o[key];
    });

    const key = keys[keys.length - 1];
    if (
      o[key] === undefined ||
      flags.bools[key] ||
      typeof o[key] === "boolean"
    ) {
      o[key] = value;
    } else if (Array.isArray(o[key])) {
      o[key].push(value);
    } else {
      o[key] = [o[key], value];
    }
  }

  function aliasIsBoolean(key) {
    return aliases[key].some(x => flags.bools[x]);
  }


  for (let i = 0; i < args.length; i++) {
    const arg = args[i];

    if (/^--.+=/.test(arg)) {
      // Using [\s\S] instead of . because js doesn't support the
      // 'dotall' regex modifier. See:
      // http://stackoverflow.com/a/1068308/13216
      const m = arg.match(/^--([^=]+)=([\s\S]*)$/);
      var key = m[1];
      let value = m[2];
      if (!flags.strings[key]) {
        if (value.toLowerCase() === "false" || value.toLowerCase() === "no")
          value = false;
        else if (
          value.toLowerCase() === "true" ||
          value.toLowerCase() === "yes"
        )
          value = true;
      }

      /*
            if (flags.bools[key]) {
                value = value !== 'false';
            }
            */
      setArg(key, value, arg);
    } else if (/^--no-.+/.test(arg)) {
      var key = arg.match(/^--no-(.+)/)[1];
      setArg(key, false, arg);
    } else if (/^--.+/.test(arg)) {
      var key = arg.match(/^--(.+)/)[1];
      var next = args[i + 1];
      if (
        next !== undefined &&
        !/^-/.test(next) &&
        !flags.bools[key] &&
        !flags.allBools &&
        (aliases[key] ? !aliasIsBoolean(key) : true)
      ) {
        setArg(key, next, arg);
        i++;
      } else if (/^(true|false)$/.test(next)) {
        setArg(key, next === "true", arg);
        i++;
      } else {
        setArg(key, flags.strings[key] ? "" : true, arg);
      }
    } else if (/^-[^-]+/.test(arg)) {
      const letters = arg.slice(1, -1).split("");

      let broken = false;
      for (let j = 0; j < letters.length; j++) {
        var next = arg.slice(j + 2);

        if (next === "-") {
          setArg(letters[j], next, arg);
          continue;
        }

        if (/[A-Za-z]/.test(letters[j]) && /=/.test(next)) {
          setArg(letters[j], next.split("=")[1], arg);
          broken = true;
          break;
        }

        if (
          /[A-Za-z]/.test(letters[j]) &&
          /-?\d+(\.\d*)?(e-?\d+)?$/.test(next)
        ) {
          setArg(letters[j], next, arg);
          broken = true;
          break;
        }

        if (letters[j + 1] && letters[j + 1].match(/\W/)) {
          setArg(letters[j], arg.slice(j + 2), arg);
          broken = true;
          break;
        } else {
          setArg(letters[j], flags.strings[letters[j]] ? "" : true, arg);
        }
      }

      var key = arg.slice(-1)[0];
      if (!broken && key !== "-") {
        if (
          args[i + 1] &&
          !/^(-|--)[^-]/.test(args[i + 1]) &&
          !flags.bools[key] &&
          (aliases[key] ? !aliasIsBoolean(key) : true)
        ) {
          setArg(key, args[i + 1], arg);
          i++;
        } else if (args[i + 1] && /true|false/.test(args[i + 1])) {
          setArg(key, args[i + 1] === "true", arg);
          i++;
        } else {
          setArg(key, flags.strings[key] ? "" : true, arg);
        }
      }
    } else {
      if (!flags.unknownFn || flags.unknownFn(arg) !== false) {
        argv._.push(flags.strings["_"] || !isNumber(arg) ? arg : Number(arg));
      }
      if (opts.stopEarly) {
        argv._.push.apply(argv._, args.slice(i + 1));
        break;
      }
    }
  }

  Object.keys(defaults).forEach(key => {
    if (!hasKey(argv, key.split("."))) {
      setKey(argv, key.split("."), defaults[key]);

      (aliases[key] || []).forEach(x => {
        setKey(argv, x.split("."), defaults[key]);
      });
    }
  });

  if (opts["--"]) {
    argv["--"] = new Array();
    notFlags.forEach(key => {
      argv["--"].push(key);
    });
  } else {
    notFlags.forEach(key => {
      argv._.push(key);
    });
  }

  return argv;
}

function hasKey(obj, keys) {
  let o = obj;
  keys.slice(0, -1).forEach(key => {
    o = o[key] || {};
  });

  const key = keys[keys.length - 1];
  return key in o;
}
