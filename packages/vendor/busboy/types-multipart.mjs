// TODO:
//  * support 1 nested multipart level
//    (see second multipart example here:
//     http://www.w3.org/TR/html401/interact/forms.html#didx-multipartform-data)
//  * support limits.fieldNameSize
//     -- this will require modifications to utils.parseParams

import {Readable as ReadableStream} from 'stream';
import {Dicer} from '../dicer/index.mjs';
import {parseParams, decodeText, basename} from './utils.mjs';

const RE_BOUNDARY = /^boundary$/i;
const RE_FIELD = /^form-data$/i;
const RE_CHARSET = /^charset$/i;
const RE_FILENAME = /^filename$/i;
const RE_NAME = /^name$/i;
const DETECT = /^multipart\/form-data/i;


export class Multipart  {
  static detect = DETECT
  constructor(boy, cfg) {
    let i;
    let len;
    const self = this;
    let boundary;
    const limits = cfg.limits;
    const parsedConType = cfg.parsedConType || [];
    const defCharset = cfg.defCharset || 'utf8';
    const preservePath = cfg.preservePath;

    const fileopts = (typeof cfg.fileHwm === 'number'
                ? { highWaterMark: cfg.fileHwm }
                : {});

    for (i = 0, len = parsedConType.length; i < len; ++i) {
      if (Array.isArray(parsedConType[i])
          && RE_BOUNDARY.test(parsedConType[i][0])) {
        boundary = parsedConType[i][1];
        break;
      }
    }

    function checkFinished() {
      if (nends === 0 && finished && !boy._done) {
        finished = false;
        process.nextTick(() => {
          boy._done = true;
          boy.emit('finish');
        });
      }
    }

    if (typeof boundary !== 'string')
      throw new Error('Multipart: Boundary not found');

    const fieldSizeLimit = (limits && typeof limits.fieldSize === 'number'
                          ? limits.fieldSize
                          : 1 * 1024 * 1024);

    const fileSizeLimit = (limits && typeof limits.fileSize === 'number'
                     ? limits.fileSize
                     : Infinity);

    const filesLimit = (limits && typeof limits.files === 'number'
                  ? limits.files
                  : Infinity);

    const fieldsLimit = (limits && typeof limits.fields === 'number'
                   ? limits.fields
                   : Infinity);

    const partsLimit = (limits && typeof limits.parts === 'number'
                  ? limits.parts
                  : Infinity);

    let nfiles = 0;
    let nfields = 0;
    var nends = 0;
    let curFile;
    let curField;
    var finished = false;

    this._needDrain = false;
    this._pause = false;
    this._cb = undefined;
    this._nparts = 0;
    this._boy = boy;

    const parserCfg = {
      boundary,
      maxHeaderPairs: (limits && limits.headerPairs)
    };
    if (fileopts.highWaterMark)
      parserCfg.partHwm = fileopts.highWaterMark;
    if (cfg.highWaterMark)
      parserCfg.highWaterMark = cfg.highWaterMark;

    this.parser = new Dicer(parserCfg);
    this.parser.on('drain', () => {
      self._needDrain = false;
      if (self._cb && !self._pause) {
        const cb = self._cb;
        self._cb = undefined;
        cb();
      }
    }).on('part', function onPart(part) {
      if (++self._nparts > partsLimit) {
        self.parser.removeListener('part', onPart);
        self.parser.on('part', skipPart);
        boy.hitPartsLimit = true;
        boy.emit('partsLimit');
        return skipPart(part);
      }

      // hack because streams2 _always_ doesn't emit 'end' until nextTick, so let
      // us emit 'end' early since we know the part has ended if we are already
      // seeing the next part

      if (curField) {
        const field = curField;
        field.emit('end');
        field.removeAllListeners('end');
      }

      part.on('header', header => {
        let contype;
        let fieldname;
        let parsed;
        let charset;
        let encoding;
        let filename;
        let nsize = 0;

        if (header['content-type']) {
          parsed = parseParams(header['content-type'][0]);
          if (parsed[0]) {
            contype = parsed[0].toLowerCase();
            for (i = 0, len = parsed.length; i < len; ++i) {
              if (RE_CHARSET.test(parsed[i][0])) {
                charset = parsed[i][1].toLowerCase();
                break;
              }
            }
          }
        }

        if (contype === undefined)
          contype = 'text/plain';
        if (charset === undefined)
          charset = defCharset;

        if (header['content-disposition']) {
          parsed = parseParams(header['content-disposition'][0]);
          if (!RE_FIELD.test(parsed[0]))
            return skipPart(part);
          for (i = 0, len = parsed.length; i < len; ++i) {
            if (RE_NAME.test(parsed[i][0])) {
              fieldname = decodeText(parsed[i][1], 'binary', 'utf8');
            } else if (RE_FILENAME.test(parsed[i][0])) {
              filename = decodeText(parsed[i][1], 'binary', 'utf8');
              if (!preservePath)
                filename = basename(filename);
            }
          }
        } else
          return skipPart(part);

        if (header['content-transfer-encoding'])
          encoding = header['content-transfer-encoding'][0].toLowerCase();
        else
          encoding = '7bit';

        let onData;
        let onEnd;
        if (contype === 'application/octet-stream' || filename !== undefined) {
          // file/binary field
          if (nfiles === filesLimit) {
            if (!boy.hitFilesLimit) {
              boy.hitFilesLimit = true;
              boy.emit('filesLimit');
            }
            return skipPart(part);
          }

          ++nfiles;

          if (!boy._events.file) {
            self.parser._ignore();
            return;
          }

          ++nends;
          const file = new FileStream(fileopts);
          curFile = file;
          file.on('end', () => {
            --nends;
            self._pause = false;
            checkFinished();
            if (self._cb && !self._needDrain) {
              const cb = self._cb;
              self._cb = undefined;
              cb();
            }
          });
          file._read = n => {
            if (!self._pause)
              return;
            self._pause = false;
            if (self._cb && !self._needDrain) {
              const cb = self._cb;
              self._cb = undefined;
              cb();
            }
          };
          boy.emit('file', fieldname, file, filename, encoding, contype);

          onData = data => {
            if ((nsize += data.length) > fileSizeLimit) {
              const extralen = (fileSizeLimit - (nsize - data.length));
              if (extralen > 0)
                file.push(data.slice(0, extralen));
              file.emit('limit');
              file.truncated = true;
              part.removeAllListeners('data');
            } else if (!file.push(data))
              self._pause = true;
          };

          onEnd = () => {
            curFile = undefined;
            file.push(null);
          };
        } else {
          // non-file field
          if (nfields === fieldsLimit) {
            if (!boy.hitFieldsLimit) {
              boy.hitFieldsLimit = true;
              boy.emit('fieldsLimit');
            }
            return skipPart(part);
          }

          ++nfields;
          ++nends;
          let buffer = [] //'';
          let truncated = false;
          curField = part;

          onData = data => {
            // do not convert here, just use plain Buffer
            /*
            if ((nsize += data.length) > fieldSizeLimit) {
              const extralen = (fieldSizeLimit - (nsize - data.length));
              buffer += data.toString('binary', 0, extralen);
              truncated = true;
              part.removeAllListeners('data');
            } else
              buffer += data.toString('binary');
            */
            buffer.push(data)
          };

          onEnd = () => {
            curField = undefined;
            if (buffer.length)
              buffer = decodeText(buffer, 'binary', charset);
            boy.emit('field', fieldname, Buffer.concat(buffer), false, truncated, encoding, contype);
            --nends;
            checkFinished();
          };
        }

        /* As of node@2efe4ab761666 (v0.10.29+/v0.11.14+), busboy had become
           broken. Streams2/streams3 is a huge black box of confusion, but
           somehow overriding the sync state seems to fix things again (and still
           seems to work for previous node versions).
        */
        part._readableState.sync = false;

        part.on('data', onData);
        part.on('end', onEnd);
      }).on('error', err => {
        if (curFile)
          curFile.emit('error', err);
      });
    }).on('error', err => {
      boy.emit('error', err);
    }).on('finish', () => {
      finished = true;
      checkFinished();
    });
  }

  write(chunk, cb) {
    let r;
    //console.dir(cb)
    if ((r = this.parser.write(chunk)) && !this._pause)
      cb();
    else {
      this._needDrain = !r;
      this._cb = cb;
    }
  }

  end() {
    //const self = this;
    if (this._nparts === 0 && !this._boy._done) {
      process.nextTick(() => {
        this._boy._done = true;
        this._boy.emit('finish');
      });
    } else if (this.parser.writable)
      this.parser.end();
  }
}

function skipPart(part) {
  part.resume();
}

class FileStream extends ReadableStream {
  constructor(opts) {
    //if (!(this instanceof FileStream))
    //  return new FileStream(opts);
    super(opts);

    this.truncated = false;
  }

  _read(n) {}
}
