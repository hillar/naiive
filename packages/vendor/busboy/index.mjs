import fs from 'fs';
import {Writable as WritableStream} from 'stream';
import { parseParams } from './utils.mjs';
import { Multipart } from './types-multipart.mjs'
import { UrlEncoded } from './types-urlencoded.mjs'

const TYPES = [
  Multipart,
  UrlEncoded
];

export class Busboy extends WritableStream {

  _done = false;
  _parser = undefined;
  _finished = false;
  counter = 0
  total = 0

  constructor(opts) {
    //if (!(this instanceof Busboy))
    //  return new Busboy(opts);
    if (opts.highWaterMark !== undefined)
      super({ highWaterMark: opts.highWaterMark });
    else
      super();

    this.opts = opts;
    if (opts.headers && typeof opts.headers['content-type'] === 'string')
      this.parseHeaders(opts.headers);
    else
      throw new Error('Missing Content-Type');
  }

  emit(ev) {
    if (ev === 'finish') {
      if (!this._done) {
        this._parser && this._parser.end();
        return;
      } else if (this._finished) {
        return;
      }
      this._finished = true;
    }
    WritableStream.prototype.emit.apply(this, arguments);
  }

  parseHeaders(headers) {
    this._parser = undefined;
    if (headers['content-type']) {
      const parsed = parseParams(headers['content-type']);
      let matched;
      let type;
      for (let i = 0; i < TYPES.length; ++i) {
        type = TYPES[i];
        if (typeof type.detect === 'function')
          matched = type.detect(parsed);
        else
          matched = type.detect.test(parsed[0]);
        if (matched)
          break;
      }
      if (matched) {
        const cfg = {
          limits: this.opts.limits,
          headers,
          parsedConType: parsed,
          highWaterMark: undefined,
          fileHwm: undefined,
          defCharset: undefined,
          preservePath: false
        };
        if (this.opts.highWaterMark)
          cfg.highWaterMark = this.opts.highWaterMark;
        if (this.opts.fileHwm)
          cfg.fileHwm = this.opts.fileHwm;
        cfg.defCharset = this.opts.defCharset;
        cfg.preservePath = this.opts.preservePath;
        this._parser = new type(this, cfg);
        return;
      }
    }
    throw new Error(`Unsupported content type: ${headers['content-type']}`);
  }

  _write(chunk, encoding, cb) {
    this.counter ++
    this.total += chunk.length
    if (!this._parser)
      return cb(new Error('Not ready to parse. Missing Content-Type?'));
    this._parser.write(chunk, cb);
  }
}
