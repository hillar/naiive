/*
Copyright Brian White. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.

*/

import {EventEmitter} from 'events';
import {Writable as WritableStream, Readable as ReadableStream} from 'stream';

import { StreamSearch } from '../streamsearch/index.mjs';


const DASH = 45;
const B_ONEDASH = Buffer.from('-');
const B_CRLF = Buffer.from('\r\n');
const EMPTY_FN = () => {};

const B_DCRLF = Buffer.from('\r\n\r\n'); // from node's http_parser
const RE_CRLF = /\r\n/g;
const RE_HDR = /^([^:]+):[ \t]?([\x00-\xFF]+)?$/;

const // from node's http.js
MAX_HEADER_PAIRS = 2000;

const MAX_HEADER_SIZE = 80 * 1024;

class HeaderParser extends EventEmitter {
  constructor(cfg) {
    super();

    const self = this;
    this.nread = 0;
    this.maxed = false;
    this.npairs = 0;
    this.maxHeaderPairs = (cfg && typeof cfg.maxHeaderPairs === 'number'
                           ? cfg.maxHeaderPairs
                           : MAX_HEADER_PAIRS);
    this.buffer = '';
    this.header = {};
    this.finished = false;
    this.ss = new StreamSearch(B_DCRLF);
    this.ss.on('info', (isMatch, data, start, end) => {
      if (data && !self.maxed) {
        if (self.nread + (end - start) > MAX_HEADER_SIZE) {
          end = (MAX_HEADER_SIZE - self.nread);
          self.nread = MAX_HEADER_SIZE;
        } else
          self.nread += (end - start);

        if (self.nread === MAX_HEADER_SIZE)
          self.maxed = true;

        self.buffer += data.toString('binary', start, end);
      }
      if (isMatch)
        self._finish();
    });
  }

  push(data) {
    const r = this.ss.push(data);
    if (this.finished)
      return r;
  }

  reset() {
    this.finished = false;
    this.buffer = '';
    this.header = {};
    this.ss.reset();
  }

  _finish() {
    if (this.buffer)
      this._parseHeader();
    this.ss.matches = this.ss.maxMatches;
    const header = this.header;
    this.header = {};
    this.buffer = '';
    this.finished = true;
    this.nread = this.npairs = 0;
    this.maxed = false;
    this.emit('header', header);
  }

  _parseHeader() {
    if (this.npairs === this.maxHeaderPairs)
      return;

    const lines = this.buffer.split(RE_CRLF);
    const len = lines.length;
    let m;
    let h;
    let modded = false;

    for (let i = 0; i < len; ++i) {
      if (lines[i].length === 0)
        continue;
      if (lines[i][0] === '\t' || lines[i][0] === ' ') {
        // folded header content
        // RFC2822 says to just remove the CRLF and not the whitespace following
        // it, so we follow the RFC and include the leading whitespace ...
        this.header[h][this.header[h].length - 1] += lines[i];
      } else {
        m = RE_HDR.exec(lines[i]);
        if (m) {
          h = m[1].toLowerCase();
          if (m[2]) {
            if (this.header[h] === undefined)
              this.header[h] = [m[2]];
            else
              this.header[h].push(m[2]);
          } else
            this.header[h] = [''];
          if (++this.npairs === this.maxHeaderPairs)
            break;
        } else {
          this.buffer = lines[i];
          modded = true;
          break;
        }
      }
    }
    if (!modded)
      this.buffer = '';
  }
}

class PartStream extends ReadableStream {
  constructor(opts) {
    super(opts);
  }
  _read(n) {}
}

export class Dicer extends WritableStream {
  constructor(cfg) {
    //if (!(this instanceof Dicer))
    //  return new Dicer(cfg);
    super(cfg);

    if (!cfg || (!cfg.headerFirst && typeof cfg.boundary !== 'string'))
      throw new TypeError('Boundary required');

    if (typeof cfg.boundary === 'string')
      this.setBoundary(cfg.boundary);
    else
      this._bparser = undefined;

    this._headerFirst = cfg.headerFirst;

    const self = this;

    this._dashes = 0;
    this._parts = 0;
    this._finished = false;
    this._realFinish = false;
    this._isPreamble = true;
    this._justMatched = false;
    this._firstWrite = true;
    this._inHeader = true;
    this._part = undefined;
    this._cb = undefined;
    this._ignoreData = false;
    this._partOpts = (typeof cfg.partHwm === 'number'
                      ? { highWaterMark: cfg.partHwm }
                      : {});
    this._pause = false;

    this._hparser = new HeaderParser(cfg);
    this._hparser.on('header', header => {
      self._inHeader = false;
      self._part.emit('header', header);
    });
  }

  emit(ev) {
    if (ev === 'finish' && !this._realFinish) {
      if (!this._finished) {
        const self = this;
        process.nextTick(() => {
          self.emit('error', new Error('Unexpected end of multipart data'));
          if (self._part && !self._ignoreData) {
            const type = (self._isPreamble ? 'Preamble' : 'Part');
            self._part.emit('error', new Error(`${type} terminated early due to unexpected end of multipart data`));
            self._part.push(null);
            process.nextTick(() => {
              self._realFinish = true;
              self.emit('finish');
              self._realFinish = false;
            });
            return;
          }
          self._realFinish = true;
          self.emit('finish');
          self._realFinish = false;
        });
      }
    } else
      WritableStream.prototype.emit.apply(this, arguments);
  }

  _write(data, encoding, cb) {

    // ignore unexpected data (e.g. extra trailer data after finished)
    if (!this._hparser && !this._bparser)
      return cb();

    if (this._headerFirst && this._isPreamble) {
      if (!this._part) {
        this._part = new PartStream(this._partOpts);
        if (this._events.preamble)
          this.emit('preamble', this._part);
        else
          this._ignore();
      }
      const r = this._hparser.push(data);
      if (!this._inHeader && r !== undefined && r < data.length)
        data = data.slice(r);
      else
        return cb();
    }

    // allows for "easier" testing
    if (this._firstWrite) {
      this._bparser.push(B_CRLF);
      this._firstWrite = false;
    }

    this._bparser.push(data);

    if (this._pause)
      this._cb = cb;
    else
      cb();
  }

  reset() {
    this._part = undefined;
    this._bparser = undefined;
    this._hparser = undefined;
  }

  setBoundary(boundary) {
    const self = this;
    //this._bparser = new StreamSearch(`\r\n--${boundary}`);
    //'\r\n--' + boundary
    this._bparser = new StreamSearch('\r\n--' + boundary);
    this._bparser.on('info', (isMatch, data, start, end) => {
      self._oninfo(isMatch, data, start, end);
    });
  }

  _ignore() {
    if (this._part && !this._ignoreData) {
      this._ignoreData = true;
      this._part.on('error', EMPTY_FN);
      // we must perform some kind of read on the stream even though we are
      // ignoring the data, otherwise node's Readable stream will not emit 'end'
      // after pushing null to the stream
      this._part.resume();
    }
  }

  _oninfo(isMatch, data, start, end) {
    let buf;
    const self = this;
    let i = 0;
    let r;
    let ev;
    let shouldWriteMore = true;

    if (!this._part && this._justMatched && data) {
      while (this._dashes < 2 && (start + i) < end) {
        if (data[start + i] === DASH) {
          ++i;
          ++this._dashes;
        } else {
          if (this._dashes)
            buf = B_ONEDASH;
          this._dashes = 0;
          break;
        }
      }
      if (this._dashes === 2) {
        if ((start + i) < end && this._events.trailer)
          this.emit('trailer', data.slice(start + i, end));
        this.reset();
        this._finished = true;
        // no more parts will be added
        if (self._parts === 0) {
          self._realFinish = true;
          self.emit('finish');
          self._realFinish = false;
        }
      }
      if (this._dashes)
        return;
    }
    if (this._justMatched)
      this._justMatched = false;
    if (!this._part) {
      this._part = new PartStream(this._partOpts);
      this._part._read = n => {
        self._unpause();
      };
      ev = this._isPreamble ? 'preamble' : 'part';
      if (this._events[ev])
        this.emit(ev, this._part);
      else
        this._ignore();
      if (!this._isPreamble)
        this._inHeader = true;
    }
    if (data && start < end && !this._ignoreData) {
      if (this._isPreamble || !this._inHeader) {
        if (buf)
          shouldWriteMore = this._part.push(buf);
        shouldWriteMore = this._part.push(data.slice(start, end));
        if (!shouldWriteMore)
          this._pause = true;
      } else if (!this._isPreamble && this._inHeader) {
        if (buf)
          this._hparser.push(buf);
        r = this._hparser.push(data.slice(start, end));
        if (!this._inHeader && r !== undefined && r < end)
          this._oninfo(false, data, start + r, end);
      }
    }
    if (isMatch) {
      this._hparser.reset();
      if (this._isPreamble)
        this._isPreamble = false;
      else {
        ++this._parts;
        this._part.on('end', () => {
          if (--self._parts === 0) {
            if (self._finished) {
              self._realFinish = true;
              self.emit('finish');
              self._realFinish = false;
            } else {
              self._unpause();
            }
          }
        });
      }
      this._part.push(null);
      this._part = undefined;
      this._ignoreData = false;
      this._justMatched = true;
      this._dashes = 0;
    }
  }

  _unpause() {
    if (!this._pause)
      return;

    this._pause = false;
    if (this._cb) {
      const cb = this._cb;
      this._cb = undefined;
      cb();
    }
  }
}
