.PHONY: test build clean local-docker flush-tags


test:
	echo 'Test'

build:
	echo 'building ..'
	yarn run build

clean:
	echo 'cleaning'
	find . -type d -name 'node_modules' | while read d; do rm -rf $$d;done
	find . -type d -name 'dist' | while read d; do rm -rf $$d;done
	find . -type d -name 'cache' | while read d; do rm -rf $$d;done
	find . -type f -name 'package-lock.json' | while read d; do rm $$d;done

local-docker:
	find . -type d -name 'node_modules' | while read d; do rm -rf $$d;done
	yarn
	yarn run docker

flush-tags:
	git tag -l | xargs -n 1 git push --delete origin
	git tag | xargs git tag -d

define release
    VERSION=`node -pe "require('./package.json').version"` && \
    NEXT_VERSION=`node -pe "require('semver').inc(\"$$VERSION\", '$(1)')"` && \
    node -e "\
        var j = require('./package.json');\
        j.version = \"$$NEXT_VERSION\";\
        var s = JSON.stringify(j, null, 2);\
        require('fs').writeFileSync('./package.json', s);" && \
		sed -i '' -e "s/VERSION:.*/VERSION: $$NEXT_VERSION/" .gitlab-ci.yml && \
		sed -i '' -e "s/VERSION=.*/VERSION=$$NEXT_VERSION/" ./docker/.env && \
		git add .gitlab-ci.yml && \
		git add package.json && \
		git add ./docker/.env && \
	  git commit -m "Version $$NEXT_VERSION" && \
		git push && \
	  git tag "$$NEXT_VERSION" -m "Version $$NEXT_VERSION"
		git push origin --tags
endef

release-patch: test
	@$(call release,patch)

release-minor: test
	@$(call release,minor)

release-major: test
	@$(call release,major)
