for impatient: download from [docker/singlehost](https://gitlab.com/hillar/naiive/tree/master/docker/singelhost) [compose](https://gitlab.com/hillar/naiive/raw/master/docker/singelhost/docker-compose.yml) and [envir](https://gitlab.com/hillar/naiive/raw/master/docker/singelhost/.env) files and run `docker-compose up` 

# naïve (WIP)

`naïve` is simple cluster of simple file servers.
But, it is not *File Server* for general use.
However, it can be useful, as content extraction, entity recognition, search and auth come on the top. Plus it scales out well. Plusplus `curl` works and desktop client apps are available for Linux, Mac OS X and Windows.

`curl -X PUT -T someworddocs.zip naiive.local:7200`
`curl -X PUT -F data=@someworddocs.zip naiive.local:7200`
`curl -X PUT -F foo=bar -F path=$(pwd) -F data=@someworddocs.zip naiive.local:7200`

![Fragaria × ananassa](/docs/maasikas.jpg)
